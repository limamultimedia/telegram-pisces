<?php

/**
 * The file that defines the order class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */

Class Telebotstore_PostType
{
    const order = 'telebotstore_order';
	const item = 'telebotstore_item';
    const toko = 'telebotstore_toko';
    const ticket = 'telebotstore_ticket';

    /**
     * register post type for Order
     * @return [type] [description]
     */
    public function order()
    {
        $labels = array(
          'name'                => _x( 'Orders', 'Post Type General Name', 'telebotstore' ),
          'singular_name'       => _x( 'Order', 'Post Type Singular Name', 'telebotstore' ),
          'menu_name'           => __( 'Order', 'telebotstore' ),
          'parent_item_colon'   => __( 'Parent Order', 'telebotstore' ),
          'all_items'           => __( 'All Order', 'telebotstore' ),
          'view_item'           => __( 'View Order', 'telebotstore' ),
          'add_new_item'        => __( 'Add Order', 'telebotstore' ),
          'add_new'             => __( 'Add New', 'telebotstore' ),
          'edit_item'           => __( 'Edit Order', 'telebotstore' ),
          'update_item'         => __( 'Update Order', 'telebotstore' ),
          'search_items'        => __( 'Search Order', 'telebotstore' ),
          'not_found'           => __( 'Not Found', 'telebotstore' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'telebotstore' ),
        );

        $args = array(
          'label'               => __( 'Order', 'telebotstore' ),
          'description'         => __( 'Orders', 'telebotstore' ),
          'labels'              => $labels,
          'supports'            => array( 'title','comments', 'post-templates' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => false,
          'show_in_admin_bar'   => false,
          'menu_position'       => 5,
          'can_export'          => true,
          'has_archive'         => false,
          'exclude_from_search' => true,
          'publicly_queryable'  => true,
          'capability_type'     => array('telebotstore','telebotstores'),
          'capabilities' => array(
              'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
          ),
          'map_meta_cap'        => true,
          'rewrite'             => array( 'slug' => 'order', 'with_front' => false ),
        );

        register_post_type( self::order, $args );

    }

	/**
     * register post type for Items
     * @return [type] [description]
     */
    public function item()
    {
        $labels = array(
          'name'                => _x( 'Items', 'Post Type General Name', 'telebotstore' ),
          'singular_name'       => _x( 'Item', 'Post Type Singular Name', 'telebotstore' ),
          'menu_name'           => __( 'Item', 'telebotstore' ),
          'parent_item_colon'   => __( 'Parent Item', 'telebotstore' ),
          'all_items'           => __( 'All Item', 'telebotstore' ),
          'view_item'           => __( 'View Item', 'telebotstore' ),
          'add_new_item'        => __( 'Add Item', 'telebotstore' ),
          'add_new'             => __( 'Add New', 'telebotstore' ),
          'edit_item'           => __( 'Edit Item', 'telebotstore' ),
          'update_item'         => __( 'Update Item', 'telebotstore' ),
          'search_items'        => __( 'Search Item', 'telebotstore' ),
          'not_found'           => __( 'Not Found', 'telebotstore' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'telebotstore' ),
        );

        $args = array(
          'label'               => __( 'Item', 'telebotstore' ),
          'description'         => __( 'Items', 'telebotstore' ),
          'labels'              => $labels,
          'supports'            => array( 'title' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => false,
          'show_in_admin_bar'   => false,
          'menu_position'       => 5,
          'can_export'          => true,
          'has_archive'         => false,
          'exclude_from_search' => true,
          'publicly_queryable'  => true,
          'capability_type'     => array('telebotstore','telebotstores'),
          'map_meta_cap'        => true,
          'rewrite'             => array( 'slug' => 'item', 'with_front' => false ),
        );

        register_post_type( self::item, $args );

    }

    /**
     * register post type for Tokos
     * @return [type] [description]
     */
    public function toko()
    {
        $labels = array(
          'name'                => _x( 'Toko', 'Post Type General Name', 'telebotstore' ),
          'singular_name'       => _x( 'Toko', 'Post Type Singular Name', 'telebotstore' ),
          'menu_name'           => __( 'Toko', 'telebotstore' ),
          'parent_Toko_colon'   => __( 'Parent Toko', 'telebotstore' ),
          'all_Tokos'           => __( 'All Toko', 'telebotstore' ),
          'view_Toko'           => __( 'View Toko', 'telebotstore' ),
          'add_new_Toko'        => __( 'Add Toko', 'telebotstore' ),
          'add_new'             => __( 'Add New', 'telebotstore' ),
          'edit_Toko'           => __( 'Edit Toko', 'telebotstore' ),
          'update_Toko'         => __( 'Update Toko', 'telebotstore' ),
          'search_Tokos'        => __( 'Search Toko', 'telebotstore' ),
          'not_found'           => __( 'Not Found', 'telebotstore' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'telebotstore' ),
        );

        $args = array(
          'label'               => __( 'Toko', 'telebotstore' ),
          'description'         => __( 'Tokos', 'telebotstore' ),
          'labels'              => $labels,
          'supports'            => array( 'title'),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => false,
          'show_in_admin_bar'   => false,
          'menu_position'       => 5,
          'can_export'          => true,
          'has_archive'         => false,
          'exclude_from_search' => true,
          'publicly_queryable'  => true,
          'capability_type'     => array('telebotstore','telebotstores'),
          'map_meta_cap'        => true,
          'rewrite'             => array( 'slug' => 'toko', 'with_front' => false ),
        );

        register_post_type( self::toko, $args );

    }

    public function ticket()
    {
        $labels = array(
          'name'                => _x( 'Ticket', 'Post Type General Name', 'telebotstore' ),
          'singular_name'       => _x( 'Ticket', 'Post Type Singular Name', 'telebotstore' ),
          'menu_name'           => __( 'Ticket', 'telebotstore' ),
          'parent_Toko_colon'   => __( 'Parent Ticket', 'telebotstore' ),
          'all_Tokos'           => __( 'All Ticket', 'telebotstore' ),
          'view_Toko'           => __( 'View Ticket', 'telebotstore' ),
          'add_new_Toko'        => __( 'Add Ticket', 'telebotstore' ),
          'add_new'             => __( 'Add New', 'telebotstore' ),
          'edit_Toko'           => __( 'Edit Ticket', 'telebotstore' ),
          'update_Toko'         => __( 'Update Ticket', 'telebotstore' ),
          'search_Tokos'        => __( 'Search Ticket', 'telebotstore' ),
          'not_found'           => __( 'Not Found', 'telebotstore' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'telebotstore' ),
        );

        $args = array(
          'label'               => __( 'Ticket', 'telebotstore' ),
          'description'         => __( 'Tickets', 'telebotstore' ),
          'labels'              => $labels,
          'supports'            => array( 'title','editor','comments'),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => false,
          'show_in_admin_bar'   => false,
          'menu_position'       => 5,
          'can_export'          => true,
          'has_archive'         => false,
          'exclude_from_search' => true,
          'publicly_queryable'  => true,
          'capability_type'     => array('telebotstore','telebotstores'),
          'map_meta_cap'        => true,
          'rewrite'             => array( 'slug' => 'ticket', 'with_front' => false ),
        );

        register_post_type( self::ticket, $args );

    }
}
