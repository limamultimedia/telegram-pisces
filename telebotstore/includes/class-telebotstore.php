<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Telebotstore_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		$this->version = TELEBOT;
		$this->plugin_name = 'telebotstore';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Telebotstore_Loader. Orchestrates the hooks of the plugin.
	 * - Telebotstore_i18n. Defines internationalization functionality.
	 * - Telebotstore_Admin. Defines all hooks for the admin area.
	 * - Telebotstore_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-i18n.php';

		/**
		 * The class responsible for defining post type register
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-post-type.php';

		/**
		 * The class responsible for defining comment walker
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-comment.php';

		/**
		 * The class responsible for defining order classes
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-order.php';

		/**
		 * The class responsible for defining onesignal api classes
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-onesignal.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-comments-tag.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-order-status-log.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-checkin.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-item.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-ticket.php';
		
		//adjust by limamultimedia
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-contact-log.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-checkin-log.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-sales-area.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-jadwal-kunjungan.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-statistik-toko.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-order-reminder.php';

		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-toko-contact.php';


		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-toko-order-csi.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telebotstore-s3.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-telebotstore-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-telebotstore-public.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-telebotstore-url.php';

		$this->loader = new Telebotstore_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Telebotstore_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Telebotstore_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}


	

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Telebotstore_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'plugins_loaded', $plugin_admin, 'carbon_init', 1);
		$this->loader->add_action( 'init', $plugin_admin, 'telebotstore_init' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'telebotstore_menu' );
		$this->loader->add_action( 'wp_ajax_search_toko', $plugin_admin, 'ajax_seacrh_toko' );
		$this->loader->add_action( 'wp_ajax_search_user', $plugin_admin, 'ajax_seacrh_user' );
		$this->loader->add_action( 'wp_ajax_search_item', $plugin_admin, 'ajax_seacrh_item' );
		$this->loader->add_filter( 'posts_search', $plugin_admin, 'search_by_title_only', 500, 2);
		$this->loader->add_action( 'telebot_midnight', $plugin_admin, 'cron_webapi_checking');
		$this->loader->add_action( 'telebot_midnight_plus', $plugin_admin, 'cron_sheetapi_checking');
		$this->loader->add_action( 'wp_ajax_manual_sync_toko', $plugin_admin, 'manual_checking');
		$this->loader->add_filter( 'upload_mimes', $plugin_admin, 'custom_myme_types', 1, 1);
		$this->loader->add_action( 'wp_ajax_export_toko', $plugin_admin, 'ajax_toko_export_excel');
		/**
		 * column post type order
		 * @var [type]
		 */
		$this->loader->add_filter( 'manage_edit-telebotstore_order_columns',  $plugin_admin, 'order_columns' );
		$this->loader->add_action( 'manage_telebotstore_order_posts_custom_column', $plugin_admin, 'manage_order_columns', 10, 2 );
		/**
		 * column post type toko
		 * @var [type]
		 */
		$this->loader->add_filter( 'manage_edit-telebotstore_toko_columns',  $plugin_admin, 'store_columns' );
		$this->loader->add_action( 'manage_telebotstore_toko_posts_custom_column', $plugin_admin, 'manage_store_columns', 10, 2 );
		$this->loader->add_filter( 'manage_edit-telebotstore_item_columns',  $plugin_admin, 'item_columns' );
		$this->loader->add_action( 'manage_telebotstore_item_posts_custom_column', $plugin_admin, 'manage_item_columns', 10, 2 );

		$this->loader->add_action( 'carbon_fields_register_fields', $plugin_admin, 'add_options' );
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin, 'add_post_meta' );
		$this->loader->add_action( 'save_post', $plugin_admin, 'post_meta_save', 1, 2 );
		$this->loader->add_filter( 'bulk_actions-edit-telebotstore_order', $plugin_admin, 'order_bulk_action' );
		$this->loader->add_filter( 'bulk_actions-edit-telebotstore_item', $plugin_admin, 'delete_admin_bulk_action' );
		$this->loader->add_filter( 'bulk_actions-edit-telebotstore_toko', $plugin_admin, 'delete_admin_bulk_action' );
		$this->loader->add_filter( 'handle_bulk_actions-edit-telebotstore_order', $plugin_admin, 'order_bulk_action_handler', 10, 3 );
		$this->loader->add_action( 'restrict_manage_posts', $plugin_admin, 'custom_order_filter' );
		$this->loader->add_filter( 'parse_query', $plugin_admin, 'result_order_filter' );
		$this->loader->add_filter( 'months_dropdown_results', $plugin_admin, 'remove_month_filters' , 10, 2 );
		$this->loader->add_action( 'admin_init',$plugin_admin, 'add_role_caps',999);
		$this->loader->add_action( 'admin_head-edit.php', $plugin_admin, 'add_export_button');
		$this->loader->add_action( 'admin_head-edit.php', $plugin_admin, 'add_toko_export_button');
		$this->loader->add_action( 'pre_get_posts', $plugin_admin, 'extend_admin_search' );
		$this->loader->add_filter( 'views_edit-telebotstore_toko', $plugin_admin, 'manual_update_toko');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Telebotstore_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'dequeue_unwanted_scripts',9999999);
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles', 9999999);
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts', 9999999);
		$this->loader->add_action( 'template_redirect', $plugin_public, 'set_template_redirect' );
		$this->loader->add_filter( 'comments_open', $plugin_public, 'set_comments_open', 10, 2 );
		$this->loader->add_filter( 'comments_template', $plugin_public, 'set_comments_template', 10, 2 );
		$this->loader->add_action( 'wp_ajax_get_order_list', $plugin_public, 'ajax_get_order_list' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_order_list', $plugin_public, 'ajax_get_order_list' );
		$this->loader->add_action( 'wp_ajax_get_log_list', $plugin_public, 'ajax_get_log_list' );
		$this->loader->add_action( 'wp_ajax_get_pending_list', $plugin_public, 'ajax_get_pending_list' );
		$this->loader->add_action( 'wp_ajax_get_reject_list', $plugin_public, 'ajax_get_reject_list' );
		$this->loader->add_action( 'wp_ajax_get_tagged_list', $plugin_public, 'ajax_get_tagged_list' );
		$this->loader->add_action( 'wp_ajax_get_comment_list', $plugin_public, 'ajax_get_comment_list' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_log_list', $plugin_public, 'ajax_get_log_list' );
		$this->loader->add_action( 'wp_ajax_change_order_status', $plugin_public, 'ajax_change_status_order' );
		$this->loader->add_action( 'wp_ajax_trash_order_status', $plugin_public, 'ajax_trash_status_order' );
		$this->loader->add_action( 'wp_ajax_nopriv_change_order_status', $plugin_public, 'ajax_change_status_order' );
		$this->loader->add_action( 'wp_ajax_export_orders', $plugin_public, 'ajax_export_excel' );
		$this->loader->add_action( 'wp_ajax_nopriv_export_orders', $plugin_public, 'ajax_export_excel' );
		$this->loader->add_action( 'wp_ajax_export_orders_spreadsheet', $plugin_public, 'ajax_export_spreadsheet' );
		$this->loader->add_action( 'wp_ajax_nopriv_export_order_psreadsheet', $plugin_public, 'ajax_export_spreadsheet' );
		$this->loader->add_action( 'wp_head', $plugin_public, 'add_onesignal_script', 0);
		$this->loader->add_action( 'comment_post', $plugin_public, 'send_comment_notif', 11, 2 );
		$this->loader->add_filter( 'comment_post', $plugin_public, 'insert_comment_tag_data', 11, 2 );
		$this->loader->add_action( 'wp_ajax_get_single_order', $plugin_public, 'ajax_get_single_order' );

		$this->loader->add_action( 'wp_ajax_change_item_status', $plugin_public, 'ajax_change_item_status' );
		$this->loader->add_action( 'wp_ajax_nopriv_change_item_status', $plugin_public, 'ajax_change_item_status' );

		$this->loader->add_action( 'wp_ajax_change_item_status_all', $plugin_public, 'ajax_change_item_status_all' );
		$this->loader->add_action( 'wp_ajax_nopriv_change_item_status_all', $plugin_public, 'ajax_change_item_status_all' );

		$this->loader->add_action( 'wp_ajax_get_insight', $plugin_public, 'ajax_get_insight' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_insight', $plugin_public, 'ajax_get_insight' );

		$this->loader->add_action( 'wp_ajax_get_contact', $plugin_public, 'ajax_get_contact' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_contact', $plugin_public, 'ajax_get_contact' );

		$this->loader->add_action( 'wp_ajax_get_ordercsi', $plugin_public, 'ajax_get_ordercsi' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_ordercsi', $plugin_public, 'ajax_get_ordercsi' );

		$this->loader->add_action( 'wp_ajax_get_ordercsi_list', $plugin_public, 'ajax_get_ordercsi_list' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_ordercsi_list', $plugin_public, 'ajax_get_ordercsi_list' );

		$this->loader->add_action( 'wp_ajax_get_ordercsi_single', $plugin_public, 'ajax_get_ordercsi_single' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_ordercsi_single', $plugin_public, 'ajax_get_ordercsi_single' );

		$this->loader->add_action( 'wp_ajax_delete_ordercsi_single', $plugin_public, 'ajax_delete_ordercsi_single' );
		$this->loader->add_action( 'wp_ajax_nopriv_delete_ordercsi_single', $plugin_public, 'ajax_deltte_ordercsi_single' );

		$this->loader->add_action( 'wp_ajax_export_ordercsi_list', $plugin_public, 'ajax_export_ordercsi_list' );
		$this->loader->add_action( 'wp_ajax_nopriv_export_ordercsi_list', $plugin_public, 'ajax_export_ordercsi_list' );

		$this->loader->add_action( 'wp_ajax_get_telegram', $plugin_public, 'ajax_get_telegram' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_telegram', $plugin_public, 'ajax_get_telegram' );

		$this->loader->add_action( 'wp_ajax_get_visit_list', $plugin_public, 'ajax_get_visit_list' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_visit_list', $plugin_public, 'ajax_get_visit_list' );

		$this->loader->add_action( 'wp_ajax_get_visit_lists', $plugin_public, 'ajax_get_visit_lists' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_visit_lists', $plugin_public, 'ajax_get_visit_lists' );

		$this->loader->add_action( 'wp_ajax_get_visit_list_user', $plugin_public, 'ajax_get_visit_list_user' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_visit_list_user', $plugin_public, 'ajax_get_visit_list_user' );

		$this->loader->add_action( 'wp_ajax_visit_user_export_excel', $plugin_public, 'ajax_visit_user_export_excel' );
		$this->loader->add_action( 'wp_ajax_nopriv_visit_user_export_excel', $plugin_public, 'ajax_visit_user_export_excel' );

		$this->loader->add_action( 'wp_ajax_mark_comment_as_solution', $plugin_public, 'ajax_mark_comment_as_solution' );
		$this->loader->add_action( 'wp_ajax_nopriv_mark_comment_as_solution', $plugin_public, 'ajax_mark_comment_as_solution' );

		$this->loader->add_action( 'wp_ajax_get_tickets_list', $plugin_public, 'ajax_get_tickets_list' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_tickets_list', $plugin_public, 'ajax_get_tickets_list' );

		$this->loader->add_action( 'wp_ajax_get_toko_contacts_list', $plugin_public, 'ajax_get_toko_contacts_list' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_toko_contacts_list', $plugin_public, 'ajax_get_toko_contacts_list' );

		$this->loader->add_action( 'wp', $plugin_public, 'wp' );

		$this->loader->add_action('init',$plugin_public,'do_insert_checkin');

		$this->loader->add_action('wp_ajax_load_add_store',$plugin_public,'ajax_load_add_store');

		$this->loader->add_action('wp_ajax_action_add_store',$plugin_public,'ajax_action_add_store');

		$url = new Telebotstore_url();
		$this->loader->add_action( 'wp_loaded', $url, 'rewrite' );
		$this->loader->add_filter( 'query_vars', $url, 'query_vars' );
		$this->loader->add_action( 'parse_request', $url, 'set_response' );
		$this->loader->add_action( 'parse_request', $url, 'webhook_response' );
		$this->loader->add_action( 'parse_request', $url, 'export_response' );
		$this->loader->add_action( 'parse_request', $url, 'cron_response' );
		$this->loader->add_action( 'parse_request', $url, 'aws_response' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Telebotstore_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public static function menu()
	{
		$current_roles = wp_get_current_user()->roles;

		$roles = array('admin_toko', 'administrator', 'editor' );

		$comment_tag = new Telebotstore_Comments_Tag();

		$notifs = $comment_tag->get_notif_by_current_user();

		ob_start();
		?>
		<div class="btn-group pull-right" style="margin-bottom: 30px;">
		    <a href="<?php echo get_the_permalink(carbon_get_theme_option('order_form')); ?>" class="btn btn-info" title="Add New Order"><svg baseProfile="tiny" height="15px" id="Layer_1" version="1.2" viewBox="0 0 20 20" width="20px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path fill="#ffffff" d="M18,10h-4V6c0-1.104-0.896-2-2-2s-2,0.896-2,2l0.071,4H6c-1.104,0-2,0.896-2,2s0.896,2,2,2l4.071-0.071L10,18  c0,1.104,0.896,2,2,2s2-0.896,2-2v-4.071L18,14c1.104,0,2-0.896,2-2S19.104,10,18,10z"/></svg></a>
			<div class="btn-group">
				<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CRM <span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('checkin')); ?>">Check In</a>
					</li>
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('jadwal_kunjungan')); ?>">Jadwal Kunjungan</a>
					</li>
					<?php if( !array_diff($current_roles, $roles) ) : ?>
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('checkin_log')); ?>">Check In Log</a>
					</li>
					
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('visit_count')); ?>">Statistik Checkin</a>
					</li>

					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('sales_area')); ?>">Sales Area</a>
					</li>

					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('list_jadwal_kunjungan')); ?>">List Jadwal Kunjungan</a>
					</li>
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('report_kunjungan')); ?>">Report Kunjungan</a>
					</li>
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('statistik_toko')); ?>">Statistik Toko</a>
					</li>
					<?php endif; ?>
					
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('ticket')); ?>">Ticket</a>
					</li>
					<li>
						<a href="<?php echo get_the_permalink(carbon_get_theme_option('toko_contact')); ?>">Contact Customer</a>
					</li>
				</ul>
			</div><!-- /btn-group -->
			<div class="btn-group">
				<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Order <span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="<?php echo get_the_permalink(carbon_get_theme_option('list_order')); ?>">List Order</a></li>
					<li><a href="<?php echo get_the_permalink(carbon_get_theme_option('list_order_log')); ?>">List Order Log</a></li>
					<li><a href="<?php echo get_the_permalink(carbon_get_theme_option('list_order_reject')); ?>">List Order Reject</a></li>
				</ul>
			</div><!-- /btn-group -->
			<div class="btn-group">
				<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Order CSI<span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="<?php echo get_the_permalink(carbon_get_theme_option('list_order_csi')); ?>">List Order CSI</a></li>
				</ul>
			</div><!-- /btn-group -->
			<div class="btn-group">
				<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">List Comment <span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="<?php echo get_the_permalink(carbon_get_theme_option('list_comment')); ?>">All Comment</a></li>
					<?php if( !array_diff($current_roles, $roles) ) : ?>
						<li><a href="<?php echo get_the_permalink(carbon_get_theme_option('list_comment')); ?>?tagged">Tagged Comment</a></li>
					<?php endif; ?>
				</ul>
			</div><!-- /btn-group -->
			<a href="<?php echo get_the_permalink(carbon_get_theme_option('list_order_pending')); ?>" class="btn btn-info">No Activity > 3 Hari</a>
			<div class="btn-group">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
					Notif <span class="badge badge-warning" <?php if( $comment_tag->count_notif_by_current_user() > 0 ) { ?>style="background: red;color:#ffffff" <?php } ?>><?php echo $comment_tag->count_notif_by_current_user(); ?></span></button>
					<ul class="dropdown-menu dropdown-menu-right list-group" role="menu">
						<?php if( $notifs ): ?>
							<?php foreach( $notifs as $notif ):

								if( empty($notif->comment_id) || $notif->comment_id == 0 )continue;

								if( $notif->comment_id == -1 ):
									$post = get_post($notif->post_id);

									$user_tagged_id = $post->post_author;

									$respond_link = get_the_permalink($notif->post_id);

								else :
									$comment = get_comment( $notif->comment_id );

									$user_tagged_id = $comment->user_id;

									$respond_link = get_the_permalink($notif->post_id).'?replytocom='.$notif->comment_id.'#respond';

								endif;
								$user = get_userdata($user_tagged_id);
							?>
								<li class="list-group-item"><a href="<?php echo $respond_link; ?>"><?php echo $user->display_name; ?> tagged you at <?php echo substr(get_post_meta($notif->post_id, '_order_store', true), 0, 40); ?>, click to reply</a></li>
							<?php endforeach; ?>
							<li class="list-group-item"><a href="<?php echo get_the_permalink(carbon_get_theme_option('list_notification')); ?>">Lihat semua komentar yang belum Anda balas</a></li>
						<?php else : ?>
							<li class="list-group-item"><a>Tidak ada Komentar yang menandai Anda yang belum Anda balas</a></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		echo $content;
	}  

	/**
	 * html modal
	 * @param  array  $data [title,content,modal_css]
	 * @return html
	 */
	public function modal($data=array()){

		$title = isset($data['title']) ? $data['title'] : '';
		$content = isset($data['content']) ? $data['content'] : '';
		$modal_css = isset($data['modal_css']) ? $data['modal_css'] : '';
		?>
		<div class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog <?= $modal_css; ?>" role="document">
            <div class="modal-content ">
              <div class="modal-header">
                <h5 class="modal-title"><?= $title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p><?= $content; ?></p>
              </div>
              <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
		<?php
	}


	
}
