<?php

/**
 * The file that defines the comment tag class
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */

Class Telebotstore_Comments_Tag
{
    private $table;
    private $wpdb;
    private $args;
    private $count;

    public function __construct($args = array())
    {
        global $wpdb;

        $this->wpdb = $wpdb;
        $this->table = $wpdb->prefix.'comments_tag';
        $this->args = $args;
    }

    /**
     * insert new comment tag in databse
     * @return [type] [description]
     */
    public function insert()
    {
        $data = array(
            'user_id' => (int)$this->args['user_id'],
            'post_id' => (int)$this->args['comment_post_ID'],
            'comment_id' => (int)$this->args['comment_id'],
            'replied' => 0,
        );

        $this->wpdb->insert( $this->table, $data );
    }

    /**
     * insert new comment tag in databse
     * @return [type] [description]
     */
    public function replied()
    {
        $where = array(
            'user_id' => (int)$this->args['user_id'],
            'post_id' => (int)$this->args['comment_post_ID'],
            'comment_id' => (int)$this->args['comment_id'],
        );

        $this->wpdb->update( $this->table, ['replied' => 1], $where );
    }

    /**
     * count notification by user
     * @return [type] [description]
     */
    public function count_notif_by_current_user()
    {
        $user_id = get_current_user_id();
        $notif = $this->wpdb->get_var( "SELECT COUNT(*) FROM $this->table WHERE user_id = ".$user_id." AND replied = 0 AND comment_id != 0 " );

        return $notif;
    }

    /**
     * count notification by user
     * @return [type] [description]
     */
    public function get_notif_by_current_user()
    {
        $user_id = get_current_user_id();
        $notif = $this->wpdb->get_results( "SELECT * FROM $this->table WHERE user_id = ".$user_id." AND replied = 0 AND comment_id != 0 " );

        return $notif;
    }

    public function get_tagged_user()
    {
        $notif = $this->wpdb->get_results( "SELECT user_id FROM $this->table WHERE comment_id = ".$this->args['comment_id'] );

        return $notif;
    }

    public function get($args)
    {
        $sql = "SELECT * FROM $this->table WHERE replied = 0";

        if( isset($args['user']) && !empty($args['user']) ):
            $sql .= ' AND user_id = "'.$args['user'].'"';
        endif;

        $sql .= ' ORDER BY ID DESC';

        $this->wpdb->get_results( $sql, OBJECT );
		$count = $this->wpdb->num_rows;

        $sql .= ' LIMIT '.$args['length'].' OFFSET '.$args['start'];

		$results = $this->wpdb->get_results( $sql, OBJECT );

        $this->count = $count;

        $pengirimans = carbon_get_user_meta( get_current_user_id(), 'telebotstore_user_pengiriman');

        $objs = array();

        foreach($results as $result )
        {
            $comment_date = get_comment_date( 'Y-m-d H:i:s', $result->comment_id );
            $user = get_userdata($result->user_id);

            $via = get_post_meta( $result->post_id, '_order_pengiriman', true);

            if( $pengirimans ):
                if( in_array($via, $pengirimans) ):
                    $objs[] = array(
                        $comment_date,
                        $user->display_name,
                        get_comment_text( $result->comment_id ),
                        get_the_permalink($result->post_id).'#comment-'.$result->comment_id,
                    );
                endif;
            else:
                $objs[] = array(
                    $comment_date,
                    $user->display_name,
                    get_comment_text( $result->comment_id ),
                    get_the_permalink($result->post_id).'#comment-'.$result->comment_id,
                );
            endif;
        }

        return $objs;
    }

    public function count()
    {
        return $this->count;
    }
}
