<?php 

class Telebotstore_Jadwal_Kunjungan{

	private $table;

	public function __construct(){
		global $wpdb;
		$this->table = $wpdb->prefix.'jadwal_kunjungan';

		$this->hook();

	}


	public function hook(){
		add_action('wp_ajax_get_form_pengajuan_jadwal',[$this,'form_html']);

		add_action('wp_ajax_save_jadwal_kunjungan',[$this,'save']);

		add_action('wp_ajax_filter_list_jadwal_kunjungan',[$this,'get_list']);

		add_action('wp_ajax_pengajuan_jadwal_action',[$this,'action']);

		add_action('wp_ajax_filter_report_kunjungan',[$this,'report_kunjungan']);
	}


	public function get($andWhere=''){
		global $wpdb;

		$query = $wpdb->get_results("SELECT * FROM ".$this->table." WHERE 1 ".$andWhere);
		return $query;
	}


	public function save(){
		
		$user_id = get_current_user_id();
		$stores = $_POST['store'];

		foreach($stores as $date => $values){

			foreach($values as $toko_id){
				$data = array(
							'datecreated' => current_time('mysql'),
							'user_id' => $user_id,
							'toko_id' => $toko_id,
							'tanggal' => $date
						);

				$this->insert($data);
			}
		}

		echo '<div class="alert alert-success">Data Tersimpan</div>';

		die();

	}


	public function insert($data){
		global $wpdb;

		$table = $this->table;
		$query = $wpdb->get_var("SELECT id FROM ".$table." WHERE tanggal='".$data['tanggal']."' AND user_id='".$data['user_id']."' AND toko_id='".$data['toko_id']."' LIMIT 1 ");

		if(!$query){
			$wpdb->insert($table,$data);
		}
	}

	/**
	 * list day mon - sat
	 * @param  integer $add        [description]
	 * @param  date  $start_date Y-m-d
	 * @return array
	 */
	public function get_tanggal_pengajuan($add=1,$start_date=''){

	    $timezone = new \DateTimeZone('Asia/Jakarta');
	    $dt = new \DateTime($start_date);  
	    $dt->setTimeZone($timezone); 
	    
	    $param = ($add == 0) ? 0 : intval($add);
  
	    $dt->setISODate($dt->format('o'), $dt->format('W')+$param, 1 );
	    $periods = new \DatePeriod($dt, new \DateInterval('P1D'), 5);    
	    $days = iterator_to_array($periods);

	    return $days;
	}


	public function form_html(){
		
		$user_id = get_current_user_id();
		$week = isset($_POST['add']) ? $_POST['add'] : 0;
        $start_date = date('Y-m-d');
        $list_days = $this->get_tanggal_pengajuan($week,$start_date);

        ?>
        <div class="panel panel-default">
            <div class="panel-heading">Masukan Jadwal</div>
            <div class="panel-body">
            	<form action="" method="POST" id="form-pengajuan-jadwal">
			        <?php
			        foreach($list_days as $dates){

			            $date = $dates->format('Y-m-d');

			            $hari = date_to_hari($date);

			            $andWhere = " AND user_id='".$user_id."' AND tanggal='".$date."' ";
			            $query = $this->get($andWhere);

			            $option = '';
			            if($query){
			            	foreach ($query as $row) {
			            		$toko_id = $row->toko_id;
			            		$toko = get_post($toko_id)->post_title;
			            		$address = get_post_meta($toko_id, '_telebotstore_toko_address', true);

			            		$option .= '<option value="'.$toko_id.'" selected >'.$toko.' ===> '.$address.'</option>';
			            	}
			            }
			            ?>      
			                <div class="form-group row">
			                    <div class="col-md-2">
			                        <label><?= $hari.', '.$date; ?> </label>
			                    </div>
			                    <div class="col-md-10">
			                        <select class="form-control search-toko-select2 search-toko-checkin" name="store[<?= $date; ?>][]" multiple>
			                            <?= $option; ?>
			                        </select>
			                    </div>
			                </div>
			                               
			            <?php
			        }
			        ?>
			        	<button type="submit" class="btn btn-primary btn-block" data-loading="Process...">SIMPAN</button>
			        	<input type="hidden" name="action" value="save_jadwal_kunjungan">
			        </form>
        	</div>
        </div>
        <?php
        die();
	}


	function get_list(){

		$start = $_POST['start'];
		$end = $_POST['end'];

		$andWhere = " AND tanggal BETWEEN '".$start."' AND '".$end."' ";
		$query = $this->get($andWhere);

        include TELE_TEMPLATES.'/jadwal-kunjungan/list.php';

        die();
	}


	public function report_kunjungan(){
		$start = $_POST['start'];
		$end = $_POST['end'];

		$andWhere = " AND tanggal BETWEEN '".$start."' AND '".$end."' AND status !='99' ";
		$query = $this->get($andWhere);

        include TELE_TEMPLATES.'/jadwal-kunjungan/report-kunjungan.php';

        die();
	}


	public function action(){

        global $wpdb;

        $table = $this->table;

        $act = $_POST['act'];
        $ids = $_POST['id'];

        if($act == 'delete'){
            
            foreach($ids as $id){
                $where = ['id' => $id];
                $wpdb->delete($table,$where);
            }

        }else if($act == 'reject' || $act == 'approve'){
            
            foreach($ids as $id){

                $status = $act == 'reject' ? 99 : 1;
                $update = ['status' => $status,'approve_by' => get_current_user_id(),'approve_date' => current_time('mysql')];
                $where = ['id' => $id];
                $wpdb->update($table,$update,$where);
            }

        }
	}

}


$Telebotstore_Jadwal_Kunjungan = new Telebotstore_Jadwal_Kunjungan;

?>