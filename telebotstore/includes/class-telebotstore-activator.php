<?php

class Telebotstore_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		remove_role( 'admin_toko' );
		self::add_custom_role();
		self::create_comment_tag_table();
		self::create_order_status_log_table();
		self::create_checkin_table();
		self::create_toko_contact_table();
		self::create_toko_order_csi_table();
		self::create_toko_contact_log_table(); //limamultimedia

		if (! wp_next_scheduled ( 'telebot_midnight' )) {
			$midnight = strtotime( 'midnight' ) - ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
			wp_schedule_event($midnight, 'daily', 'telebot_midnight');
		}

		if (! wp_next_scheduled ( 'telebot_midnight_plus' )) {
			$midnight_plus = strtotime( 'midnight +1 hours' ) - ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
			wp_schedule_event($midnight_plus, 'daily', 'telebot_midnight_plus');
		}


		if (! wp_next_scheduled ( 'telebot_order_pending_reminder' )) {
				wp_schedule_event(strtotime('07:00:00'), 'daily', 'telebot_order_pending_reminder');
		}

	}

	public static function add_custom_role()
	{
		add_role('admin_toko',
            'Admin Toko',
            array(
                'read' => true,
                'edit_posts' => false,
                'delete_posts' => false,
                'publish_posts' => false,
                'upload_files' => false,
            )
        );
	}

	public static function create_comment_tag_table()
	{
		global $wpdb;

        $table = $wpdb->prefix.'comments_tag';

        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
          user_id int(11) NULL,
          post_id int(11) NULL,
          comment_id int(11) NULL,
          replied int(1) NULL DEFAULT 0,
          PRIMARY KEY (ID)
        )";

        $wpdb->query($sql);
	}

	public static function create_order_status_log_table()
	{
		global $wpdb;

        $table = $wpdb->prefix.'order_status_log';

        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
          order_id int(11) NULL,
          user_id int(11) NULL,
          datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          status varchar(255) NULL,
          PRIMARY KEY (ID)
        )";

        $wpdb->query($sql);
	}

	public static function create_checkin_table()
	{
		global $wpdb;

        $table = $wpdb->prefix.'checkin';

        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
		  user_id int(11) NOT NULL,
          visit_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          store text NULL,
		  learned text NULL,
		  photo text NULL,
          revisit_datetime TIMESTAMP NULL,
          location longtext NULL,
		  detail longtext NULL,
          PRIMARY KEY (ID)
        )";

        $wpdb->query($sql);
	}

	public static function create_toko_contact_table()
	{
		global $wpdb;

        $table = $wpdb->prefix.'toko_contact';

        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
		  toko_id int(11) NOT NULL,
          first_name varchar(255) NULL,
		  last_name varchar(255) NULL,
          email varchar(255) NULL,
		  phone varchar(255) NULL,
		  address longtext NULL,
		  birth_day date NULL,
		  hobby text NULL,
		  detail longtext NULL,
          PRIMARY KEY (ID)
        )";

        $wpdb->query($sql);
	}

	/**
	* @author limamultimedia
	* @since 02/04/21
	**/
	public static function create_toko_contact_log_table()
	{
		global $wpdb;

        $table = $wpdb->prefix.'toko_contact_log';

        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
          datemodified datetime NULL,
          contact_id int(11) NOT NULL,
		  toko_id int(11) NOT NULL,
		  update_by int(11) NOT NULL,
          log LONGTEXT NULL,
          PRIMARY KEY (ID)
        )";

        $wpdb->query($sql);
	}

	public static function create_toko_order_csi_table()
	{
		global $wpdb;

        $table = $wpdb->prefix.'toko_order_csi';

        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
		  toko_id int(11) NOT NULL,
          entry_no varchar(255) NULL,
		  posting_date date NULL,
          document_no varchar(255) NULL,
		  document_type varchar(255) NULL,
		  customer_name text NULL,
		  salesperson_code varchar(255) NULL,
		  item_no varchar(50) NULL,
		  quantity int(11) NULL,
		  price varchar(255) NULL,
		  location_code varchar(50) NULL,
		  sales_inc_ppn varchar(255) NULL,
		  item_category varchar(255) NULL,
		  manufacture_code varchar(255) NULL,
		  city varchar(255) NULL,
		  dlm_lr_kota varchar(50) NULL,
		  iq int(11) NULL,
		  sales_exc_ppn varchar(255) NULL,
          PRIMARY KEY (ID)
        )";

        $wpdb->query($sql);
	}
}
