<?php 

class Telebotstore_Order_Reminder{

	private $post_type = 'telebotstore_order';

	private $key_status = '_order_status';

	private $pending = 'pending';

	private $settings;


	public function __construct(){

		add_action('telebot_order_pending_reminder',[$this,'run']);

	}


	public function get_setting(){

		if(!$this->settings){
			$reminder_order_pending_spv = carbon_get_theme_option('reminder_order_pending_spv');
			$reminder_order_pending_manager = carbon_get_theme_option('reminder_order_pending_manager');
			$email_reminder_spv = carbon_get_theme_option('email_reminder_spv');
			$email_reminder_manager = carbon_get_theme_option('email_reminder_manager');

			$setting = array(
							'reminder_order_pending_spv' => $reminder_order_pending_spv,
							'reminder_order_pending_manager' => $reminder_order_pending_manager,
							'email_reminder_spv' => $email_reminder_spv,
							'email_reminder_manager' => $email_reminder_manager
						);

			$this->settings = $setting;
		}

		return $this->settings;
	}



	public function get_order_pending($day=2){

		$day_before = "-".$day." month";

		$args = array(
            'posts_per_page' => '-1',
			'post_status'    => 'publish',
		    'orderby'        => 'ID',
            'order'          => 'ASC',
        );

		$args['meta_query'] = array(
			array(
				'key' => '_order_status',
				'value' => 'pending',
				'compare' => '='
			),
		);

		$args['date_query'] = array(
			'column' => 'post_date',
			'before' => date('Y-m-d H:i:s', strtotime($day_before)),
		);

		$args['post_type'] = $this->post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		return $posts;
	}


	public function validate_day($day_before){

		$yesterday = date("Y-m-d", strtotime("yesterday")); ;
		$date_before = date('Y-m-d',strtotime("-".$day_before. "day"));
		$begin = new DateTime( $date_before );
		$end   = new DateTime( $yesterday );

		$index = 0;
		for($i = $begin; $i <= $end; $i->modify('+1 day')){
		    
		    if($i->format("D") == 'Sun'){
		    	$index++;
		    }

		    $index++;
		}

		return $index;
	}


	public function run(){
		/**
		 * 1. Get Setting
		 * 2. Cek hari < {reminder_order_pending_spv,reminder_order_pending_manager} apakah ada hari minggu
		 * 3. Jika Ya, setting reminder + jml hari minggu
		 * 4. Query Order < Hari setting reminder
		 * 5. Send Email (SPV & Manager)
		 */

		
		$settings = $this->get_setting();

		$day_before = $settings['reminder_order_pending_spv'];
		$day = $this->validate_day($day_before);

		$orders = $this->get_order_pending($day);
		if(!$orders) return;

		//send email to spv 
		$email = $settings['email_reminder_spv'];
		$this->send_email($orders,$email,$day_before);

		
		$day_before = $settings['reminder_order_pending_manager'];
		$day = $this->validate_day($day_before);

		$orders = $this->get_order_pending($day);
		if(!$orders) return;

		//send email to manager
		$email = $settings['email_reminder_manager'];
		$this->send_email($orders,$email,$day_before);
	}


	public function send_email($orders,$email,$day_before){

		if(!$email || !$orders) return;

		$css_td = 'style="font-size:13px; color:#000; padding-right:20px; padding-left:5px; text-align:left;"';
		$css_td_end = 'style="border-bottom:1px solid #555; font-size:13px; color:#000; padding-left:5px; text-align:left;"';

		$subject = 'List Order Pending ('.date('d/M/y').') ';

		$content = '<p>Berikut List Order Pending lebih dari '.$day_before.' hari kerja. <strong> Total '.count($orders).' Order</strong>: </p>';

		$content .= '<table border="0" cellpadding="5" cellspacing="0" style="border:2px solid #555;">';
		
		$no = 1;
		foreach($orders as $row){
			$id = $row->ID;
			$tanggal = get_the_date('d-M-y', $id);

			$toko_id = get_post_meta($id,'_order_toko',true);
			$toko = get_post($toko_id)->post_title;

			$user_info = get_userdata($row->post_author);
			$sales = $user_info->display_name;

			$produk = [];
			$items = get_post_meta( $id, '_order_items', false);
			foreach( (array) $items as $key=>$val ){
				$value = explode('|', $val);
				$item = Telebotstore_Item::get_by_slug( $value[0] );
				$qty = intval($value[1]);

				$produk[] = $item->post_title.' ('.$qty.')';
			}

			$produk = implode(', ', $produk);

			$content .= '<tr>
							<th '.$css_td.'>No</th>';
			$content .= 	'<td '.$css_td.'>: '.$no.'</td>';
			$content .= '</tr>';
			$content .= '<tr>';
			$content .= 	'<th '.$css_td.'>Tanggal</th>';
			$content .= 	'<td '.$css_td.'>: '.$tanggal.'</td>';
			$content .= '</tr>';
			$content .= '<tr>';
			$content .= 	'<th '.$css_td.'>Toko</th>';
			$content .= 	'<td '.$css_td.'>: <a href="'.get_the_permalink($row->ID).'">'.$toko.'</a></td>';
			$content .= '</tr>';
			$content .= '<tr>';
			$content .= 	'<th '.$css_td.'>Item (s)</th>';
			$content .= 	'<td '.$css_td.'>: '.$produk.'</td>';
			$content .= '</tr>';
			$content .= '<tr>';
			$content .= 	'<th '.$css_td_end.'>Sales</th>';
			$content .= 	'<td '.$css_td_end.'>: '.$sales.'</td>';
			$content .= '</tr>';	

			$no++;			
		}

		$content .= '</table>';

		$html_content = '<div style="padding:10px; background:#F0F0F0; border:1px solid #CCC;">';
		$html_content .= '<div style="padding:20px; background:#FFF; border:1px solid #DDD;">';
		$html_content .= $content;
		$html_content .= '<p>Email ini di kirim otomatis oleh system.</p>';
		$html_content .= '</div>';
		$html_content .= '</div>';

		
		wp_mail($email,$subject,$html_content);
	}

}

$Telebotstore_Order_Reminder = new Telebotstore_Order_Reminder;