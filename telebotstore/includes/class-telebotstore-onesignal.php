<?php

Class Telebotstore_OneSignal
{
    /**
	 * curl send notif
	 * @param  [type] $fields [description]
	 * @return [type]         [description]
	 */
	public static function send($title,$content,$url)
	{
        if(! function_exists('curl_version')):
            return;
        endif;

        $app_id = carbon_get_theme_option('onesignal_app_id');
		$api_key = carbon_get_theme_option('onesignal_app_key');

		$body = array(
			'app_id' => $app_id,
			'included_segments' => array('Active Users'),
			'contents' => array(
				"en" => $content
			),
			'headings' => array(
				"en" => $title
			),
			'url' => $url,
		);

		$fields = json_encode($body);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			"Content-Type: application/json; charset=utf-8",
			"Authorization: Basic '.$api_key.'"
		]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}
}
