<?php

/**
 * The file that defines the order class
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */

Class Telebotstore_Item
{
	const post_type = 'telebotstore_item';

    /**
     * get item by slug
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public static function get_by_slug( $slug )
    {
        $args = array(
            'name'        => $slug,
            'post_type'   => self::post_type,
        );

        $items = get_posts($args);

        if($items) return $items[0];

		return false;
    }

    /**
     * get name of item list
     * @return [type] [description]
     */
    public static function get_item_name_list()
    {
        $args = array(
            'post_type' => self::post_type,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'orderby'        => 'ID',
            'order'          => 'DESC',
        );

        $q = new WP_Query();
		$items = $q->query( $args );

		$objs = array();

		foreach ( (array) $items as $item ) :
			$objs[] = array(
                'text' => '/item '.$item->post_name
            );
		endforeach;

        return $objs;

    }
}
