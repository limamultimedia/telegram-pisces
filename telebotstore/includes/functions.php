<?php 
function lima_set_html_mail_content_type() {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'lima_set_html_mail_content_type' );

function data_day_en_id($day=''){
    $days = [
        'Sunday'    => 'Minggu',
        'Monday'    => 'Senin',
        'Tuesday'   => 'Selasa',
        'Wednesday' => 'Rabu',
        'Thursday'  => 'Kamis',
        'Friday'    => 'Jumat',
        'Saturday'  => 'Sabtu'
    ];

    if($day){
        return $days[$day];
    }

    return $days;
}


function date_to_hari($date){

	$day = date('l',strtotime($date));
	$hari = data_day_en_id($day);

	return $hari;

}


function get_display_name($user_id){
    $user = get_user_by('ID',$user_id);
    return $user->display_name;
}


function generate_tgl_pengajuan_jadwal($week=0){
    $Telebotstore_Jadwal_Kunjungan = new Telebotstore_Jadwal_Kunjungan;

    $start_date = date('Y-m-d');
    $list_days = $Telebotstore_Jadwal_Kunjungan->get_tanggal_pengajuan($week,$start_date);

    $index = 0;
    $first_date = $start_date;
    foreach($list_days as $dates){

        $date = $dates->format('Y-m-d');
        if($index == 0){
            $first_date = $date;
        }

        $index++;
    }

    $data = array(
                'start' => $first_date,
                'end' => $date
            );

    return $data;
}


function get_toko_last_visit($toko_id,$date=''){
    global $wpdb;

    $date = !$date ? date('Y-m-d') : $date;

    $query = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."checkin WHERE store='".$toko_id."' AND DATE(visit_datetime)<= '".$date."' ORDER BY visit_datetime DESC LIMIT 1 ");

    return $query;
}


function check_toko_is_visit($toko_id,$date,$user_id=''){
    global $wpdb;

    $query = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."checkin WHERE store='".$toko_id."' AND DATE(visit_datetime)= '".$date."' ORDER BY visit_datetime DESC LIMIT 1 ");

    return $query;
}

?>