<?php

use Aws\S3\S3Client;
use Aws\Exception\AwsException;


/**
 * amazon s3 classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore_S3 {

    private $key = '';
    private $secret = '';
    private $bucket_name = '';

	/**
     * Construction
     */
	public function __construct() {

        $this->key = carbon_get_theme_option('telebotstore_s3_key');
        $this->secret = carbon_get_theme_option('telebotstore_s3_secret');
        $this->bucket_name = carbon_get_theme_option('telebotstore_s3_bucket_name');
	}

    public function upload($file = false,$pathfile){

        if( !$file ) return;

		$client = new S3Client([
		    'version' => 'latest',
		    'region' => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => $this->key,
		        'secret' => $this->secret
		    ]
		]);
		try {
			$upload = $client->putObject([
				'Bucket' => $this->bucket_name,
				'Key'    => $pathfile,
				'Body'   => 'this is the body!',
				'SourceFile' => $file
			]);

			//telebotstore_debug($upload);

			//telebotstore_debug( $client->getObjectUrl($this->bucket_name, basename($file)) );

			// $get = $client->getObject([
			// 	'Bucket' => $this->bucket_name,
		    //     'Key'    => basename($file)
		    // ]);

		    $result = array(
                'status' => 'SUCCESS',
                'url' =>  $upload['ObjectURL'],
            );

            return $result;

		} catch (Aws\S3\Exception\S3Exception $e) {
		    // output error message if fails
            $result = array(
                'status' => 'FAILED',
                'message' =>  $e->getMessage(),
            );

            return $result;
		}

    }

}
