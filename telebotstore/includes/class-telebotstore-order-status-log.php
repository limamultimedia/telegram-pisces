<?php

/**
 * The file that defines the comment tag class
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
Class Telebotstore_Order_Status_Log
{
    private $table;
    private $wpdb;
    private $args;
    private $count;

    public function __construct($args = array())
    {
        global $wpdb;

        $this->wpdb = $wpdb;
        $this->table = $wpdb->prefix.'order_status_log';
        $this->args = $args;
    }

    /**
     * insert new comment tag in databse
     * @return [type] [description]
     */
    public function insert()
    {
        $data = array(
            'order_id' => (int)$this->args['order_id'],
            'user_id' => (int)$this->args['user_id'],
            'status' => $this->args['status'],
        );

        $this->wpdb->insert( $this->table, $data );
    }

    public function get()
    {
        $sql = "SELECT * FROM $this->table WHERE order_id = ".$this->args['order_id'];

        $sql .= ' ORDER BY ID ASC';

		$results = $this->wpdb->get_results( $sql, OBJECT );

        return $results;
    }

    public function get_last_by_post_id()
    {
        $sql = "SELECT * FROM $this->table WHERE order_id = ".$this->args['order_id'];

        $sql .= ' ORDER BY ID DESC';

		$results = $this->wpdb->get_results( $sql, OBJECT );

        return isset($results[0]) ? $results[0] : false;
    }
}
