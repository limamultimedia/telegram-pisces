<?php

/**
 * comment walker using botstrap
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */

 Class Telebotstore_Bootstrap_Comment extends Walker_Comment
 {
 	protected function html5_comment( $comment, $depth, $args ) {

        $comment_tag = new Telebotstore_Comments_Tag(array('comment_id'=> $comment->comment_ID));

		$notifs = $comment_tag->get_tagged_user();

        $tagged = '';
        foreach( $notifs as $notif ):
            $user = get_userdata($notif->user_id);
            $tagged .= '<span>'.$user->display_name.'</span>,&nbsp;';
        endforeach;

         ?>
 		<li id="comment-<?php comment_ID(); ?>" <?php comment_class( $this->has_children ? 'parent media commentar' : 'media commentar' ); ?>>
 			<div class="media-left">
 				<a href="<?php echo get_comment_author_url(); ?>" class="media-object">
 					<?php echo get_avatar( $comment, 50 ); ?>
 				</a>
 			</div>

 			<div class="media-body">

 				<?php printf( '<h4 class="media-heading">%s</h4>', get_comment_author_link() ); ?>
				<time datetime="<?php comment_time( 'c' ); ?>">
					<?php printf( _x( '%1$s at %2$s', '1: date, 2: time' ), get_comment_date(), get_comment_time('H:i:s') ); ?>
				</time>

 				<?php if ( '0' == $comment->comment_approved ) : ?>
 				<p class="comment-awaiting-moderation label label-info"><?php _e( 'Your comment is awaiting moderation.' ); ?></p>
 				<?php endif; ?>

                <?php

                $solution = get_comment_meta($comment->comment_ID, '_solution', true);

                ?>

 				<div class="comment-content <?php if( $solution) {?>alert alert-info<?php } ?>">
 					<?php comment_text(); ?>
                    <?php if($notifs): ?>
                        <div class="comment-tagged">Tagged: <?php echo $tagged; ?></div>
                    <?php endif; ?>
 				</div><!-- .comment-content -->

 				<ul class="list-inline comment-reply">
 					<?php
 						comment_reply_link( array_merge( $args, array(
 							//'add_below' => 'div-comment',
 							'depth'     => $depth,
 							'max_depth' => $args['max_depth'],
 							'before'    => '<li class="reply-link">',
 							'after'     => '</li>'
 						) ) );
 					?>
                    <?php
                    $status = get_post_meta(get_the_ID(), '_ticket_status',true);

                    if( get_post_type($comment->comment_post_ID) == 'telebotstore_ticket' && $status !== 'closed' ): ?>
                        <li class="reply-link"><a rel="nofollow" style="color:red; cursor:pointer" onclick="markAsSolution('<?php echo $comment->comment_ID; ?>','<?php echo get_comment_author( $comment ); ?>');" aria-label="Reply to Suwarno">Mark As Solution</a></li>
                    <?php endif; ?>
 				</ul>

 			</div>
 <?php
 	}
 }
