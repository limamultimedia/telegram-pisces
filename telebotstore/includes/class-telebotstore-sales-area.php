<?php

/**
 * sales area classes
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2.1
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore_Sales_Area {

    const TABLE = 'sales_area';

    public function __construct(){
        $this->hook();
    }

    public function get($andWhere=''){
        global $wpdb;

        $query = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix.self::TABLE." WHERE 1 ".$andWhere);
        return $query;

    }



    public function insert($data){
        global $wpdb;

        $query = $wpdb->get_var("SELECT id FROM ".$wpdb->prefix.self::TABLE. " WHERE request_by='".$data['request_by']."' AND toko_id='".$data['toko_id']."' LIMIT 1 ");

        if(!$query){
            $wpdb->insert($wpdb->prefix.self::TABLE,$data);
            return $wpdb->insert_id;
        }else{
            return $query;
        }

    }


    public function hook(){
       

        add_action('wp_ajax_get_my_sales_area',[$this,'ajax_get_my_sales_area']);

        add_action('wp_ajax_add_store_to_list',[$this,'add_store_to_list']);

        add_action('wp_ajax_load_list_sales_area',[$this,'load_list_sales_area']);

        add_action('wp_ajax_load_detail_sales_area',[$this,'load_detail_sales_area']);

        add_action('wp_ajax_sales_area_action',[$this,'sales_area_action']);

    }


    public function ajax_get_my_sales_area(){

        $toko_id = $_POST['toko_id'];
        $user_id = get_current_user_id();

        $andWhere = " AND request_by='".$user_id."' AND toko_id='".$toko_id."' ";

        $query = $this->get($andWhere);
        if(!$query){
            $html = '<div class="alert alert-warning info-add-toko"> <i class="fa fa-circle-info"></i> Toko ini belum masuk ke List Kunjungan Anda. <a href="javascript:;" id="add-store-to-list" data-id="'.$toko_id.'">Tambahkan</a> </div>';
        }else{
            $html = '';
        }

        echo $html;
        die();
    }


    public function load_list_sales_area(){
        $Telebotstore_Sales_Area = new Telebotstore_Sales_Area;

        $query = $Telebotstore_Sales_Area->get(" GROUP BY request_by ORDER BY datecreated DESC ");
        include TELE_TEMPLATES.'/sales-area/list.php';

        die();
    }


    public function add_store_to_list(){

        $toko_id = $_POST['toko_id'];
        $user_id = get_current_user_id();
        $now = current_time('mysql');

        $data = array(
                    'datecreated' => $now,
                    'request_by' => $user_id,
                    'toko_id' => $toko_id,
                    'st_request' => 1
                );

       $insert = $this->insert($data);
        if($insert){
            $html = '<div class="alert alert-success "> <i class="fa fa-circle-check"></i> Toko Sukses di Tambahkan ke List. Menunggu Approval Admin/SPV</div>';
        }else{
            $html = '<div class="alert alert-danger "> <i class="fa fa-circle-info"></i> Update Data Gagal!</div>';
        }

        echo $html;
        die();

    }


    public function load_detail_sales_area(){
        $sales_id = $_POST['sales'];

        $query = $this->get(" AND request_by='".$sales_id."' ORDER BY datecreated DESC ");

        include TELE_TEMPLATES.'/sales-area/detail.php';
        die();
    }


    public function sales_area_action(){
        global $wpdb;

        $table = $wpdb->prefix.self::TABLE;

        $act = $_POST['act'];
        $ids = $_POST['id'];

        if($act == 'delete'){
            
            foreach($ids as $id){
                $where = ['id' => $id];
                $wpdb->delete($table,$where);
            }

        }else if($act == 'reject' || $act == 'approve'){
            
            foreach($ids as $id){

                $status = $act == 'reject' ? 99 : 1;
                $update = ['status' => $status,'update_by' => get_current_user_id(),'datemodiffied' => current_time('mysql')];
                $where = ['id' => $id];
                $wpdb->update($table,$update,$where);
            }

        }
    }

}

$Telebotstore_Sales_Area = new Telebotstore_Sales_Area;