<?php

/**
 * checkin classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore_Checkin {

    const TABLE = 'checkin';


    public function __construct(){
        add_action('init',[$this,'do_insert']);
    }


 

    public function insert($args){

        global $wpdb;

        if( empty($args) ) return false;

        $Telebotstore_Checkin_Log = new Telebotstore_Checkin_Log;

        $insert = array(
            'user_id'          => $args['user_id'],
            'visit_datetime'   => isset($args['visit_datetime']) ? sanitize_text_field($args['visit_datetime']): NULL,
            'store'            => isset($args['store']) ? sanitize_text_field($args['store']) : NULL,
            'learned'          => isset($args['learned']) ? sanitize_text_field($args['learned']) : NULL,
            'photo'            => isset($args['photo']) ? sanitize_text_field($args['photo']) : NULL,
            'revisit_datetime' => isset($args['revisit_datetime']) ? sanitize_text_field($args['revisit_datetime']) : NULL,
            'location'         => isset($args['location']) ? sanitize_text_field($args['location']) : NULL,
            'detail'           => isset($args['detail']) ? sanitize_text_field($args['detail']) : NULL,
        );

        $do_insert = $wpdb->insert( $wpdb->prefix.self::TABLE, $insert );

        $id = $wpdb->insert_id;
        $coordinate = $Telebotstore_Checkin_Log->get_coordinate_location($insert['location']);

        $date = date('Y-m-d',strtotime($insert['visit_datetime']));
        $Telebotstore_Checkin_Log->insert_log($insert['user_id'],$date,$id,$coordinate);

        

        return $do_insert;
    }


}

$Telebotstore_Checkin = new Telebotstore_Checkin;


class Telebotstore_Query_Checkin{

    private $args = array();
    private $wpdb = array();

    /**
     * construction
     * @param [type] $args [description]
     */
    public function __construct($args = array()){
        global $wpdb;

        $this->wpdb = $wpdb;

        $default = array(
            'page'   => 1,
            'offset' => 0,
            'limit'  => 20,
            'select' => '*'
        );

        if( isset($args['page']) && $args['page'] > 1 ):
            $limit = isset($args['limit']) && $args['limit'] ? $args['limit'] : $default['limit'];
            $args['offset'] = ($args['page'] - 1 ) * $limit;
        endif;

        $args = wp_parse_args($args, $default);

        $this->args = $args;

        
    }



    /**
     * run query
     * @return [type] [description]
     */
    public function query(){

        $sql = 'SELECT '.$this->args['select'].' FROM '.$this->wpdb->prefix.'checkin';

        $where = array();

        if( isset($this->args['user_id']) && $this->args['user_id'] ):
            $where[] = 'user_id = "'.intval($this->args['user_id']).'"';
        endif;

        if( isset($this->args['store']) && $this->args['store'] ):
            $where[] = 'store = "'.intval($this->args['store']).'"';
        endif;

        if( isset($this->args['date_start']) && $this->args['date_start'] ):
            $where[] = 'visit_datetime BETWEEN "'.$this->args['date_start'].'" AND "'.$this->args['date_end'].'"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $sql .= ' ORDER BY ID DESC LIMIT '.$this->args['limit'].' OFFSET '.$this->args['offset'];

        return $this->wpdb->get_results($sql);

    }

    public function count_visit_today(){

        $sql = 'SELECT user_id,COUNT(*) AS visit_count FROM '.$this->wpdb->prefix.'checkin';


        $sql .= ' WHERE visit_datetime BETWEEN "'.date('Y-m-d').'" AND "'.date('Y-m-d', strtotime('+1 day')).'"';

        $sql .= ' GROUP BY user_id';

        return $this->wpdb->get_results($sql);

    }

    public function count_visit_yesterday(){

        $sql = 'SELECT user_id,COUNT(*) AS visit_count FROM '.$this->wpdb->prefix.'checkin';


        $sql .= ' WHERE visit_datetime BETWEEN "'.date('Y-m-d', strtotime('-1 day')).'" AND "'.date('Y-m-d').'"';

        $sql .= ' GROUP BY user_id';

        return $this->wpdb->get_results($sql);

    }

    public function count_visit_day_before_yesterday(){

        $sql = 'SELECT user_id,COUNT(*) AS visit_count FROM '.$this->wpdb->prefix.'checkin';


        $sql .= ' WHERE visit_datetime BETWEEN "'.date('Y-m-d', strtotime('-2 day')).'" AND "'.date('Y-m-d', strtotime('-1 day')).'"';

        $sql .= ' GROUP BY user_id';

        return $this->wpdb->get_results($sql);

    }

    public function count_visits(){

        $sql = 'SELECT user_id,COUNT(*) AS visit_count FROM '.$this->wpdb->prefix.'checkin';

        $where = array();

        // if( isset($this->args['date_start']) && $this->args['date_start'] ):
        //     $sql .= ' WHERE visit_datetime BETWEEN "'.$this->args['date_start'].'" AND "'.$this->args['date_end'].'"';
        // endif;

        if( isset($this->args['store']) && $this->args['store'] ):
            $where[] = 'store = "'.intval($this->args['store']).'"';
        endif;

        if( isset($this->args['date_start']) && $this->args['date_start'] ):
            $where[] = 'visit_datetime BETWEEN "'.$this->args['date_start'].'" AND "'.$this->args['date_end'].'"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $sql .= ' GROUP BY user_id';

        return $this->wpdb->get_results($sql);

    }


    public function total(){

        $sql = 'SELECT COUNT(*) as total FROM '.$this->wpdb->prefix.'checkin';

        $where = array();

        if( isset($this->args['user_id']) && $this->args['user_id'] ):
            $where[] = 'user_id = "'.$this->args['user_id'].'"';
        endif;

        if( isset($this->args['store']) && $this->args['store'] ):
            $where[] = 'store = "'.$this->args['store'].'"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $result = $this->wpdb->get_results($sql);

        return isset($result[0]) ? $result[0]->total : false;
    }

}
