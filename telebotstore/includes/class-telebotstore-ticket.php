<?php

/**
 * The file that defines the order class
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
Class Telebotstore_Ticket
{
	const post_type = 'telebotstore_ticket';

	/**
	 * add new order for telegram bot
	 * @param [type] $data [description]
	 */
	public static function insert( $arg )
	{

        if( empty($arg) ) return;

        $ticket = array(
            'post_author'           => $arg['user_id'],
            'post_title'            => $arg['subject'],
            'post_status'           => 'publish',
            'post_type'             => self::post_type,
            'post_content'          => $arg['content'],
        );

        $ticket_id = wp_insert_post( $ticket );
        add_post_meta( $ticket_id, '_ticket_status', 'open' );
        add_post_meta( $ticket_id, '_ticket_store', $arg['store'] );
        add_post_meta( $ticket_id, '_ticket_priority', $arg['priority'] );

        return $ticket_id;
	}

}

Class Telebotstore_Query_Ticket{

    const post_type = 'telebotstore_ticket';
    private $arg = array();
	private $count = 0;

    /**
     * construction
     * @param [type] $args [description]
     */
    public function __construct( $args ){

        $default = array(
            'page'   => 1,
            'offset' => 0,
            'limit'  => 20,
        );

        if( isset($args['page']) && $args['page'] > 1 ):
            $limit = isset($args['limit']) && $args['limit'] ? $args['limit'] : $default['limit'];
            $args['offset'] = ($args['page'] - 1 ) * $limit;
        endif;

        $this->arg = wp_parse_args($args, $default);

    }

    public function get()
	{
		$args = array(
            'posts_per_page' => $this->arg['limit'],
			'post_status'    => 'publish',
			'offset'         => $this->arg['offset'],
            'orderby'        => 'ID',
            'order'          => 'DESC',
        );

		if( ! empty($this->arg['user']) ):
			$args['author'] = $this->arg['user'];
		endif;

		if( ! empty($this->arg['date']) ):
			$date = explode(' - ', $this->arg['date']);
			$before = date('d-m-Y',date(strtotime("+1 day", strtotime($date[1]))));
			$args['date_query'] = array(
				'column' => 'post_date',
				'after'  => $date[0],
				'before' => $before,
			);
		endif;

		$meta_query = array();

        if ( ! empty( $this->arg['status'] ) ) :
            $meta_query[] = array(
		            'key' => '_ticket_status',
		            'value' => $this->arg['status'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( ! empty( $this->arg['store'] ) ) :
            $meta_query[] = array(
		            'key' => '_ticket_store',
		            'value' => $this->arg['store'],
		            'compare' => 'LIKE'
                );
        endif;

		if ( ! empty( $this->arg['priority'] ) ) :
            $meta_query[] = array(
		            'key' => '_ticket_priority',
		            'value' => $this->arg['priority'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( count($meta_query) > 1 ) :
			$meta_query['relation'] = 'AND';
		endif;

		if ( $meta_query ):
			$args['meta_query'] = $meta_query;
		endif;

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		$this->count = $q->found_posts;

		return $posts;
	}

    public function count(){

        return $this->count;
    }
}
