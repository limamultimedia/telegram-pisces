<?php

/**
 * checkin classes
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore_Toko_Contact {

    const TABLE = 'toko_contact';

    /**
     * inserted new data
     * @param  [type] $args [description]
     * @return [type]       [description]
     */
    public static function insert($args){

        global $wpdb;

        if( empty($args) ) return false;

        $insert = array(
            'toko_id'    => intval($args['toko_id']),
            'first_name' => isset($args['first_name']) ? sanitize_text_field($args['first_name']): NULL,
            'last_name'  => isset($args['last_name']) ? sanitize_text_field($args['last_name']): NULL,
            'email'      => isset($args['email']) ? sanitize_email($args['email']) : NULL,
            'handphone_1'      => isset($args['handphone_1']) ? sanitize_text_field($args['handphone_1']) : NULL,
            'handphone_2'      => isset($args['handphone_2']) ? sanitize_text_field($args['handphone_2']) : NULL,
            'phone'      => isset($args['phone']) ? sanitize_text_field($args['phone']) : NULL,
            'address'    => isset($args['address']) ? sanitize_text_field($args['address']) : NULL,
            'birth_day'  => isset($args['birth_day']) ? sanitize_text_field($args['birth_day']) : NULL,
            'hobby'      => isset($args['hobby']) ? sanitize_text_field($args['hobby']) : NULL,
            'detail'     => isset($args['detail']) ? sanitize_text_field($args['detail']) : NULL,
        );

        return $wpdb->insert( $wpdb->prefix.self::TABLE, $insert );
    }

    /**
     * update row data
     * @param  [type] $args  [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public static function update($args, $where){

        global $wpdb;



        if( empty($args) ) return false;


        $update = array();

        if( isset($args['toko_id']) && $args['toko_id'] ):
            $update['toko_id'] = intval($args['toko_id']);
        endif;

        if( isset($args['first_name']) && $args['first_name'] ):
            $update['first_name'] =  sanitize_text_field($args['first_name']);
        endif;

        if( isset($args['last_name']) && $args['last_name'] ):
            $update['last_name'] =  sanitize_text_field($args['last_name']);
        endif;

        if( isset($args['email']) && $args['email'] ):
            $update['email'] =  sanitize_email($args['email']);
        endif;

        if( isset($args['phone']) && $args['phone'] ):
            $update['phone'] =  sanitize_text_field($args['phone']);
        endif;

        if( isset($args['address']) && $args['address'] ):
            $update['address'] =  sanitize_text_field($args['address']);
        endif;

        if( isset($args['birth_day']) && $args['birth_day'] ):
            $update['birth_day'] =  sanitize_text_field($args['birth_day']);
        endif;

        if( isset($args['hobby']) && $args['hobby'] ):
            $update['hobby'] =  sanitize_text_field($args['hobby']);
        endif;

        if( isset($args['detail']) && $args['detail'] ):
            $update['detail'] =  sanitize_text_field($args['detail']);
        endif;


        //update by limamultimedia
        $Telebotstore_Toko_Contact_Log = new Telebotstore_Toko_Contact_Log;
        $Telebotstore_Toko_Contact_Log->doing_write_log($args,$where);


        return $wpdb->update( $wpdb->prefix.self::TABLE, $update, $where );


    }

    /**
     * delete row data
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public static function delete($where){

        global $wpdb;

        return $wpdb->delete( $wpdb->prefix.self::TABLE, $where );
    }
}

class Telebotstore_Query_Toko_Contact{

    private $args = array();
    private $wpdb = array();

    /**
     * construction
     * @param [type] $args [description]
     */
    public function __construct($args = array()){
        global $wpdb;

        $this->wpdb = $wpdb;

        $default = array(
            'page'   => 1,
            'offset' => 0,
            'limit'  => 20,
            'select' => '*'
        );

        if( isset($args['page']) && $args['page'] > 1 ):
            $limit = isset($args['limit']) && $args['limit'] ? $args['limit'] : $default['limit'];
            $args['offset'] = ($args['page'] - 1 ) * $limit;
        endif;

        $args = wp_parse_args($args, $default);

        $this->args = $args;
    }

    /**
     * run query
     * @return [type] [description]
     */
    public function query(){

        $sql = 'SELECT '.$this->args['select'].' FROM '.$this->wpdb->prefix.'toko_contact';

        $where = array();

        if( isset($this->args['ID']) && $this->args['ID'] ):
            $where[] = 'ID = "'.$this->args['ID'].'"';
        endif;

        if( isset($this->args['toko_id']) && $this->args['toko_id'] ):
            $where[] = 'toko_id = "'.$this->args['toko_id'].'"';
        endif;

        if( isset($this->args['name']) && $this->args['name'] ):
            $where[] = 'first_name LIKE "%'.$this->args['name'].'%" OR last_name LIKE "%'.$this->args['name'].'%"';
        endif;

        if( isset($this->args['email']) && $this->args['email'] ):
            $where[] = 'email LIKE "%'.$this->args['email'].'%"';
        endif;

        if( isset($this->args['phone']) && $this->args['phone'] ):
            $where[] = 'phone LIKE "%'.$this->args['phone'].'%"';
        endif;

        if( isset($this->args['birth_day']) && $this->args['birth_day'] ):
            $where[] = 'birth_day LIKE "%'.$this->args['birth_day'].'%"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $sql .= ' ORDER BY ID DESC LIMIT '.$this->args['limit'].' OFFSET '.$this->args['offset'];


        return $this->wpdb->get_results($sql);

    }

    public function total(){

        $sql = 'SELECT COUNT(*) as total FROM '.$this->wpdb->prefix.'toko_contact';

        $where = array();

        if( isset($this->args['ID']) && $this->args['ID'] ):
            $where[] = 'ID = "'.$this->args['ID'].'"';
        endif;

        if( isset($this->args['toko_id']) && $this->args['toko_id'] ):
            $where[] = 'toko_id = "'.$this->args['toko_id'].'"';
        endif;

        if( isset($this->args['name']) && $this->args['name'] ):
            $where[] = 'first_name LIKE "%'.$this->args['name'].'%" OR last_name LIKE "%'.$this->args['name'].'%"';
        endif;

        if( isset($this->args['email']) && $this->args['email'] ):
            $where[] = 'email LIKE "%'.$this->args['email'].'%"';
        endif;

        if( isset($this->args['phone']) && $this->args['phone'] ):
            $where[] = 'phone LIKE "%'.$this->args['phone'].'%"';
        endif;

        if( isset($this->args['birth_day']) && $this->args['birth_day'] ):
            $where[] = 'birth_day LIKE "%'.$this->args['birth_day'].'%"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $result = $this->wpdb->get_results($sql);

        return isset($result[0]) ? $result[0]->total : false;
    }

}
