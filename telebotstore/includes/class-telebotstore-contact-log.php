<?php

/**
 * checkin classes
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore_Toko_Contact_Log {

	const TABLE = 'toko_contact_log';
	const TABLE_CONTACT = 'toko_contact';

	public function validate_is_any_update($args,$where){

		global $wpdb;

		$andWhere = '';
		foreach($where as $col => $val){
			$andWhere .= " AND ".$col."='".$val."' ";
		}

		$sql = "SELECT * FROM ".$wpdb->prefix.self::TABLE_CONTACT." WHERE 1 ".$andWhere;
		$query = $wpdb->get_row($sql);

		$updates = array();
		foreach($args as $col => $val_update){

			if($query->{$col} == $val_update){
				continue;
			}else{
				$updates[$col] = array('old_value' => $query->{$col},'new_value' => $val_update);
			}

		}


		return $updates;

	}


	public function doing_write_log($args,$where){
		
		global $wpdb;

		$is_update = $this->validate_is_any_update($args,$where);

		if(!$is_update) return;

		date_default_timezone_set("Asia/Bangkok");

		$insert = array(
					'datemodified' => date('Y-m-d H:i:s'),
					'update_by' => get_current_user_id(),
					'toko_id' => $args['toko_id'],
					'contact_id' => $where['ID'],
					'log' => serialize($is_update)
					);

		
		$wpdb->insert($wpdb->prefix.self::TABLE,$insert);

	}


	public function get_name_by_id($user_id){

		$user = get_userdata($user_id);

		return $user->user_login;

	}


	public function show_log($id){
		global $wpdb;
		
		$logs = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix.self::TABLE." WHERE contact_id='".$id."' ORDER BY datemodified DESC ");

		
		?>
		<div class="contact-log">
            <h3>Log Update</h3>
            <div class="log-container">
            	<table class="table table-striped">
            		<thead>
            			<tr>
            				<th>No</th>
            				<th>Date Modified</th>
            				<th>Update</th>
            				<th>Update By</th>
            			</tr>
            		</thead>	

            		<tbody>
            	
            	<?php
            	if($logs){
            		$no = 1;
            		foreach($logs as $log){

            			$log_updates = $log->log;

            			$log_updates = unserialize($log_updates);
            			?>
            				<tr>
            					<td><?= $no; ?></td>
            					<td><?= $log->datemodified; ?></td>
            					<td>
	            					<ul>
	            						<?php
	            							foreach($log_updates as $key=>$val) { ?>
	            								<li>
	            									<?php 

	            									echo '<strong>'.$key.': </strong> '.
	            										$this->get_old_value_new_value_html($val); 

	            									?>
	            											
	            								</li>
	            						<?php } ?>	
	            						
	            					</ul>	
            					</td>	
            					<td><?= $this->get_name_by_id($log->update_by); ?></td>
            				</tr>
            			<?php
            			$no++;
            		}

            	}else{
            		?>
            		<tr>
            			<td colspan="4">No Log!</td>
            		</tr>
            		<?php
            	}
            	?>
            		</tbody>
            	</table>
            </div>
        </div>
		<?php
	}


	public function get_old_value_new_value_html($args){

		$html = $args['old_value'].' &#8594; '.$args['new_value'];

		return $html;
	}

}


$Telebotstore_Toko_Contact_Log = new Telebotstore_Toko_Contact_Log;

?>