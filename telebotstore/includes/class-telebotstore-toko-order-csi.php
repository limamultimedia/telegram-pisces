<?php

/**
 * checkin classes
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore_Toko_Order_Csi {

    const TABLE = 'toko_order_csi';

    /**
     * inserted new data
     * @param  [type] $args [description]
     * @return [type]       [description]
     */
    public static function insert($args){

        global $wpdb;

        if( empty($args) ) return false;

        $insert = array(
            'toko_id'          => intval($args['toko_id']),
            'entry_no'         => isset($args['entry_no']) ? sanitize_text_field($args['entry_no']): NULL,
            'posting_date'     => isset($args['posting_date']) ? date('Y-m-d', strtotime($args['posting_date'])) : NULL,
            'document_no'      => isset($args['document_no']) ? sanitize_text_field($args['document_no']) : NULL,
            'document_type'    => isset($args['document_type']) ? sanitize_text_field($args['document_type']) : NULL,
            'customer_name'    => isset($args['customer_name']) ? sanitize_text_field($args['customer_name']) : NULL,
            'salesperson_code' => isset($args['salesperson_code']) ? sanitize_text_field($args['salesperson_code']) : NULL,
            'item_no'          => isset($args['item_no']) ? sanitize_text_field($args['item_no']) : NULL,
            'quantity'         => isset($args['quantity']) ? sanitize_text_field($args['quantity']) : NULL,
            'price'            => isset($args['price']) ? sanitize_text_field($args['price']): NULL,
            'location_code'    => isset($args['location_code']) ? sanitize_text_field($args['location_code']) : NULL,
            'sales_inc_ppn'    => isset($args['sales_inc_ppn']) ? sanitize_text_field($args['sales_inc_ppn']) : NULL,
            'item_category'    => isset($args['item_category']) ? sanitize_text_field($args['item_category']) : NULL,
            'manufacture_code' => isset($args['manufacture_code']) ? sanitize_text_field($args['manufacture_code']) : NULL,
            'city'             => isset($args['city']) ? sanitize_text_field($args['city']) : NULL,
            'dlm_lr_kota'      => isset($args['dlm_lr_kota']) ? sanitize_text_field($args['dlm_lr_kota']) : NULL,
            'iq'               => isset($args['iq']) ? sanitize_text_field($args['iq']) : NULL,
            'sales_exc_ppn'    => isset($args['sales_exc_ppn']) ? sanitize_text_field($args['sales_exc_ppn']) : NULL,
        );

        return $wpdb->insert( $wpdb->prefix.self::TABLE, $insert );
    }

    /**
     * update row data
     * @param  [type] $args  [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public static function update($args, $where){

        global $wpdb;

        if( empty($args) ) return false;

        $update = array();

        if( isset($args['toko_id']) && $args['toko_id'] ):
            $update['toko_id'] = intval($args['toko_id']);
        endif;

        if( isset($args['first_name']) && $args['first_name'] ):
            $update['first_name'] =  sanitize_text_field($args['first_name']);
        endif;

        if( isset($args['last_name']) && $args['last_name'] ):
            $update['last_name'] =  sanitize_text_field($args['last_name']);
        endif;

        if( isset($args['email']) && $args['email'] ):
            $update['email'] =  sanitize_email($args['email']);
        endif;

        if( isset($args['phone']) && $args['phone'] ):
            $update['phone'] =  sanitize_text_field($args['phone']);
        endif;

        if( isset($args['address']) && $args['address'] ):
            $update['address'] =  sanitize_text_field($args['address']);
        endif;

        if( isset($args['birth_day']) && $args['birth_day'] ):
            $update['birth_day'] =  sanitize_text_field($args['birth_day']);
        endif;

        if( isset($args['hobby']) && $args['hobby'] ):
            $update['hobby'] =  sanitize_text_field($args['hobby']);
        endif;

        if( isset($args['detail']) && $args['detail'] ):
            $update['detail'] =  sanitize_text_field($args['detail']);
        endif;

        return $wpdb->update( $wpdb->prefix.self::TABLE, $update, $where );


    }

    /**
     * delete row data
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public static function delete($where){

        global $wpdb;

        return $wpdb->delete( $wpdb->prefix.self::TABLE, $where );
    }
}

class Telebotstore_Query_Toko_Order_Csi{

    private $args = array();
    private $wpdb = array();

    /**
     * construction
     * @param [type] $args [description]
     */
    public function __construct($args = array()){
        global $wpdb;

        $this->wpdb = $wpdb;

        $default = array(
            'page'   => 1,
            'offset' => 0,
            'limit'  => 20,
            'select' => '*'
        );

        if( isset($args['page']) && $args['page'] > 1 ):
            $limit = isset($args['limit']) && $args['limit'] ? $args['limit'] : $default['limit'];
            $args['offset'] = ($args['page'] - 1 ) * $limit;
        endif;

        $args = wp_parse_args($args, $default);

        $this->args = $args;
    }

    /**
     * run query
     * @return [type] [description]
     */
    public function query(){

        $sql = 'SELECT '.$this->args['select'].' FROM '.$this->wpdb->prefix.'toko_order_csi';

        $where = array();

        if( isset($this->args['ID']) && $this->args['ID'] ):
            $where[] = 'ID = "'.$this->args['ID'].'"';
        endif;

        if( isset($this->args['entry_no']) && $this->args['entry_no'] ):
            $where[] = 'entry_no = "'.$this->args['entry_no'].'"';
        endif;

        if( isset($this->args['toko_id']) && $this->args['toko_id'] ):
            $where[] = 'toko_id = "'.$this->args['toko_id'].'"';
        endif;

        if( isset($this->args['toko_ids']) && $this->args['toko_ids'] ):
            $where[] = 'toko_id IN ('.$this->args['toko_ids'].')';
        endif;

        if( isset($this->args['search']) && $this->args['search'] ):
            $where[] = 'item_no LIKE "%'.$this->args['search'].'%" OR entry_no LIKE "%'.$this->args['search'].'%"';
        endif;

        if( isset($this->args['from_date']) && $this->args['from_date'] ):
            $where[] = 'posting_date BETWEEN "'.$this->args['from_date'].'" AND "'.$this->args['to_date'].'"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $sql .= ' ORDER BY ID DESC LIMIT '.$this->args['limit'].' OFFSET '.$this->args['offset'];

        return $this->wpdb->get_results($sql);

    }

    public function sum_sales_inc_ppn(){

        $sql = 'SELECT ROUND(SUM(sales_inc_ppn), 3) AS total, SUM(sales_inc_ppn) as sum FROM '.$this->wpdb->prefix.'toko_order_csi';

        $where = array();

        if( isset($this->args['ID']) && $this->args['ID'] ):
            $where[] = 'ID = "'.$this->args['ID'].'"';
        endif;

        if( isset($this->args['entry_no']) && $this->args['entry_no'] ):
            $where[] = 'entry_no = "'.$this->args['entry_no'].'"';
        endif;

        if( isset($this->args['toko_id']) && $this->args['toko_id'] ):
            $where[] = 'toko_id = "'.$this->args['toko_id'].'"';
        endif;

        if( isset($this->args['toko_ids']) && $this->args['toko_ids'] ):
            $where[] = 'toko_id IN ('.$this->args['toko_ids'].')';
        endif;

        if( isset($this->args['search']) && $this->args['search'] ):
            $where[] = 'item_no LIKE "%'.$this->args['search'].'%" OR entry_no LIKE "%'.$this->args['search'].'%"';
        endif;

        if( isset($this->args['from_date']) && $this->args['from_date'] ):
            $where[] = 'posting_date BETWEEN "'.$this->args['from_date'].'" AND "'.$this->args['to_date'].'"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $result = $this->wpdb->get_results($sql);

        return isset($result[0]->total) ? $result[0]->total : 0;
    }

    public function total(){

        $sql = 'SELECT COUNT(*) as total FROM '.$this->wpdb->prefix.'toko_order_csi';

        $where = array();

        if( isset($this->args['ID']) && $this->args['ID'] ):
            $where[] = 'ID = "'.$this->args['ID'].'"';
        endif;

        if( isset($this->args['entry_no']) && $this->args['entry_no'] ):
            $where[] = 'entry_no = "'.$this->args['entry_no'].'"';
        endif;

        if( isset($this->args['toko_id']) && $this->args['toko_id'] ):
            $where[] = 'toko_id = "'.$this->args['toko_id'].'"';
        endif;

        if( isset($this->args['toko_ids']) && $this->args['toko_ids'] ):
            $where[] = 'toko_id IN ('.$this->args['toko_ids'].')';
        endif;

        if( isset($this->args['search']) && $this->args['search'] ):
            $where[] = 'item_no LIKE "%'.$this->args['search'].'%" OR entry_no LIKE "%'.$this->args['search'].'%"';
        endif;

        if( isset($this->args['from_date']) && $this->args['from_date'] ):
            $where[] = 'posting_date BETWEEN "'.$this->args['from_date'].'" AND "'.$this->args['to_date'].'"';
        endif;

        if( count($where) >= 1 ){
            $sql .= ' WHERE '.implode(' AND ', $where );
        }

        $result = $this->wpdb->get_results($sql);

        return isset($result[0]) ? $result[0]->total : false;
    }

}
