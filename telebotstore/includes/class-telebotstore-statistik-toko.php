<?php 

class Telebotstore_Statistik_Toko{

	public function __construct(){

		$this->hook();
	}


	public function hook(){
		add_action('wp_ajax_filter_statistik_toko',[$this,'list_data']);
	}


	public function get_data(){
		global $wpdb;

		$args = array(
            'posts_per_page' => $arg['length'],
			'post_status'    => 'publish',
			'offset'         => $arg['start'],
            'orderby'        => 'ID',
            'order'          => 'DESC',
        );

		if( ! empty($arg['user']) ):
			$args['author'] = $arg['user'];
		endif;

		if( ! empty($arg['date']) ):
			$date = explode(' - ', $arg['date']);
			$before = date('d-m-Y',date(strtotime("+1 day", strtotime($date[1]))));
			$args['date_query'] = array(
				'column' => 'post_date',
				'after'  => $date[0],
				'before' => $before,
			);
		endif;

		$meta_query = array();

        if ( ! empty( $arg['status'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_status',
		            'value' => $arg['status'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( ! empty( $arg['store'] ) ) :
			$toko = get_post( $arg['store'] );
            $sub_meta_query[] = array(
		            'key' => '_order_toko',
		            'value' => $arg['store'],
		            'compare' => '='
		        );
			$sub_meta_query[] = array(
		            'key' => '_order_store',
		            'value' => $toko->post_title,
		            'compare' => 'LIKE'
		        );
			$sub_meta_query['relation'] = 'OR';

			$meta_query[] = $sub_meta_query;
        endif;

		if ( ! empty( $arg['tempo'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_tempo',
		            'value' => $arg['tempo'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( ! empty( $arg['via'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_pengiriman',
		            'value' => $arg['via'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( ! empty( $arg['item'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_items',
		            'value' => $arg['item'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( count($meta_query) > 1 ) :
			$meta_query['relation'] = 'AND';
		endif;

		if ( $meta_query ):
			$args['meta_query'] = $meta_query;
		endif;

		$args['post_type'] = 'telebotstore_order';

		$q = new WP_Query();
		$posts = $q->query( $args );

		echo '<pre>';
		print_r($posts);
		echo '</pre>';
	}


	public function list_data(){
	
		$query = $this->get_data();		

		include TELE_TEMPLATES.'/statistik-toko/list.php';
        
		die();
	}

}

$Telebotstore_Statistik_Toko = new Telebotstore_Statistik_Toko;
?>