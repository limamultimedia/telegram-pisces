<?php

/**
 * checkin classes
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2.1
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
class Telebotstore_Checkin_Log {

    const TABLE = 'checkin';
    const TABLE_LOG = 'checkin_log';


    public function __construct(){
        $this->hook();
        $this->shortcode();
    }


    public function hook(){
        add_action('show_user_profile', [$this,'user_profile_target_sales']);
        add_action('edit_user_profile', [$this,'user_profile_target_sales']);

        add_action('personal_options_update', [$this,'user_profile_update_action']);
        add_action('edit_user_profile_update', [$this,'user_profile_update_action']);

        add_action('wp_ajax_get_checkin_dashboard',[$this,'get_checkin_dashboard']);

        add_action('wp_ajax_re_calculate_distance',[$this,'ajax_re_calculate']);
        
    }


    public function shortcode(){
        add_shortcode('checkin_dashboard',[$this,'checkin_dashboard']);
    }

    public function user_profile_target_sales($user) {

        $user_id = $user->ID;

        $checked = get_user_meta($user_id,'enable_target_sales',true) ? ' checked="checked"' : '';
        $visit_target = get_user_meta($user_id,'visit_target',true);
        $distance_target = get_user_meta($user_id,'distance_target',true);
        ?>
          <h2>Target Sales</h2>
          <table class="form-table" role="presentation">
            <tbody>
                <tr>
                    <th>Aktifkan Target</th>
                    <td><input name="enable_target_sales" type="checkbox" id="enable_target_sales" value="1"<?php echo $checked; ?>> Ya</td>
                </tr>
                <tr>
                    <th>Target Kunjungan</th>
                    <td><input name="visit_target" type="number" id="visit_target" value="<?php echo $visit_target; ?>" > Toko</td>
                </tr>
                <tr>
                    <th>Target Jarak</th>
                    <td><input name="distance_target" type="number" id="distance_target" value="<?php echo $distance_target; ?>" > Km</td>
                </tr>
            </tbody>
          </table>
          <input type="hidden" name="_target_sales_nonce" value="<?= wp_create_nonce('target_sales'); ?>">
        <?php 
    }


    public function user_profile_update_action($user_id) {
        if(isset($_POST['_target_sales_nonce'])){        

            update_user_meta($user_id, 'enable_target_sales', isset($_POST['enable_target_sales']));
            update_user_meta($user_id, 'visit_target', $_POST['visit_target']);
            update_user_meta($user_id, 'distance_target', $_POST['distance_target']);
        }
    }


    public function insert_log($user_id,$date,$id,$coordinate){
        global $wpdb;

        $query_checkin = $this->get(" AND id='".$id."' ");

        $query = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."checkin_log WHERE visit_date='".$date."' AND user_id='".$user_id."' LIMIT 1");

        $store = $query_checkin[0]->store;
        $toko = get_post($store);
        $store_name = $toko->post_title;

        $enable_target_sales = get_user_meta($user_id,'enable_target_sales',true);

        $visit_target = $enable_target_sales ? get_user_meta($user_id,'visit_target',true) : 0;
        $distance_target = $enable_target_sales ? get_user_meta($user_id,'distance_target',true) : 0;

        if(!$query){

            $detail = [];
            $start = '';
            $end = $coordinate;
            $detail[$id] = array(
                            'start' => $start,
                            'end' => $end,
                            'checkin_time' => $query_checkin[0]->visit_datetime,
                            'store' => $store_name, 
                            'distance' => 0,
                            'duration' => 0
                        );

            $data = array(
                    'user_id' => $user_id,
                    'visit_date' => $date,
                    'distance' => 0,
                    'detail' => serialize($detail),
                    'visit_target' => $visit_target,
                    'distance_target' => $distance_target
                );
            $wpdb->insert($wpdb->prefix."checkin_log",$data);

        }else{

            $log_id = $query->id;
            $details = $query->detail;
            
            $details = unserialize($details);

            $new_details = [];
            $total_distance = 0;
            foreach($details as $key => $detail){
                if($key == $id) return;

                $new_details[$key] = $detail;
                $start = $detail['end'];
                $total_distance += floatval($detail['distance']);
            }

            $data_distance = $this->get_distance($start,$coordinate);
            $new_distance = $data_distance['distance'];
            $duration = $data_distance['duration'];

            $new_details[$id] = array(
                                    'start' => $start,
                                    'end' => $coordinate,
                                    'checkin_time' => $query_checkin[0]->visit_datetime,
                                    'store' => $store_name, 
                                    'distance' => $new_distance,
                                    'duration' => $duration
                                );

            $total_distance += floatval($new_distance);

            $update = [
                        'distance' => $total_distance, 
                        'detail' => serialize($new_details),
                        'visit_target' => $visit_target,
                        'distance_target' => $distance_target
                    ];
            $where = ['id' => $log_id];
            $wpdb->update($wpdb->prefix."checkin_log",$update,$where);

        }

        $sales_target = $this->get_sales_target($user_id);
        if(!$sales_target) return;

        $sales_count = $this->get_count($date,$date,$user_id);

        $total_visit = $sales_count[0]->visit_count;

        if($total_visit >= $sales_target['visit'] || $total_distance >= $sales_target['distance']){
            $this->insert_reward($user_id,$date);
        }
    }


    public function get_sales_target($user_id){

        $enable_target_sales = get_user_meta($user_id,'enable_target_sales',true);
        if(!$enable_target_sales) return false;

        $visit_target = get_user_meta($user_id,'visit_target',true);
        $distance_target = get_user_meta($user_id,'distance_target',true);

        $data = ['visit' => $visit_target,'distance' => $distance_target];
        return $data;
    }


    /**
     * doing insert reward if target pass
     * @param  int $user_id
     * @param  date $date    Y-m-d
     * @return 
     */
    public function insert_reward($user_id,$date){
        global $wpdb;

        $query = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."sales_reward WHERE user_id='".$user_id."' AND visit_date='".$date."' LIMIT 1 ");
        if($query) return;

        $insert = array(
                    'user_id' => $user_id,
                    'visit_date' => $date,
                    'reward' => 1,
                    'datecreated' => current_time('mysql')
                );
        $wpdb->insert($wpdb->prefix.'sales_reward',$insert);

    }


    public function get_reward_count($user_id=0){
        if(!$user_id){
            $user_id = get_current_user_id();
        }
       
        global $wpdb;

        $query = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."sales_reward WHERE user_id='".$user_id."' ");

        return $query;
    }



    public function get_count($start,$end,$user_id=0){
        global $wpdb;

        $sql = 'SELECT user_id,DATE(visit_datetime) as visit_datetime,COUNT(*) AS visit_count FROM '.$wpdb->prefix.'checkin';

        $sql .= " WHERE DATE(visit_datetime) BETWEEN '".$start."' AND '".$end."' ";
        $sql .= $user_id ? " AND user_id='".$user_id."' " : "";
        $sql .= ' GROUP BY user_id, DATE(visit_datetime) ORDER BY visit_datetime DESC';

        return $wpdb->get_results($sql);

    }

    public function get($andWhere=''){
        global $wpdb;

        $query = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix.self::TABLE." WHERE 1 ".$andWhere." LIMIT 100");
        return $query;
    }


    public function get_checkin_log($user_id,$date){
        global $wpdb;

        $query = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."checkin_log WHERE user_id='".$user_id."' AND visit_date='".$date."' LIMIT 1 " );

        return $query;
    }

    public function get_coordinate_location($location){

        if(!$location) return $location;

        $location = str_replace("\\", "",$location);
        $location = json_decode($location);

        $geometry = $location[0]->geometry;
        $lat = $geometry->location->lat;
        $lng = $geometry->location->lng;

        return $lat.','.$lng;
    }


    public function ajax_re_calculate(){

        global $wpdb;

        $lat_lng_origin = $_POST['origin'];
        $lat_lng_destination  = $_POST['destination'];
        $checkin_id = $_POST['checkin_id'];
        $checkin_log_id = $_POST['checkin_log_id'];

        $data = $this->get_distance($lat_lng_origin,$lat_lng_destination);

        $query = $this->get(" AND id='".$checkin_log_id."' LIMIT 1 ");
        $row = $query[0];
        $detail = $row->detail;
        
        $new_details = [];
        foreach(unserialize($detail) as $id => $data_detail){

            if($id == $checkin_id){
                $data_detail['distance'] = $data['distance'];
                $data_detail['duration'] = $data['duration'];
            }
            $new_details[$id] = $data_detail;
        }

        $update = ['detail' => $new_details];
        $where = ['id' => $checkin_log_id];
        $wpdb->update($wpdb->prefix.self::TABLE,$update,$where);

        echo json_encode($data);
        die();

    }


    /**
     * get distance from google
     * @param  string $lat_lng_origin      lat,long
     * @param  string $lat_lng_destination lat,long
     * @return int distance (m)
     */
    public function get_distance($lat_lng_origin,$lat_lng_destination){

        $api_key = 'AIzaSyCkDCCS0EdhwHRUe7YpfoKI98zt7JP9CzU';
        $api_url = 'https://maps.googleapis.com/maps/api/distancematrix/json';

        $url = $api_url.'?key='.$api_key.'&origins='.$lat_lng_origin.'&destinations='.$lat_lng_destination;

        $data = wp_remote_get($url);

        if($data && $data['response']['code'] == 200){
            $distance = json_decode($data['body'])->rows[0]->elements[0]->distance->value;
            $duration = json_decode($data['body'])->rows[0]->elements[0]->duration->value;

            $return = array('distance' => $distance, 'duration' => $duration);
            return $return;
        }

        return 0;
    }


    public function get_checkin_dashboard(){
        echo $this->checkin_dashboard();
        die();
    }



    public function checkin_dashboard(){
        ob_start();

        $start = date('Y-m-d');
        $end = $start;
        $user_id = get_current_user_id();
        $data_visit = $this->get_count($start,$end,$user_id);
        if($data_visit){
            $total_visit = $data_visit[0]->visit_count;
        }else{
            $total_visit = 0;
        }

        $data_distance = $this->get_checkin_log($user_id,$start);
        if($data_distance){
            $total_distance = number_format($data_distance->distance/1000,1);
        }else{
            $total_distance = 0;
        }

        $enable_target_sales = get_user_meta($user_id,'enable_target_sales',true);
        if($enable_target_sales){
            $visit_target = get_user_meta($user_id,'visit_target',true);
            $distance_target = get_user_meta($user_id,'distance_target',true);
        }else{
            $visit_target = 0;
            $distance_target = 0;
        }

        $percent_visit = ceil(($total_visit/$visit_target) * 100);
        $percent_distance = ceil(($total_distance/$distance_target) * 100);

        if($total_visit >= $visit_target || $total_distance >= $distance_target ){
            $target = 'PASS';
            $target_css = 'success';
        }else{
            $target = 'NOT PASS';
            $target_css = 'danger';
        }


        ?>
        <div class="clearfix"></div>

        <div class="checkin-dashboard">

            <div class="col-md-10">
                <div class="col-md-2">
                    <label>Visit</label>
                </div>
                <div class="col-md-10">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="<?= $percent_visit; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent_visit; ?>%">
                        <?= $percent_visit; ?>%
                      </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <label>Jarak</label>
                </div>
                <div class="col-md-10">
                    <div class="progress">
                      <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="<?= $percent_distance; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent_distance; ?>%">
                        <?= $percent_distance; ?>%
                      </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2 reward">
                <div class="panel panel-yellow text-center">
                    <div class="panel panel-heading"><i class="fa fa-solid fa-award"></i> 
                    Reward</div>
                    <div class="panel-body">
                           <div class="star"><i class="fa fa-star fa-2x"></i></div>
                           <div class="count"><?= number_format($this->get_reward_count(),0); ?></div>
                    </div>
                    
                </div>
            </div> 
            <hr>           
            <div class="clearfix"></div>
        </div>
        <?php

        return ob_get_clean();
    }

}

$Telebotstore_Checkin_Log = new Telebotstore_Checkin_Log;

