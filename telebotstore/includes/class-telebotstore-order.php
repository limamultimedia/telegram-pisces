<?php

/**
 * The file that defines the order class
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/includes
 */
Class Telebotstore_Order
{
	const post_type = 'telebotstore_order';
	private static $data = array();
	private static $item = array();
	private static $count = '';

	/**
	 * add new order for telegram bot
	 * @param [type] $data [description]
	 */
	public static function add_order( $data )
	{
		if( $data ):

			$order = array(
	            'post_author'           => $data['user_id'],
	            'post_title'            => $data['store'],
	            'post_status'           => 'draft',
	            'post_type'             => self::post_type,
	            'post_content'          => '',
	        );

	        $order_id = wp_insert_post( $order );
	        add_post_meta( $order_id, '_order_status', 'open' );
			add_post_meta( $order_id, '_order_store', $data['store'] );
			add_post_meta( $order_id, '_order_items', array() );

	        return $order_id;
		endif;

		return false;
	}

	/**
	 * add new order for telegram bot
	 * @param [type] $data [description]
	 */
	public static function checkout( $data )
	{
		if( $data ):

			$order = array(
	            'post_author'           => $data['user_id'],
	            'post_title'            => $data['store'],
	            'post_status'           => 'publish',
	            'post_type'             => self::post_type,
	            'post_content'          => '',
	        );

	        $order_id = wp_insert_post( $order );
	        update_post_meta( $order_id, '_order_status', 'pending' );
			update_post_meta( $order_id, '_order_store', $data['store'] );
			update_post_meta( $order_id, '_order_toko', false );
			update_post_meta( $order_id, '_order_tempo', $data['tempo'] );
			update_post_meta( $order_id, '_order_pengiriman', $data['via'] );
			foreach( $data['items'] as $i ):
				$itm = $i['item'].'|'.$i['qty'];
				add_post_meta( $order_id, '_order_items', $itm );
			endforeach;

			$items = get_post_meta( $order_id, '_order_items', false);
			$data_items = array();

			foreach( $items as $key=>$val ):
				$value = explode('|', $val);
				$item = Telebotstore_Item::get_by_slug( $value[0] );
				$data_items[] = array(
					'item' => $item->post_title,
					'qty' => $value[1],
				);
			endforeach;

			$user = get_userdata( $data['user_id'] );

			$title      = 'Order Baru dari '.$user->user_login;
		    $content    = 'Toko '.$data['store'].'>> KLIK DISINI untuk melihat.';
			$url        = get_the_permalink($order_id);

			Telebotstore_OneSignal::send($title,$content,$url);

			$response = array(
				'items' => $data_items,
				'permalink' => get_the_permalink( $order_id ),
			);

			return $response;
		endif;

		return false;
	}


	/**
	 * add item for last opened order
	 * @param [type] $data [description]
	 */
	public static function add_item( $data )
	{
		$order_id = self::get_opened_order_id( $data['user_id'] );
		$items = get_post_meta($order_id, '_order_items', true);
		$items[] = array(
			'item' => $data['item'],
			'qty' => '',
		);

		carbon_set_post_meta( $order_id, 'order_items', $items );

	}

	/**
	 * add quantity for last opened order
	 * @param [type] $data [description]
	 */
	public static function add_quantity( $data )
	{
		$order_id = self::get_opened_order_id( $data['user_id'] );
		$items = get_post_meta($order_id, '_order_items', false);
		$keys = array_keys($items);
		$new_items = array();
		foreach( $items as $key=>$val ):
			if( $key == end($keys)):
				$new_items[] = array(
					'item' => $val['item'],
					'qty' => $data['qty'],
				);
			else :
				$new_items[] = array(
					'item' => $val['item'],
					'qty' => $val['qty'],
				);
			endif;
		endforeach;

		carbon_set_post_meta( $order_id, 'order_items', $new_items );

	}


	/**
	 * get openned order
	 * @param  [type] $author [description]
	 * @return [type]         [description]
	 */
	public static function get_opened_order_id($user_id)
	{
		$args = array(
			'post_type' => 'telebotstore_order',
			'author' => $user_id,
			'posts_per_page' => 1,
			'post_status' => 'draft',
			'orderby' => 'ID',
            'order' => 'DESC',
			'meta_query' => array(
				array(
					'key' => '_order_status',
					'value' => 'open',
					'compare' => '='
				),
			),
		);

		$q = new WP_Query();
		$orders = $q->query( $args );

		return $orders[0]->ID;
	}

	/**
	 * complete order to pending status
	 * @param  [type] $user_id [description]
	 * @return [type]          [description]
	 */
	public static function complete($user_id)
	{
		$order_id = self::get_opened_order_id( $data['user_id'] );

		carbon_set_post_meta( $order_id, 'order_status', 'pending' );

		$order = array(
            'ID'                    => $order_id,
            'post_author'           => $user_id,
            'post_status'           => 'publish',
        );

        $order_id = wp_update_post( $order );
		$items = carbon_get_post_meta( $order_id, 'order_items');
		$data_items = array();

		foreach( $items as $key=>$val ):
			$item = Telebotstore_Item::get_by_slug( $val['item'] );
			$data_items[] = array(
				'item' => $item->post_title,
				'qty' => $val['qty'],
			);
		endforeach;


		$response = array(
			'items' => $data_items,
			'permalink' => get_the_permalink( $order_id ),
		);

		return $response;
	}


	/**
	 * get all order list
	 * @param  [type] $arg [description]
	 * @return [type]      [description]
	 */
	public static function get_order_list( $arg )
	{
		$args = array(
            'posts_per_page' => $arg['length'],
			'post_status'    => 'publish',
			'offset'         => $arg['start'],
            'orderby'        => 'ID',
            'order'          => 'DESC',
        );

		if( ! empty($arg['user']) ):
			$args['author'] = $arg['user'];
		endif;

		if( ! empty($arg['date']) ):
			$date = explode(' - ', $arg['date']);
			$before = date('d-m-Y',date(strtotime("+1 day", strtotime($date[1]))));
			$args['date_query'] = array(
				'column' => 'post_date',
				'after'  => $date[0],
				'before' => $before,
			);
		endif;

		$meta_query = array();

        if ( ! empty( $arg['status'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_status',
		            'value' => $arg['status'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( ! empty( $arg['store'] ) ) :
			$toko = get_post( $arg['store'] );
            $sub_meta_query[] = array(
		            'key' => '_order_toko',
		            'value' => $arg['store'],
		            'compare' => '='
		        );
			$sub_meta_query[] = array(
		            'key' => '_order_store',
		            'value' => $toko->post_title,
		            'compare' => 'LIKE'
		        );
			$sub_meta_query['relation'] = 'OR';

			$meta_query[] = $sub_meta_query;
        endif;

		if ( ! empty( $arg['tempo'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_tempo',
		            'value' => $arg['tempo'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( ! empty( $arg['via'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_pengiriman',
		            'value' => $arg['via'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( ! empty( $arg['item'] ) ) :
            $meta_query[] = array(
		            'key' => '_order_items',
		            'value' => $arg['item'],
		            'compare' => 'LIKE'
		        );
        endif;

		if ( count($meta_query) > 1 ) :
			$meta_query['relation'] = 'AND';
		endif;

		if ( $meta_query ):
			$args['meta_query'] = $meta_query;
		endif;

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		$objs = array();

		foreach ( (array) $posts as $post ) :
			$user_info = get_userdata($post->post_author);
			$toko_id = get_post_meta( $post->ID, '_order_toko', true);
			if( empty($toko_id) ):
				$store = array(
					'name' => get_post_meta( $post->ID, '_order_store', true),
					'url' => '',
				);
			else:
				$provider = get_post_meta($toko_id, '_provider', true);
				if($provider == 'api'):
					$toko_name = carbon_get_post_meta($toko_id, 'name');
				else :
					$toko_name = get_the_title($toko_id);
				endif;
				$store = array(
					'name' => $toko_name,
					'url' => get_the_permalink($toko_id),
				);
			endif;
			$objs[] = array(
				$post->ID,
				[
					'first_name' => $user_info->first_name,
					'last_name' => $user_info->last_name,
					'username' => $user_info->user_login
				],
				get_the_date('d-M-Y', $post->ID),
				$store,
				get_post_meta( $post->ID, '_order_tempo', true),
				get_post_meta( $post->ID, '_order_pengiriman', true),
				ucfirst(get_post_meta( $post->ID, '_order_status', true)),
				get_the_permalink($post->ID),
            );
		endforeach;

		return $objs;
	}

	public static function get_total_order_today( $arg = array() )
	{
		$args = array(
            'posts_per_page' => -1,
			'post_status'    => 'publish',
            'orderby'        => 'ID',
            'order'          => 'DESC',
			'author'         => intval($arg['user_id']),
			'fields' => 'ids',
		);

		$date = date('Y-m-d');

		$date = explode('-', $date);

		$args['date_query'] = array(
			array(
				'year' => $date[0],
				'month' => $date[1],
				'day' => $date[2]
			)
		);

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		// $objs = array();
		//
		// foreach ( (array) $posts as $postid ) :
		//
		// 	$items = get_post_meta( $postid, '_order_items', false);
		// 	$data_items = array();
		//
		// 	$subtotal = array();
		//
		// 	foreach( $items as $key=>$val ):
		// 		$value = explode('|', $val);
		// 		$item = Telebotstore_Item::get_by_slug( $value[0] );
		// 		$subtotal[] = (int)carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1];
		// 		$data_items[] = array(
		// 			'item' => $item->post_title,
		// 			'qty'  => $value[1],
		// 			'price' => number_format(carbon_get_post_meta($item->ID, 'item_price'),0,',','.'),
		// 			'sub' => number_format((int)carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1],0,',','.'),
		// 		);
		// 	endforeach;
		//
		// 	$total = array_sum($subtotal);
		//
		// 	$objs[] = $total;
		//
		// endforeach;
		//
		// $rp = $objs ? number_format(array_sum($objs),0,',','.') : 0;

		return count($posts).' Invoice';
	}

	public static function get_total_order_yesterday( $arg = array() )
	{
		$args = array(
            'posts_per_page' => -1,
			'post_status'    => 'publish',
            'orderby'        => 'ID',
            'order'          => 'DESC',
			'author'         => intval($arg['user_id']),
			'fields' => 'ids',
		);

		$date = date('Y-m-d',strtotime('-1 day'));

		$date = explode('-', $date);

		$args['date_query'] = array(
			array(
				'year' => $date[0],
				'month' => $date[1],
				'day' => $date[2]
			)
		);

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		// $objs = array();
		//
		// foreach ( (array) $posts as $postid ) :
		//
		// 	$items = get_post_meta( $postid, '_order_items', false);
		// 	$data_items = array();
		//
		// 	$subtotal = array();
		//
		// 	foreach( $items as $key=>$val ):
		// 		$value = explode('|', $val);
		// 		$item = Telebotstore_Item::get_by_slug( $value[0] );
		// 		$subtotal[] = (int)carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1];
		// 		$data_items[] = array(
		// 			'item' => $item->post_title,
		// 			'qty'  => $value[1],
		// 			'price' => number_format(carbon_get_post_meta($item->ID, 'item_price'),0,',','.'),
		// 			'sub' => number_format((int)carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1],0,',','.'),
		// 		);
		// 	endforeach;
		//
		// 	$total = array_sum($subtotal);
		//
		// 	$objs[] = $total;
		//
		// endforeach;
		//
		// $rp = $objs ? number_format(array_sum($objs),0,',','.') : 0;
		//
		// return 'Rp '.$rp;

		return count($posts).' Invoice';
	}

	public static function get_total_order_before_yesterday( $arg = array() )
	{
		$args = array(
            'posts_per_page' => -1,
			'post_status'    => 'publish',
            'orderby'        => 'ID',
            'order'          => 'DESC',
			'author'         => intval($arg['user_id']),
			'fields' => 'ids',
		);

		$date = date('Y-m-d',strtotime('-2 day'));

		$date = explode('-', $date);

		$args['date_query'] = array(
			array(
				'year' => $date[0],
				'month' => $date[1],
				'day' => $date[2]
			)
		);

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		// $objs = array();
		//
		// foreach ( (array) $posts as $postid ) :
		//
		// 	$items = get_post_meta( $postid, '_order_items', false);
		// 	$data_items = array();
		//
		// 	$subtotal = array();
		//
		// 	foreach( $items as $key=>$val ):
		// 		$value = explode('|', $val);
		// 		$item = Telebotstore_Item::get_by_slug( $value[0] );
		// 		$subtotal[] = (int)carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1];
		// 		$data_items[] = array(
		// 			'item' => $item->post_title,
		// 			'qty'  => $value[1],
		// 			'price' => number_format(carbon_get_post_meta($item->ID, 'item_price'),0,',','.'),
		// 			'sub' => number_format((int)carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1],0,',','.'),
		// 		);
		// 	endforeach;
		//
		// 	$total = array_sum($subtotal);
		//
		// 	$objs[] = $total;
		//
		// endforeach;
		//
		// $rp = $objs ? number_format(array_sum($objs),0,',','.') : 0;
		//
		// return 'Rp '.$rp;

		return count($posts).' Invoice';
	}

	public static function get_total_order_by_user( $user_id = 0, $var = array() )
	{
		$args = array(
            'posts_per_page' => -1,
			'post_status'    => 'publish',
            'orderby'        => 'ID',
            'order'          => 'DESC',
			'author'         => intval($user_id),
			'fields' => 'ids',
		);

		if( isset($var['date_start']) && $var['date_start'] && isset($var['date_end']) && $var['date_end']){

			$args['date_query'] = array(
				'after' => date('F s, Y',strtotime($var['date_start'])),
				'before' => date('F s, Y',strtotime($var['date_end'])),
				'inclusive' => true,
			);
		}

		$meta_query = array();

		if ( ! empty( $var['store'] ) ) :
			$toko = get_post( $var['store'] );
            $sub_meta_query[] = array(
		            'key' => '_order_toko',
		            'value' => $var['store'],
		            'compare' => '='
		        );
			$sub_meta_query[] = array(
		            'key' => '_order_store',
		            'value' => $toko->post_title,
		            'compare' => 'LIKE'
		        );
			$sub_meta_query['relation'] = 'OR';

			$meta_query[] = $sub_meta_query;
        endif;

		if ( count($meta_query) > 1 ) :
			$meta_query['relation'] = 'AND';
		endif;

		if ( $meta_query ):
			$args['meta_query'] = $meta_query;
		endif;

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		return count($posts).' Invoice';
	}



	/**
	 * get all order list
	 * @param  [type] $arg [description]
	 * @return [type]      [description]
	 */
	public static function get_log_list( $arg )
	{
		$args = array(
            'posts_per_page' => $arg['length'],
			'post_status'    => 'publish',
			'offset'         => $arg['start'],
            'orderby'        => 'post_modified',
            'order'          => 'DESC',
        );

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		$objs = array();

		foreach ( (array) $posts as $post ) :

			$updated_id = get_post_meta( $post->ID, '_order_status_id_update', true);
			$updated_date =  get_post_meta( $post->ID, '_order_status_date_update', true);
			$logs = new Telebotstore_Order_Status_Log(['order_id' => $post->ID]);
			$log = $logs->get_last_by_post_id();

			$user = get_userdata($log->user_id);
			$objs[] = array(
				$post->ID,
				[
					'first_name' => get_the_author_meta( 'first_name', $post->post_author ),
					'last_name' => get_the_author_meta( 'last_name', $post->post_author ),
					'date' => $post->post_date,

				],
				[
					'status' => ucfirst(get_post_meta($post->ID, '_order_status', true)),
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'date' => $log->datetime,
				],
				get_the_permalink($post->ID),
            );
		endforeach;

		return $objs;
	}

	/**
	 * get all order list
	 * @param  [type] $arg [description]
	 * @return [type]      [description]
	 */
	public static function get_pending_list( $arg )
	{
		$args = array(
            'posts_per_page' => $arg['length'],
			'post_status'    => 'publish',
			'offset'         => $arg['start'],
            'orderby'        => 'ID',
            'order'          => 'DESC',
        );

		$args['meta_query'] = array(
			array(
				'key' => '_order_status',
				'value' => 'pending',
				'compare' => 'LIKE'
			),
		);

		$args['date_query'] = array(
			'column' => 'post_modified',
			'after'  => date('Y-m-d H:i:s', strtotime("-3 year")),
			'before' => date('Y-m-d H:i:s', strtotime("-3 day")),
		);

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		$objs = array();

		foreach ( (array) $posts as $post ) :

			$toko_id = get_post_meta( $post->ID, '_order_toko', true);
			if( empty($toko_id) ):
				$store = get_post_meta( $post->ID, '_order_store', true);
			else:
				$provider = get_post_meta($toko_id, '_provider', true);
				if($provider == 'api'):
					$store = sprintf( '<a href="%s">%s</a>', get_the_permalink($toko_id), carbon_get_post_meta($toko_id, 'name') );
				else :
					$store = sprintf( '<a href="%s">%s</a>', get_the_permalink($toko_id), get_the_title($toko_id) );
				endif;
			endif;

			$updated_id = get_post_meta( $post->ID, '_order_status_id_update', true);
			$updated_date =  get_post_meta( $post->ID, '_order_status_date_update', true);
			$objs[] = array(
				$post->ID,
				[
					'first_name' => get_the_author_meta( 'first_name', $post->post_author ),
					'last_name' => get_the_author_meta( 'last_name', $post->post_author ),
					'date' => $post->post_date,

				],
				$store,
				get_the_permalink($post->ID),
            );
		endforeach;

		return $objs;
	}

	/**
	 * get all order list
	 * @param  [type] $arg [description]
	 * @return [type]      [description]
	 */
	public static function get_reject_list( $arg )
	{
		$args = array(
            'posts_per_page' => $arg['length'],
			'post_status'    => 'publish',
			'offset'         => $arg['start'],
            'orderby'        => 'ID',
            'order'          => 'DESC',
        );

		$args['meta_query'] = array(
			array(
				'key' => '_order_status',
				'value' => 'reject',
				'compare' => 'LIKE'
			),
		);

		// $args['date_query'] = array(
		// 	'column' => 'post_modified',
		// 	'after'  => date('Y-m-d H:i:s', strtotime("-3 year")),
		// 	'before' => date('Y-m-d H:i:s', strtotime("-3 day")),
		// );

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$count = $q->found_posts;

		$objs = array();

		$current_roles = wp_get_current_user()->roles;
		$roles = array('admin_toko', 'administrator', 'editor' );

		if( !array_diff($current_roles, $roles) ) :
			$is_admin = 'yes';
		else:
			$is_admin = 'no';
		endif;

		foreach ( (array) $posts as $post ) :
			$user = get_userdata($post->post_author);
			$updated_id = get_post_meta( $post->ID, '_order_status_id_update', true);
			$updated_date =  get_post_meta( $post->ID, '_order_status_date_update', true);
			$user_info = get_userdata($post->post_author);
			$toko_id = get_post_meta( $post->ID, '_order_toko', true);
			if( empty($toko_id) ):
				$store = array(
					'name' => get_post_meta( $post->ID, '_order_store', true),
					'url' => '',
				);
			else:
				$provider = get_post_meta($toko_id, '_provider', true);
				if($provider == 'api'):
					$toko_name = carbon_get_post_meta($toko_id, 'name');
				else :
					$toko_name = get_the_title($toko_id);
				endif;
				$store = array(
					'name' => $toko_name,
					'url' => get_the_permalink($toko_id),
				);
			endif;
			$reject_reason = get_post_meta( $post->ID, '_order_reject_reason', true);
			if($reject_reason):
				$reason = $reject_reason;
			else :
				$reason = '-';
			endif;
			$objs[] = array(
				[
					'first_name' => $user->first_name,
					'last_name' => $user->last_name,
					'username' => $user->user_login,
					'date' => $post->post_date,
				],
				$store,
				get_post_meta( $post->ID, '_order_tempo', true),
				get_post_meta( $post->ID, '_order_pengiriman', true),
				$reason,
				[
					'id' => $post->ID,
					'is_admin' => $is_admin,
					'link' => get_the_permalink($post->ID),
				],
            );
		endforeach;

		return $objs;
	}


	/**
     * Delete Order
     * @param [type] $data [description]
     */
    public static function delete( $user_id )
    {
		$order_id = self::get_opened_order_id( $user_id );

        return wp_delete_post( $order_id, true );

    }

	public function updated($id)
	{
		$post = array(
			'ID' => (int)$id,
			'post_modified' => date( 'Y:m:d H:i:s' ),
		);
		wp_update_post( $post );
	}

	/**
	 * get count of get order
	 * @return [type] [description]
	 */
	public static function count_order_list()
	{
		return self::$count;
	}

}
