<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.ridwanarifandi.com
 * @since             1.0.0
 * @package           Telebotstore
 *
 * @wordpress-plugin
 * Plugin Name:       Telegram Bot Store
 * Plugin URI:        https://www.orangewordpress.com/
 * Description:       Telegram Bot for online store - Update Checkin Log, Sales Area, Jadwal Kunjungan, Order Pending Reminder
 * Version:           2.5.3.1
 * Author:            Ridwan Arifandy
 * i
 * Author URI:        https://www.ridwanarifandi.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       telebotstore
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'TELEBOT', '2.5.3.1.2' );

define('TELE_TEMPLATES',plugin_dir_path( __FILE__ ).'public/templates');

//just for debuging test
require_once('lima-test.php');
require_once( __DIR__ . '/includes/functions.php');


if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
	require( __DIR__ . '/vendor/autoload.php' );
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-telebotstore-activator.php
 */
function activate_telebotstore() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-telebotstore-activator.php';
	Telebotstore_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-telebotstore-deactivator.php
 */
function deactivate_telebotstore() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-telebotstore-deactivator.php';
	Telebotstore_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_telebotstore' );
register_deactivation_hook( __FILE__, 'deactivate_telebotstore' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-telebotstore.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_telebotstore() {

	$plugin = new Telebotstore();
	$plugin->run();

}
run_telebotstore();
show_admin_bar( false );

function telebotstore_debug()
{
	?><pre><?php
	print_r(func_get_args());
	?></pre><?php
}

function is_developer(){
	if(get_current_user_id() == 55) return true;

	return false;
}

require 'plugin-update/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/fiqhid24/telebotstore',
	__FILE__,
	'telebotstore'
);
$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'EvTjq6L4dhXuXXSPhP',
	'consumer_secret' => '83U9BgkjnsKFcKjmAXCTPMQXasetADRB',
));
//$myUpdateChecker->setBranch('stable-branch-name');
