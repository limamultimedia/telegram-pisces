<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class TambahCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'tambah';
    /**
     * @var string
     */
    protected $description = 'Tambahkan item command';
    /**
     * @var string
     */
    protected $usage = '/tambah';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $items = \Telebotstore_Item::get_item_name_list();
        $keyboard_item = array_chunk( $items, 2);
        $keyboards = new Keyboard( ... $keyboard_item);

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        if( empty($user) ):
            return;
        endif;

        $keyboard = $keyboards
            ->setResizeKeyboard(true)
            ->setOneTimeKeyboard(true)
            ->setSelective(false);

        $text    = 'Pilih item dengan menekan tombol item';
        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
            'reply_markup' => $keyboard,
        ];
        return Request::sendMessage($data);
    }
}
