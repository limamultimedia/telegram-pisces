<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class QtyCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'qty';
    /**
     * @var string
     */
    protected $description = 'Quantity command';
    /**
     * @var string
     */
    protected $usage = '/qty <text>';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $keyboards = array();
        $keyboards[] = new Keyboard(
            ['/keranjang', '/tambah'],
            ['/batalkan', '/selesai']
        );

        $quantities = explode(PHP_EOL, carbon_get_theme_option('order_item_qty'));

        $obj = array();
        if( $quantities ):
            foreach($quantities as $key=>$val ):
                $obj[] = array(
                    'text' => '/qty ' . $val,
                );
            endforeach;
        endif;

        //Keyboard examples
        /** @var Keyboard[] $keyboards */
        $keyboard_item = array_chunk( $obj, 3);
        $keyboard_qty = new Keyboard( ... $keyboard_item);

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $qty    = trim($message->getText(true));

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        $session = get_transient( $username );

        if( empty($user) ):
            return;
        endif;

        if( empty($session) ):
            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Anda tidak memulai dengan benar, silahkan awali dengan command /baru',
            ];

            return Request::sendMessage($data);
        endif;

        if($qty):

            $data_session['store'] = $session['store'];
            $data_session['tempo'] = $session['tempo'];
            $data_session['via'] = $session['via'];

            $items = $session['items'];

            $keys = array_keys($items);
    		$new_items = array();
    		foreach( $items as $key=>$val ):
    			if( $key == end($keys)):
    				$new_items[] = array(
    					'item' => $val['item'],
    					'qty' => $qty,
    				);
    			else :
    				$new_items[] = array(
    					'item' => $val['item'],
    					'qty' => $val['qty'],
    				);
    			endif;
    		endforeach;

            $data_session['items'] = $new_items;

            set_transient( $username, $data_session, 12 * HOUR_IN_SECONDS );

            $session = get_transient( $username );

            $items = $session['items'];

            /**
             * get last item
             * @var [type]
             */
            $last_item = $items[end($keys)]['item'];

            // $data_qty = array(
            //     'user_id' => $user_id,
            //     'qty' => $qty,
            // );

            // \Telebotstore_Order::add_quantity($data_qty);
            //
            $keyboard = $keyboards[0]
                ->setResizeKeyboard(true)
                ->setOneTimeKeyboard(true)
                ->setSelective(false);

            $text = 'Item '.$last_item.' dengan Quantity '.$qty.', telah di tambahkan'.PHP_EOL;

            $data = [
                'chat_id' => $chat_id,
                'text'    => $text,
                'reply_markup' => $keyboard,
            ];

        else :

            $key = $keyboards_qty
                ->setResizeKeyboard(true)
                ->setOneTimeKeyboard(true)
                ->setSelective(false);

            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Format Anda salah, sertakan Jumlah Quantity item setelah command contoh /qty 9',
                'reply_markup' => $key,
            ];

        endif;

        return Request::sendMessage($data);

        return;
    }
}
