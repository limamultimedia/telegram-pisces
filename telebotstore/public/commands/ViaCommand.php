<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class ViaCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'via';
    /**
     * @var string
     */
    protected $description = 'Pengiriman via command';
    /**
     * @var string
     */
    protected $usage = '/via <text>';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $items = \Telebotstore_Item::get_item_name_list();
        $keyboard_item = array_chunk( $items, 2);
        $keyboards = new Keyboard( ... $keyboard_item);

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $via    = trim($message->getText(true));

        $keyboard = $keyboards->setResizeKeyboard(true)
                              ->setOneTimeKeyboard(true)
                              ->setSelective(false);

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        $session = get_transient( $username );

        if( empty($user) ):
            return;
        endif;

        if( empty($session) ):
            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Anda tidak memulai dengan benar, silahkan awali dengan command /baru',
            ];

            return Request::sendMessage($data);
        endif;

        if(empty($via)) :
            return;
        else :

            $data_session['store'] = $session['store'];
            $data_session['tempo'] = $session['tempo'];
            $data_session['via'] = $via;
            $data_session['items'] = array();

            set_transient( $username, $data_session, 12 * HOUR_IN_SECONDS );

            $session = get_transient( $username );

            $text = 'Toko Anda '.$session['store'].PHP_EOL;
            $text .= 'Tempo pembayaran : '.$session['tempo'].PHP_EOL;
            $text .= 'Di kirim via : '.$session['via'].PHP_EOL;
            $text .= 'Silahkan pilih item dengan menekan tombol item';

            $data = [
                'chat_id' => $chat_id,
                'text'    => $text,
                'reply_markup' => $keyboard,
            ];

            return Request::sendMessage($data);

        endif;
    }
}
