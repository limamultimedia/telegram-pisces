<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class TCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 't';
    /**
     * @var string
     */
    protected $description = 'Toko command';
    /**
     * @var string
     */
    protected $usage = '/t <text>';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $payments = explode(PHP_EOL, carbon_get_theme_option('order_item_pembayaran'));

        $obj = array();
        if( $payments ):
            foreach($payments as $key=>$val ):
                $obj[] = array(
                    'text' => '/tempo ' . $val,
                );
            endforeach;
        endif;

        //Keyboard examples
        /** @var Keyboard[] $keyboards */
        $keyboard_item = array_chunk( $obj, 2);
        $keyboards = new Keyboard( ... $keyboard_item);

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $store    = trim($message->getText(true));

        $keyboard = $keyboards->setResizeKeyboard(true)
                              ->setOneTimeKeyboard(true)
                              ->setSelective(false);

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        if( empty($user) ):
            return;
        endif;

        if(empty($store)) :
            $data = [
                'chat_id' => $chat_id,
                'text'    => 'Anda belum mengetikan nama toko silahkan ulangi dengan mengetik /t namatoko atau /toko namatoko',
            ];
        else :

            $data_session['store'] = $store;

            set_transient( $username, $data_session, 12 * HOUR_IN_SECONDS );

            $session = get_transient( $username );

            $data = [
                'chat_id' => $chat_id,
                'text'    => 'Toko Anda "'.$session['store'].'", silahkan tentukan tempo pembayaran',
                'reply_markup' => $keyboard,
            ];

        endif;
        return Request::sendMessage($data);
    }
}
