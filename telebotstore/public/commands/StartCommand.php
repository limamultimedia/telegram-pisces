<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class StartCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'start';
    /**
     * @var string
     */
    protected $description = 'Start command';
    /**
     * @var string
     */
    protected $usage = '/start';
    /**
     * @var string
     */
    protected $version = '1.1.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $first_name = $message->getFrom()->getFirstName();
        $last_name = $message->getFrom()->getLastName();
        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        // if( $user ):
        //     $text    = 'Selamat datang '.$first_name.' '.$last_name.'! '. PHP_EOL . 'Ketik /baru untuk memulai!';
        //     $data = [
        //         'chat_id' => $chat_id,
        //         'text'    => $text,
        //     ];
        // else :
        //     $text    = 'Maaf Anda belum terdaftar';
        //     $data = [
        //         'chat_id' => $chat_id,
        //         'text'    => $text,
        //     ];
        // endif;

        $text    = 'Maaf order via telegram saat ini di nonaktifkan.';
        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
        ];

        return Request::sendMessage($data);
    }
}
