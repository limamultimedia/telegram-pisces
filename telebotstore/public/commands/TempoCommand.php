<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class TempoCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'tempo';
    /**
     * @var string
     */
    protected $description = 'Tempo Pembayaran Command';
    /**
     * @var string
     */
    protected $usage = '/tempo <text>';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $pengiriman = explode(PHP_EOL, carbon_get_theme_option('order_item_pengiriman'));

        $obj = array();
        if( $pengiriman ):
            foreach($pengiriman as $key=>$val ):
                $obj[] = array(
                    'text' => '/via ' . $val,
                );
            endforeach;
        endif;

        //Keyboard examples
        /** @var Keyboard[] $keyboards */
        $keyboard_item = array_chunk( $obj, 1);
        $keyboards = new Keyboard( ... $keyboard_item);

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $tempo    = trim($message->getText(true));

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        $session = get_transient( $username );

        if( empty($user) ):
            return;
        endif;

        if( empty($session) ):
            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Anda tidak memulai dengan benar, silahkan awali dengan command /baru',
            ];

            return Request::sendMessage($data);
        endif;

        if($tempo):

            $data_session['store'] = $session['store'];
            $data_session['tempo'] = $tempo;

            set_transient( $username, $data_session, 12 * HOUR_IN_SECONDS );

            $session = get_transient( $username );

            $keyboard = $keyboards
                ->setResizeKeyboard(true)
                ->setOneTimeKeyboard(true)
                ->setSelective(false);

            $text = 'Toko Anda : '.$session['store'].PHP_EOL;
            $text .= 'Tempo pembayaran : '.$session['tempo'].PHP_EOL;
            $text .= 'Silahkan tentukan moda pengiriman';

            $data = [
                'chat_id' => $chat_id,
                'text'    => $text,
                'reply_markup' => $keyboard,
            ];

            return Request::sendMessage($data);

        endif;
    }
}
