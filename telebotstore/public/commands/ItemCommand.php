<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\UserCommands;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;

Class ItemCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'item';
    /**
     * @var string
     */
    protected $description = 'Show all item on keyboard with reply markup';
    /**
     * @var string
     */
    protected $usage = '/item';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $quantities = explode(PHP_EOL, carbon_get_theme_option('order_item_qty'));

        $obj = array();
        if( $quantities ):
            foreach($quantities as $key=>$val ):
                $obj[] = array(
                    'text' => '/qty ' . $val,
                );
            endforeach;
        endif;

        //Keyboard examples
        /** @var Keyboard[] $keyboards */
        $keyboard_item = array_chunk( $obj, 3);
        $keyboards = new Keyboard( ... $keyboard_item);

        //Return a random keyboard.
        $keyboard = $keyboards
            ->setResizeKeyboard(true)
            ->setOneTimeKeyboard(true)
            ->setSelective(false);

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $item    = trim($message->getText(true));

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        if( empty($user) ):
            return;
        endif;

        $session = get_transient( $username );

        if( empty($session) ):
            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Anda tidak memulai dengan benar, silahkan awali dengan command /baru',
            ];

            return Request::sendMessage($data);
        endif;

        if($item):

            $data_session['store'] = $session['store'];
            $data_session['tempo'] = $session['tempo'];
            $data_session['via'] = $session['via'];

            $new_item = array();
            if( !empty($session['items']) ):
                foreach( (array) $session['items'] as $i ):
                    $new_items[] = array(
                        'item' => $i['item'],
                        'qty' => $i['qty'],
                    );
                endforeach;
            endif;

            $new_items[] = array(
                'item' => $item,
                'qty' => '',
            );

            $data_session['items'] = $new_items;

            set_transient( $username, $data_session, 12 * HOUR_IN_SECONDS );

            // $data_items = array(
            //     'user_id' => $user->ID,
            //     'item' => $item,
            // );
            //
            // \Telebotstore_Order::add_item($data_items);

            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Quantity '.$item,
                'reply_markup' => $keyboard,
            ];
        else :

            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Format Anda salah sertakan Nama item setelah command contoh /item nama-item',
            ];

        endif;

        return Request::sendMessage($data);

        return;
    }
}
