<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class BaruCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'baru';
    /**
     * @var string
     */
    protected $description = 'Baru command';
    /**
     * @var string
     */
    protected $usage = '/baru';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        if( empty($user) ):
            return;
        endif;

        $text    = 'Masukan Nama Toko Anda, awali dengan command /t atau /toko di ikuti dengan nama toko Anda, contoh /t abcstore atau /toko abcstore';
        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
        ];
        return Request::sendMessage($data);
    }
}
