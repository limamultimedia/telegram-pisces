<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;

/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class SelesaiCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'selesai';
    /**
     * @var string
     */
    protected $description = 'Selasaikan order item command';
    /**
     * @var string
     */
    protected $usage = '/selesai';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $keyboards[] = new Keyboard(
            ['/baru']
        );

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        if( empty($user) ):
            return;
        endif;

        $session = get_transient( $username );

        if( empty($session) ):
            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Anda tidak memulai dengan benar, silahkan awali dengan command /baru',
            ];

            return Request::sendMessage($data);
        endif;

        $session['user_id'] = $user->ID;

        $result = \Telebotstore_Order::checkout( $session );
        $i = 1;
        $text = 'Selesai Order Anda Telah di buat'.PHP_EOL;
        $text .= '=========================='.PHP_EOL;
        $text .= '====== DETAIL ORDER ======'.PHP_EOL;
        $text .= '=========================='.PHP_EOL;
        $text .= 'Nama Toko : ' . $session['store'].PHP_EOL;
        $text .= 'Tempo pembayaran: '.$session['tempo'].PHP_EOL;
        $text .= 'Di kirim via : '.$session['via'].PHP_EOL;
        $text .= 'Items : '.PHP_EOL;
        foreach( $result['items'] as $item ):
            $text .= $i++ . '. ' . $item['item']. ' => '.$item['qty'].PHP_EOL;
        endforeach;
        $text .= 'Detail '.$result['permalink'].PHP_EOL;
        $text .= 'Untuk membuat order baru klik /baru';

        $keyboard = $keyboards[0]
            ->setResizeKeyboard(true)
            ->setOneTimeKeyboard(true)
            ->setSelective(false);

        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
            'reply_markup' => $keyboard,
        ];

        Request::sendMessage($data);

        delete_transient( $username );

        return;
    }
}
