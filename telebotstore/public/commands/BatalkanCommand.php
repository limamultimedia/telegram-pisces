<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
/**
 * Start command
 *
 * Gets executed when a user first starts using the bot.
 */
Class BatalkanCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'batalkan';
    /**
     * @var string
     */
    protected $description = 'Batalkan order item command';
    /**
     * @var string
     */
    protected $usage = '/batalkan';
    /**
     * @var string
     */
    protected $version = '1.0.0';
    /**
     * @var bool
     */
    protected $private_only = true;
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $keyboards[] = new Keyboard(
            ['/baru']
        );

        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();

        $username = $message->getFrom()->getUsername();
        $user = get_user_by('login', $username);

        if( empty($user) ):
            return;
        endif;

        $keyboard = $keyboards[0]
            ->setResizeKeyboard(true)
            ->setOneTimeKeyboard(true)
            ->setSelective(false);

        $session = get_transient( $username );

        if( empty($session) ):
            $data    = [
                'chat_id'      => $chat_id,
                'text'         => 'Anda tidak memulai dengan benar, silahkan awali dengan command /baru',
            ];

            return Request::sendMessage($data);
        else :
            delete_transient( $username );
        endif;

        // \Telebotstore_Order::delete($user->ID);

        $text    = 'Invoice Order Anda telah di batalkan, untuk membuat order baru klik /baru';
        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
            'reply_markup' => $keyboard,
        ];
        return Request::sendMessage($data);
    }
}
