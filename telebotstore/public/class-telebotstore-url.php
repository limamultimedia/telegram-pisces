<?php
use Longman\TelegramBot\Entities\Keyboard;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
Class Telebotstore_Url
{
	private $telegram_api_key;
	private $telegram_bot_username;
	private $telegram_bot_admin;

	/**
	 * Construction
	 */
	private function options()
	{
		$this->telegram_api_key  = carbon_get_theme_option( 'telegram_api_key' );
		$this->telegram_bot_username = carbon_get_theme_option( 'telegram_bot_username' );
		$this->telegram_bot_admin = carbon_get_theme_option( 'telegram_bot_admin' );
	}

	/**
	 * rewrite url
	 * @return [type] [description]
	 */
	public function rewrite()
	{
		add_rewrite_rule('^set', 'index.php?set=1', 'top');
		add_rewrite_rule('^webhook', 'index.php?webhook=1', 'top');
		add_rewrite_rule('^export', 'index.php?export=1', 'top');
		add_rewrite_rule('^cron', 'index.php?cron=1', 'top');
		add_rewrite_rule('^test', 'index.php?test=1', 'top');
		add_rewrite_rule('^aws', 'index.php?aws=1', 'top');
	}

	/**
     * [make vars from rewrite]
     * @param  [type] $vars [description]
     * @return [type]       [description]
     */
    public function query_vars( $vars )
    {
        $vars[] = 'set';
		$vars[] = 'webhook';
		$vars[] = 'cron';
		$vars[] = 'export';
		$vars[] = 'aws';
		return $vars;
    }

	/**
	 * set and register telegram webhook
	 * @param [type] $wp [description]
	 */
	public function set_response( $wp )
	{
		$this->options();

		if (array_key_exists('set', $wp->query_vars)) :

			if( $wp->query_vars['set'] == 1 ) :

				$hook_url = site_url().'/webhook/';

				try {
				    // Create Telegram API object
				    $telegram = new Longman\TelegramBot\Telegram($this->telegram_api_key, $this->telegram_bot_username);

				    // Set webhook
				    $result = $telegram->setWebhook($hook_url);
				    if ($result->isOk()) {
				        echo $result->getDescription();
				    }
				} catch (Longman\TelegramBot\Exception\TelegramException $e) {
					echo 'error';
				    // log telegram errors
				    echo $e->getMessage();
				}

				exit;
			endif;
		endif;
	}

	/**
	 * webhook response
	 * @param  [type] $wp [description]
	 * @return [type]     [description]
	 */
	public function webhook_response( $wp )
	{
		$this->options();

		if (array_key_exists('webhook', $wp->query_vars)) :

			if( $wp->query_vars['webhook'] == 1 ) :

				return;

				$admin_users = [
					$this->telegram_bot_admin
				];
				$commands_paths = [
					__DIR__ . '/commands/',
				];

				try {
				    // Create Telegram API object
				    $telegram = new Longman\TelegramBot\Telegram($this->telegram_api_key, $this->telegram_bot_username);

					// Add commands paths containing your custom commands
					$telegram->addCommandsPaths($commands_paths);
					// Enable admin users
					$telegram->enableAdmins($admin_users);
					// Enable MySQL
					//$telegram->enableMySql($mysql_credentials);
					// Logging (Error, Debug and Raw Updates)
					Longman\TelegramBot\TelegramLog::initErrorLog(__DIR__ . "/".$this->telegram_bot_username."_error.log");
					Longman\TelegramBot\TelegramLog::initDebugLog(__DIR__ . "/".$this->telegram_bot_username."_debug.log");
					Longman\TelegramBot\TelegramLog::initUpdateLog(__DIR__ . "/".$this->telegram_bot_username."_update.log");

					// If you are using a custom Monolog instance for logging, use this instead of the above
					//Longman\TelegramBot\TelegramLog::initialize($your_external_monolog_instance);
					// Set custom Upload and Download paths
					$telegram->setDownloadPath(__DIR__ . '/Download');
					$telegram->setUploadPath(__DIR__ . '/Upload');
					// Here you can set some command specific parameters
					// e.g. Google geocode/timezone api key for /date command
					//$telegram->setCommandConfig('date', ['google_api_key' => ' AIzaSyA196_r0rjU1wDzDNTYTzrzS76tReG4ZHw']);
					// Botan.io integration
					//$telegram->enableBotan('your_botan_token');
					// Requests Limiter (tries to prevent reaching Telegram API limits)
					$telegram->enableLimiter();

				    // Handle telegram webhook request
				    $telegram->handle();
				} catch (Longman\TelegramBot\Exception\TelegramException $e) {
				    // Silence is golden!
				    // log telegram errors
				    echo $e->getMessage();
				}

				exit;
			endif;
		endif;
	}

	/**
	 * export to excel url
	 * @param  [type] $wp [description]
	 * @return [type]     [description]
	 */
	public function export_response( $wp )
	{
		global $wpdb;

		set_time_limit(0);

		$this->options();

		if (array_key_exists('export', $wp->query_vars)) :

			if( $wp->query_vars['export'] == 1 ) :

				$file = plugin_dir_path(__FILE__).'example.xlsx';

				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				//$reader->setLoadSheetsOnly(["Data toko"]);
				//$reader->getReadDataOnly(true);
				$spreadsheet = $reader->load($file);

				telebotstore_debug($spreadsheet->getActiveSheet()->toArray());

			exit;
			endif;
		endif;
	}

	public function cron_response( $wp )
	{
		set_time_limit(0);
		//global $wpdb;

		//$this->options();

		if (array_key_exists('cron', $wp->query_vars)) :
			if( $wp->query_vars['cron'] == 1 ) :

				$post = $_POST;
				$post['start'] = 0;
				$post['length'] = -1;

				$emails = explode(',', 'kkk');

				if($emails && !is_email($emails[0]) ):

					echo '<label>Result :</label><br/><strong>Warning!</strong> Invalid Email.';
					exit;
				endif;

				$orders = array();

				$orders[] = array(
					'Order ID',
					'User',
					'Tanggal',
					'Toko',
					'Tempo',
					'Di Kirim Via',
					'Status',
					'Item',
					'Quantity',
					'Link',
				);

				$data_orders = Telebotstore_Order::get_order_list($post);

				foreach( $data_orders as $key => $val ):
					$orders[] = array(
						$val[0],
						$val[1]['username'],
						$val[2],
						$val[3]['name'],
						$val[4],
						$val[5],
						$val[6],
						'-',
						'-',
						$val[7]
					);
					$items = get_post_meta($val[0], '_order_items', false);
					foreach( $items as $key=>$val ):
						$value = explode('|', $val);
						$orders[] = array(
							'-',
							'-',
							'-',
							'-',
							'-',
							'-',
							'-',
							$value[0],
							$value[1],
							'-',
						);
					endforeach;
				endforeach;

				$line = count($orders);

				$client = new \Google_Client();
				$client->setApplicationName('Pisces Telebot Store');
				$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
				$client->setAccessType('offline');
				$client->addScope('https://www.googleapis.com/auth/drive');
				$client->addScope('https://www.googleapis.com/auth/drive.appdata');

				$jsonAuth = file_get_contents(carbon_get_theme_option('google_sheet_api'));
				$client->setAuthConfig(json_decode($jsonAuth, true));

				$sheets = new \Google_Service_Sheets($client);
				$drive = new Google_Service_Drive($client);

				$properties = new \Google_Service_Sheets_SpreadsheetProperties();
				$properties->setTitle('title5 '.date('Y-m-d H:i:s'));

				$newSpreadsheet = new Google_Service_Sheets_Spreadsheet();
				$newSpreadsheet->setProperties($properties);

				$response = $sheets->spreadsheets->create($newSpreadsheet);

				foreach( (array) $emails as $email ):
					if( !is_email($email) ):
						continue;
					endif;

					$userPermission = new Google_Service_Drive_Permission(
						array(
							'type' => 'user',
							'role' => 'writer',
							'emailAddress' => $email,
						)
					);

					$drive->permissions->create($response->spreadsheetId, $userPermission, array('fields' => 'id'));
				endforeach;

				// $body = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(
				// 	array('requests' => array(
				// 		'addSheet' => array(
				// 			'properties' => array(
				// 				'title' => $sheet_name
				// 			)
				// 		)
				// 	)
				// ));
				//
				// $sheets->spreadsheets->batchUpdate($spreadsheetId, $body);

				$range = 'Sheet1!A1:J'.$line;

				$updateBody = new \Google_Service_Sheets_ValueRange([
					'range' => $range,
					'majorDimension' => 'ROWS',
					'values' => $orders,
				]);
				$sheets->spreadsheets_values->update(
					$response->spreadsheetId,
					$range,
					$updateBody,
					['valueInputOption' => 'USER_ENTERED']
				);

				echo '<label>Result :</label><br/><strong>Success!</strong> <a href="'.$response->spreadsheetUrl.'">'.$response->spreadsheetUrl.'</a>';
				exit;
			endif;
		endif;
	}

	public function aws_response($wp){
		global $wpdb;

		if (array_key_exists('aws', $wp->query_vars)) :
			if( $wp->query_vars['aws'] == 1 ) :
				if( isset($_GET['code']) == 'crmpisces' ){
					set_time_limit(0);

					$sql = 'SELECT * FROM '.$wpdb->prefix.'checkin';

			        $where = array();

					$val = site_url();
					$val = str_replace('https://', '', $val);
					$val = str_replace('http://', '', $val);

			        $where[] = 'photo LIKE "%'.$val.'%"';

			        // if( isset($this->args['date_start']) && $this->args['date_start'] ):
			        //     $where[] = 'visit_datetime BETWEEN "'.$this->args['date_start'].'" AND "'.$this->args['date_end'].'"';
			        // endif;

			        if( count($where) >= 1 ){
			            $sql .= ' WHERE '.implode(' AND ', $where );
			        }

			        $sql .= ' ORDER BY ID ASC LIMIT 20 OFFSET 0';

			        $r = $wpdb->get_results($sql);

					//telebotstore_debug($sql);

					//$file = '/home/runcloud/webapps/pisces/wp-content/uploads/2019/01/1548054667058736481533.jpg';

					$s3 = new Telebotstore_S3();

					// $upload = $s3->upload($file,'2019/01/');
					//
					// telebotstore_debug($upload);

					// $image = new \Gumlet\ImageResize($file);
					// $image->resizeToWidth(700);
					// $image->save('/home/runcloud/webapps/pisces/wp-content/uploads/2019/01/1548054667058736481533-3.jpg');


					$url = WP_CONTENT_URL;
					$url = str_replace('https://', 'http://', $url);

					foreach((array) $r as $d ){

						$photo = str_replace('https://', 'http://', $d->photo);
						$photo = str_replace($url, WP_CONTENT_DIR, $photo);
						$path = explode('uploads/',$photo);

						if( file_exists($photo) ){
							$upload = $s3->upload($photo,$path[1]);
							if( isset($upload['status']) && $upload['status'] == 'SUCCESS' ){
								$update = $wpdb->update(
									$wpdb->prefix.'checkin',
									[
										'photo' => $upload['url'],
									],
									[
										'ID' => $d->ID
									]
								);

								if( is_wp_error($update) ){
									telebotstore_debug($update);
								}else{
									wp_delete_file($photo);
									telebotstore_debug($upload);
								}
							}
						}else{
							$d->photo = $photo;
							telebotstore_debug($d);
						}
					}
				}
				exit;
			endif;
		endif;
	}
}
