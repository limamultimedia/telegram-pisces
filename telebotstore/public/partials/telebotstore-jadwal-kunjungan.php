<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2.1
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo get_current_user_id(); ?>">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">Jadwal Kunjungan</h3>
                    </div>

                    <div class="col-md-12">
                        <div class="btn-group form-group">
                            <button class="btn filter-week btn-primary" data-add="0" type="button">Minggu Ini</button>
                            <button class="btn btn-default filter-week" data-add="1" type="button">Minggu Depan</button>
                        </div>
                    </div>

                    <div class="col-md-12" id="form-jadwal">
                                 
                        <!-- content here -->

                    </div>

                </div>

        </section>
        <?php wp_footer(); ?>
        <script type="text/javascript">
            jQuery(function(){
                jQuery('.datatable').DataTable();
                handle_click_week();
                get_form(0);
            });


            function handle_click_week(){
                jQuery('body').on('click','.filter-week',function(){
                    jQuery('.filter-week').removeClass('btn-primary');
                    jQuery(this).addClass('btn-primary');
                    var add = jQuery(this).data('add');
                    get_form(add);
                });
            }


            function get_form(add){

                var data = {'action':'get_form_pengajuan_jadwal','add':add};
                jQuery('#form-jadwal').html('Loading ...');
                jQuery.ajax({
                    url : phpjs.ajax_url,
                    type:'POST',
                    data:data,
                    success:function(response){
                        jQuery('#form-jadwal').html(response);
                        dropdown_search_toko();
                        save_pengajuan_jadwal();
                    }
                });
            }


            function dropdown_search_toko(){
                jQuery('body').find('.search-toko-select2').select2({
                    ajax: {
                        url: phpjs.ajax_url,
                        dataType: "json",
                        data: function(params){

                            var user_id = jQuery('#user_id').val();
                            if( user_id == 'undefined' ){
                                var uid = false;
                            }else{
                                var uid = user_id;
                            }
                            var query = {
                                key: params.term,
                                action: 'search_toko',
                                page: params.page || 1,
                                user_id: uid,
                            }

                            return query;
                        },
                    }
                    });
            }


            function save_pengajuan_jadwal(){
                jQuery('body').on('submit','#form-pengajuan-jadwal',function(e){
                    e.preventDefault();
                    var data = jQuery(this).serialize();
                    jQuery(this).find('button[type="submit"]').button('loading');
                    jQuery.ajax({
                        url : phpjs.ajax_url,
                        type:'POST',
                        data:data,
                        success:function(response){
                            jQuery('#form-jadwal').html(response);

                            var add = jQuery('body').find('.filter-week.btn-primary').data('add');
                            get_form(add);
                        }
                    });

                });
            }
        </script>
    </body>
</html>