<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 *
 */

$user_couriers = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );
$order_courier = get_post_meta($post->ID, '_order_pengiriman', true);
$toko_id = get_post_meta($post->ID, '_order_toko', true);

$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator');

$statuses = array(
    'pending' => 'Pending',
    'done'    => 'Done',
    'reject'  => 'Reject',
);

if( $user_couriers ):
    if( ! in_array( $order_courier, $user_couriers) ):
        wp_redirect( site_url());
        exit;
    endif;
endif;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px;">
                    <?php Telebotstore::menu(); ?>
                </div>
                <div class="col-12">
                    <h2 class="jumbotron-heading text-center">Order #<?php echo get_the_ID(); ?></h1>
                </div>
                <div class="col-12">
                    <h4 class="jumbotron-heading text-center">Tempo Pembayaran : <?php echo get_post_meta($post->ID, '_order_tempo', true); ?> | Pengiriman Via : <?php echo get_post_meta($post->ID, '_order_pengiriman', true); ?></h4>
                </div>
                <hr />
                <div class="col-12">
                    <?php if($toko_id): ?>
                        <?php
                        $provider = get_post_meta($toko_id, '_provider', true);
    	                if($provider == 'api'):
    	                    $toko_name = sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), carbon_get_post_meta($toko_id, 'name') );
                            $toko_address = carbon_get_post_meta($toko_id, 'address');
    	                else :
    	                    $toko_name = sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), get_the_title($toko_id) );
                            $toko_address = apply_filters('the_content', get_post_field('post_content', $toko_id));

                            $address_meta = get_post_meta($toko_id, '_telebotstore_toko_address', true);

            				if( $address_meta ):
            					$toko_address = $address_meta;
            				else:
            					$toko_address = apply_filters('the_content', get_post_field('post_content', $toko_id));
            				endif;
    	                endif;
                        ?>
                        <div class="col-12 text-center">
                            <h4>Toko : <?php echo $toko_name; ?></h4>
                            <h5><?php echo $toko_address; ?></h5>
                        </div>
                        <hr />
                    <?php else: ?>
                        <div class="alert alert-warning text-center">
                            <strong>Perhatian!</strong> Belum di lakukan Mapping.
                        </div>
                    <?php endif; ?>
                    <div class="col-12 text-center">
                        <h5><?php echo get_post_meta($post->ID, '_order_store', true); ?></h5>
                    </div>
                </div>
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col" class="text-center">Quantity</th>
                                    <th scope="col" class="text-right">Harga @</th>
                                    <th scope="col" class="text-right">Sub Total</th>
                                    <th scope="col" class="text-center">Di Kirim</th>
                                    <th scope="col" class="text-center">Reject</th>
                                    <th scope="col" class="text-right">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?>
                                <?php foreach( $data_items as $item ): ?>
                                    <?php
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $item['item']; ?></td>
                                        <td class="text-center"><?php echo $item['qty']; ?></td>
                                        <td class="text-right">Rp <?php echo number_format($item['price'],0,',','.'); ?></td>
                                        <td class="text-right">Rp <?php echo number_format($item['subtotal'],0,',','.'); ?></td>
                                        <td class="text-center">
                                            <?php if( in_array($current_roles[0], $roles) ) : ?>
                                                <input type="number" min="0" style="width:55px;margin: 0 auto;padding: 6px" class="form-control item-change" item="<?php echo $item['item_key']; ?>" id="item_sent_<?php echo $item['item_key']; ?>" value="<?php echo $item['sent']; ?>">
                                            <?php else : ?>
                                                <?php echo $item['sent']; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if( in_array($current_roles[0], $roles) ) : ?>
                                                <?php
                                                $reject_max = intval($item['qty']) - intval($item['sent']);
                                                ?>
                                                <input type="number" min="0" style="width:55px;margin: 0 auto;padding: 6px" class="form-control item-change" item="<?php echo $item['item_key']; ?>" id="item_reject_<?php echo $item['item_key']; ?>" value="<?php echo $item['reject']; ?>">
                                            <?php else : ?>
                                                <?php echo $item['reject']; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-right" style="position:relative">
                                            <?php if( $toko_id == '000000' ): ?>
                                                <select class="form-control itemStatus selectItemStatus" item="<?php echo $item['item_key']; ?>" id="select_item_<?php echo $i; ?>">
                                                    <?php foreach( (array) $statuses as $key=>$val ): ?>
                                                        <option value="<?php echo $key; ?>" <?php if( $key == $item['status'] ){ echo 'selected="selected"'; }?>><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate icon-loader" id="icon_loader_select_item_<?php echo $i; ?>"></span>
                                                <span class="glyphicon glyphicon-ok icon-yes" id="icon_yes_select_item_<?php echo $i; ?>"></span>
                                            <?php endif; ?>
                                            <?php if( in_array($current_roles[0], $roles) ) : ?>
                                                <span id="item_status_<?php echo $item['item_key']; ?>"><?php echo ucfirst($item['status']); ?></span>
                                            <?php else : ?>
                                                <?php echo ucfirst($item['status']); ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col" class="text-right">Total</th>
                                    <th scope="col" class="text-right">Rp <?php echo number_format($total,0,',','.'); ?></th>
                                    <th scope="col">
                                        <?php if( in_array($current_roles[0], $roles) ) : ?>
                                            <div class="text-right">
                                                <button class="btn btn-primary item_status_all" data-status="done">Done All</button>
                                                <button class="btn btn-info item_status_all" data-status="reject">Reject All</span></button>
                                            </div>
                                        <?php endif; ?>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php $reason = 'Reason : '.get_post_meta( $post->ID, '_order_reject_reason', true); ?>
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Created : <span style="font-weight:bold"><?php echo get_the_author_meta( 'first_name', $post->post_author ); ?> <?php echo get_the_author_meta( 'last_name', $post->post_author ); ?></span> On : <span style="font-weight:bold"><?php echo $post->post_date; ?></span>
                        </li>
                        <?php if( $updated_id ): ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Change Status : <span style="font-weight:bold"><?php echo ucfirst(get_post_meta($post->ID, '_order_status', true)); ?></span> By <span style="font-weight:bold"><?php echo get_the_author_meta( 'first_name', $updated_id ); ?> <?php echo get_the_author_meta( 'last_name', $updated_id ); ?></span> On : <span style="font-weight:bold"><?php echo $updated_date; ?></span>
                            </li>
                        <?php endif; ?>
                        <?php foreach( $logs as $log ): ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Change Status : <span style="font-weight:bold"><?php echo ucfirst($log->status); ?></span> By <span style="font-weight:bold"><?php echo get_the_author_meta( 'first_name', $log->user_id ); ?> <?php echo get_the_author_meta( 'last_name', $log->user_id ); ?></span> On : <span style="font-weight:bold"><?php echo $log->datetime; ?></span>
                                <?php if( $log->status == 'reject'){ echo $reason; } ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <?php comments_template(); ?>
        </div>
        <script>
        jQuery(document).ready(function(){
            jQuery('.item-change').on('input', function(){

                let item_id = jQuery(this).attr('item'),
                item_sent = jQuery('#item_sent_'+item_id).val(),
                item_reject = jQuery('#item_reject_'+item_id).val();

                jQuery.ajax({
    				type: "POST",
    				url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
    				dataType: "json",
    				data: {
    					action: 'change_item_status',
    					nonce: '<?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
    					post_id: '<?php echo get_the_ID(); ?>',
                        item: item_id,
                        item_sent: item_sent,
                        item_reject: item_reject,
    				},
    				success: function(data){
                        if( data.respons == 'success'){
                            jQuery('#item_status_'+item_id).html(data.status);
                        }else{
                            if(!alert('Data di kirim + reject tidak boleh melebihi quantity')){window.location.reload();}
                        }
    				}
                });
            });

            jQuery('.itemStatus').change(function(){

                let element_id = jQuery(this).attr('id');

                jQuery(this).css({'opacity':0});

                jQuery('#icon_loader_'+element_id).show();
                jQuery('#icon_yes_'+element_id).hide();

                jQuery.ajax({
    				type: "POST",
    				url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
    				dataType: "json",
    				data: {
    					action: 'change_item_status',
    					nonce: '<?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
    					post_id: '<?php echo get_the_ID(); ?>',
                        item: jQuery(this).attr('item'),
                        status: jQuery(this).val(),
                        id: element_id,
    				},
    				success: function(data){
                        jQuery('#'+data).css({'opacity':1});
                        jQuery('#icon_loader_'+data).hide();
                        jQuery('#icon_yes_'+data).show();
    				}
                });
            });

            jQuery('.item_status_all').on('click', function(){

                let status = jQuery(this).attr('data-status'),
                sure = 1;

                if( status == 'reject' ){
                    confirm("Anda Yakin?");
                }

                if( sure != null ){
                    jQuery.ajax({
        				type: "POST",
        				url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
        				dataType: "json",
        				data: {
        					action: 'change_item_status_all',
        					nonce: '<?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
        					post_id: '<?php echo get_the_ID(); ?>',
                            status: status,
        				},
        				success: function(data){
                            window.location.reload();
                        }
                    });
                }
            });
        })
        </script>
        <?php wp_footer(); ?>
    </body>
</html>
