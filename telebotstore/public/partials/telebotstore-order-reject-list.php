<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">List Order Reject</h3>
                        <?php if( !array_diff($current_roles, $roles) ) : ?>
                            <!---<div class="form-group row">
                                <div class="col-sm-3">
                                    <input type="checkbox" class="form-control select-all"/>
                                </div>
                                <div class="col-sm-9">
                                    <button class="form-control btn btn-info" id="order-pending">Reopen Order To Pending</button>
                                </div>
                            </div>--->
                        <?php endif; ?>
                        <table id="reject-list" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>User & Tanggal</th>
                                    <th>Toko</th>
                                    <th>Tempo</th>
                                    <th>Via</th>
                                    <th>Alasan</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>User & Tanggal</th>
                                    <th>Toko</th>
                                    <th>Tempo</th>
                                    <th>Via</th>
                                    <th>Alasan</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <?php wp_footer(); ?>
    </body>
</html>
