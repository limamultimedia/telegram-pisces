<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">Comments</h3>
                        <table id="comment-list" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Order</th>
                                    <th style="width: 100px;">Author</th>
                                    <th>Comment</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Date</th>
                                    <th>Order</th>
                                    <th>Author</th>
                                    <th>Comment</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <?php wp_footer(); ?>
    </body>
</html>
