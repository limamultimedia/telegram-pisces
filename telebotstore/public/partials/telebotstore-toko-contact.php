<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

if( isset($_GET['edit']) ){

    $contact_id = intval($_GET['edit']);

    $get_contact = new Telebotstore_Query_Toko_Contact(['ID'=>$contact_id]);

    $contact = json_decode(json_encode($get_contact->query()), true);

    $contact_detail = unserialize($contact[0]['detail']);

    if( empty($contact) ):

        wp_safe_redirect(get_the_permalink());
        exit;
    endif;
}

$custom_fields = carbon_get_theme_option('toko_contact_detail_fields');

$fields = array();

foreach( (array)$custom_fields as $field ):

    $options = $field['option'] ? $field['option'] : false;

    $options = $field['checkbox_value'] ? $field['checkbox_value'] : $options;

    $value = isset($contact_detail[$field['name']]) ? $contact_detail[$field['name']] : '';

    $fields[] = array(
        'field' => $this->custom_field($field['type'], $field['name'], $value, $options),
        'label' => $field['label'],
    );
endforeach;

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <?php if(isset($_GET['new']) || isset($_GET['edit']) && $_GET['edit']) : ?>
                        <div class="col-md-12">
                        <h3 class="head text-center"><?php if(isset($_GET['edit'])){echo 'Edit';}else{echo 'New';}?> Contact</h3>
                        <?php if (isset($_GET['new']) && $_GET['new'] == 'success' ): ?>
                            <div class="alert alert-info" role="alert">Sukses, Contact Baru telah di buat</div>
                        <?php elseif( isset($_GET['new']) && $_GET['new'] == 'error') : ?>
                            <div class="alert alert-warning" role="alert">Error Silahkan Ulangi</div>
                        <?php endif; ?>
                        <form method="POST" action="<?php echo get_the_permalink(); ?>" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Toko</label>
                                <div class="col-sm-10">
                                    <select class="form-control search-toko-select2" name="store" required>
                                        <?php if(isset($contact[0]['toko_id'])){
                                            $toko = get_post($contact[0]['toko_id']);
                                        ?>
                                            <option value="<?php echo $toko->ID; ?>" selected="selected"><?php echo $toko->post_title; ?></option>
                                        <?php }else{ ?>
                                            <option value="" selected="selected">Pilih Toko</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="first_name" value="<?php if(isset($contact[0]['first_name'])){echo $contact[0]['first_name'];} ?>" placeholder="First Name" required>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="last_name" value="<?php if(isset($contact[0]['last_name'])){echo $contact[0]['last_name'];} ?>" placeholder="Last Name" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" name="email" value="<?php if(isset($contact[0]['email'])){echo $contact[0]['email'];} ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Handphone</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="handphone_1" value="<?php if(isset($contact[0]['handphone_1'])){echo $contact[0]['handphone_1'];} ?>" required>
                                    <div><small>Handphone 1</small></div>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="handphone_2" value="<?php if(isset($contact[0]['handphone_2'])){echo $contact[0]['handphone_2'];} ?>" >
                                    <div><small>Handphone 2 (optional)</small></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="phone" value="<?php if(isset($contact[0]['phone'])){echo $contact[0]['phone'];} ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="address"><?php if(isset($contact[0]['address'])){echo $contact[0]['address'];} ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control datepicker" name="birth_day" value="<?php if(isset($contact[0]['birth_day'])){echo $contact[0]['birth_day'];} ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Hobby</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="hobby"><?php if(isset($contact[0]['hobby'])){echo $contact[0]['hobby'];} ?></textarea>
                                </div>
                            </div>
                            <?php foreach( (array)$fields as $f ): ?>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"><?php echo $f['label']; ?></label>
                                    <div class="col-sm-10">
                                        <?php echo $f['field']; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">&nbsp;</label>
                                <div class="col-sm-10">
                                    <input type="hidden" value="<?php if(isset($contact[0]['ID'])){echo $contact[0]['ID'];} ?>" name="contact_id">
                                    <input type="hidden" value="<?php echo get_current_user_id(); ?>" name="user_id"/>
                                    <input type="hidden" value="<?php echo get_the_permalink(); ?>" name="redirect"/>
                                    <button type="submit" class="btn btn-primary btn-block" name="save_contact"><?php if(isset($_GET['edit'])){echo 'Update';}else{echo 'Save';}?> Contact</button>
                                    <?php if(isset($_GET['edit'])){?>
                                        <button type="submit" class="btn btn-danger btn-block" name="delete_contact" onclick="return confirmDeleteContact(event);">Delete Contact</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php else : ?>
                        <div class="col-md-12">
                        <h3 class="head text-center">List Contact</h3>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <a href="<?php echo get_the_permalink(get_the_ID()); ?>?new"><button class="btn btn-info btn-block">Create New Contact</button></a>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="contact-name" name="contact-name" value="" placeholder="Filter By Name">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="contact-email" name="contact-email" value="" placeholder="Filter By Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="contact-phone" name="contact-phone" value="" placeholder="Filter By Phone">
                            </div>
                            <div class="col-sm-6">
                                <select name="contact-toko" id="contact-toko" class="form-control search-toko-select2">
                                    <option value=""><?php _e('Filter By Toko', 'telebotstore'); ?></option>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary btn-block" id="contact-filter">Filter</button>
                            </div>
                        </div>

                        <table id="toko-contacts" class="table table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Toko</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Toko</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>

                <?php 
                if(isset($_GET['edit']) && $_GET['edit']) {

                    $Telebotstore_Toko_Contact_Log = new Telebotstore_Toko_Contact_Log;

                    $Telebotstore_Toko_Contact_Log->show_log($_GET['edit']);

                 } 
                 ?>

            </div>
        </section>

        <?php wp_footer(); ?>
    </body>
</html>
