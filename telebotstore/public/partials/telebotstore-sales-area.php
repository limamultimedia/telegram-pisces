<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2.1
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo get_current_user_id(); ?>">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">Sales Area</h3>
                    </div>

                    <div class="col-md-12" id="sales-area-container">
                        
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </section>
        <?php wp_footer(); ?>
         <?php 

         $Telebotstore = new Telebotstore();
         
         $args = [
                    'title' => 'Sales Area Detail',
                    'content' => 'Loading ...',
                    'modal_css'=>'modal-lg'
                ];   
         $Telebotstore->modal($args);

         wp_footer(); ?>
        <script type="text/javascript">
            jQuery(function(){
                load_main_list();              
                
            });

            function load_main_list(){
                var data = {'action':'load_list_sales_area'};
                jQuery.ajax({
                    url : phpjs.ajax_url,
                    data:data,
                    type : 'POST',
                    success : function(response){
                        jQuery('#sales-area-container').html(response);
                        jQuery('.datatable').DataTable();
                        action_sales_area();
                    }

                });
            }


            function action_sales_area(){
                jQuery('body').on('click','.action-sales-area',function(){
                    
                    var sales = jQuery(this).attr('data-sales');  

                        open_popup_detail(sales);

                });
            }


            function open_popup_detail(sales){
                var data = {'action':'load_detail_sales_area','sales':sales};

                jQuery('.modal-body').html('Loading ..');

                jQuery.ajax({
                    url : phpjs.ajax_url,
                    data:data,
                    type : 'POST',
                    success : function(response){
                        jQuery('.modal-body').html(response);
                        set_enable_disable_button();
                    }

                });

                jQuery('.modal').modal('show');

                action_button();
                check_all();
                change_checklist_row();
                
            }


            function action_button(){

                jQuery('body').on('click','.form-sales-area .action',function(e){
                    e.preventDefault();

                    var act = jQuery(this).attr('data-action');

                    if(act === 'delete'){
                        var q = confirm('Apakah akan menghapus data?');
                    }else{
                        q = true;
                    }

                    if(q){

                        var data = jQuery(this).closest('form').serialize()+'&action=sales_area_action&act='+act;
                        var sales_id = jQuery('#sales_id').val();

                        jQuery.ajax({
                            url : phpjs.ajax_url,
                            data:data,
                            type : 'POST',
                            success : function(response){
                                open_popup_detail(sales_id);
                                load_main_list();
                            }

                        });

                    }

                });
            }

            

            function change_checklist_row(){
                jQuery('body').on('change','.checkbox-row',function(){
                    if(!jQuery(this).is(':checked')){
                        jQuery(this).closest('table').find('.select-all').prop('checked',false);
                    };

                    set_enable_disable_button();
                });
            }


            function set_enable_disable_button(){
                jQuery('body').find('.form-sales-area').each(function(){
                    var el = jQuery(this);
                    looping_checkbox_onform(el);

                });
            }

            function looping_checkbox_onform(el){
                var is_checked = 0;
                el.find('input[type="checkbox"]').each(function(){
                    if(jQuery(this).is(':checked')){
                        is_checked++;
                    }
                });

                if(!is_checked){
                    el.find('.action').prop('disabled',true);
                }else{
                    el.find('.action').prop('disabled',false);
                }
            }

        </script>
    </body>
</html>