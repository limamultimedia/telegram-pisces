<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

// $comment_tag = new Telebotstore_Comments_Tag();
// $notif = $comment_tag->get_notif_by_current_user();
// telebotstore_debug($comment_tag->get_notif_by_current_user());
//
//
// $comment = get_comment($notif[0]->comment_id);
// telebotstore_debug($comment);
//
// exit;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <?php if(isset($_GET['new'])) : ?>
                        <div class="col-md-12">
                        <h3 class="head text-center">New Ticket</h3>
                        <?php
                        if ( isset($_POST["save_ticket"]) ) :

                            $post = $_POST;

                            $args = array(
                                'subject'  => sanitize_text_field($post['subject']),
                                'user_id'  => sanitize_text_field($post['user_id']),
                                'content'  => sanitize_text_field($post['content']),
                                'priority' => sanitize_text_field($post['priority']),
                                'store'    => sanitize_text_field($post['store']),
                            );

                            $ticket_id = Telebotstore_Ticket::insert($args);

                            if($ticket_id):

                                $assigns = isset($post['assign']) ? $post['assign'] : array();

                                foreach( (array) $assigns as $key=>$user_id):
                					$data = array(
                						'user_id' => $user_id,
                						'comment_post_ID' => $ticket_id,
                						'comment_id' => -1,
                					);

                					$comment_tag = new Telebotstore_Comments_Tag($data);
                					$comment_tag->insert();
                				endforeach;

                                ?>
                                <div class="alert alert-info" role="alert">Sukses, Ticket Baru telah di buat</div>
                                <?php
                            else :
                                ?>
                                <div class="alert alert-warning" role="alert">Error Silahkan Ulangi</div>
                                <?php
                            endif;
                        endif;
                        ?>
                        <form method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Toko</label>
                                <div class="col-sm-10">
                                    <select class="form-control search-toko-select2" name="store" required>
                                        <option value="" selected="selected">Pilih Toko</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Subject</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="subject" value="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Kasus</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="content" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Priority</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="priority" required>
                                        <option value="low">Low</option>
                                        <option value="medium">Medium</option>
                                        <option value="high">High</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Assign Ticket To</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="comment-tag" name="assign[]" multiple="multiple" required>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">&nbsp;</label>
                                <div class="col-sm-10">
                                    <input type="hidden" value="<?php echo get_current_user_id(); ?>" name="user_id"/>
                                    <button type="submit" class="btn btn-primary btn-block" name="save_ticket">Save New Ticket</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php else : ?>
                        <div class="col-md-12">
                        <h3 class="head text-center">List Tickets</h3>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <a href="<?php echo get_the_permalink(get_the_ID()); ?>?new"><button class="btn btn-info btn-block">Create New Ticket</button></a>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="date-ticket" name="date_ticket" value="" placeholder="Filter By Date">
                            </div>
                            <div class="col-sm-6">
                                <select name="user_ticket" id="user-ticket" class="form-control">
                                    <option value=""><?php _e('Filter By User', 'telebotstore'); ?></option>
                                    <?php  foreach ($users as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select name="store_ticket" id="store-ticket" class="form-control search-toko-select2">
                                    <option value=""><?php _e('Filter By Toko', 'telebotstore'); ?></option>
                    	        </select>
                            </div>
                            <div class="col-sm-6">
                                <select name="tempo_ticket" id="priority-ticket" class="form-control">
                                    <option value=""><?php _e('Filter By Priority', 'telebotstore'); ?></option>
                                    <option value="high"><?php _e('High', 'telebotstore'); ?></option>
                                    <option value="menium"><?php _e('Medium', 'telebotstore'); ?></option>
                                    <option value="low"><?php _e('Low', 'telebotstore'); ?></option>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select name="status_ticket" id="status-ticket" class="form-control">
                                    <option value=""><?php _e('Filter By Status', 'telebotstore'); ?></option>
                                    <option value="open"><?php _e('Open', 'telebotstore'); ?></option>
                                    <option value="closed"><?php _e('Closed', 'telebotstore'); ?></option>
                    	        </select>
                            </div>
                            <div class="col-sm-6">
                                <button class="form-control btn btn-primary" id="ticket-filter">Filter</button>
                            </div>
                        </div>

                        <table id="tickets" class="table table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>User</th>
                                    <th>Tanggal</th>
                                    <th>Toko</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>User</th>
                                    <th>Tanggal</th>
                                    <th>Toko</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <?php wp_footer(); ?>
    </body>
</html>
