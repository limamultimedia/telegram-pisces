<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2.1
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor');

$current_weeks = generate_tgl_pengajuan_jadwal(0);
$prev_weeks = generate_tgl_pengajuan_jadwal(-1);



?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo get_current_user_id(); ?>">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">Report Kunjungan</h3>
                    </div>

                    <div class="col-md-12">
                        <div class="btn-group form-group">
                            <button class="btn btn-default filter-week" data-add="1" type="button" data-start="<?= $prev_weeks['start']; ?>" data-end="<?= $prev_weeks['end']; ?>" >Minggu Lalu</button>
                            <button class="btn filter-week btn-primary" type="button" data-start="<?= $current_weeks['start']; ?>" data-end="<?= $current_weeks['end']; ?>" id="current-week">Minggu Ini</button>
                            <button class="btn btn-default filter-week" type="button" data-start="" data-end="" >Custom</button>
                        </div>
                        <form action="" method="" id="form-report-kunjungan">
                            <div class="form-group form-inline">
                                <input type="date" name="start" class="form-control" placeholder="Dari Tanggal" value="<?= $current_weeks['start']; ?>" > - 
                                <input type="date" name="end" class="form-control" placeholder="Sampai Tanggal" value="<?= $current_weeks['end']; ?>" >
                                <button class="btn btn-primary" id="submit-form"><i class="fa fa-filter"></i> Filter</button>

                                <input type="hidden" name="action" value="filter_report_kunjungan">
                            </div>
                        </form>
                    </div>

                    <form action="" method="POST" id="form-list">
                        <div class="col-md-12" id="list-jadwal"></div>
                    </form>

                </div>

        </section>
        <?php wp_footer(); ?>
        <script type="text/javascript">
            jQuery(function(){

                handle_filter_week();
                submit_form_filter();

                jQuery('#current-week').trigger('click');

            });

            function handle_filter_week(){
                jQuery('body').on('click','.filter-week',function(){
                    jQuery('.filter-week').removeClass('btn-primary');
                     jQuery('.filter-week').removeClass('btn-default');
                    jQuery(this).addClass('btn-primary');

                    var start = jQuery(this).data('start'),
                        end = jQuery(this).data('end');

                        jQuery('input[name="start"]').val(start);
                        jQuery('input[name="end"]').val(end);

                        jQuery('#submit-form').trigger('click');
                });
            }


            function submit_form_filter(){
                jQuery('body').on('submit','#form-report-kunjungan',function(e){
                    e.preventDefault();

                    jQuery('#list-jadwal').html('Loading ...');

                    var data = jQuery(this).serialize();
                    jQuery.ajax({
                        url : phpjs.ajax_url,
                        type:'POST',
                        data:data,
                        success:function(response){
                            jQuery('#list-jadwal').html(response);
                            jQuery('.datatable').DataTable();

                        }
                    });

                });
            }


             
        </script>
    </body>
</html>    