<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

$custom_fields = carbon_get_theme_option('checkin_detail_fields');

$fields = array();

foreach( (array)$custom_fields as $field ):

    $options = $field['option'] ? $field['option'] : false;

    $options = $field['checkbox_value'] ? $field['checkbox_value'] : $options;

    $fields[] = array(
        'field' => $this->custom_field($field['type'], $field['name'], '', $options),
        'label' => $field['label'],
    );
endforeach;

// $args = array(
//     'page' => 2,
//     'limit' => 5,
// );
//
// $query = new Telebotstore_Query_Toko_Order_Csi($args);
//
// telebotstore_debug($query->query());
// exit;

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo get_current_user_id(); ?>">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">Check In</h3>
                    </div>
                    
                    <div id="checkin-dashboard">
                        
                    </div>

                    <div class="col-md-12">
                        <?php
                                            

                        if(isset($_GET['submit'])){

                            if( wp_verify_nonce($_GET['nonce'],date('H:i')) ) {
                                if($_GET['submit'] == 'success'){
                                    ?>
                                     <div class="alert alert-success" role="alert">Check In Tresimpan</div>
                                    <?php
                                }else{
                                    ?>
                                    <div class="alert alert-danger" role="alert">Error Silahkan Ulangi</div>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <form method="POST" enctype="multipart/form-data" id="savedCheckin">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Toko</label>
                                <div class="col-sm-10">
                                    <select class="form-control search-toko-select2 search-toko-checkin" name="store" required>
                                        <option value="" selected="selected">Pilih Toko</option>
                                    </select>

                                    <a href="javascript:;" type="button" class="label label-primary" id="add-store"><i class="fa fa-plus"></i> Tempat Baru</a>

                                    <div id="store-info"></div>
                                    <br/>
                                    <div class="col-sm-12 row stores" style="display:none">
                                        <h4>List Pemilik</h4>
                                        <div id="list-contact"></div>

                                        <h4>Insight</h4>
                                        <div class="col-sm-12">
                                            <ul class="list-group insight"></ul>
                                        </div>
                                        

                                        <h4>History Pembelian</h4>
                                        <div class="col-sm-12">
                                            <ul class="list-group ordercsi"></ul>
                                        </div>
                                        <h4>History Telegram</h4>
                                        <div class="col-sm-12">
                                            <ul class="list-group telegram">
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Yang Di Pelajari</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="learned" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Photo</label>
                                <div class="col-sm-10">
                                    <input type="file" accept="image/*" capture class="form-control" name="photo" value="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tanggal & Jam Revisit</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control datetimepicker" name="revisit_datetime" value="" required>
                                </div>
                            </div>
                            <?php foreach( (array)$fields as $f ): ?>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"><?php echo $f['label']; ?></label>
                                    <div class="col-sm-10">
                                        <?php echo $f['field']; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">&nbsp;</label>
                                <div class="col-sm-10">
                                    <div class="alert alert-warning location-alert" style="display:none"></div>
                                    <input type="hidden" name="visit_datetime" value="<?php echo current_time('mysql'); ?>" required>
                                    <input type="hidden" name="location" id="location" value="" required>

                                    <div id="map" style="height: 200px; width: 100%;margin-bottom: 10px;"></div>


                                    <button type="submit" class="btn btn-primary btn-block" id="checkinSaved" name="save_checkin" disabled data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing" >Save Check In</button>
                                </div>
                            </div>
                          </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>

        <?php 
        $Telebotstore = new Telebotstore();
         
         $args = [
                    'title' => 'Tempat Baru',
                    'content' => 'Loading ...',
                    'modal_css'=>''
                ];   
        $Telebotstore->modal($args);

        wp_footer(); ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo carbon_get_theme_option('google_maps_api'); ?>&callback=embed_google_map"></script>

  
        <script>
            jQuery(function(){

                get_checkin_dashboard();
                change_store();
                submit_checkin();
                click_add_store();

            });


            function submit_checkin(){
                jQuery('body').on('submit','#savedCheckin',function(e){
                    var $this = jQuery(this).find('#checkinSaved');
                    $this.button('loading');
                });
            }

        function get_checkin_dashboard(){
            var data = {'action':'get_checkin_dashboard'};

            jQuery('#checkin-dashboard').html('<div class="text-center"><i class="fa fa-spin fa-spinner fa-2x"></i></div>');
           
            jQuery.ajax({
                url : phpjs.ajax_url,
                type:'POST',
                data:data,
                success:function(response){
                    jQuery('#checkin-dashboard').html(response);
                }
            });
        }    


        function change_store(){
            jQuery('body').on('change','select[name="store"]',function(){

                var toko_id = jQuery(this).val();
                var data = {'action':'get_my_sales_area','toko_id':toko_id};
                jQuery.ajax({
                    url : phpjs.ajax_url,
                    type:'POST',
                    data:data,
                    success:function(response){
                        jQuery('#store-info').html(response);
                        add_store_to_list();
                    }
                });

            });
        }


        function add_store_to_list(){
            jQuery('body').on('click','#add-store-to-list',function(e){
                e.preventDefault();
                var toko_id = jQuery(this).attr('data-id');

                var q = confirm('Akan menambahkan toko ke List Anda?');
                if(q){
                    var data = {'action':'add_store_to_list','toko_id':toko_id};
                    jQuery.ajax({
                        url : phpjs.ajax_url,
                        type:'POST',
                        data:data,
                        success:function(response){
                            jQuery('#store-info').html(response);
                        }
                    });
                }

            });
        }
            
        function getAddress (latitude, longitude) {
            jQuery.ajax('https://maps.googleapis.com/maps/api/geocode/json?' + 'latlng=' + latitude + ',' + longitude + '&key=<?php echo carbon_get_theme_option('google_maps_api'); ?>').then(
                function success (response) {
                    console.log('User\'s Address Data is ', response);
                    jQuery('#location').val(JSON.stringify(response.results));
                    jQuery('#checkinSaved').attr('disabled', false);

                    embed_google_map(latitude,longitude);
                },
                function fail (status) {
                    console.log('Request failed.  Returned status of',status);
                    jQuery('#checkinSaved').attr('disabled', true);
                }
            )
        }


        function embed_google_map(latitude,longitude){

            
            const curpos = { lat: latitude, lng: longitude };
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 16,
                center: curpos,
              });
            const marker = new google.maps.Marker({
                position: curpos,
                map: map,
            });
        }

        if("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(
                function success(position) {
                    console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude);
                    getAddress(position.coords.latitude,position.coords.longitude);
                },
                function error(error_message) {
                    console.error('An error has occured while retrieving ' + 'location', error_message);
                    jQuery('.location-alert').html('Anda harus mengizinkan website <?php echo site_url(); ?> mengakses lokasi Anda.<br/>Silahkan refresh halaman ini dan klik izinkan/allow saat muncul popup permintaan izin akses lokasi. <br/> Jika popup permintaan izin akses lokasi tidak muncul setelah halaman ini di refresh, silahkan hapus data izin lokasi di pengaturan browser Anda, kemudian refresh kembali halaman ini.').show();
                    jQuery('#checkinSaved').attr('disabled', true);
                }
            );
        } else {
            console.log('geolocation is not enabled on this browser');
            alert('Please .. enable geolocation on this broswer');
            jQuery('#checkinSaved').attr('disabled', true);
        }


        function click_add_store(){
            jQuery('body').on('click','#add-store',function(){

                var data = {'action':'load_add_store'};

                jQuery('.modal-body').html('Loading ..');

                jQuery.ajax({
                    url : phpjs.ajax_url,
                    data:data,
                    type : 'POST',
                    success : function(response){
                        jQuery('.modal-body').html(response);
                    }

                });

                jQuery('.modal').modal('show');
                action_add_store();
            });
        }


        function action_add_store(){
            jQuery('body').on('submit','#form-store',function(e){
                e.preventDefault();

                jQuery(this).find('button[type="submit"]').button('loading');

                var location = jQuery('#location').val();
                var data = jQuery(this).serialize()+'&location='+location;
                jQuery.ajax({
                    url : phpjs.ajax_url,
                    data:data,
                    type : 'POST',
                    dataType: 'json',
                    success : function(response){
                        jQuery('.modal-body').html(response.message);

                    }

                });

            })
        }

        </script>


        
    </body>
</html>
