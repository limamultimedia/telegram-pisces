<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

if( array_diff($current_roles, $roles) ) :
    wp_redirect( site_url() );
endif;

$user_pengiriman = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">List Tagged Comment</h3>
                        <?php foreach( $user_pengiriman as $key=>$val ): ?>
                            <input type="hidden" class="form-control user_pengiriman" name="user_pengiriman[]" value="<?php echo $val; ?>"/>
                        <?php endforeach; ?>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select name="user_order" id="user-order" class="form-control">
                                    <option value=""><?php _e('Filter By User', 'telebotstore'); ?></option>
                                    <?php  foreach ($users as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                            <div class="col-sm-6">
                                <button class="form-control btn btn-primary" id="order-filter">Filter</button>
                            </div>
                        </div>
                        <table id="tagged-list" class="table table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Tagged user</th>
                                    <th>Text Komen</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Tagged user</th>
                                    <th>Text Komen</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <?php wp_footer(); ?>
    </body>
</html>
