<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

$user_pengiriman = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">List Order</h3>
                        <?php foreach( $user_pengiriman as $key=>$val ): ?>
                            <input type="hidden" class="form-control user_pengiriman" name="user_pengiriman[]" value="<?php echo $val; ?>"/>
                        <?php endforeach; ?>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="date-order" name="date_order" value="" placeholder="Filter By Date">
                            </div>
                            <div class="col-sm-6">
                                <select name="user_order" id="user-order" class="form-control">
                                    <option value=""><?php _e('Filter By User', 'telebotstore'); ?></option>
                                    <?php  foreach ($users as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select name="store_order" id="store-order" class="form-control">
                                    <option value=""><?php _e('Filter By Toko', 'telebotstore'); ?></option>
                    	        </select>
                            </div>
                            <div class="col-sm-6">
                                <select name="tempo_order" id="tempo-order" class="form-control">
                                    <option value=""><?php _e('Filter By Tempo', 'telebotstore'); ?></option>
                                    <?php  foreach ($pays as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <select name="item_order" id="item-order" class="form-control">
                                    <option value=""><?php _e('Filter By Item', 'telebotstore'); ?></option>
                                    <?php  foreach ($all_items as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="via_order" id="via-order" class="form-control">
                                    <?php if( count($user_pengiriman) == 0 ) : ?>
                                        <option value=""><?php _e('Filter By Pengiriman', 'telebotstore'); ?></option>
                                    <?php endif; ?>
                                    <?php  foreach ($vias as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="status_order" id="status-order" class="form-control">
                                    <option value=""><?php _e('Filter By Status', 'telebotstore'); ?></option>
                                    <option value="pending"><?php _e('Pending', 'telebotstore'); ?></option>
                                    <option value="done"><?php _e('Done', 'telebotstore'); ?></option>
                                    <option value="reject"><?php _e('Reject', 'telebotstore'); ?></option>
                                    <option value="cr"><?php _e('CR', 'telebotstore'); ?></option>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <button class="form-control btn btn-primary" id="order-filter">Filter</button>
                            </div>
                            <div class="col-sm-3">
                                <button class="form-control btn btn-warning" id="order-export">Export To Excel</button>
                            </div>
                            <div class="col-sm-3">
                                <button class="form-control btn btn-warning" data-toggle="modal" data-target="#send-spreadsheet">Send To Spreadsheet</button>
                            </div>
                        </div>
                        <?php if( !array_diff($current_roles, $roles) ) : ?>
                            <div class="form-group row" style="display:none">
                                <div class="col-sm-3">
                                    <input type="checkbox" class="form-control select-all"/>
                                </div>
                                <div class="col-sm-3">
                                    <button class="form-control btn btn-info" id="order-pending">Pending</button>
                                </div>
                                <div class="col-sm-3">
                                    <button class="form-control btn btn-info" id="order-done">Done</button>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn-block btn btn-info" id="order-reject-modal">Reject</button>
                                </div>
                            </div>
                        <?php endif; ?>
                        <table id="order-list" class="table table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>User</th>
                                    <th>Tanggal</th>
                                    <th>Toko</th>
                                    <th>Tempo</th>
                                    <th>Di kirim via</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>User</th>
                                    <th>Tanggal</th>
                                    <th>Toko</th>
                                    <th>Tempo</th>
                                    <th>Di kirim via</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <!-- Modal -->
        <div id="send-spreadsheet" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Send To Google Spreadsheet</h4>
                    </div>
                    <div class="modal-body">
                        <p class="form-group">
                            <label>Spreadsheet Title</label><br/>
                            <input type="text" name="spreadsheet-title" id="spreadsheet-title" class="form-control">
                            <div class="alert alert-warning sheet-title" style="display:none">
                                <strong>Warning!</strong> Title is required.
                            </div>
                        </p>
                        <p class="form-group">
                            <label>Gamil Address</label><br/>
                            <span style="font-style:italic">Alamat email dari gmail atau email dari domain gsuite yang akan menerima akses google spreadsheet.</span><br>
                            <span style="font-style:italic">Jika lebih dari satu alamat gmail pisahkan dengan koma.</span><br>
                            <span style="font-style:italic">Ex: test@gmail.com,test2@gmail.com</span>
                            <textarea name="spreadsheet-email" id="spreadsheet-email" class="form-control"></textarea>
                            <div class="alert alert-warning sheet-email" style="display:none">
                                <strong>Warning!</strong> Email is required.
                            </div>
                        </p>
                        <div class="alert alert-info sheet-result" style="display:none">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="order-export-spreadsheet">Send Now</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alasan Reject</h4>
                    </div>
                    <div class="modal-body">
                        <p class="form-group">
                            <textarea name="reject-reason" id="reject-reason" class="form-control"></textarea>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="order-reject" disabled>Reject Now</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="orderModal" class="popup">
            <div class="popupLoader">
                 <div class="loading"></div>
            </div>
            <div class="popupContent">
                <div class="col-md-12 field">
                    <div class="col-sm-6">
                        <a class="popupPermalink" href="#" target="_blank"><button class="btn btn-primary btn-block">Detail</button></a>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-default btn-block popupClose">Close</button>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-4">
                        <label>Order</label>
                    </div>
                    <div class="col-sm-8">
                        <span id="popupOrderId"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-4">
                        <label>Tempo</label>
                    </div>
                    <div class="col-sm-8">
                        <span id="popupTempo"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-4">
                        <label>Pengiriman</label>
                    </div>
                    <div class="col-sm-8">
                        <span id="popupPengiriman"></span>
                    </div>
                </div>
                <div class="col-md-12 mapping field">
                    <div class="col-sm-4">
                        <label>Toko</label>
                    </div>
                    <div class="col-sm-8">
                        <span id="popupToko"></span>
                    </div>
                </div>
                <div class="col-md-12 mapping field">
                    <div class="col-sm-4">
                        <label>Alamat</label>
                    </div>
                    <div class="col-sm-8">
                        <span id="popupAlamat"></span>
                    </div>
                </div>
                <div class="col-md-12 no-mapping field">
                    <div class="col-sm-12">
                        <div class="alert alert-warning text-center">
                            <strong>Perhatian!</strong> Belum di lakukan Mapping.
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <span id="popupNoMapping"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <table class="table table-item">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Sub Total</th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="col">Total</th>
                                    <th scope="col">&nbsp;</th>
                                    <th scope="col" class="text-right">&nbsp;</th>
                                    <th scope="col" class="text-right totales">98778638</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <ul id="popupLogs" class="list-group">

                    </ul>
                </div>
                <div class="col-md-12 field" id="commentList">

                    <div id="comments">
                        <h3>Comments</h3>
                        <ul id="popupComment" class="list-unstyled">

                        </ul>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-6">
                        <a class="popupPermalink" href="" target="_blank"><button class="btn btn-primary btn-block">Detail</button></a>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-default btn-block popupClose">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>
