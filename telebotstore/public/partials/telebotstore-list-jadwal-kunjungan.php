<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2.1
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor');

$current_weeks = generate_tgl_pengajuan_jadwal(0);
$next_weeks = generate_tgl_pengajuan_jadwal(1);

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo get_current_user_id(); ?>">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">List Pengajuan Jadwal Kunjungan</h3>
                    </div>

                    <div class="col-md-12">
                        <div class="btn-group form-group">
                            <button class="btn filter-week btn-primary" type="button" data-start="<?= $current_weeks['start']; ?>" data-end="<?= $current_weeks['end']; ?>" id="current-week">Minggu Ini</button>
                            <button class="btn btn-default filter-week" data-add="1" type="button" data-start="<?= $next_weeks['start']; ?>" data-end="<?= $next_weeks['end']; ?>" >Minggu Depan</button>
                            <button class="btn btn-default filter-week" type="button" data-start="" data-end="" >Custom</button>
                        </div>
                        <form action="" method="" id="form-list-jadwal-kunjungan">
                            <div class="form-group form-inline">
                                <input type="date" name="start" class="form-control" placeholder="Dari Tanggal" value="<?= $current_weeks['start']; ?>" > - 
                                <input type="date" name="end" class="form-control" placeholder="Sampai Tanggal" value="<?= $current_weeks['end']; ?>" >
                                <button class="btn btn-primary" id="submit-form"><i class="fa fa-filter"></i> Filter</button>

                                <input type="hidden" name="action" value="filter_list_jadwal_kunjungan">
                            </div>
                        </form>
                    </div>

                    <form action="" method="POST" id="form-list">
                        <div class="col-md-12" id="list-jadwal"></div>
                    </form>

                </div>

        </section>
        <?php wp_footer(); ?>
        <script type="text/javascript">
            jQuery(function(){

                handle_filter_week();
                submit_form_filter();
                change_checklist_row();

                jQuery('#current-week').trigger('click');

            });

            function handle_filter_week(){
                jQuery('body').on('click','.filter-week',function(){
                    jQuery('.filter-week').removeClass('btn-primary');
                    jQuery('.filter-week').removeClass('btn-default');
                    jQuery(this).addClass('btn-primary');

                    var start = jQuery(this).data('start'),
                        end = jQuery(this).data('end');

                        jQuery('input[name="start"]').val(start);
                        jQuery('input[name="end"]').val(end);

                        jQuery('#submit-form').trigger('click');
                });
            }


            function submit_form_filter(){
                jQuery('body').on('submit','#form-list-jadwal-kunjungan',function(e){
                    e.preventDefault();

                    jQuery('#list-jadwal').html('Loading ...');

                    var data = jQuery(this).serialize();
                    jQuery.ajax({
                        url : phpjs.ajax_url,
                        type:'POST',
                        data:data,
                        success:function(response){
                            jQuery('#list-jadwal').html(response);
                            jQuery('.datatable').DataTable();

                            set_enable_disable_button();
                            check_all();
                            action_button(); 

                        }
                    });

                });
            }


             function change_checklist_row(){
                jQuery('body').on('change','.checkbox-row',function(){
                    if(!jQuery(this).is(':checked')){
                        jQuery(this).closest('table').find('.select-all').prop('checked',false);
                    };

                    set_enable_disable_button();
                });
            }


            function set_enable_disable_button(){
                jQuery('body').find('#form-list').each(function(){
                    var el = jQuery(this);
                    looping_checkbox_onform(el);

                });
            }

            function looping_checkbox_onform(el){
                var is_checked = 0;
                el.find('input[type="checkbox"]').each(function(){
                    if(jQuery(this).is(':checked')){
                        is_checked++;
                    }
                });

                if(!is_checked){
                    el.find('.action').prop('disabled',true);
                }else{
                    el.find('.action').prop('disabled',false);
                }
            }


            function action_button(){

                jQuery('body').on('click','#form-list .action',function(e){
                    e.preventDefault();

                    var act = jQuery(this).attr('data-action');

                    if(act === 'delete'){
                        var q = confirm('Apakah akan menghapus data?');
                    }else{
                        q = true;
                    }

                    if(q){

                        var data = jQuery(this).closest('form').serialize()+'&action=pengajuan_jadwal_action&act='+act;

                        jQuery.ajax({
                            url : phpjs.ajax_url,
                            data:data,
                            type : 'POST',
                            success : function(response){
                                jQuery('#submit-form').trigger('click');
                            }

                        });

                    }

                });
            }

        </script>
    </body>
</html>    