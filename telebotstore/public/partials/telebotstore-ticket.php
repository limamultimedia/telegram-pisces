<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 *
 */

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px;">
                    <?php Telebotstore::menu(); ?>
                </div>
                <div class="col-12">
                    <h2 class="jumbotron-heading text-center">Ticket #<?php echo get_the_ID(); ?></h1>
                </div>
                <div class="col-12">
                    <h4 class="jumbotron-heading text-center">By : <?php echo get_the_author_meta( 'first_name', $post->post_author ); ?> <?php echo get_the_author_meta( 'last_name', $post->post_author ); ?> On : <?php echo $post->post_date; ?> | Priority : <?php echo ucfirst(get_post_meta($post->ID, '_ticket_priority', true)); ?></h4>
                </div>
                <hr />
                <div class="col-sm-12">
                    <div class="col-12 text-center">
                        <?php
                        $toko = get_post(get_post_meta(get_the_ID(), '_ticket_store', true) );
                        ?>

                        <h4>Toko : <?php echo $toko->post_title; ?></h4>
                    </div>
                    <br/>
                    <br/>
                    <div class="col-12">
                        <h4><?php the_title() ?></h4>
                    </div>
                    <div class="col-sm-12">
                        <div><?php echo apply_filters('the_content', $post->post_content); ?></div>
                    </div>
                </div>
            </div>
            <div id="markAsSolution" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">Anda Yakin ? </h5>
                        </div>
                        <div class="modal-body">
                            <p class="modal-title">Anda Yakin ? akan menandai comment dari <span id="comment-author"></span> Sebagai solusi ?</p>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="comment-id" value=""/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal" onclick="markAsSolutionNow('<?php echo get_the_ID(); ?>')">Yakin</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php comments_template(); ?>
            <div style="margin-top:50px;"></div>
        </div>
        <script>
        var markAsSolution = function(commentId,commentAuthor){
            jQuery('#comment-author').text(commentAuthor);
            jQuery('#comment-id').val(commentId);
            jQuery('#markAsSolution').modal('show');
        }
        var markAsSolutionNow = function(postid){
            var cid = jQuery('#comment-id').val();

            jQuery.ajax({
                url: phpjs.ajax_url,
                dataType: "json",
                data: {
                    action: 'mark_comment_as_solution',
                    postid: postid,
                    commentid: cid,
                    nonce: phpjs.nonce,
                },
                success: function(response){
                    location.reload();
                },
            });
        }
        </script>
        <?php wp_footer(); ?>
    </body>
</html>
