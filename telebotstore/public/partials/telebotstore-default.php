<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo get_current_user_id(); ?>">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center"><?= the_title(); ?></h3>
                    </div>
                    <div class="col-md-12">

                        <?php 
                        echo apply_filters('the_content',the_content() );
                        ?>

                    </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<?php wp_footer(); ?>
</body>
</html>    