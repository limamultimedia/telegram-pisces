<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.limamultimedia.com
 * @since      2.5.2.1
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

$Telebotstore_Checkin_Log = new Telebotstore_Checkin_Log;

$default_start = date('Y-m-01');
$default_end = date('Y-m-t');    

$from = isset($_GET['from']) ? $_GET['from'] : $default_start;
$to = isset($_GET['to']) ? $_GET['to'] : $default_end;

/*$andWhere =" AND DATE(visit_datetime) BETWEEN '".$from."' AND '".$to."' ";
$query = $Telebotstore_Checkin_Log->get($andWhere);
foreach($query as $row){
    $id = $row->ID;

    $coordinate = $Telebotstore_Checkin_Log->get_coordinate_location($row->location);

    $date = date('Y-m-d',strtotime($row->visit_datetime));
    $Telebotstore_Checkin_Log->insert_log($row->user_id,$date,$id,$coordinate);
}*/


$query = $Telebotstore_Checkin_Log->get_count($from,$to);

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo get_current_user_id(); ?>">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">Check In Log</h3>
                    </div>
                    <div class="col-md-12">
                        <form action="" class="form-inline">
                            <p>
                            <div class="form-group">
                                <input type="date" name="from" class="form-control" value="<?= $from; ?>">
                                <input type="date" name="to" class="form-control" value="<?= $to; ?>">
                                <button class="btn btn-primary">Filter</button>    
                            </div>
                            </p>
                        </form>
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Sales</th>
                                    <th>Jml Visit</th>
                                    <th>Jarak Tempuh (Km)</th>
                                    <th>Detail</th>
                                    <th>Target</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 

                               $no = 1;
                               $lat_origin = $lng_origin = '';
                               foreach($query as $row){

                                    $sales_id = $row->user_id;
                                    $user = get_user_by('id',$sales_id);
                                    $sales = $user->display_name;
                                    $date = $row->visit_datetime;
                                    $visit_count = $row->visit_count;
                                    
                                                                    
                                    //get Data from checkin_log
                                    $row_log = $Telebotstore_Checkin_Log->get_checkin_log($sales_id,$date);

                                    $visit_target = $row_log->visit_target;
                                    $distance_target = $row_log->distance_target;
                                    $total_distance = ($row_log->distance/1000);
                                    $detail = unserialize($row_log->detail);


                                    $first_detail = array_values($detail)[0];
                                    $first_time = date('H:i',strtotime($first_detail['checkin_time']));
                                    $html = $first_detail['store'].' | '.$first_time;
                                    $origin_start = $first_detail['end'];

                                    $waypoints = '';
                                    $detail_html = '<ul class="log-detail">';
                                    $detail_html .= '<li>'.$html.'</li>';
                                    

                                    foreach($detail as $checkin_id => $data ){

                                        if($data['checkin_time'] !== $first_detail['checkin_time']){

                                            $link_direction = 'https://www.google.com/maps/dir/?api=1&origin='.$data['start'].'&destination='.$data['end'];

                                            $end = $data['store'];
                                            $end_time = date('H:i',strtotime($data['checkin_time']));



                                            $html = '<a href="'.$link_direction.'" target="_blank">'.$start.' | '.$start_time.' &#8594; '.$end.' | '.$end_time.'</a> ('.number_format( ($data['distance']/1000),1).' Km)';

                                            $duration_real = strtotime($end_time) - strtotime($start_time);
                                            $duration_real = ceil($duration_real/60);

                                            $duration = isset($data['duration']) ? $data['duration'] : '0';
                                            $duration = ceil($duration/60);

                                            $html .= ' | <span title="Waktu Tempuh Real/ Waktu Tempuh Normal"><i class="fa-regular fa-clock" ></i> '.$duration_real.'/'.$duration.' min</span>';
                                            $html .= ' <a href="javascript:;" title="Re- Calculate Jarak & Waktu Tempuh" class="re-calculate" data-checkin-id="'.$checkin_id.'" data-checkin-log-id="'.$row_log->id.'" data-origin="'.$data['start'].'" data-destination="'.$data['end'].'" ><i class="fa fa-sync" ></i></a>';

                                            $waypoints .= $data['end'].'/';
                                            
                                            $detail_html .= '<li>'.$html.'</li>';

                                        }

                                        $start_time = date('H:i',strtotime($data['checkin_time']));    
                                        $start = $data['store'];

                                    }

                                    $detail_html .= '</ul>';

                                    $waypoints = rtrim($waypoints,'/');
                                    $all_direction = 'https://www.google.com/maps/dir/'.$origin_start.'/'.$waypoints;

                                    if($visit_target || $distance_target){

                                        if($visit_count>=$visit_target || $total_distance>=$distance_target){
                                            $target = 'PASS';
                                        }else{
                                            $target = 'FAILED';
                                        }

                                    }else{
                                        $target = '-';
                                    }
            
                                ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= date('d-m-Y',strtotime($row->visit_datetime)); ?></td>
                                    <td><?= $sales; ?></td>
                                    <td><?= $visit_count; ?>/<?= $visit_target; ?></td>
                                    <td><?= number_format($total_distance,1); ?>/<?= $distance_target; ?> Km<br><a href="<?= $all_direction; ?>" target="_blank">All Direction</a></td>
                                    <td><?= $detail_html; ?></td>
                                    <td><?= $target; ?></td>
                                </tr>
                                <?php
                                $no++;
                               }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <?php wp_footer(); ?>
        <script type="text/javascript">
            jQuery(function(){
                jQuery('.datatable').DataTable();
                recalculate_distance();
            });


            function recalculate_distance(){
                jQuery('body').on('click','.re-calculate',function(e){
                    e.preventDefault();

                    var $this = jQuery(this);
                    var origin = $this.data('origin'),
                        destination = $this.data('destination'),
                        checkin_id = $this.data('checkin-id'),
                        checkin_log_id = $this.data('checkin-log-id');

                    var data = {'action':'re_calculate_distance','origin':origin,'destination':destination,'checkin_id':checkin_id,'checkin_log_id':checkin_log_id};

                    console.log(data);

                    jQuery.ajax({
                        url : phpjs.ajax_url,
                        data:data,
                        type : 'POST',
                        dataType: 'json',
                        success : function(response){
                            console.log(response);
                        }

                    });
                });
            }
        </script>
    </body>
</html>
