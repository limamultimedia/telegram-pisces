<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

$user_pengiriman = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <?php while (have_posts()) : the_post();?>
            <section style="background:#efefe9;">
                <div class="container">
                    <div style="display:none">
                        <input type="hidden" id="store-order" name="store_order" value="<?php echo get_the_ID(); ?>" style="display:none !important">
                        <input type="hidden" id="store-page" name="store_page" value="yes">
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 20px;">
                            <?php Telebotstore::menu(); ?>
                        </div>
                        <div class="col-md-12">
                            <h3 class="head text-center">List Order Toko : <?php the_title(); ?></h3>
                            <div class="panel-group">
                                <div class="panel panel-default text-center">
                                    <?php
                                    $address_meta = get_post_meta($post->ID, '_telebotstore_toko_address', true);

                    				if( $address_meta ):
                    					$toko_address = $address_meta;
                    				else:
                    					$toko_address = $post->post_content;
                    				endif;
                                    ?>
                                    <div class="panel-heading"><?php echo $toko_address; ?></div>
                                    <div class="panel-body">Phone: <?php echo carbon_get_post_meta( get_the_ID(), 'telebotstore_toko_phone' ); ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4" style="position:relative">
                                    <input type="text" class="form-control" id="date-order" name="date_order" value="" placeholder="Filter By Date">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="user-order" name="user_order" value="" placeholder="Order By Username">
                                </div>
                                <div class="col-sm-4">
                                    <select name="tempo_order" id="tempo-order" class="form-control">
                                        <option value=""><?php _e('Filter By Tempo', 'telebotstore'); ?></option>
                                        <?php  foreach ($pays as $key => $val) : ?>
                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                        	        </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <select name="item_order" id="item-order" class="form-control">
                                        <option value=""><?php _e('Filter By Item', 'telebotstore'); ?></option>
                                        <?php  foreach ($all_items as $key => $val) : ?>
                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                        	        </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="via_order" id="via-order" class="form-control">
                                        <?php if( count($user_pengiriman) == 0 ) : ?>
                                            <option value=""><?php _e('Filter By Pengiriman', 'telebotstore'); ?></option>
                                        <?php endif; ?>
                                        <?php  foreach ($vias as $key => $val) : ?>
                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                        	        </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="status_order" id="status-order" class="form-control">
                                        <option value=""><?php _e('Filter By Status', 'telebotstore'); ?></option>
                                        <option value="pending"><?php _e('Pending', 'telebotstore'); ?></option>
                                        <option value="done"><?php _e('Done', 'telebotstore'); ?></option>
                                        <option value="reject"><?php _e('Reject', 'telebotstore'); ?></option>
                        	        </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <button class="form-control btn btn-primary" id="order-filter">Filter</button>
                                </div>
                                <div class="col-sm-4">
                                    <button class="form-control btn btn-warning" id="order-export">Export To Excel</button>
                                </div>
                            </div>
                            <?php if( !array_diff($current_roles, $roles) ) : ?>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <input type="checkbox" class="form-control select-all"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="form-control btn btn-info" id="order-pending">Pending</button>
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="form-control btn btn-info" id="order-done">Done</button>
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="form-control btn btn-info" id="order-reject-modal">Reject</button>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <table id="order-list" class="table table-striped" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>User</th>
                                        <th>Tanggal</th>
                                        <th>Toko</th>
                                        <th>Tempo</th>
                                        <th>Di kirim via</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>User</th>
                                        <th>Tanggal</th>
                                        <th>Toko</th>
                                        <th>Tempo</th>
                                        <th>Di kirim via</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Alasan Reject</h4>
                        </div>
                        <div class="modal-body">
                            <p class="form-group">
                                <textarea name="reject-reason" id="reject-reason" class="form-control"></textarea>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="order-reject" disabled>Reject Now</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_footer(); ?>
    </body>
</html>
