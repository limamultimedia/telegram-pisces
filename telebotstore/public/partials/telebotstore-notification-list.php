<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 *
 */

 $comment_tag = new Telebotstore_Comments_Tag();

 $notifs = $comment_tag->get_notif_by_current_user();

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px;">
                    <?php Telebotstore::menu(); ?>
                </div>
                <div class="col-12">
                    <h2 class="jumbotron-heading text-center">Notification</h1>
                </div>
                <hr />
                <div class="col-12">
                    <div class="table-responsive">
                        <?php if( $notifs ): ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">&nbsp;</th>
                                        <th scope="col" class="text-center">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach( $notifs as $notif ):

                                        if( empty($notif->comment_id) || $notif->comment_id == 0 )continue;

                                        if( $notif->comment_id == -1 ):
                                            $post = get_post($notif->post_id);

                                            $user_tagged_id = $post->post_author;

                                            $respond_link = get_the_permalink($notif->post_id);

                                        else :
                                            $comment = get_comment( $notif->comment_id );

                                            $user_tagged_id = $comment->user_id;

                                            $respond_link = get_the_permalink($notif->post_id).'?replytocom='.$notif->comment_id.'#respond';

                                        endif;
                                        $user = get_userdata($user_tagged_id);

                                        ?>
                                        <tr>
                                            <td><a href="<?php echo $respond_link; ?>"><?php echo $user->display_name; ?> tagged you at <?php echo substr(get_post_meta($notif->post_id, '_order_store', true), 0, 40); ?>, click to reply</a></td>
                                            <td class="text-center"><a href="<?php echo $respond_link; ?>" class="btn btn-info">Balas</a></td>
                                        </tr>
            						<?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else: ?>
                            <div style="text-align:center;margin-bottom:50px;">Tidak ada komentar yang menandai Anda</div>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>
