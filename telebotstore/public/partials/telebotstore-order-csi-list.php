<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator');

if( in_array($current_roles[0], $roles) ):
    $is_admin = 1;
else:
    $is_admin = 0;
endif;

// $args = array(
//     'toko_ids' => '14146'
// );
//
// $query = new Telebotstore_Query_Toko_Order_Csi($args);
//
// telebotstore_debug($query->sum_sales_inc_ppn());
//
// telebotstore_debug($query->query());
//
// exit;

$user_pengiriman = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12" style="margin-bottom:50px;">
                        <h3 class="head text-center">List Order CSI</h3>
                        <input type="hidden" id="userrole" value="<?php echo $is_admin; ?>"/>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control daterangepickers" id="orderCsiFilterDate" name="order_csi_filter_date" value="" placeholder="Filter By Daterange">
                            </div>
                            <div class="col-sm-6">
                                <select name="order_csi_filter_sales" id="OrderCsiFilterSales" class="form-control">
                                    <option value=""><?php _e('Filter By Sales', 'telebotstore'); ?></option>
                                    <?php  foreach ($users as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="orderCsiFilterSearch" name="order_csi_filter_search" value="" placeholder="Filter By Entry No or SKU">
                            </div>
                            <div class="col-sm-6">
                                <select id="orderCsiFilterToko" class="form-control">
                                    <option value=""><?php _e('Filter By Toko', 'telebotstore'); ?></option>
                    	        </select>
                            </div>
                        </div>
                        <?php if( in_array($current_roles[0], $roles) ): ?>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <input type="checkbox" class="form-control select-all" style="cursor:pointer"/>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-warning btn-block" id="orderCsiDelete">Delete Selected Data</button>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-primary btn-block" id="orderCsiFilterClear">Clear Filter</button>
                                </div>
                                <div class="col-sm-4">
                                    <button class="btn btn-info btn-block" id="orderCsiExportToExcel">Export To Excel</button>
                                </div>
                            </div>
                        <?php endif; ?>
                        <table id="order-csi-list" class="table table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Entry No</th>
                                    <th>Date</th>
                                    <th>Document No</th>
                                    <th>Customer Name</th>
                                    <th>Sales</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Entry No</th>
                                    <th>Date</th>
                                    <th>Document No</th>
                                    <th>Customer Name</th>
                                    <th>Sales</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                        <table class="table table-striped" width="100%" cellspacing="0">
                            <tbody>
                                <tr>
                                    <th><h3>Total Sales Include PPN :</h3></th>
                                    <th><h3 style="text-align:right" class="OrderCsiTotalIncPpn"></h3></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div id="orderCsiModal" class="popup">
            <div class="popupLoader">
                 <div class="loading"></div>
            </div>
            <div class="popupContent">
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <button class="btn btn-default btn-block" onclick="orderCsiModalClose();">Close</button>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Entry No :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiEntryNo"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Posting Date :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiPostingDate"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Document No :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiDocumentNo"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Document Type :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiDocumentType"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Customer Name :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiCustomerName"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Salesperson Code :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiSalesCode"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Item No :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiItemNo"></span>
                    </div>
                </div>
                <div class="col-md-12 mapping field">
                    <div class="col-sm-12">
                        <label>Quantity :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiQuantity"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Price :</label>
                    </div>
                    <div class="col-sm-8">
                        <span id="orderCsiPrice"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Location Code :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiLocationCode"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Sales Include PPN :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiSalesIncPpn"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Item Category</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiItemCategory"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Manufacture Code :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiManufactureCode"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>City</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiCity"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Dalam Atau Luar Kota</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiDlmLrKota"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>Sales Exclude PPN :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiSalesExcPpn"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <label>IQ :</label>
                    </div>
                    <div class="col-sm-12">
                        <span id="orderCsiIq"></span>
                    </div>
                </div>
                <div class="col-md-12 field">
                    <div class="col-sm-12">
                        <button class="btn btn-default btn-block" onclick="orderCsiModalClose();">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php wp_footer(); ?>
        <script>
        var orderCsiModal = function(id){
            jQuery('.popupLoader').show();
            jQuery('.popupContent').hide();
            jQuery('#orderCsiModal').hide();
            jQuery('#orderCsiModal').show();
            jQuery.ajax({
				type: "GET",
				url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
				dataType: "json",
				data: {
					action: 'get_ordercsi_single',
					nonce: '<?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
					data: id,
				},
				success: function(data){
                    jQuery('#orderCsiEntryNo').html(data.entry_no);
                    jQuery('#orderCsiPostingDate').html(data.posting_date);
                    jQuery('#orderCsiDocumentNo').html(data.document_no);
                    jQuery('#orderCsiDocumentType').html(data.document_type);
                    jQuery('#orderCsiCustomerName').html(data.customer_name);
                    jQuery('#orderCsiSalesCode').html(data.salesperson_code);
                    jQuery('#orderCsiItemNo').html(data.item_no);
                    jQuery('#orderCsiQuantity').html(data.quantity);
                    jQuery('#orderCsiPrice').html(data.price);
                    jQuery('#orderCsiLocationCode').html(data.location_code);
                    jQuery('#orderCsiSalesIncPpn').html(data.sales_inc_ppn);
                    jQuery('#orderCsiItemCategory').html(data.item_category);
                    jQuery('#orderCsiManufactureCode').html(data.manufacture_code);
                    jQuery('#orderCsiCity').html(data.city);
                    jQuery('#orderCsiDlmLrKota').html(data.dlm_lr_kota);
                    jQuery('#orderCsiIq').html(data.iq);
                    jQuery('#orderCsiSalesExcPpn').html(data.sales_exc_ppn);

                    jQuery('.popupLoader').hide();
                    jQuery('.popupContent').show();
				}
            });
        }
        // var orderCsiDelete = function(id){
        //
        //     let konfirm = confirm('Anda Yakin akan menghapus data ini ?');
        //
        //     if( konfirm == true ){
        //
        //         jQuery.ajax({
    	// 			type: "GET",
    	// 			url: '</?php echo admin_url( 'admin-ajax.php' ); ?>',
    	// 			dataType: "json",
    	// 			data: {
    	// 				action: 'delete_ordercsi_single',
    	// 				nonce: '</?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
    	// 				data: id,
    	// 			},
    	// 			success: function(data){
        //                 jQuery('[data-id='+id+']').remove();
    	// 			}
        //         });
        //     }
        // }
        var orderCsiModalClose = function(){
            jQuery('#orderCsiModal').hide();
        }
        jQuery(document).ready(function(){

            jQuery('#orderCsiExportToExcel').on('click', function(){

                jQuery(this).html('Processing...').attr('disabled','disabled');
                jQuery.ajax({
    				type: "GET",
    				url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
    				dataType: "json",
    				data: {
    					action: 'export_ordercsi_list',
    					nonce: '<?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
    					date: jQuery("#orderCsiFilterDate").val(),
    					sales: jQuery("#OrderCsiFilterSales").val(),
    					search: jQuery("#orderCsiFilterSearch").val(),
    				},
    				success: function(data){
                        jQuery('#orderCsiExportToExcel').html('Export To excel').removeAttr('disabled');
    					console.log(data.url);
    					window.open(data.url, '_blank');
    				}
                });
            });

            window.onkeydown = function( event ) {
                if ( event.keyCode == 27 ) {
                    jQuery('#orderCsiModal').hide();
                }
            };
        })
        </script>
    </body>
</html>
