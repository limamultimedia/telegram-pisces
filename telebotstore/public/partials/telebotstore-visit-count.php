<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

$user_id = isset($_GET['user_id']) ? intval($_GET['user_id']) : 0;

$user = get_userdata($user_id);

if( isset($_GET['user_id']) && empty($user) ){
    wp_safe_redirect(get_the_permalink());
    exit;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <?php if( isset($_GET['user_id']) && $_GET['user_id'] ): ?>
                        <div class="col-md-12">
                            <h3 class="head text-center">Statistik Check In / Visit For <?php echo $user->first_name.' '.$user->last_name; ?></h3>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="visitFilterDate" placeholder="Filter By Date">
                                    <input type="hidden" class="form-control" id="visitFilterUser" value="<?php echo $_GET['user_id']; ?>">
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn btn-warning btn-block" id="visitUserExport">Export To Excel</button>
                                </div>
                            </div>
                            <table id="visit-list-user" class="table table-striped" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Customer/Toko</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Customer/Toko</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div id="visitUserModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Check In</h4>
                                    </div>
                                    <div class="modal-body visitUserModalData">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="col-md-12">
                            <h3 class="head text-center">Statistik Check In / Visit</h3>
                            <table id="visit-list" class="table table-striped" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Sales</th>
                                        <th>Visit Today</th>
                                        <th>Visit Yesterday</th>
                                        <th>Visit Day Before Yesterday</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Sales</th>
                                        <th>Visit Today</th>
                                        <th>Visit Yesterday</th>
                                        <th>Visit Day Before Yesterday</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="col-md-12">
                            <h3 class="head text-center">Statistik Check In / Visit</h3>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="visitsFilterDate" placeholder="Filter By Date">
                                </div>
                                <div class="col-sm-6">
                                    <select id="visitsFilterToko" class="form-control">
                                        <option value=""><?php _e('Filter By Toko', 'telebotstore'); ?></option>
                        	        </select>
                                </div>
                            </div>
                            <table id="visit-lists" class="table table-striped" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Sales</th>
                                        <th>Visit</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Sales</th>
                                        <th>Visit</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo carbon_get_theme_option('google_maps_api'); ?>"></script>
        <?php wp_footer(); ?>
    </body>
</html>
