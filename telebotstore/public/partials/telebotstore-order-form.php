<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 *
 */

$payments = explode(PHP_EOL, carbon_get_theme_option('order_item_pembayaran'));
$shippings = explode(PHP_EOL, carbon_get_theme_option('order_item_pengiriman'));
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px;">
                    <?php Telebotstore::menu(); ?>
                </div>
                <div class="col-12" style="margin-top:100px !important;">
                    <h2 class="jumbotron-heading text-center">Order Form</h1>
                </div>
                <hr />
                <?php
                if ( isset($_POST["new_order"]) ) :

                    $post = $_POST;

                    if(isset($post['item'])):
                        $new_post = array(
                            'post_author' => get_current_user_id(),
                            'post_content' => '',
                            'post_status' => "publish",
                            'post_title' => $post['toko'],
                            'post_type' => "telebotstore_order",
                        );

                        $post_id = wp_insert_post( $new_post );
                        add_post_meta( $post_id, '_order_status', 'pending');
                        //add_post_meta( $post_id, '_order_store', $post['toko']);
                        add_post_meta( $post_id, '_order_toko', $post['toko']);
                        add_post_meta( $post_id, '_order_tempo', $post['tempo']);
                        add_post_meta( $post_id, '_order_pengiriman', $post['pengiriman']);

                        foreach( (array) $post['item'] as $key=>$val ):
                            $add_item = $val.'|'.$post['qty'][$key];
                            add_post_meta( $post_id, '_order_items', $add_item);
                        endforeach;

                        $post_id;

                        ?>

                        <div class="col-md-12">
                            <div class="alert alert-info" role="alert">
                                BERHASIL ! Order anda telah di simpan cek order Anda <a href="<?php echo get_the_permalink($post_id); ?>" target="_blank">DI SINI</a>
                            </div>
                        </div>

                        <?php
                    else :
                        ?>

                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                PERHATIAN ! Anda belum menentukan Item beserta quantitinya
                            </div>
                        </div>

                        <?php
                    endif;

                endif;
                ?>

                <div class="col-md-12">
                    <form method="POST" action="">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Store</label>
                            <div class="col-sm-10">
                                <select id="new-order-toko" class="form-control" name="toko" required>
                                    <option value="" selected="selected">Pilih Toko</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Payment</label>
                            <div class="col-sm-10">
                                <?php foreach( (array)$payments as $key=>$val ): ?>
                                    <div class="form-check radio-inline">
                                        <input class="form-check-input" type="radio" name="tempo" id="pembayaran-<?php echo rtrim($val); ?>" value="<?php echo rtrim($val); ?>" required>
                                        <label class="form-check-label" for="pembayaran-<?php echo $val; ?>"><?php echo $val; ?></label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Shipping</label>
                            <div class="col-sm-10">
                                <?php foreach( (array)$shippings as $key=>$val ): ?>
                                    <div class="form-check radio-inline">
                                        <input class="form-check-input" type="radio" name="pengiriman" id="pengiriman-<?php echo rtrim($val); ?>" value="<?php echo rtrim($val); ?>" required>
                                        <label class="form-check-label" for="pembayaran-<?php echo $val; ?>"><?php echo $val; ?></label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Items</label>
                            <div class="col-sm-10 row" style="position:relative;">
                                <div id="czContainer">
                                    <div id="first">
                                        <div class="recordset clearfix" style="position:relative;">
                                            <div class="form-group col-md-6">
                                                <select class="form-control new-order-item" name="item[]" required>
                                                    <option value="" selected="selected">Pilih Item</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="number" class="form-control qtys" id="qty" name="qty[]" min="1" value="1" required>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <input type="text" class="form-control prices" value="0" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-6 text-right">
                                <strong>Total</strong>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="totale" value="0" readonly>
                            </div>
                            <div class="col-sm-1">
                                &nbsp;
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                          <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary btn-block" name="new_order">Place Order</button>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>
