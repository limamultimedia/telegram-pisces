<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/public/partials
 */
$current_roles = wp_get_current_user()->roles;
$roles = array('admin_toko', 'administrator', 'editor' );

$user_pengiriman = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
        <?php wp_head(); ?>
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <?php Telebotstore::menu(); ?>
                    </div>
                    <div class="col-md-12">
                        <h3 class="head text-center">Order Sumary Toko</h3>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label>From</label>
                            </div>
                            <div class="col-sm-10">
                                <select name="user_order" id="user-order" class="form-control">
                                    <option value=""><?php _e('All Month', 'telebotstore'); ?></option>
                                    <?php  foreach ($users as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label>User</label>
                            </div>
                            <div class="col-sm-10">
                                <select name="user_order" id="user-order" class="form-control">
                                    <option value=""><?php _e('All User', 'telebotstore'); ?></option>
                                    <?php  foreach ($users as $key => $val) : ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php endforeach; ?>
                    	        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-10">
                                <button class="form-control btn btn-primary" id="order-filter">Filter</button>
                            </div>
                        </div>
                        <table id="order-list" class="table table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>User</th>
                                    <th>Tanggal</th>
                                    <th>Toko</th>
                                    <th>Tempo</th>
                                    <th>Di kirim via</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>User</th>
                                    <th>Tanggal</th>
                                    <th>Toko</th>
                                    <th>Tempo</th>
                                    <th>Di kirim via</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <?php wp_footer(); ?>
        <script>
        jQuery(document).ready(function($){
            var countries = [
                { value: 'Andorra', data: 'AD' },
                // ...
                { value: 'Zimbabwe', data: 'ZZ' }
            ];

            $('#store-order').autocomplete({
                lookup: countries,
                onSelect: function (suggestion) {
                    alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                }
            });
        });
        </script>
    </body>
</html>
