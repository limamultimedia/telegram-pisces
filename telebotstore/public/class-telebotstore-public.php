<?php

class Telebotstore_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		//add_action('wp', [$this, 'ajax_get_visit_lists']);

	}

	public function store_pages()
	{
		$store_pages = array(
			carbon_get_theme_option('list_order'),
			carbon_get_theme_option('list_order_log'),
			carbon_get_theme_option('list_comment'),
			carbon_get_theme_option('list_order_pending'),
			carbon_get_theme_option('list_order_reject'),
			carbon_get_theme_option('list_notification'),
			carbon_get_theme_option('order_form'),
			carbon_get_theme_option('order_sumary_store'),
			carbon_get_theme_option('checkin'),
			carbon_get_theme_option('visit_count'),
			carbon_get_theme_option('ticket'),
			carbon_get_theme_option('toko_contact'),
			carbon_get_theme_option('list_order_csi'),
			carbon_get_theme_option('checkin_log'), //Lima Multimedia 180622
			carbon_get_theme_option('sales_area'), //Lima Multimedia 110722
			carbon_get_theme_option('jadwal_kunjungan'),
			carbon_get_theme_option('list_jadwal_kunjungan'),
			carbon_get_theme_option('report_kunjungan'),
			carbon_get_theme_option('statistik_toko')
		);

		return $store_pages;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{
		global $post;
		$post_type = get_post_type( get_the_ID() );


		if( in_array(get_the_ID(), $this->store_pages()) || in_array($post_type, ['telebotstore_order', 'telebotstore_toko', 'telebotstore_ticket']) || is_page() ):

			wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Roboto:300,400', array(), NULL, 'all' );

			wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '3.3.7', 'all' );

			wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array(), NULL, 'all' );

			//wp_enqueue_style( 'datatables', 'https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css', array(), '1.10.16', 'all' );

			wp_enqueue_style( 'datatables-responsive', 'https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css', array(), '2.2.1', 'all' );

			wp_enqueue_style( 'datatables-select', 'https://cdn.datatables.net/select/1.2.4/css/select.dataTables.min.css', array(), '1.2.4', 'all' );

			//wp_enqueue_style( 'datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.18/jquery.datetimepicker.min.css', array(), NULL, 'all' );

			wp_enqueue_style( 'daterangepicker', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css', array(), NULL, 'all' );

			wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css', array(), NULL, 'all' );

			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/public.css', array(), $this->version, 'all' );

		endif;

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		global $post;

		$post_type = get_post_type( get_the_ID() );

		if( in_array(get_the_ID(), $this->store_pages()) || in_array($post_type, ['telebotstore_order', 'telebotstore_toko', 'telebotstore_ticket']) || is_page() ):

			wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', ['jquery'], '3.3.7', false );

			wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', array( 'jquery' ), NULL, false );

			wp_enqueue_script( 'block-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js', ['jquery'], '2.70', false );

			wp_enqueue_script( 'datatables', 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js', ['jquery'], '2.70', false );

			wp_enqueue_script( 'datatables-bootstrap', 'https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js', ['jquery'], '2.70', false );

			wp_enqueue_script( 'datatables-responsive', 'https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js', ['jquery'], '2.2.1', false );

			wp_enqueue_script( 'datatables-select', 'https://cdn.datatables.net/select/1.2.4/js/dataTables.select.min.js', ['jquery'], '1.2.4', false );

			wp_enqueue_script( 'jquery-mask', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js', ['jquery'], '1.2.4', false );

			//wp_enqueue_script( 'datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.18/jquery.datetimepicker.full.min.js', array( 'jquery' ), NULL, false );

			wp_enqueue_script( 'momentt', 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js', array( 'jquery' ), NULL, false );

			wp_enqueue_script( 'daterangepicker', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js', array( 'jquery' ), NULL, false );

			// wp_enqueue_script( 'underscore', '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js', array( 'jquery' ), NULL, false );
			//
			// wp_enqueue_script( 'elastic', plugin_dir_url( __FILE__ ) . 'js/jquery.elastic.js', array( 'jquery' ), NULL, true );
			//
			// wp_enqueue_script( 'event-input', plugin_dir_url( __FILE__ ) . 'js/jquery.events.input.js', array( 'jquery' ), $this->version, true );
			//
			// wp_enqueue_script( 'mention', plugin_dir_url( __FILE__ ) . 'js/jquery.mentionsInput.js', array( 'jquery' ), $this->version, true );

			wp_enqueue_script( 'comment-reply' );

			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/telebotstore-public.js', array( 'jquery' ), $this->version, true );

			$phpjs = array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'nonce' => wp_create_nonce( 'telebotstore_nonce' ),
				'postID' => $post->ID
			);
			wp_localize_script( $this->plugin_name, "phpjs", $phpjs );


		endif;



	}

	/**
	 * dequeue unwanted script
	 * @return [type] [description]
	 */
	public function dequeue_unwanted_scripts()
	{
		global $wp_styles,$wp_scripts, $post;
		$styles = $scripts = [];

		$post_type = get_post_type( get_the_ID() );

		if( in_array(get_the_ID(), $this->store_pages()) || in_array($post_type, ['telebotstore_order', 'telebotstore_toko']) ):
			foreach( (array) $wp_styles->registered as $key => $object) :
  			  if(!( false !== strpos($object->src,'wp-includes') || false !== strpos($object->src,'wp-admin') || $object->src == '' || $object->src == 1)  ) :
  				  wp_dequeue_style($key);
  						  else :
  							  $styles[]	= $key;
  			  endif;
  		  endforeach;


  		  foreach((array) $wp_scripts->registered as $key => $object) :
  			  if(!(
  					  false !== strpos($object->src,'wp-includes') ||
  					  false !== strpos($object->src,'wp-admin') ||
  					  $object->src == '' ||
  					  $object->src == 1)
  				  ) :
  				  wp_dequeue_script($key);
  			  else :
  				  $scripts[]	= $key;
  			  endif;
  		  endforeach;
		endif;

	}


	/**
	 * template redirect
	 * @return [type] [description]
	 */
	public function set_template_redirect()
	{
		global $wpdb,$post;

		$table = $wpdb->prefix.'postmeta';

		$sql = 'SELECT meta_value FROM '.$table.' WHERE meta_key = "_order_store"';

		$stores = $wpdb->get_results( $sql, OBJECT );
		$toko = array();

		foreach( $stores as $store ):
			$toko[$store->meta_value] = $store->meta_value;
		endforeach;

		$payments = explode(PHP_EOL, carbon_get_theme_option('order_item_pembayaran'));

		$pays = array();
		foreach( (array) $payments as $key=>$val ):
			$pays[$val] = $val;
		endforeach;

		$couriers = explode(PHP_EOL, carbon_get_theme_option('order_item_pengiriman'));

		$user_couriers = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );

		$vias = array();
		foreach( (array) $couriers as $key=>$val ):
			if( $user_couriers ):
				if( in_array( rtrim($val), (array) $user_couriers) ):
					$vias[rtrim($val)] = $val;
				endif;
			else :
				$vias[rtrim($val)] = $val;
			endif;
		endforeach;

		$users = $this->get_all_user();


		$is_order_list_page = get_post_meta( get_the_ID(), '_telebotstore_order_list_page', true);
		$is_log_list_page = get_post_meta( get_the_ID(), '_telebotstore_log_list_page', true);
		$is_comment_list_page = get_post_meta( get_the_ID(), '_telebotstore_comment_list_page', true);
		$post_type = get_post_type( get_the_ID() );

		if( get_the_ID() ==  carbon_get_theme_option('list_order') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			$all_items = $this->get_all_items();

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order-list.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('list_order_log') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-log-list.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('list_comment') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-comment-list.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('list_order_reject') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order-reject-list.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('list_order_pending') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order-pending-list.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('list_notification') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-notification-list.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('order_form') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order-form.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('order_sumary_store') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order-sumary-store.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('checkin') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-checkin.php');
			exit;
			//Adjust Lima Multimedia 180622
		elseif( get_the_ID() ==  carbon_get_theme_option('checkin_log') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-checkin-log.php');
			exit;	
		//Adjust Lima Multimedia 110722
		elseif( get_the_ID() ==  carbon_get_theme_option('sales_area') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-sales-area.php');
			exit;	
		elseif( get_the_ID() ==  carbon_get_theme_option('jadwal_kunjungan') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-jadwal-kunjungan.php');
			exit;	
		elseif( get_the_ID() ==  carbon_get_theme_option('list_jadwal_kunjungan') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-list-jadwal-kunjungan.php');
			exit;				
		elseif( get_the_ID() ==  carbon_get_theme_option('report_kunjungan') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-report-kunjungan.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('statistik_toko') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-statistik-toko.php');
			exit;	
		elseif( get_the_ID() ==  carbon_get_theme_option('visit_count') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-visit-count.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('ticket') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-tickets.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('toko_contact') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-toko-contact.php');
			exit;
		elseif( get_the_ID() ==  carbon_get_theme_option('list_order_csi') ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order-csi-list.php');
			exit;
		elseif( 'telebotstore_order' == $post_type ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			$args = array(
				'user_id' => get_current_user_id(),
				'order_id' => get_the_ID(),
			);

			$log = new Telebotstore_Order_Status_Log($args);
			$logs = $log->get();

			$status = get_post_meta( get_the_ID(), '_order_status', true);

			$updated_id = get_post_meta( get_the_ID(), '_order_status_id_update', true);
			$updated_date =  get_post_meta( get_the_ID(), '_order_status_date_update', true);

			$items = get_post_meta( get_the_ID(), '_order_items', false);
			$data_items = array();

			$subtotal = array();

			foreach( (array) $items as $key=>$val ):
				$value = explode('|', $val);
				$item = Telebotstore_Item::get_by_slug( $value[0] );
				$subtotal[] = carbon_get_post_meta($item->ID, 'item_price') * $value[1];
				$item_status = isset($value[2]) ? $value[2] : $status;
				$sent = isset($value[3]) ? intval($value[3]) : 0;
				$reject = isset($value[4]) ? intval($value[4]) : 0;
				if( $status == 'done'){
					$sent = isset($value[3]) ? $value[3] : intval($value[1]);
				}else if( $status == 'reject' ){
					$reject = isset($value[4]) ? $value[4] : intval($value[1]);
				}
				if( $sent == 0 && $reject == 0 ){
					$item_status = 'pending';
				}
				$data_items[] = array(
					'item'     => $item->post_title,
					'item_key' => $value[0],
					'qty'      => intval($value[1]),
					'price'    => carbon_get_post_meta($item->ID, 'item_price'),
					'subtotal' => carbon_get_post_meta($item->ID, 'item_price') * $value[1],
					'sent'     => $sent,
					'reject'   => $reject,
					'status'   => $item_status,
				);
			endforeach;

			$total = array_sum($subtotal);

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order.php');
			exit;
		elseif( 'telebotstore_toko' == $post_type ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			$status = get_post_meta( get_the_ID(), '_order_status', true);

			$updated_id = get_post_meta( get_the_ID(), '_order_status_id_update', true);
			$updated_date =  get_post_meta( get_the_ID(), '_order_status_date_update', true);

			$items = get_post_meta( get_the_ID(), '_order_items', false);
			$data_items = array();

			$args = array(
				'user_id' => get_current_user_id(),
				'order_id' => get_the_ID(),
			);
			$log = new Telebotstore_Order_Status_Log($args);
			$logs = $log->get();

			foreach( $items as $key=>$val ):
				$value = explode('|', $val);
				$item = Telebotstore_Item::get_by_slug( $value[0] );
				$data_items[] = array(
					'item' => $item->post_title,
					'qty' => $value[1],
				);
			endforeach;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-order-list-toko.php');
			exit;
		elseif( 'telebotstore_ticket' == $post_type ):

			if(!is_user_logged_in()) :
				wp_redirect( site_url());
				exit;
			endif;

			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-ticket.php');
			exit;
		elseif (!is_home() && !is_front_page() && !is_admin() ) :
			
			//limamultimedia
			include( plugin_dir_path( __FILE__ ) . 'partials/telebotstore-default.php');
			exit;


		endif;
	}

	/**
	 * set post type telebotstore_order opened
	 * hooked via comments_open filter
	 * @param [type] $open    [description]
	 * @param [type] $post_id [description]
	 */
	public function set_comments_open( $open, $post_id )
	{
		if ( 'telebotstore_order' == get_post_type( $post_id ) ):
			$open = true;
		endif;

		if ( 'telebotstore_ticket' == get_post_type( $post_id ) ):
			$open = true;
		endif;

		return $open;
	}

	/**
	 * changed comment template for order post type
	 * hooked via comment template filter
	 * @param [type] $template [description]
	 */
	public function set_comments_template($template)
	{
		if ( in_array( get_post_type( get_the_id() ), ['telebotstore_order', 'telebotstore_ticket']) ):
			$template = plugin_dir_path( __FILE__ ) . 'partials/telebotstore-comment.php';
		endif;

		return $template;
	}

	/**
	 * ajax all list order
	 * @return [type] [description]
	 */
	public function ajax_get_order_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$order = Telebotstore_Order::get_order_list($_GET);

		$response['data']            = $order;
		$response['recordsTotal']    = Telebotstore_Order::count_order_list();
		$response['recordsFiltered'] = Telebotstore_Order::count_order_list();

		echo json_encode( $response );
		exit;
	}

	/**
	 * ajax all list order
	 * @return [type] [description]
	 */
	public function ajax_get_log_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$logs = Telebotstore_Order::get_log_list($_GET);

		$response['data']            = $logs;
		$response['recordsTotal']    = Telebotstore_Order::count_order_list();
		$response['recordsFiltered'] = Telebotstore_Order::count_order_list();

		echo json_encode( $response );
		exit;
	}

	/**
	 * ajax all list order
	 * @return [type] [description]
	 */
	public function ajax_get_pending_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$logs = Telebotstore_Order::get_pending_list($_GET);

		$response['data']            = $logs;
		$response['recordsTotal']    = Telebotstore_Order::count_order_list();
		$response['recordsFiltered'] = Telebotstore_Order::count_order_list();

		echo json_encode( $response );
		exit;
	}

	/**
	 * ajax all list order
	 * @return [type] [description]
	 */
	public function ajax_get_reject_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$logs = Telebotstore_Order::get_reject_list($_GET);

		$response['data']            = $logs;
		$response['recordsTotal']    = Telebotstore_Order::count_order_list();
		$response['recordsFiltered'] = Telebotstore_Order::count_order_list();

		echo json_encode( $response );
		exit;
	}

	/**
	 * ajax all list order
	 * @return [type] [description]
	 */
	public function ajax_get_tagged_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$tagged = new Telebotstore_Comments_Tag();

		$data = $tagged->get($_GET);

		$response['data']            = $data;
		$response['recordsTotal']    = $tagged->count();
		$response['recordsFiltered'] = $tagged->count();

		echo json_encode( $response );
		exit;
	}


	/**
	 * ajax all list order
	 * @return [type] [description]
	 */
	public function ajax_get_comment_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$args = array(
		    'number' => $_GET['length'],
		    'offset' => $_GET['start'],
		);


		$q = new WP_Comment_Query($args);
		$count = get_comment_count();

		$objs = array();

		foreach( (array) $q->comments as $comment ):
			$objs[] = array(
				$comment->comment_date,
				$comment->comment_post_ID,
				$comment->comment_author,
				[
					'toko' => get_post_meta($comment->comment_post_ID, '_order_store', true),
					'comment' => $comment->comment_content,
				],
				get_the_permalink($comment->comment_post_ID).'?replytocom='.$comment->comment_ID.'#respond',
			);
		endforeach;

		$response['data']            = $objs;
		$response['recordsTotal']    = $count['all'];
		$response['recordsFiltered'] = $count['all'];

		echo json_encode( $response );
		exit;
	}

	/**
	 * ajax change order status
	 * @return [type] [description]
	 */
	public function ajax_change_status_order()
	{
		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$data = json_decode(stripslashes($_POST['data']));

		foreach( $data as $key=>$val ):
			$status = get_post_meta( $val, '_order_status', true);
			if( $status == $_POST['status'] ):
				continue;
			endif;

			update_post_meta( $val, '_order_status', sanitize_text_field( $_POST['status'] ) );
			//update_post_meta( $val, '_order_status_id_update', get_current_user_id() );
			//update_post_meta( $val, '_order_status_date_update', date('Y-m-d H:i:s'));

			if( 'reject' == $_POST['status'] && isset($_POST['alasan'])){
				update_post_meta( $val, '_order_reject_reason', sanitize_text_field( $_POST['alasan'] ) );
			}

			$args = array(
				'user_id' => get_current_user_id(),
				'order_id' => $val,
				'status' => $_POST['status'],
			);
			$log = new Telebotstore_Order_Status_Log($args);
			$log->insert();
			$post = array(
				'ID' => $val,
				'post_modified' => date( 'Y-m-d H:i:s' ),
			);

			wp_update_post( $post );
		endforeach;

		echo 1;
		exit;
	}

	/**
	 * ajax change order status
	 * @return [type] [description]
	 */
	public function ajax_trash_status_order()
	{
		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$data = json_decode(stripslashes($_POST['data']));

		foreach( $data as $key=>$val ):
			wp_trash_post( $val  );
			$args = array(
				'user_id' => get_current_user_id(),
				'order_id' => $val,
				'status' => $_POST['status'],
			);
			// $log = new Telebotstore_Order_Status_Log($args);
			// $log->insert();
			// $post = array(
			//     'ID' => $val,
			//     'post_modified' => date( 'Y-m-d H:i:s' ),
			// );
			//
			// wp_update_post( $post );
		endforeach;

		echo 1;
		exit;
	}

	/**
	 * get all list user
	 * @return [type] [description]
	 */
	public function get_all_user()
	{
		$args = array();

		$users = get_users( $args );

		foreach( $users as $user ):
			$u[$user->ID] = $user->data->display_name;
		endforeach;

		return $u;
	}

	/**
	 * get all items list
	 * @return [type] [description]
	 */
	public function get_all_items()
	{
		$args = array(
			'post_type' => 'telebotstore_item',
			'posts_per_page' => -1,
			'post_status' => 'publish',
		);

		$posts = get_posts( $args );

		$items = array();
		foreach( (array) $posts as $item ):
			$items[$item->post_name] = $item->post_name;
		endforeach;

		return $items;
	}

	/**
	 * add onesignal script to wp_head
	 * hooked by wp_head
	 */
	public function add_onesignal_script()
	{
		if(get_post_type( get_the_ID() ) !== 'telebotstore_order'):
			return;
		endif;
		?>
		<link rel="manifest" href="/manifest.json" />
		<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
		<script>
		var OneSignal = window.OneSignal || [];
		OneSignal.push(function() {
			OneSignal.init({
				appId: '<?php echo carbon_get_theme_option('onesignal_app_id'); ?>',
	  			autoRegister: true,
	  			notifyButton: {
					enable: true,
	  			},
	  			welcomeNotification: {
	  				"title": "pisces.co.id",
	  				"message": "Terima Kasih ! Anda telah subscribe dan akan mendapat notifikasi",
	  				// "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
	  			}
			});
		});
		</script>
		<?php
	}

	/**
	 * send comment notification with OneSignal
	 * hooked via comment_post
	 * @return [type] [description]
	 */
	public function send_comment_notif( $comment_ID, $commentdata )
	{
		$comment = get_comment($comment_ID);

		$title      = 'Komentar Baru dari '.$comment->comment_author;
	    $content    = $comment->comment_content.' >> KLIK DISINI untuk membalas.';
		$url        = get_the_permalink($comment->comment_post_ID);

		Telebotstore_OneSignal::send($title,$content,$url);

	}

	/**
	 * ajax export to excel
	 * @return [type] [description]
	 */
	public function ajax_export_excel()
	{
		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$upload_dir   = wp_upload_dir();
		$filename = date('y-m-d-H-i-s').'-orders.xlsx';


		$post = $_POST;
		$post['start'] = 0;
		$post['length'] = -1;

		$header = array(
			'Order ID'     => 'string',
			'User'         => 'string',
			'Tanggal'      => 'string',
			'Toko'         => 'string',
			'Tempo'        => 'string',
			'Di Kirim Via' => 'string',
			'Status'       => 'string',
			'Item'         => 'string',
			'Quantity'     => 'string',
			'Price @'      => 'string',
			'Sub Total'    => 'string',
			'Total'        => 'string',
			'Link'         => 'string',
		);

		$styles = array( 'font'=>'Arial','font-size'=>11,'font-style'=>'bold', 'fill'=>'#eee', 'border'=>'left,right,top,bottom');

		$data_orders = Telebotstore_Order::get_order_list($post);

		$orders = array();

		foreach( $data_orders as $key => $val ):
			$items = get_post_meta($val[0], '_order_items', false);
			$subtotal = $itemss = array();
			foreach( $items as $key=>$vals ):
				$value = explode('|', $vals);
				$item = Telebotstore_Item::get_by_slug( $value[0] );
				$subtotal[] = carbon_get_post_meta($item->ID, 'item_price') * $value[1];
				$itemss[] = array(
					'-',
					'-',
					'-',
					'-',
					'-',
					'-',
					'-',
					$value[0],
					$value[1],
					carbon_get_post_meta($item->ID, 'item_price'),
					carbon_get_post_meta($item->ID, 'item_price') * $value[1],
					'-',
				);
			endforeach;
			$orders[] = array(
				$val[0],
				$val[1]['username'],
				$val[2],
				$val[3]['name'],
				$val[4],
				$val[5],
				$val[6],
				'-',
				'-',
				'-',
				'-',
				array_sum($subtotal),
				$val[7]
			);
			foreach( $itemss as $items ):
				$orders[] = $items;
			endforeach;
		endforeach;

		$writer = new XLSXWriter();
		$writer->writeSheetHeader('Sheet1', $header, $styles);
		foreach($orders as $row):
		  $writer->writeSheetRow('Sheet1', $row );
		endforeach;

		$writer->writeToFile( $upload_dir['basedir'].'/'.$filename);

		$response['url'] = site_url() . '/wp-content/uploads/' . $filename;
		echo json_encode($response);
		exit;
	}

	/**
	 * ajax export to spreadsheet
	 * @return [type] [description]
	 */
	public function ajax_export_spreadsheet()
	{
		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		//$sheet_name = date('Y m d H:i:s');

		$post = $_POST;
		$post['start'] = 0;
		$post['length'] = -1;

		$emails = explode(',', $post['email']);

		if($emails && !is_email($emails[0]) ):

			echo '<label>Result :</label><br/><strong>Warning!</strong> Invalid Email.';
			exit;
		endif;

		$orders = array();

		$orders[] = array(
			'Order ID',
			'User',
			'Tanggal',
			'Toko',
			'Tempo',
			'Di Kirim Via',
			'Status',
			'Item',
			'Quantity',
			'Price @',
			'Sub Total',
			'Total',
			'Link',
		);

		$data_orders = Telebotstore_Order::get_order_list($post);

		foreach( $data_orders as $key => $val ):
			$items = get_post_meta($val[0], '_order_items', false);
			$subtotal = $itemss = array();
			foreach( $items as $key=>$vals ):
				$value = explode('|', $vals);
				$item = Telebotstore_Item::get_by_slug( $value[0] );
				$subtotal[] = carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1];
				$itemss[] = array(
					'-',
					'-',
					'-',
					'-',
					'-',
					'-',
					'-',
					$value[0],
					$value[1],
					carbon_get_post_meta($item->ID, 'item_price'),
					carbon_get_post_meta($item->ID, 'item_price') * (int)$value[1],
					'-',
				);
			endforeach;
			$orders[] = array(
				$val[0],
				$val[1]['username'],
				$val[2],
				$val[3]['name'],
				$val[4],
				$val[5],
				$val[6],
				'-',
				'-',
				'-',
				'-',
				array_sum($subtotal),
				$val[7]
			);
			foreach( $itemss as $items ):
				$orders[] = $items;
			endforeach;
		endforeach;

		$line = count($orders);

		$client = new \Google_Client();
		$client->setApplicationName('Pisces Telebot Store');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->addScope('https://www.googleapis.com/auth/drive');
		$client->addScope('https://www.googleapis.com/auth/drive.appdata');

		$jsonAuth = file_get_contents(carbon_get_theme_option('google_sheet_api'));
		$client->setAuthConfig(json_decode($jsonAuth, true));

		$sheets = new \Google_Service_Sheets($client);
		$drive = new Google_Service_Drive($client);

		$properties = new \Google_Service_Sheets_SpreadsheetProperties();
		$properties->setTitle($post['title'].' '.date('Y-m-d H:i:s'));

		$newSpreadsheet = new Google_Service_Sheets_Spreadsheet();
		$newSpreadsheet->setProperties($properties);

		$response = $sheets->spreadsheets->create($newSpreadsheet);

		foreach( (array) $emails as $email ):
			if( !is_email($email) ):
				continue;
			endif;

			$userPermission = new Google_Service_Drive_Permission(
				array(
					'type' => 'user',
					'role' => 'writer',
					'emailAddress' => $email,
				)
			);

			$drive->permissions->create($response->spreadsheetId, $userPermission, array('fields' => 'id'));
		endforeach;

		// $body = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(
		// 	array('requests' => array(
		// 		'addSheet' => array(
		// 			'properties' => array(
		// 				'title' => $sheet_name
		// 			)
		// 		)
		// 	)
		// ));
		//
		// $sheets->spreadsheets->batchUpdate($spreadsheetId, $body);

		$range = 'Sheet1!A1:M'.$line;

		$updateBody = new \Google_Service_Sheets_ValueRange([
			'range' => $range,
			'majorDimension' => 'ROWS',
			'values' => $orders,
		]);
		$sheets->spreadsheets_values->update(
			$response->spreadsheetId,
			$range,
			$updateBody,
			['valueInputOption' => 'USER_ENTERED']
		);

		echo '<label>Result :</label><br/><strong>Success!</strong> <a href="'.$response->spreadsheetUrl.'">'.$response->spreadsheetUrl.'</a>';
		exit;
	}

	public function insert_comment_tag_data( $comment_ID, $commentdata )
	{
		$comment_parent = $_POST['comment_parent'];
		$users = isset($_POST['commenttag']) ? $_POST['commenttag'] : '';

		$order = new Telebotstore_Order();

		$order->updated( sanitize_text_field($_POST['comment_post_ID']) );

		$post = array(
		    'ID' => $_POST['comment_post_ID'],
		    'post_modified' => date( 'Y-m-d H:i:s' ),
		);

		wp_update_post( $post );

		if( $comment_parent == 0 ):

			if( $users ):
				foreach( $users as $key=>$user_id):
					$data = array(
						'user_id' => $user_id,
						'comment_post_ID' => $_POST['comment_post_ID'],
						'comment_id' => $comment_ID,
					);

					$comment_tag = new Telebotstore_Comments_Tag($data);
					$comment_tag->insert();
				endforeach;
			endif;
		else :
			$data = array(
				'user_id' => get_current_user_id(),
				'comment_post_ID' => $_POST['comment_post_ID'],
				'comment_id' => $comment_parent,
			);

			$comment_tag = new Telebotstore_Comments_Tag($data);
			$comment_tag->replied();

			if( $users ):
				foreach( $users as $key=>$user_id):
					$data = array(
						'user_id' => $user_id,
						'comment_post_ID' => $_POST['comment_post_ID'],
						'comment_id' => $comment_ID,
					);

					$comment_tag = new Telebotstore_Comments_Tag($data);
					$comment_tag->insert();
				endforeach;
			endif;
		endif;

		$ticket = get_post($_POST['comment_post_ID']);

		if( $ticket && $ticket->post_type == 'telebotstore_ticket' ):

			$data = array(
				'user_id' => get_current_user_id(),
				'comment_post_ID' => $_POST['comment_post_ID'],
				'comment_id' => -1,
			);

			$comment_tag = new Telebotstore_Comments_Tag($data);
			$comment_tag->replied();

		endif;
	}

	public function ajax_get_single_order()
	{
		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		$order = get_post($_POST['data']);

		$args = array(
			//'user_id' => get_current_user_id(),
			'order_id' => $_POST['data'],
		);
		$logger = new Telebotstore_Order_Status_Log($args);
		$logs = $logger->get();

		$data_logs = array();

		if($logs):
			foreach( (array) $logs as $log ):
				$data_logs[] = array(
					'status' => ucfirst($log->status),
					'updated_by' => get_the_author_meta( 'first_name', $log->user_id ).' '.get_the_author_meta( 'last_name', $log->user_id ),
					'updated_date' => $log->datetime,
					'reason' => $log->status == 'reject' ? 'Reason : '.get_post_meta( $order->ID, '_order_reject_reason', true) : '',
				);
			endforeach;
		endif;

		$updated_id = get_post_meta( $_POST['data'], '_order_status_id_update', true);

		if($updated_id):
			$updated = array(
				'update' => true,
				'status' => ucfirst(get_post_meta($_POST['data'], '_order_status', true)),
				'updated_by' => get_the_author_meta( 'first_name', $updated_id ).' '.get_the_author_meta( 'last_name', $updated_id ),
				'updated_date' => get_post_meta( $_POST['data'], '_order_status_date_update', true),
			);
		else:
			$updated = array(
				'update' => false,
				'status' => '',
				'updated_by' => '',
				'updated_date' => '',
			);
		endif;

		$comments = get_comments(['post_id' => $_POST['data']]);
		$data_comments = array();

		if($comments):
			foreach($comments as $comment):
				$comment_tag = new Telebotstore_Comments_Tag(array('comment_id'=> $comment->comment_ID));

				$notifs = $comment_tag->get_tagged_user();

		        $tagged = '';
		        foreach( $notifs as $notif ):
		            $user = get_userdata($notif->user_id);
		            $tagged .= '<span>'.$user->display_name.'</span>,&nbsp;';
		        endforeach;

				$data_comments[] = array(
					'comment_id'           => $comment->comment_ID,
					'comment_author'       => $comment->comment_author,
					'comment_author_email' => $comment->comment_author_email,
					'comment_author_id'    => $comment->user_id,
					'comment_content'      => $comment->comment_content,
					'comment_parent'       => $comment->comment_parent,
					'comment_avatar'       => get_avatar_url($comment->comment_author_email),
					'comment_date'         => $comment->comment_date,
					'comment_reply_link'   => get_the_permalink($_POST['data']).'?replytocom='.$comment->comment_ID.'#respond',
					'comment_tagged' => $tagged,
				);
			endforeach;
		endif;


		$toko_id = get_post_meta($_POST['data'], '_order_toko', true);
		if($toko_id):
			$provider = get_post_meta($toko_id, '_provider', true);
			if($provider == 'api'):
				$toko = sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), carbon_get_post_meta($toko_id, 'name') );
				$alamat = carbon_get_post_meta($toko_id, 'address');
			else :
				$toko = sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), get_the_title($toko_id) );
				$alamat = apply_filters('the_content', get_post_field('post_content', $toko_id));
			endif;
		else :
			$toko = '';
			$alamat = '';
		endif;

		$items = get_post_meta( $_POST['data'], '_order_items', false);
		$data_items = array();

		$subtotal = array();

		foreach( $items as $key=>$val ):
			$value = explode('|', $val);
			$item = Telebotstore_Item::get_by_slug( $value[0] );
			$subtotal[] = carbon_get_post_meta($item->ID, 'item_price') * $value[1];
			$data_items[] = array(
				'item' => $item->post_title,
				'qty'  => $value[1],
				'price' => number_format(carbon_get_post_meta($item->ID, 'item_price'),0,',','.'),
				'sub' => number_format(carbon_get_post_meta($item->ID, 'item_price') * $value[1],0,',','.'),
			);
		endforeach;

		$total = array_sum($subtotal);

		$response = array(
			'permalink'    => get_the_permalink($_POST['data']),
			'order_id'     => '#'.$_POST['data'],
			'tempo'        => get_post_meta($_POST['data'], '_order_tempo', true),
			'pengiriman'   => get_post_meta($_POST['data'], '_order_pengiriman', true),
			'mapping'      => $toko_id ? true : false,
			'toko'         => $toko,
			'alamat'       => $alamat,
			'nomapping'    => get_post_meta($_POST['data'], '_order_store', true),
			'items'        => $data_items,
			'total'        => number_format($total,0,',','.'),
			'author'       => get_the_author_meta( 'first_name', $order->post_author ).' '.get_the_author_meta( 'last_name', $order->post_author ),
			'created_date' => $order->post_date,
			'update'       => $updated,
			'logs'         => $data_logs,
			'comments'     => $data_comments,
		);

		echo json_encode($response);
		exit;
	}

	public function get_month_list()
	{
		$args = array(
			'post_type' => 'telebotstore_order',
			'posts_per_page' => 1,
			'orderby' => 'ID',
			'order' => 'ASC',
		);

		$q = new WP_Query();
		$orders = $q->query($args);

		return $orders;
	}

	public function upload($file, $output = 'url'){

	    if( empty($file) ) return false;

	    if ( ! function_exists( 'wp_handle_upload' ) ) {
	        require_once( ABSPATH . 'wp-admin/includes/file.php' );
	    }

	    if( !in_array($file['type'], ['image/jpeg', 'image/png']) ) return false;

	    $upload_overrides = array( 'test_form' => false );
	    $uploaded = wp_handle_upload( $file, $upload_overrides );

	    return $uploaded[$output];
	}

	public function uploadS3($file, $output = 'url'){

	    if( empty($file) || !isset($file['type']) ) return false;

		if( !in_array($file['type'], ['image/jpeg', 'image/png']) ) return false;

		$photo = isset($file['tmp_name']) ? $file['tmp_name'] : false;

		if( $photo == false ) return;

		$image = new \Gumlet\ImageResize($photo);
		$image->resizeToWidth(700);
		$image->save($photo);

		if( file_exists($photo) ){
			$s3 = new Telebotstore_S3();
			$path = date('Y/m').'/'.time().'-'.$file['name'];
			$upload = $s3->upload($photo,$path);
			if( isset($upload['status']) && $upload['status'] == 'SUCCESS' ){
				return $upload['url'];
			}
		}

	    return '';
	}

	public function custom_field($type, $name, $value = '', $options = false){

		$options = explode(',',$options);

		switch($type):
		    case 'text':
			    $field = '<input type="text" class="form-control" name="'.$name.'" value="'.$value.'">';
				break;
			case 'textarea':
			    $field = '<textarea class="form-control" name="'.$name.'">'.$value.'</textarea>';
				break;
			case 'file':
			    $field = '<input type="file" class="form-control" name="'.$name.'" value="'.$value.'">';
				break;
			case 'select':
			    $field = '<select class="form-control" name="'.$name.'">';
				foreach( (array)$options as $key=>$val ):
					$selected = strtolower($val) == $value ? 'selected="selected"' : '';
					$field .= '<option value="'.strtolower($val).'" '.$selected.'>'.$val.'</option>';
				endforeach;
				$field .= '</select>';
				break;
			case 'radio':
			    $field = '';
				foreach( (array)$options as $key=>$val ):
					$checked = strtolower($val) == $value ? 'checked="checked"' : '';
					$field .= '<label><input type="radio" name="'.$name.'" value="'.strtolower($val).'" '.$checked.'>'.ucfirst($val).'&nbsp;&nbsp;</label>';
				endforeach;
				break;
			case 'checkbox':
			    $checked = strtolower($val) == $value ? 'checked="checked"' : '';
			    $field = '<input type="checkbox" name="'.$name.'" value="'.strtolower($options[0]).'" '.$checked.'>'.ucfirst($options[0]);
				break;
			default:
			    break;
		endswitch;

		return $field;

	}

	/**
	 * ajax get insight
	 * @return [type] [description]
	 */
	public function ajax_get_insight(){

		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

			$args = array(
				'select'  => 'visit_datetime,learned',
				'page'    => $_GET['page'],
				'limit'   => 5,
				'store'   => $_GET['store_id'],
				'user_id' => $_GET['user_id']
			);

			$query = new Telebotstore_Query_Checkin($args);

			$data = array();

			foreach( (array) $query->query() as $p ):
				$data[] = array(
					'date'    => $p->visit_datetime,
					'learned' => $p->learned,
				);
			endforeach;

			if( $query->total() > $_GET['page'] * $args['limit'] ):
				$more = $_GET['page'] + 1;
			else :
				$more = '';
			endif;

			$response['data'] = $data;
			$response['more'] = $more;

			echo json_encode($response);
			exit;
		endif;
		exit;

	}


	/**
	 * ajax get contact
	 * @return html
	 */
	public function ajax_get_contact(){


		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

		$args = array('toko_id' => $_REQUEST['store_id']);
		
		$Telebotstore_Query_Toko_Contact = new Telebotstore_Query_Toko_Contact($args);
		
		$query = $Telebotstore_Query_Toko_Contact->query();

		?>
			<a href="javascript:;"  data-toggle="collapse" data-target="#detail-contact" aria-expanded="false" aria-controls="detail-contact">
		    	<?= __('Lihat','telebostore'); ?>
		 	</a>
			<div class="collapse" id="detail-contact">
			  <div class="card card-body">
			  	<div class="table-responsive">
			    <table class="table ">
			    	<thead>
			    		<tr>
			    			<th>#</th>
			    			<th>Nama</th>
			    			<th>Phone</th>
			    			<th>Tgl Lahir</th>
			    			<th>Hobi</th>
			    		</tr>
			    	</thead>
			    	<tbody>
			    		<?php
			    			$no = 1;
			    			foreach($query as $row){
			    				?>
			    				<tr>
			    					<td><?= $no; ?></td>
			    					<td><?= $row->first_name.' '.$row->last_name; ?></td>
			    					<td><?= $row->phone; ?></td>
			    					<td><?= $row->birth_day; ?></td>
			    					<td><?= $row->hobby; ?></td>
			    				</tr>
			    				<?php
			    				$no++;
			    			}
			    		?>
			    	</tbody>
			    </table>
				</div>
			  </div>
			</div>
		<?php

		endif;
		wp_die();
	}

	public function ajax_get_ordercsi(){

		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

			$args = array(
				'page'    => $_GET['page'],
				'limit'   => 5,
				'toko_id' => $_GET['store_id'],
				'user_id' => $_GET['user_id']
			);

			$query = new Telebotstore_Query_Toko_Order_Csi($args);

			$data = array();

			foreach( (array) $query->query() as $p ):
				$data[] = array(
					'date'    => $p->posting_date,
					'data' => 'No : '.$p->entry_no.' | Document No : '.$p->document_no. ' | Item No : '.$p->item_no.' | Quantity : .'.$p->quantity.' | Price : '.$p->price.' | Location Code : '.$p->location_code.' | Item Category : '.$p->item_category.' | Manufature Code : '.$p->manufacture_code.' | Sales (+PPN) : '.$p->sales_inc_ppn.' | Sales (tanpa PPN) : '.$p->sales_exc_ppn,
				);
			endforeach;

			if( $query->total() > $_GET['page'] * $args['limit'] ):
				$more = $_GET['page'] + 1;
			else :
				$more = '';
			endif;

			$response['data'] = $data;
			$response['more'] = $more;

			echo json_encode($response);
			exit;
		endif;
		exit;

	}

	public function ajax_get_telegram(){

		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

			$start = (intval($_GET['page']) - 1 ) * 5;

			$args = array(
				'start'  => $start,
				'length' => 5,
				'store' => $_GET['store_id'],
			);

			$query = Telebotstore_Order::get_order_list($args);

			$data = array();

			foreach( (array) $query as $p ):

			    $items = get_post_meta( $p[0], '_order_items', false);
			    $data_items = array();

			    $subtotal = array();

			    foreach( $items as $key=>$val ):
			        $value = explode('|', $val);
			        $item = Telebotstore_Item::get_by_slug( $value[0] );
			        $subtotal[] = carbon_get_post_meta($item->ID, 'item_price') * $value[1];
			        $data_items[] = 'Item : '.$item->post_title.'('.$value[1].') @Rp '.number_format(carbon_get_post_meta($item->ID, 'item_price'),0,',','.');
			    endforeach;

			    $data[] = array(
			        'date'    => 'Tanggal : '.$p[2].'<br/>',
			        'data' => implode('<br/>', $data_items).'<br/> Total : Rp '.number_format(array_sum($subtotal),0,',','.').'<br/>Status Order: '.ucfirst(get_post_meta($p[0], '_order_status', true)),
			    );

			endforeach;

			if( Telebotstore_Order::count_order_list() > intval($_GET['page']) * intval($args['length']) ):
				$more = $_GET['page'] + 1;
			else :
				$more = '';
			endif;

			$response['data'] = $data;
			$response['more'] = $more;

			echo json_encode($response);
			exit;
		endif;
		exit;

	}

	public function ajax_get_visit_list(){

		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

			$c = new Telebotstore_Query_Checkin();

			$today_data = $c->count_visit_today();
			$yesterday_data = $c->count_visit_yesterday();
			$before_yesterday_data = $c->count_visit_day_before_yesterday();

			$data = array();

			foreach( (array)$today_data as $t ):
			    $append = array(
			        'user_id' => $t->user_id,
			        'visit_today' => $t->visit_count
			    );
			    $data[$t->user_id] = isset($data[$t->user_id]) ? array_merge($data[$t->user_id], $append) : $append;
			endforeach;

			foreach( (array)$yesterday_data as $y ):
			    $append = array(
			        'user_id' => $y->user_id,
			        'visit_yesterday' => $y->visit_count
			    );

			    $data[$y->user_id] = isset($data[$y->user_id]) ? array_merge($data[$y->user_id], $append) : $append;
			endforeach;

			foreach( (array)$before_yesterday_data as $b ):
			    $append = array(
			        'user_id' => $b->user_id,
			        'visit_before_yesterday' => $b->visit_count
			    );
			    $data[$b->user_id] = isset($data[$b->user_id]) ? array_merge($data[$b->user_id], $append) : $append;
			endforeach;

			$visit = array();

			foreach( (array) $data as $d ):

				$user = get_userdata($d['user_id']);

				$total_today = Telebotstore_Order::get_total_order_today(['user_id' => $d['user_id']]);
				$total_yesterday = Telebotstore_Order::get_total_order_yesterday(['user_id' => $d['user_id']]);
				$total_before_yesterday = Telebotstore_Order::get_total_order_before_yesterday(['user_id' => $d['user_id']]);

			    $visit[] = array(
			        [
						'id' => isset($d['user_id']) ? $d['user_id'] : '',
						'name' => isset($d['user_id']) ? $user->first_name.' '.$user->last_name : 0,
					],
			        isset($d['visit_today']) ? $d['visit_today'].'/'.$total_today : '-',
			        isset($d['visit_yesterday']) ? $d['visit_yesterday'].'/'.$total_yesterday : '-',
			        isset($d['visit_before_yesterday']) ? $d['visit_before_yesterday'].'/'.$total_before_yesterday : '-',
			    );

			endforeach;


			$response['data']            = $visit;
			$response['recordsTotal']    = count($visit);
			$response['recordsFiltered'] = count($visit);

			echo json_encode( $response );
			exit;
		endif;
		exit;

	}

	public function ajax_get_visit_lists(){

		$data_exclude_users = carbon_get_theme_option('telebotstore_checkin_exclude_user');
		$exclude_users = array();

		foreach( (array)$data_exclude_users as $data ){
			$exclude_users[] = intval($data['id']);
		}

		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

			$date = explode(' - ', $_GET['date']);

			$args = array(
				'user_id'    => intval($_GET['user_id']),
				'limit'      => $_GET['length'],
				'offset'     => $_GET['start'],
				'date_start' => isset($date[0]) ? $date[0] : '',
				'date_end'   => isset($date[1]) ? $date[1] : '',
			);

			if( isset($_GET['date']) && $_GET['date'] ){

				$date = explode(' - ', $_GET['date']);

				$args['date_start'] = isset($date[0]) ? $date[0] : '';
				$args['date_end'] = isset($date[1]) ? $date[1] : '';
			}

			if( isset($_GET['toko'])){
				$args['store'] = intval($_GET['toko']);
			}

			$c = new Telebotstore_Query_Checkin($args);

			$data = $c->count_visits();

			$visit = array();

			foreach( (array) $data as $d ):

				$user = get_userdata($d->user_id);

				if( in_array($user->ID, $exclude_users) ) continue;

				$total_order = Telebotstore_Order::get_total_order_by_user($user->ID, $args);

			    $visit[] = array(
			        [
						'id' => isset($d->user_id) ? $d->user_id : '',
						'name' => isset($d->user_id) ? $user->first_name.' '.$user->last_name : 0,
					],
			        isset($d->visit_count) ? $d->visit_count.'/'.$total_order : '-',
			    );

			endforeach;


			$response['data']            = $visit;
			$response['recordsTotal']    = count($visit);
			$response['recordsFiltered'] = count($visit);

			echo json_encode( $response );
			exit;
		endif;
		exit;

	}

	public function ajax_get_visit_list_user(){

		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

			$date = explode(' - ', $_GET['date']);

			$args = array(
				'user_id'    => $_GET['user_id'],
				'limit'      => $_GET['length'],
				'offset'     => $_GET['start'],
				'date_start' => isset($date[0]) ? $date[0] : '',
				'date_end'   => isset($date[1]) ? $date[1] : '',
			);

			$c = new Telebotstore_Query_Checkin($args);

			$custom_fields = carbon_get_theme_option('checkin_detail_fields');

			$visit = array();

			foreach( (array) $c->query() as $d ):

				$store = get_post($d->store);

				$loc = json_decode(stripslashes_deep($d->location), true);

				$custom_fields_data = unserialize($d->detail);

				$detail = array();

				foreach( (array) $custom_fields as $cf ):
					$detail[] = array(
						'label' => $cf['label'],
						'value' => isset($custom_fields_data[$cf['name']]) ? $custom_fields_data[$cf['name']] : '',
					);
				endforeach;

				$data = array(
					'date_visit' => $d->visit_datetime,
					'date_revisit' => $d->revisit_datetime,
					'customer' => array(
						'link' => get_the_permalink($d->store),
						'name' => $store->post_title,
					),
					'learned' => $d->learned,
					'photo' => $d->photo,
					'location' => array(
						'address' => $loc[0]['formatted_address'],
						'lat' => $loc[0]['geometry']['location']['lat'],
						'lng' => $loc[0]['geometry']['location']['lng'],
					),
					'detail' => $detail,
				);

			    $visit[] = array(
			        $d->visit_datetime,
			        $store->post_title,
			        json_encode($data),
			    );

			endforeach;


			$response['data']            = $visit;
			$response['recordsTotal']    = $c->total();
			$response['recordsFiltered'] = $c->total();

			echo json_encode( $response );
			exit;
		endif;
		exit;

	}

	public function ajax_visit_user_export_excel()
	{
		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit;

		$upload_dir   = wp_upload_dir();
		$user = get_userdata(intval($_POST['user']));

		$filename = 'export-date-'.date('y-m-d-H-i-s').'-visit-sales-'.$user->first_name.'.xlsx';

		$custom_fields = carbon_get_theme_option('checkin_detail_fields');

		$header = array(
			'Sales'    => 'string',
			'Customer' => 'string',
			'Visit'    => 'string',
			'Revisit'  => 'string',
			'Learned'  => 'string',
			'Photo'    => 'string',
			'Location' => 'string',
		);

		foreach( (array) $custom_fields as $cf ):
			$header[$cf['label']] = 'string';
		endforeach;

		$styles = array( 'font'=>'Arial','font-size'=>11,'font-style'=>'bold', 'fill'=>'#eee', 'border'=>'left,right,top,bottom');

		$date = explode(' - ', $_POST['date']);

		$args = array(
			'user_id'    => $_POST['user'],
			'limit'      => 9999999,
			'offset'     => 0,
			'date_start' => isset($date[0]) ? $date[0] : '',
			'date_end'   => isset($date[1]) ? $date[1] : '',
		);

		$c = new Telebotstore_Query_Checkin($args);

		$exports = array();

		foreach( (array) $c->query() as $d ):

			$store = get_post($d->store);

			$loc = json_decode(stripslashes_deep($d->location), true);

			$custom_fields_data = unserialize($d->detail);

			$detail = $data = array();

			foreach( (array) $custom_fields as $cf ):
				$detail[] = isset($custom_fields_data[$cf['name']]) ? $custom_fields_data[$cf['name']] : '';
			endforeach;

			$data = array(
				$user->first_name.' '.$user->last_name,
				$store->post_title,
				$d->visit_datetime,
				$d->revisit_datetime,
				$d->learned,
				$d->photo,
				$loc[0]['formatted_address'],
			);

			$exports[] = array_merge($data, $detail);

		endforeach;

		$writer = new XLSXWriter();
		$writer->writeSheetHeader('Sheet1', $header, $styles);
		foreach($exports as $row):
		  $writer->writeSheetRow('Sheet1', $row );
		endforeach;

		$writer->writeToFile( $upload_dir['basedir'].'/'.$filename);

		$response['url'] = site_url() . '/wp-content/uploads/' . $filename;
		echo json_encode($response);
		exit;
	}

	public function ajax_mark_comment_as_solution(){

		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( wp_verify_nonce($nonce, 'telebotstore_nonce') ):

			$commentID = isset($_GET['commentid']) ? sanitize_text_field($_GET['commentid']) : '';
			$postID = isset($_GET['postid']) ? sanitize_text_field($_GET['postid']) : '';

			update_comment_meta($commentID, '_solution', true);
			update_post_meta($postID, '_ticket_status', 'closed');

			echo 1;
			exit;
		endif;
		exit;

	}

	/**
	 * ajax get tickets list
	 * @return [type] [description]
	 */
	public function ajax_get_tickets_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit(1);

		$args = array(
			'limit'    => $_GET['length'],
			'offset'   => $_GET['start'],
			'date'     => $_GET['date'],
			'store'    => $_GET['store'],
			'status'   => $_GET['status'],
			'priority' => $_GET['priority'],
			'user'     => $_GET['user'],
		);

		$tickets = new Telebotstore_Query_Ticket($args);

		$obj = array();

		foreach( (array) $tickets->get() as $post ):
			$user_info = get_userdata($post->post_author);
			$toko_id = get_post_meta( $post->ID, '_ticket_store', true);
			$store = array(
				'name' => '',
				'url' => '',
			);
			if( !empty($toko_id) ):

				$provider = get_post_meta($toko_id, '_provider', true);
				if($provider == 'api'):
					$toko_name = carbon_get_post_meta($toko_id, 'name');
				else :
					$toko_name = get_the_title($toko_id);
				endif;
				$store = array(
					'name' => $toko_name,
					'url' => get_the_permalink($toko_id),
				);
			endif;

			$obj[] = array(
				$post->ID,
				[
					'first_name' => $user_info->first_name,
					'last_name' => $user_info->last_name,
					'username' => $user_info->user_login
				],
				get_the_date('d-M-Y', $post->ID),
				isset($store) ? $store : '',
				ucfirst(get_post_meta( $post->ID, '_ticket_priority', true)),
				ucfirst(get_post_meta( $post->ID, '_ticket_status', true)),
				get_the_permalink($post->ID),
			);
		endforeach;

		$response['data']            = $obj;
		$response['recordsTotal']    = $tickets->count();
		$response['recordsFiltered'] = $tickets->count();

		echo json_encode( $response );
		exit;
	}

	public function ajax_get_toko_contacts_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit(1);

		$args = array(
			'limit'   => $_GET['length'],
			'offset'  => $_GET['start'],
			'name'    => $_GET['name'],
			'email'   => $_GET['email'],
			'phone'   => $_GET['phone'],
			'toko_id' => $_GET['toko'],
		);

		$contacts = new Telebotstore_Query_Toko_Contact($args);

		$obj = array();

		foreach( (array) $contacts->query() as $c ):
			$user_info = get_userdata($post->post_author);
			$toko = get_post($c->toko_id);
			$store = array(
				'name' => $toko->post_title,
				'url' => get_the_permalink($c->toko_id),
			);

			$obj[] = array(
				$c->ID,
				$c->first_name.' '.$c->last_name,
				$c->email,
				$c->phone,
				$store,
				get_the_permalink(carbon_get_theme_option('toko_contact')).'?edit='.$c->ID
			);
		endforeach;

		$response['data']            = $obj;
		$response['recordsTotal']    = $contacts->total();
		$response['recordsFiltered'] = $contacts->total();

		echo json_encode( $response );
		exit;
	}

	public function wp(){

		if ( isset($_POST["save_contact"]) ) :

			$custom_fields = carbon_get_theme_option('toko_contact_detail_fields');

			$fields = array();

			foreach( (array)$custom_fields as $field ):

			    $options = $field['option'] ? $field['option'] : false;

			    $options = $field['checkbox_value'] ? $field['checkbox_value'] : $options;

			    $fields[] = array(
			        'field' => $this->custom_field($field['type'], $field['name'], $options),
			        'label' => $field['label'],
			    );
			endforeach;

			$post = $_POST;

			$customs = array();

			foreach( (array)$custom_fields as $f ):
				if( isset($post[strtolower($f['name'])]) ):
					$customs[strtolower($f['name'])] = $post[strtolower($f['name'])];
				endif;

				if( isset($_FILES[strtolower($f['name'])]) ):
					$customs[strtolower($f['name'])] = $this->upload( $_FILES[strtolower($f['name'])] );
				endif;

			endforeach;

			$args = array(
				'toko_id' => intval($post['store']),
				'first_name'    => sanitize_text_field($post['first_name']),
				'last_name'    => sanitize_text_field($post['last_name']),
				'email'   => sanitize_email($post['email']),
				'handphone_1' => sanitize_text_field($post['handphone_1']),
				'handphone_2' => sanitize_text_field($post['handphone_2']),
				'phone'   => sanitize_text_field($post['phone']),
				'address'   => sanitize_text_field($post['address']),
				'birth_day'   => sanitize_text_field($post['birth_day']),
				'hobby'   => sanitize_text_field($post['hobby']),
				'detail'  => serialize($customs),
			);

			if( isset($post['contact_id']) && $post['contact_id']):
				Telebotstore_Toko_Contact::update($args,['ID' => intval($post['contact_id'])]);

				$contact_id = $post['contact_id'];
			else:
				$contact_id = Telebotstore_Toko_Contact::insert($args);
			endif;

			if($contact_id):
				wp_safe_redirect($_POST['redirect'].'?new=success&edit='.$contact_id);
			else :
				wp_safe_redirect($_POST['redirect'].'?new=error');
			endif;
		endif;

		if ( isset($_POST["delete_contact"]) ) :

			Telebotstore_Toko_Contact::delete(['ID' => intval($_POST['contact_id'])]);

			wp_safe_redirect($_POST['redirect']);
		endif;

	}

	public function ajax_get_ordercsi_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			return;
		endif;

		if( isset($_GET['date']) && $_GET['date'] ):

            $date = explode(' - ', $_GET['date']);

            $from_date = date('Y-m-d', strtotime($date[0]));
            $to_date = date('Y-m-d', strtotime($date[1]));

        else :

            $from_date = '';
            $to_date = '';

        endif;

		if( isset($_GET['sales']) && $_GET['sales'] ):

			$args = array(
				'posts_per_page' => -1,
			    'orderby'     => 'post_date',
			    'order'       => 'DESC',
			    'post_status' => 'publish',
				'post_type'   => 'telebotstore_toko',
				'fields'      => 'ids',
			    'meta_query'  => array(
			        array(
			            'key'     => '_telebotstore_toko_sales|||0|id',
			            'value'   => intval($_GET['sales']),
			            'compare' => '=',
			        ),
			    ),
			);

			$tokos = get_posts( $args );

			$toko_ids = $tokos ? implode(',',$tokos) : '999999999999';
		else :

			$toko_ids = '';

		endif;

		if( isset($_GET['toko']) && !empty($_GET['toko']) ){
			$toko_ids = intval($_GET['toko']);
		}

		$args = array(
			'offset'    => $_GET['start'],
			'limit'     => $_GET['length'],
			'toko_ids'  => $toko_ids,
			'from_date' => $from_date,
			'to_date'   => $to_date,
			'search'    => $_GET['search'],
		);

		$query = new Telebotstore_Query_Toko_Order_Csi($args);

		$data = array();

		foreach( (array) $query->query() as $p ):

			$sales_id = get_post_meta($p->toko_id, '_telebotstore_toko_sales|||0|id', true );

			if( $sales_id ):
				$user_sales = get_userdata( intval($sales_id) );

				$sales = $user_sales->first_name.' '.$user_sales->last_name;

			else:
				$sales = 'NA';
			endif;

			$data[] = array(
				$p->ID,
				$p->entry_no,
				$p->posting_date,
				$p->document_no,
				[
					'url' => get_the_permalink($p->toko_id),
					'name' => get_the_title($p->toko_id) ? get_the_title($p->toko_id) : 'NA',
				],
				$sales,
				$p->ID,
			);
		endforeach;

		$response['data']             = $data;
		$response['totalSalesIncPpn'] = $query->sum_sales_inc_ppn() ? 'Rp '.number_format($query->sum_sales_inc_ppn(),3,',','.') : 0;
		$response['recordsTotal']     = intval($query->total());
		$response['recordsFiltered']  = intval($query->total());

		echo json_encode( $response );
		exit;
	}

	public function ajax_get_ordercsi_single()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' ))return;

		$args = array(
			'ID' => intval($_GET['data']),
		);

		$query = new Telebotstore_Query_Toko_Order_Csi($args);

		$response = $query->query();

		echo json_encode( $response[0] );
		exit;
	}

	public function ajax_delete_ordercsi_single()
	{
		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' ))return;

		$data = json_decode(stripslashes($_POST['data']));

		foreach( (array) $data as $key=>$val ):

			$args = array(
				'ID' => intval($val),
			);

			Telebotstore_Toko_Order_Csi::delete($args);

		endforeach;

		echo 1;
		exit;
	}

	public function ajax_export_ordercsi_list()
	{
		$nonce = isset($_GET['nonce']) ? $_GET['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )):
			echo false;
			exit;
		endif;

		$upload_dir   = wp_upload_dir();
		$user = get_userdata(intval($_POST['user']));

		$filename = 'exported-date-'.date('y-m-d-H-i-s').'-order-csi.xlsx';

		if( isset($_GET['date']) && $_GET['date'] ):

            $date = explode(' - ', $_GET['date']);

            $from_date = date('Y-m-d', strtotime($date[0]));
            $to_date = date('Y-m-d', strtotime($date[1]));

        else :

            $from_date = '';
            $to_date = '';

        endif;

		if( isset($_GET['sales']) && $_GET['sales'] ):

			$args = array(
				'posts_per_page' => -1,
			    'orderby'     => 'post_date',
			    'order'       => 'DESC',
			    'post_status' => 'publish',
				'post_type'   => 'telebotstore_toko',
				'fields'      => 'ids',
			    'meta_query'  => array(
			        array(
			            'key'     => '_telebotstore_toko_sales|||0|id',
			            'value'   => intval($_GET['sales']),
			            'compare' => 'LIKE',
			        ),
			    ),
			);

			$tokos = get_posts( $args );

			$toko_ids = $tokos ? implode(',',$tokos) : '999999999999';
		else :

			$toko_ids = '';

		endif;

		$args = array(
			'offset'    => 0,
			'limit'     => 99999999999,
			'toko_ids'  => $toko_ids,
			'from_date' => $from_date,
			'to_date'   => $to_date,
			'search'    => $_GET['search'],
		);

		$query = new Telebotstore_Query_Toko_Order_Csi($args);

		$header = array(
			'Toko ID'           => 'integer',
			'Entry No'          => 'integer',
			'Posting Date'      => 'date',
			'Document No'       => 'string',
			'Document Type'     => 'string',
			'Csutomer'          => 'string',
			'Salesperson Code'  => 'string',
			'Item No'           => 'string',
			'Quantity'          => 'integer',
			'Price'             => 'string',
			'Location Code'     => 'string',
			'Sales Inc PPN'     => 'string',
			'Item Category'     => 'string',
			'Manufacturer Code' => 'string',
			'City'              => 'string',
			'DLM/LR KOTA'       => 'string',
			'IQ'                => 'string',
			'Sales Exc PPN'     => 'string'
		);

		$data = array();

		foreach( (array) $query->query() as $p ):

			// $sales_id = get_post_meta($p->toko_id, '_telebotstore_toko_sales|||0|id', true );
			//
			// if( $sales_id ):
			// 	$user_sales = get_userdata( intval($sales_id) );
			//
			// 	$sales = $user_sales->first_name.' '.$user_sales->last_name;
			//
			// else:
			// 	$sales = 'NA';
			// endif;

			$data[] = array(
				$p->toko_id,
				$p->entry_no,
				$p->posting_date,
				$p->document_no,
				$p->document_type,
				$p->customer_name,
				$p->salesperson_code,
				$p->item_no,
				$p->quantity,
				$p->price,
				$p->location_code,
				$p->sales_inc_ppn,
				$p->item_category,
				$p->manufacture_code,
				$p->city,
				$p->dlm_lr_kota,
				$p->iq,
				$p->sales_exc_ppn,
			);
		endforeach;

		$styles = array( 'font'=>'Arial','font-size'=>11,'font-style'=>'bold', 'fill'=>'#eee', 'border'=>'left,right,top,bottom');

		$writer = new XLSXWriter();
		$writer->writeSheetHeader('Sheet1', $header, $styles);
		foreach($data as $row):
		  $writer->writeSheetRow('Sheet1', $row );
		endforeach;

		$writer->writeToFile( $upload_dir['basedir'].'/'.$filename);

		$response['url'] = site_url() . '/wp-content/uploads/' . $filename;
		echo json_encode($response);
		exit;
	}

	public function ajax_change_item_status(){

		global $wpdb;

		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit(2);

		$postID = intval($_POST['post_id']);
		$item   = sanitize_text_field($_POST['item']);
		$item_sent = intval($_POST['item_sent']);
		$item_reject = intval($_POST['item_reject']);

		$result = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE post_id = $postID AND meta_value LIKE '%$item%' LIMIT 10 OFFSET 0");

		$item_data = explode('|',$result[0]->meta_value);

		$jumlah = $item_sent + $item_reject;

		if( $jumlah > intval($item_data[1]) ){
			echo json_encode([
				'respons' => 'error',
			]);
			exit;
		}

		if( $item_sent == intval($item_data[1]) || $jumlah == intval($item_data[1])){
			$status = 'done';
		}else if( $item_reject == intval($item_data[1]) ){
			$status = 'reject';
		}else if( $jumlah < intval($item_data[1]) ){
			$status = 'partial';
		}else{
			$status = 'pending';
		}

		$new_item_data = array(
			'meta_value' => $item_data[0].'|'.$item_data[1].'|'.$status.'|'.$item_sent.'|'.$item_reject,
		);

		$wpdb->update( $wpdb->postmeta, $new_item_data, ['meta_id' => $result[0]->meta_id]);

		$items = get_post_meta( $postID, '_order_items', false);
		$item_status = array();

		foreach( (array) $items as $key=>$val ):
			$value = explode('|', $val);
			$item_status[] = isset($value[2]) ? $value[2] : 'pending';
		endforeach;

		$is_pending_exists = in_array('pending', $item_status);
		$is_reject_exists  = in_array('reject', $item_status);
		$is_done_exists    = in_array('done', $item_status);
		$is_partial_exists = in_array('partial', $item_status);


		if( $is_pending_exists && $is_reject_exists && $is_done_exists ):
			$order_status = 'partial';
		elseif( $is_pending_exists && $is_reject_exists && !$is_done_exists ):
			$order_status = 'pending';
		elseif( $is_pending_exists && !$is_reject_exists && $is_done_exists ):
			$order_status = 'partial';
		elseif( $is_pending_exists && !$is_reject_exists && !$is_done_exists ):
			$order_status = 'pending';
		elseif( !$is_pending_exists && $is_reject_exists && $is_done_exists ):
			$order_status = 'done';
		elseif( $is_pending_exists && $is_reject_exists && !$is_done_exists ):
			$order_status = 'reject';
		elseif( !$is_pending_exists && $is_reject_exists && !$is_done_exists ):
			$order_status = 'reject';
		elseif( $is_pending_exists && !$is_reject_exists && $is_done_exists ):
			$order_status = 'partial';
		elseif( !$is_pending_exists && $is_reject_exists && $is_done_exists ):
			$order_status = 'done';
		elseif( !$is_pending_exists && !$is_reject_exists && $is_done_exists ):
			$order_status = 'done';
		endif;

		if( $is_partial_exists ){
			$order_status = 'partial';
		}

		update_post_meta( $postID, '_order_status', sanitize_text_field( $order_status ) );
		//update_post_meta( $val, '_order_status_id_update', get_current_user_id() );
		//update_post_meta( $val, '_order_status_date_update', date('Y-m-d H:i:s'));

		// if( 'reject' == $_POST['status'] && isset($_POST['alasan'])){
		// 	update_post_meta( $val, '_order_reject_reason', sanitize_text_field( $_POST['alasan'] ) );
		// }

		$args = array(
			'user_id' => get_current_user_id(),
			'order_id' => $postID,
			'status' => $order_status,
		);
		$log = new Telebotstore_Order_Status_Log($args);
		$last_log_data = $log->get_last_by_post_id();

		if( !isset($last_log_data->status) || $last_log_data->status !== $order_status ){
			$log->insert();
			$post = array(
				'ID' => $postID,
				'post_modified' => date( 'Y-m-d H:i:s' ),
			);

			wp_update_post( $post );
		}

		echo json_encode([
			'respons' => 'success',
			'item' => $item,
			'status' => ucfirst($status),
		]);
		exit;
	}

	public function ajax_change_item_status_old(){

		global $wpdb;

		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit(2);

		$postID = intval($_POST['post_id']);
		$item   = sanitize_text_field($_POST['item']);
		$status = sanitize_text_field($_POST['status']);

		$result = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE post_id = $postID AND meta_value LIKE '%$item%' LIMIT 10 OFFSET 0");

		$item_data = explode('|',$result[0]->meta_value);

		$new_item_data = array(
			'meta_value' => $item_data[0].'|'.$item_data[1].'|'.$status,
		);

		$wpdb->update( $wpdb->postmeta, $new_item_data, ['meta_id' => $result[0]->meta_id]);

		$items = get_post_meta( $postID, '_order_items', false);
		$item_status = array();

		foreach( (array) $items as $key=>$val ):
			$value = explode('|', $val);
			$item_status[] = isset($value[2]) ? $value[2] : 'pending';
		endforeach;

		$is_pending_exists = in_array('pending', $item_status);
		$is_reject_exists  = in_array('reject', $item_status);
		$is_done_exists    = in_array('done', $item_status);


		if( $is_pending_exists && $is_reject_exists && $is_done_exists ):
			$order_status = 'partial';
		elseif( $is_pending_exists && $is_reject_exists && !$is_done_exists ):
			$order_status = 'pending';
		elseif( $is_pending_exists && !$is_reject_exists && $is_done_exists ):
			$order_status = 'partial';
		elseif( $is_pending_exists && !$is_reject_exists && !$is_done_exists ):
			$order_status = 'pending';
		elseif( !$is_pending_exists && $is_reject_exists && $is_done_exists ):
			$order_status = 'done';
		elseif( $is_pending_exists && $is_reject_exists && !$is_done_exists ):
			$order_status = 'reject';
		elseif( !$is_pending_exists && $is_reject_exists && !$is_done_exists ):
			$order_status = 'reject';
		elseif( $is_pending_exists && !$is_reject_exists && $is_done_exists ):
			$order_status = 'partial';
		elseif( !$is_pending_exists && $is_reject_exists && $is_done_exists ):
			$order_status = 'done';
		elseif( !$is_pending_exists && !$is_reject_exists && $is_done_exists ):
			$order_status = 'done';
		endif;

		update_post_meta( $postID, '_order_status', sanitize_text_field( $order_status ) );
		//update_post_meta( $val, '_order_status_id_update', get_current_user_id() );
		//update_post_meta( $val, '_order_status_date_update', date('Y-m-d H:i:s'));

		// if( 'reject' == $_POST['status'] && isset($_POST['alasan'])){
		// 	update_post_meta( $val, '_order_reject_reason', sanitize_text_field( $_POST['alasan'] ) );
		// }

		$args = array(
			'user_id' => get_current_user_id(),
			'order_id' => $postID,
			'status' => $order_status,
		);
		$log = new Telebotstore_Order_Status_Log($args);
		$log->insert();
		$post = array(
			'ID' => $postID,
			'post_modified' => date( 'Y-m-d H:i:s' ),
		);

		wp_update_post( $post );

		echo json_encode($_POST['id']);
		exit;
	}

	public function ajax_change_item_status_all(){

		global $wpdb;

		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit(2);

		$postID = intval($_POST['post_id']);
		$status = sanitize_text_field($_POST['status']);

		$items = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE post_id = $postID AND  meta_key = '_order_items' LIMIT 100 OFFSET 0");

		foreach( (array) $items as $item ):
			$item_data = explode('|', $item->meta_value);
			$sent = 0;
			$reject = 0;
			if( $status == 'done' ){
				$sent = $item_data[1];
			}else if( $status == 'reject' ){
				$reject = $item_data[1];
			}
			$new_item_data = array(
				'meta_value' => $item_data[0].'|'.$item_data[1].'|'.$status.'|'.$sent.'|'.$reject,
			);

			$wpdb->update( $wpdb->postmeta, $new_item_data, ['meta_id' => $item->meta_id]);
		endforeach;

		update_post_meta( $postID, '_order_status', sanitize_text_field( $status ) );

		$args = array(
			'user_id' => get_current_user_id(),
			'order_id' => $postID,
			'status' => $status,
		);
		$log = new Telebotstore_Order_Status_Log($args);
		$log->insert();
		$post = array(
			'ID' => $postID,
			'post_modified' => date( 'Y-m-d H:i:s' ),
		);

		wp_update_post( $post );

		echo json_encode($status);
		exit;
	}

	public function ajax_change_item_status_all_old(){

		global $wpdb;

		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit(2);

		$postID = intval($_POST['post_id']);
		$items = json_decode(stripslashes($_POST['items']), true);
		$status = sanitize_text_field($_POST['status']);

		foreach( (array) $items as $key=>$item ):

			$result = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE post_id = $postID AND meta_value LIKE '%$item%' LIMIT 10 OFFSET 0");

			$item_data = explode('|',$result[0]->meta_value);

			$new_item_data = array(
				'meta_value' => $item_data[0].'|'.$item_data[1].'|'.$status,
			);

			$wpdb->update( $wpdb->postmeta, $new_item_data, ['meta_id' => $result[0]->meta_id]);

		endforeach;

		update_post_meta( $postID, '_order_status', sanitize_text_field( $status ) );

		$args = array(
			'user_id' => get_current_user_id(),
			'order_id' => $postID,
			'status' => $status,
		);
		$log = new Telebotstore_Order_Status_Log($args);
		$log->insert();
		$post = array(
			'ID' => $postID,
			'post_modified' => date( 'Y-m-d H:i:s' ),
		);

		wp_update_post( $post );

		echo json_encode($status);
		exit;
	}

	public function do_insert_checkin(){

        if ( isset($_POST["save_checkin"]) ) :

            $post = $_POST;

            $customs = array();

            foreach( (array)$custom_fields as $f ):
                if( isset($post[strtolower($f['name'])]) ):
                    $customs[strtolower($f['name'])] = $post[strtolower($f['name'])];
                endif;

                if( isset($_FILES[strtolower($f['name'])]) ):
                    $customs[strtolower($f['name'])] = $this->upload( $_FILES[strtolower($f['name'])] );
                endif;

            endforeach;

            $args = array(
                'visit_datetime'   => sanitize_text_field($post['visit_datetime']),
                'store'            => sanitize_text_field($post['store']),
                'learned'          => sanitize_text_field($post['learned']),
                'photo'            => $this->uploadS3($_FILES['photo']),
                'revisit_datetime' => sanitize_text_field($post['revisit_datetime']),
                'location'         => sanitize_text_field($post['location']),
                'user_id'          => get_current_user_id(),
                'detail'           => serialize($customs),
            );
            // telebotstore_debug($_FILES['photo']);
            // telebotstore_debug($args);

            $checkin_id = Telebotstore_Checkin::insert($args);

            if($checkin_id):

                $result = 'success';               
            else :
               $result = 'failed';
            endif;

            $nonce = wp_create_nonce(date('H:i'));
            wp_redirect(get_permalink().'?submit='.$result.'&nonce='.$nonce);
            exit();

        endif;
    }


    public function ajax_load_add_store(){
    	include TELE_TEMPLATES.'/store/form.php';
        die();
    }


    public function ajax_action_add_store(){

    	global $wpdb;

    	//$Telebotstore_Checkin_Log = new $Telebotstore_Checkin_Log;
    	
    	$location = $_POST['location'];
    	$coordinates = Telebotstore_Checkin_Log::get_coordinate_location($location);
    	$ex = explode(',', $coordinates);
    	$lat = $ex[0];
    	$long = $ex[1];

    	$data = array(
    				'post_title' => $_POST['nama'],
    				'post_type' => 'telebotstore_toko',
    				'post_author' => get_current_user_id(),
    				'post_status' => 'publish'
    			);
    	$id = wp_insert_post($data);

    	if($id){
	    	update_post_meta($id,'_telebotstore_toko_kategori',$_POST['kategori']);
	    	update_post_meta($id,'_telebotstore_toko_address',$_POST['alamat']);
	    	update_post_meta($id,'_telebotstore_toko_phone',$_POST['phone']);
	    	update_post_meta($id,'_telebotstore_toko_latitude',$lat);
	    	update_post_meta($id,'_telebotstore_toko_longitude',$long);

	    	$result = 'success';
	    	$message = '<div class="alert alert-success">Tambah Data Sukses. Silahkan Cari di Form Nama Toko</div>';
	    }else{
	    	$result = 'failed';
	    	$message = '<div class="alert alert-danger">Tambah Data Gagal! Ulangi Kembali</div>';
	    }

	    $response = array('result' => $result,'message' => $message, 'id' => $id);

	    echo json_encode($response);
	    die();
    }
}
