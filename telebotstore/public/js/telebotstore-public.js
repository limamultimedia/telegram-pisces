/*
Title: Cozeit More plugin by Yasir Atabani
Documentation: na
Author: Yasir O. Atabani
Website: http://www.cozeit.com
Twitter: @yatabani

MIT License, https://github.com/cozeit/czMore/blob/master/LICENSE.md
*/
(function ($, undefined) {
    $.fn.czMore = function (options) {

        //Set defauls for the control
        var defaults = {
            max: 5,
            min: 0,
            onLoad: null,
            onAdd: null,
            onDelete: null
        };
        //Update unset options with defaults if needed
        var options = $.extend(defaults, options);
        $(this).bind("onAdd", function (event, data) {
            options.onAdd.call(event, data);
        });
        $(this).bind("onLoad", function (event, data) {
            options.onLoad.call(event, data);
        });
        $(this).bind("onDelete", function (event, data) {
            options.onDelete.call(event, data);
        });
        //Executing functionality on all selected elements
        return this.each(function () {
            var obj = $(this);
            var i = obj.children(".recordset").size();
            var divPlus = '<div id="btnPlus" />';
            var count = '<input id="' + this.id + '_czMore_txtCount" name="' + this.id + '_czMore_txtCount" type="hidden" value="0" size="5" />';

            obj.before(count);
            var recordset = obj.children("#first");
            obj.after(divPlus);
            var set = recordset.children(".recordset").children().first();
            var btnPlus = obj.siblings("#btnPlus");

            btnPlus.css({
                'float': 'none',
                'border': '0px',
                'background-position': 'center center',
                'background-repeat': 'no-repeat',
                'height': '25px',
                'width': '100px',
                'cursor': 'pointer',
				'margin': '20px'
            });

            if (recordset.length) {
                obj.siblings("#btnPlus").click(function () {
                    var i = obj.children(".recordset").size();
                    var item = recordset.clone().html();
                    i++;
                    item = item.replace(/\[([0-9]\d{0})\]/g, "[" + i + "]");
                    item = item.replace(/\_([0-9]\d{0})\_/g, "_" + i + "_");
                    //$(element).html(item);
                    //item = $(item).children().first();
                    //item = $(item).parent();

                    obj.append(item);
                    loadMinus(obj.children().last());
                    minusClick(obj.children().last());
                    if (options.onAdd != null) {
                        obj.trigger("onAdd", i);
                    }

                    obj.siblings("input[name$='czMore_txtCount']").val(i);
                    return false;
                });
                recordset.remove();
                for (var j = 0; j <= i; j++) {
                    loadMinus(obj.children()[j]);
                    minusClick(obj.children()[j]);
                    if (options.onAdd != null) {
                        obj.trigger("onAdd", j);
                    }
                }

                if (options.onLoad != null) {
                    obj.trigger("onLoad", i);
                }
                //obj.bind("onAdd", function (event, data) {
                //If you had passed anything in your trigger function, you can grab it using the second parameter in the callback function.
                //});
            }

            function resetNumbering() {
                $(obj).children(".recordset").each(function (index, element) {
                   $(element).find('input:text, input:password, input:file, select, textarea').each(function(){
                        old_name = this.name;
                        new_name = old_name.replace(/\_([0-9]\d{0})\_/g, "_" + (index + 1) + "_");
                        this.id = this.name = new_name;
                        //alert(this.name);
                    });
                    index++
                    minusClick(element);
                });
            }

            function loadMinus(recordset) {
                var divMinus = '<div id="btnMinus" />';
                $(recordset).children().first().before(divMinus);
                var btnMinus = $(recordset).children("#btnMinus");
                btnMinus.css({
                    'float': 'right',
                    'border': '0px',
                    'background-position': 'center center',
                    'background-repeat': 'no-repeat',
                    'height': '25px',
                    'width': '50px',
                    'cursor': 'pointer',
					'margin-top': '7px'
                });
            }

            function minusClick(recordset) {

                var values = 0;
                if( $('.prices') ){
                    $('.prices').each(function(index, invoice){
                        values = values + parseInt($(invoice).attr('data-price'));
                    });
                }

                $('#totale').val(format_number(values));

                $(recordset).children("#btnMinus").click(function () {
                    var i = obj.children(".recordset").size();
                    var id = $(recordset).attr("data-id")
                    $(recordset).remove();
                    resetNumbering();
                    obj.siblings("input[name$='czMore_txtCount']").val(obj.children(".recordset").size());
                    i--;
                    if (options.onDelete != null) {
                        if (id != null)
                            obj.trigger("onDelete", id);
                    }
                });
            }
        });
    };
})(jQuery);

(function( $ ) {
	'use strict';

	var ordertable;
	var logtable;
	var commenttable;
	var rejecttable;
	var pendingtable;
	var taggedtable;
    var visittable;
    var ticketstable;
    var tokoContactTable;
    var visitUserTable;
    var visitsTable;
    var orderCsiTable;

	$(document).ready(function(){

        let date = new Date();

        $('#totale').val('');

		var user_via = [];
		$('.user_pengiriman').each(function(){
			user_via.push( $(this).val() );
		});

		console.log(user_via);

		var page = $("#store-page").val();
		$("#date-order").val("");
		if( 'yes' == page ){

		}else {
			$("#store-order").val("");
		}
		$("#status-order").val("");
		$("#item-order").val("");
		$("#user-order").val("");
		$("#tempo-order").val("");
		// if(user_via > 0 ){
		// 	console.log('bbbb');
		// } else {
		// 	console.log('ccc');
		// 	$("#via-order").val("");
		// }

		$('#date-order').daterangepicker({
            autoUpdateInput: false,
            maxDate: new Date(),
			locale: {
				format:'DD-MM-YYYY'
			}
        }, function(start_date, end_date, label) {
            this.element.val(start_date.format('DD-MM-YYYY')+' - '+end_date.format('DD-MM-YYYY'));
		});

        $('.datetimepicker').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            //autoUpdateInput: false,
            minDate: new Date(),
			locale: {
				format:'YYYY-MM-DD hh:mm'
			}
        }, function(start_date, end_date, label) {
            this.element.val(start_date.format('YYYY-MM-DD hh:mm'));
		});

        $('.datepicker').daterangepicker({
            singleDatePicker: true,
            timePicker: false,
            showDropdowns: true,
            maxYear: date.getFullYear,
            autoUpdateInput: false,
            //minDate: new Date(),
			locale: {
				format:'YYYY-MM-DD'
			}
        }, function(start_date, end_date, label) {
            this.element.val(start_date.format('YYYY-MM-DD'));
		});

		ordertable = $('#order-list').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_order_list";
					d.nonce    = phpjs.nonce;
					d.date     = $("#date-order").val();
					d.store    = $("#store-order").val();
					d.status   = $("#status-order").val();
					d.item     = $("#item-order").val();
					d.tempo    = $("#tempo-order").val();
					d.via      = $("#via-order").val();
					d.user     = $("#user-order").val();
					//d.user_via = user_via;
				}
			},
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('data-id', data[0]);
            },
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
					return '<input type="checkbox" class="allcheck" name="order[]" value="'+data+'"/>';
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data.first_name +' '+data.last_name+'<br>@'+data.username;
				}
			},
			{
				targets: 2,
				width : '82px',
			},
			{
				targets: 3,
				width : '82px',
				render: function ( data, type, row ) {
					if( data.url ){
						return '<a target="_blank" href="'+data.url+'">'+data.name+'</a>';
					} else {
						return data.name;
					}
				}
			},
			{
				targets: 4,
				width : '82px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 5,
				width : '80px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 6,
				width : '40px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 7,
				width : '40px',
				render: function ( data, type, row ) {
					return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-default">Detail</a></div>';
				}
			}],
            "initComplete": function(settings, json) {
                console.log('completed')
            }
		});

		taggedtable = $('#tagged-list').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_tagged_list";
					d.nonce    = phpjs.nonce;
					d.user     = $("#user-order").val();
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
					return data;
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 2,
				width : '82px',
			},
			{
				targets: 3,
				width : '40px',
				render: function ( data, type, row ) {
					return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-default">Detail</a></div>';
				}
			}]
		});

		logtable = $('#log-list').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action = "get_log_list";
					d.nonce  = phpjs.nonce;
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
					return '<input type="checkbox" class="allcheck" name="order[]" value="'+data+'"/>';
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function( data, type, row ){
					return data.first_name+' '+data.last_name+' On : '+data.date;
				}
			},
			{
				targets: 2,
				width : '200px',
				render: function( data, type, row ){
					if( data.date ){
						return data.status+' Updated By : '+data.first_name+' '+data.last_name+' On : '+data.date;
					}else{
						return data.status;
					}
				}
			},
			{
				targets: 3,
				width : '40px',
				render: function ( data, type, row ) {
					return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-default">Detail</a></div>';
				}
			}]
		});

		rejecttable = $('#reject-list').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action = "get_reject_list";
					d.nonce  = phpjs.nonce;
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '50px',
				render: function( data, type, row ){
                    var first = '@'+data.username+'<br>'+data.date;
					return first;
				}
			},
			{
				targets: 1,
				width : '150px',
				render: function( data, type, row ){
					if(data.url){
						return '<a href="'+data.url+'" target="_blank">'+data.name+'</a>';
					}else{
						return data.name;
					}
				}
			},
			{
				targets: 2,
				width : '40px',
				render: function( data, type, row ){
					return data;
				}
			},
			{
				targets: 3,
				width : '40px',
				render: function( data, type, row ){
					return data;
				}
			},
            {
				targets: 4,
				width : '150px',
				render: function( data, type, row ){
					return data;
				}
			},
			{
				targets: 5,
				width : '150px',
				render: function ( data, type, row ) {
					if( data.is_admin == 'yes' ){
						return '<div style="text-align:right"><button class="btn btn-info rejects" ids="'+data.id+'" onclick="return confirm(\'Reopen, Are you sure?\');">Reopen</button><a href="'+data.link+'" target="_blank" class="btn btn-primary">Detail</a><button class="btn btn-default trash" ids="'+data.id+'" onclick="return confirm(\'CR, Are you sure?\');">CR</button></div>';

					}else {
						return '<div style="text-align:right"><a href="'+data.link+'" target="_blank" class="btn btn-primary">Detail</a></div>';
					}

				}
			}]
		});

		pendingtable = $('#pending-list').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action = "get_pending_list";
					d.nonce  = phpjs.nonce;
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
					return '<input type="checkbox" class="allcheck" name="order[]" value="'+data+'"/>';
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function( data, type, row ){
					return data.first_name+' '+data.last_name+' On : '+data.date;
				}
			},
			{
				targets: 2,
				width : '200px',
				render: function( data, type, row ){
					return data;
				}
			},
			{
				targets: 3,
				width : '40px',
				render: function ( data, type, row ) {
					return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-primary">Detail</a></div>';
				}
			}]
		});

		commenttable = $('#comment-list').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action = "get_comment_list";
					d.nonce  = phpjs.nonce;
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '100px',
				render: function( data, type, row ){
					return data;
				}
			},
			{
				targets: 1,
				width : '40px',
				render: function( data, type, row ){
					return '#'+data;
				}
			},
			{
				targets: 2,
				width : '100px',
				render: function( data, type, row ){
					return data;
				}
			},
			{
				targets: 3,
				width : '400px',
				render: function ( data, type, row ) {
					return '<strong>'+data.toko+'</strong><br/>'+data.comment;
				}
			},
			{
				targets: 4,
				width : '40px',
				render: function ( data, type, row ) {
					return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-primary">Detail</a></div>';
				}
			}]
		});

		$("#order-filter").on('click', function(){
			ordertable.ajax.reload();
			taggedtable.ajax.reload();
		});

		$('.select-all').on('change', function(){
			if( $(this).is(':checked') ){
				$('.allcheck').attr('checked', 'checked');
			} else {
				$('.allcheck').removeAttr('checked');
			}
		});

		$('#order-pending').on('click', function(){
			var checked = [];
			$('.allcheck:checked').each(function(){
				checked.push( $(this).val() );
			});
			if( checked < 1){
				alert( 'Pilih Order yang akan di ubah setatusnya' );
			}else {
				$(this).html('Proses..');
				$.ajax({
					type: "POST",
					url: phpjs.ajax_url,
					dataType: "json",
					data: {
						action: 'change_order_status',
						nonce: phpjs.nonce,
						data: JSON.stringify(checked),
						status: 'pending',
					},
					success: function(data){
						$('#order-pending').html('Pending');
	                    ordertable.ajax.reload();
						logtable.ajax.reload();
						rejecttable.ajax.reload();
					}
				});
			}
		});

		$('#order-done').on('click', function(){
			var checked = [];
			$('.allcheck:checked').each(function(){
				checked.push( $(this).val() );
			});
			console.log(checked);
			if( checked < 1){
				alert( 'Pilih Order yang akan di ubah setatusnya' );
			}else {
				$(this).html('Proses..');
				$.ajax({
					type: "POST",
					url: phpjs.ajax_url,
					dataType: "json",
					data: {
						action: 'change_order_status',
						nonce: phpjs.nonce,
						data: JSON.stringify(checked),
						status: 'done',
					},
					success: function(data){
						$('#order-done').html('Done');
	                    ordertable.ajax.reload();
						logtable.ajax.reload();
						rejecttable.ajax.reload();
					}
				});
			}
		});

        $('#order-reject-modal').on('click', function(){
            $('#reject-reason').val('');
            $('#order-reject').attr('disabled','disabled');
            var checked = [];
			$('.allcheck:checked').each(function(){
				checked.push( $(this).val() );
			});
			if( checked < 1){
				alert( 'Pilih Order yang akan di ubah setatusnya' );
			}else {
                $('#myModal').modal('toggle');
            }
        });

        $('#reject-reason').on('keyup', function(){
            console.log( $(this).val().length);
            var length_text = $(this).val().length;

            if( length_text > 5){
                $('#order-reject').removeAttr('disabled');
            }else{
                $('#order-reject').attr('disabled','disabled');
            }
        })

		$('#order-reject').on('click', function(){
            var checked = [];
			$('.allcheck:checked').each(function(){
				checked.push( $(this).val() );
			});
            var reason = $('#reject-reason').val();
            if( reason ){
                $('#order-reject-modal').html('Proses..');
                $.ajax({
                    type: "POST",
                    url: phpjs.ajax_url,
                    dataType: "json",
                    data: {
                        action: 'change_order_status',
                        nonce: phpjs.nonce,
                        data: JSON.stringify(checked),
                        status: 'reject',
                        alasan: reason,
                    },
                    success: function(data){
                        $('#order-reject-modal').html('Reject');
                        ordertable.ajax.reload();
                        logtable.ajax.reload();
                        rejecttable.ajax.reload();
                    }
                });
            }else{
                alert("Reject Reason is required");
            }
		});

		$('#order-export').on('click', function(){
			$(this).html('Proses..');

			$.ajax({
				type: "POST",
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
					action: 'export_orders',
					nonce: phpjs.nonce,
					date: $("#date-order").val(),
					store: $("#store-order").val(),
					status: $("#status-order").val(),
					item: $("#item-order").val(),
					tempo: $("#tempo-order").val(),
					via: $("#via-order").val(),
					user: $("#user-order").val(),
				},
				success: function(data){
					$('#order-export').html('Export To excel');
					console.log(data.url);
					window.open(data.url, '_blank');
				}
			});
		});

        $('#order-export-spreadsheet').on('click', function(){
            var title = $("#spreadsheet-title").val();
            var email = $("#spreadsheet-email").val();
            $('.sheet-title').hide();
            $('.sheet-email').hide();

            if( title == 0 ){
                $('.sheet-title').show();
            } else if( email == 0 ) {
                $('.sheet-email').show();
            } else {

                $(this).attr("disabled", "disabled").html('Proses..');
                $.ajax({
    				type: "POST",
    				url: phpjs.ajax_url,
    				data: {
    					action: 'export_orders_spreadsheet',
    					nonce: phpjs.nonce,
    					date: $("#date-order").val(),
    					store: $("#store-order").val(),
    					status: $("#status-order").val(),
    					item: $("#item-order").val(),
    					tempo: $("#tempo-order").val(),
    					via: $("#via-order").val(),
    					user: $("#user-order").val(),
                        title: title,
                        email: email,
    				},
    				success: function(data){
    					$('#order-export-spreadsheet').removeAttr('disabled').html('Send Now');
                        $('.sheet-result').show().html(data);
    				}
    			});
            }
		});

		$('#store-order').select2({
			ajax: {
				url: phpjs.ajax_url,
				dataType: "json",
				data: function(params){
					var query = {
						key: params.term,
						action: 'search_toko',
                        page: params.page || 1
					}

					return query;
				},
			}
		});

        $('#comment-tag').select2({
			ajax: {
				url: phpjs.ajax_url,
				dataType: "json",
				data: function(params){
					var query = {
						key: params.term,
						action: 'search_user',
						page: params.page || 1,
					}

					return query;
				},
			}
		});

		$('#reject-list').on('click', '.rejects', function(){
			let id = $(this).attr('ids');
			var ids = [];
			ids.push(id);
			$(this).html('Proses..');
			$.ajax({
				type: "POST",
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
					action: 'change_order_status',
					nonce: phpjs.nonce,
					data: JSON.stringify(ids),
					status: 'pending',
				},
				success: function(data){
					ordertable.ajax.reload();
					logtable.ajax.reload();
					rejecttable.ajax.reload();
				}
			});
		});

		$('#reject-list').on('click', '.trash', function(){
			let id = $(this).attr('ids');
			var ids = [];
			ids.push(id);
			$(this).html('Proses..');
			$.ajax({
				type: "POST",
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
					action: 'change_order_status',
					nonce: phpjs.nonce,
					data: JSON.stringify(ids),
					status: 'cr',
				},
				success: function(data){
					ordertable.ajax.reload();
					logtable.ajax.reload();
					rejecttable.ajax.reload();
				}
			});
		});

		$('#new-order-toko').select2({
			ajax: {
				url: phpjs.ajax_url,
				dataType: "json",
				data: function(params){
					var query = {
						key: params.term,
						action: 'search_toko',
                        page: params.page || 1
					}

					return query;
				},
			}
		});

       dropdown_search_toko();

        $('.search-toko-select2').on('select2:select', function(e){

            var store_id = $(this).val();

            /*
            *custom by limamultimedia April 21
            **/
            $.ajax({
				url: phpjs.ajax_url,
				dataType: "html",
				data: {
                    action: 'get_contact',
                    store_id: store_id,
                    nonce: phpjs.nonce,
                },
                success: function(response){
                    
                    $('#list-contact').html(response);
                },
			});

			/** End custom **/

            $.ajax({
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
                    action: 'get_insight',
                    page: '1',
                    user_id: $('#user_id').val(),
                    store_id: store_id,
                    nonce: phpjs.nonce,
                },
                success: function(response){
                    var li = '';
                    $.each(response.data, function(key,val){
                        li += '<li class="list-group-item">'+val.date+' : '+val.learned+'</li>';
                    });

                    if( response.more ){
                        li += '<li style="cursor:pointer" class="list-group-item insight-loadmore" onclick="insight_load_more(\''+response.more+'\',\''+store_id+'\');">Load More</li>';
                    }

                    $('.insight').empty();
                    $('.insight').append(li);
                },
			});

            $.ajax({
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
                    action: 'get_ordercsi',
                    page: '1',
                    user_id: $('#user_id').val(),
                    store_id: store_id,
                    nonce: phpjs.nonce,
                },
                success: function(response){
                    var li = '';
                    $.each(response.data, function(key,val){
                        li += '<li class="list-group-item">'+val.date+' '+val.data+'</li>';
                    });

                    if( response.more ){
                        li += '<li style="cursor:pointer" class="list-group-item ordercsi-loadmore" onclick="ordercsi_load_more(\''+response.more+'\',\''+store_id+'\');">Load More</li>';
                    }

                    $('.ordercsi').empty();
                    $('.ordercsi').append(li);
                },
			});

            $.ajax({
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
                    action: 'get_telegram',
                    page: '1',
                    user_id: $('#user_id').val(),
                    store_id: store_id,
                    nonce: phpjs.nonce,
                },
                success: function(response){
                    var li = '';
                    $.each(response.data, function(key,val){
                        li += '<li class="list-group-item">'+val.date+val.data+'</li>';
                    });

                    if( response.more ){
                        li += '<li style="cursor:pointer" class="list-group-item telegram-loadmore" onclick="telegram_load_more(\''+response.more+'\',\''+store_id+'\');">Load More</li>';
                    }

                    $('.telegram').empty();
                    $('.telegram').append(li);
                },
			});

            $('.stores').show();
        });

		$("#czContainer").czMore({
			// maximum number of form fields allowed
		    max: 95,

		    // minimum number of form fields allowed
		    min: 1,

		    // callback functions
		    onLoad: null,
		    onAdd: function(number){
                // $('.qtys').addClass('qtys-'+number);
                // $('.prices').addClass('price-'+number);

                var no = number - 1;

                var field = $('.new-order-item');
                var qty = $('.qtys');
                var prices = $('.prices');

                if( typeof field[no] !== 'undefined' ) {
                    field[no].id = number ;
                    qty[no].id = "qty-"+number ;
                    qty[no].setAttribute("data-qty", number);
                    prices[no].id = "price-"+number ;
                }

				$('.new-order-item').select2({
					ajax: {
						url: phpjs.ajax_url,
						dataType: "json",
						data: function(params){
							var query = {
								key: params.term,
								action: 'search_item'
							}

							return query;
						},
					},
				}).on('select2:select', function(e){
                    console.log(e);
                    var data = e.params.data;
                    var qty = $('#qty-'+this.id).val();
                    var price = data.price * qty;
                    $('#price-'+this.id).val(format_number(price));
                    $('#price-'+this.id).attr('data-price', price);
                    $(this).attr('data-price', data.price);

                    hitung_total();
                });

                $('.qtys').on('input', function(){
                    var id = this.getAttribute('data-qty');
                    var val = $(this).val();
                    var price = $('#'+id).attr('data-price');

                    var newPrice = price * val;
                    $('#price-'+id).val(format_number(newPrice));
                    $('#price-'+id).attr('data-price', newPrice);

                    hitung_total();
                });
			},
		    onDelete: function(e){
                hitung_total();
            }

		});

        $('.popupClose').on('click', function(){
            $('#orderModal').hide();
        });

        window.onkeydown = function( event ) {
            if ( event.keyCode == 27 ) {
                $('#orderModal').hide();
            }
        };

        $('#order-list tbody').on('click', 'tr', function(){
            $('.popupLoader').show();
            $('.popupContent').hide();
            $('#orderModal').hide();
			let id = $(this).attr('data-id');
            $('#order-list tbody tr').removeAttr('style');
            $('#orderModal').show();
			$.ajax({
				type: "POST",
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
					action: 'get_single_order',
					nonce: phpjs.nonce,
					data: id,
				},
				success: function(data){
                    $('#order-list tbody tr[data-id="'+id+'"]').css({'background-color':'#337ab7','color':'#ffffff'});
                    $('#popupOrderId').html(data.order_id);
                    $('#popupTempo').html(data.tempo);
                    $('#popupPengiriman').html(data.pengiriman);
                    $('.popupPermalink').attr('href', data.permalink);
                    if(data.mapping == true){
                        $('.no-mapping').hide();
                        $('.mapping').show();
                        $('#popupToko').html(data.toko);
                        $('#popupAlamat').html(data.alamat);
                    }else {
                        $('.no-mapping').show();
                        $('.mapping').hide();
                        $('#popupNoMapping').html(data.nomapping);
                    }
                    if(data.items){
                        var tr = '';
                        $.each(data.items, function(key,val){
                            tr += '<tr><td>'+val.item+'</td><td>'+val.qty+'</td><td>'+val.price+'</td><td>'+val.sub+'</td></tr>';
                        });

                        $('.table-item tbody').empty().append(tr);
                    }

                    var logs = '<li class="list-group-item d-flex justify-content-between align-items-center">Created : <span style="font-weight:bold">'+data.author+'</span> On : <span style="font-weight:bold">'+data.created_date+'</span></li>';

                    if(data.update.update == true){
                        logs += '<li class="list-group-item d-flex justify-content-between align-items-center">Change Status : <span style="font-weight:bold">'+data.update.status+'</span> By <span style="font-weight:bold">'+data.update.updated_by+'</span> On : <span style="font-weight:bold">'+data.update.updated_date+'</span></li>'
                    }

                    if(data.logs){
                        $.each(data.logs, function(key,val){
                            logs += '<li class="list-group-item d-flex justify-content-between align-items-center">Change Status : <span style="font-weight:bold">'+val.status+'</span> By <span style="font-weight:bold">'+val.updated_by+'</span> On : <span style="font-weight:bold">'+val.updated_date+'</span> '+val.reason+'</li>';
                        });
                    }

                    if(data.total){
                        $('.totales').html(data.total);
                    }

                    $('#popupLogs').empty().append(logs);

                    if(data.comments){
                        var comment = '';
                        $.each(data.comments, function(key,val){
                            comment += '<li id="comment-'+val.comment_id+'" class="comment" style="margin-bottom:30px;"><div class="media-left"><a href="" class="media-object"><img alt="" src="'+val.comment_avatar+'" class="avatar avatar-50 photo" width="50" height="50"></a></div><div class="media-body"><h4 class="media-heading">'+val.comment_author+'</h4><time datetime="'+val.comment_date+'">'+val.comment_date+'</time><div class="comment-content"><p>'+val.comment_content+'</p><div class="comment-tagged">Tagged: '+val.comment_tagged+'</div></div><!-- .comment-content --><ul class="list-inline comment-reply"><li class="reply-link"><a rel="nofollow" class="comment-reply-link" href="'+val.comment_reply_link+'" target="_blank" aria-label="Reply to '+val.comment_author+'">Reply</a></li></ul></div></li>';
                        });

                        $('#popupComment').empty().append(comment);
                    }

                    $('.popupLoader').hide();
                    $('.popupContent').show();
				}
			});
		});

        visittable = $('#visit-list').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 50,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_visit_list";
					d.nonce    = phpjs.nonce;
					d.store    = $("#store").val();
					d.sales     = $("#sales").val();
				}
			},
            "createdRow": function( row, data, dataIndex ) {
                //$(row).attr('data-id', data[0]);
            },
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
                    if(data.id){
						return '<a href="?user_id='+data.id+'" target="_blank">'+data.name+'</a>';
					}else{
						return data.name;
					}
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 2,
				width : '82px',
			},
			{
				targets: 3,
				width : '82px',
				render: function ( data, type, row ) {
					return data;
				}
			}],
            "initComplete": function(settings, json) {
                console.log('completed')
            }
		});

        visitUserTable = $('#visit-list-user').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_visit_list_user";
					d.nonce    = phpjs.nonce;
					d.date    = $("#visitFilterDate").val();
                    d.user_id    = $("#visitFilterUser").val();
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
                    return data;
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 2,
				width : '82px',
                render: function ( data, type, row ) {
					return '<div style="text-align:right"><button class="btn btn-default" onclick="visitUserModal(\''+btoa(data)+'\');">Detail</button></div>';
				}
			}],
            // "initComplete": function(settings, json) {
            //     console.log('completed')
            // }
		});

        $('#visitFilterDate').daterangepicker({
            autoUpdateInput: false,
            maxDate: new Date(),
			locale: {
				format:'YYYY-MM-DD'
			}
        }, function(start_date, end_date, label) {
            this.element.val(start_date.format('YYYY-MM-DD')+' - '+end_date.format('YYYY-MM-DD'));
            visitUserTable.ajax.reload();
		});

        visitsTable = $('#visit-lists').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_visit_lists";
					d.nonce    = phpjs.nonce;
					d.date    = $("#visitsFilterDate").val();
                    d.toko    = $("#visitsFilterToko").val();
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
                    if(data.id){
						return '<a href="?user_id='+data.id+'" target="_blank">'+data.name+'</a>';
					}else{
						return data.name;
					}
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data;
				}
			}],
            // "initComplete": function(settings, json) {
            //     console.log('completed')
            // }
		});

        $('#visitsFilterDate').daterangepicker({
            autoUpdateInput: false,
            maxDate: new Date(),
			locale: {
				format:'YYYY-MM-DD'
			}
        }, function(start_date, end_date, label) {
            this.element.val(start_date.format('YYYY-MM-DD')+' - '+end_date.format('YYYY-MM-DD'));
            visitsTable.ajax.reload();
		});

        $('#visitsFilterToko').select2({
			ajax: {
				url: phpjs.ajax_url,
				dataType: "json",
				data: function(params){
					var query = {
						key: params.term,
						action: 'search_toko',
                        page: params.page || 1
					}

					return query;
				},
			}
		});

        $('#visitsFilterToko').on('select2:select', function(e){
            visitsTable.ajax.reload();
        });

        $('#visitUserExport').on('click', function(){
			$(this).html('Proses..').attr('disabled', true);

			$.ajax({
				type: "POST",
				url: phpjs.ajax_url,
				dataType: "json",
				data: {
					action: 'visit_user_export_excel',
					nonce: phpjs.nonce,
					date: $("#visitFilterDate").val(),
                    user: $("#visitFilterUser").val(),
				},
				success: function(data){
					$('#visitUserExport').html('Export To excel').attr('disabled', false);
					window.open(data.url, '_blank');
				}
			});
		});

        ticketstable = $('#tickets').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_tickets_list";
					d.nonce    = phpjs.nonce;
					d.date     = $("#date-ticket").val();
					d.store    = $("#store-ticket").val();
					d.status   = $("#status-ticket").val();
					d.priority = $("#priority-ticket").val();
					d.user     = $("#user-ticket").val();
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
					return '<input type="checkbox" class="allcheck" name="order[]" value="'+data+'"/>';
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data.first_name +' '+data.last_name+'<br>@'+data.username;
				}
			},
			{
				targets: 2,
				width : '82px',
			},
			{
				targets: 3,
				width : '82px',
				render: function ( data, type, row ) {
                    if(data.url){
						return '<a href="'+data.url+'" target="_blank">'+data.name+'</a>';
					}else{
						return data.name;
					}
				}
			},
			{
				targets: 4,
				width : '82px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 5,
				width : '80px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 6,
				width : '40px',
				render: function ( data, type, row ) {
					return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-default">Detail</a></div>';
				}
			}],
            // "initComplete": function(settings, json) {
            //     console.log('completed')
            // }
		});

        $("#ticket-filter").on('click', function(){
			ticketstable.ajax.reload();
		});

        tokoContactTable = $('#toko-contacts').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_toko_contacts_list";
					d.nonce    = phpjs.nonce;
					d.name     = $("#contact-name").val();
					d.email    = $("#contact-email").val();
					d.phone   = $("#contact-phone").val();
					d.toko = $("#contact-toko").val();
				}
			},
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
					return '<input type="checkbox" class="allcheck" name="order[]" value="'+data+'"/>';
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 2,
				width : '82px',
			},
			{
				targets: 3,
				width : '82px',
			},
			{
				targets: 4,
				width : '82px',
				render: function ( data, type, row ) {
                    if(data.url){
						return '<a href="'+data.url+'" target="_blank">'+data.name+'</a>';
					}else{
						return data.name;
					}
				}
			},
			{
				targets: 5,
				width : '40px',
				render: function ( data, type, row ) {
					return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-default">Detail</a></div>';
				}
			}],
            "initComplete": function(settings, json) {
                console.log('completed')
            }
		});

        $("#contact-filter").on('click', function(){
			tokoContactTable.ajax.reload();
		});

        orderCsiTable = $('#order-csi-list').DataTable({
            "processing": true,
			"serverSide": true,
			"searching": false,
			"lengthChange": false,
			"pageLength": 20,
			"ajax": {
				"url" : phpjs.ajax_url,
				"type" : "GET",
				"data" : function(d){
					d.action   = "get_ordercsi_list";
					d.nonce    = phpjs.nonce;
					d.date     = $("#orderCsiFilterDate").val();
					d.sales    = $("#OrderCsiFilterSales").val();
					d.search   = $("#orderCsiFilterSearch").val();
                    d.toko = $("#orderCsiFilterToko").val();
				}
			},
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('data-id', data[0]);
            },
			"columnDefs": [
			{
				targets: 0,
				width : '20px',
				render: function( data, type, row ){
					return '<input type="checkbox" class="allcheck" name="ordercsi[]" value="'+data+'"/>';
				}
			},
			{
				targets: 1,
				width : '100px',
				render: function ( data, type, row ) {
					return data;
				}
			},
            {
				targets: 2,
				width : '82px',
				render: function ( data, type, row ) {
					return data;
				}
			},
            {
				targets: 3,
				width : '82px',
				render: function ( data, type, row ) {
					return data;
				}
			},
            {
				targets: 4,
				width : '82px',
				render: function ( data, type, row ) {
					if( data.url ){
						return '<a target="_blank" href="'+data.url+'">'+data.name+'</a>';
					} else {
						return data.name;
					}
				}
			},
            {
				targets: 5,
				width : '82px',
				render: function ( data, type, row ) {
					return data;
				}
			},
			{
				targets: 6,
				width : '40px',
				render: function ( data, type, row ) {
					let details = '<button class="btn btn-default" onclick="orderCsiModal('+data+');">Detail</button>';

                    //let deletes = '<span onclick="orderCsiDelete('+data+');" class="glyphicon glyphicon-trash" style="cursor:pointer"></span>';

                    let userrole = $('#userrole').val();

                    if( userrole == 1 ){
                        return '<div style="text-align:right">'+details+'</div>';
                    }else{
                        return '<div style="text-align:right">'+details+'</div>';
                    }
				}
			}],
            "initComplete": function(settings, json) {
                $('.OrderCsiTotalIncPpn').html(json.totalSalesIncPpn);
            }
		});

        $("#orderCsiFilterSearch").on('keyup', function(){
			orderCsiTable.ajax.reload( function(json){
                $('.OrderCsiTotalIncPpn').html(json.totalSalesIncPpn);
            });
		});

        $("#OrderCsiFilterSales").change( function(e){
            orderCsiTable.ajax.reload( function(json){
                $('.OrderCsiTotalIncPpn').html(json.totalSalesIncPpn);
            });
            if( e.target.value ){
                $("#orderCsiFilterToko").val('').prop('disabled', true);
            }else{
                $("#orderCsiFilterToko").prop('disabled', false);
            }
		});

        $('#orderCsiFilterToko').select2({
			ajax: {
				url: phpjs.ajax_url,
				dataType: "json",
				data: function(params){
					var query = {
						key: params.term,
						action: 'search_toko',
                        page: params.page || 1
					}

					return query;
				},
			}
		});

        $('#orderCsiFilterToko').on('select2:select', function(e){
            if( e.target.value ){
                $("#OrderCsiFilterSales").val('').prop('disabled', true);
            }
            orderCsiTable.ajax.reload( function(json){
                $('.OrderCsiTotalIncPpn').html(json.totalSalesIncPpn);
            });
        });

        $('#orderCsiFilterDate').daterangepicker({
            autoUpdateInput: false,
            maxDate: new Date(),
			locale: {
				format:'DD-MM-YYYY'
			}
        }, function(start_date, end_date, label) {
            this.element.val(start_date.format('DD-MM-YYYY')+' - '+end_date.format('DD-MM-YYYY'));
            orderCsiTable.ajax.reload( function(json){
                $('.OrderCsiTotalIncPpn').html(json.totalSalesIncPpn);
            });
		});

        $("#orderCsiFilterClear").on('click', function(){
            $('#orderCsiFilterDate').val('');
            $('#OrderCsiFilterSales').val('').prop('disabled', false);
            $('#orderCsiFilterSearch').val('');
            $('#orderCsiFilterToko').val('').prop('disabled', false);
            orderCsiTable.ajax.reload( function(json){
                $('.OrderCsiTotalIncPpn').html(json.totalSalesIncPpn);
            });
		});

        $("#orderCsiDelete").on('click', function(){

            var checked = [];
			$('.allcheck:checked').each(function(){
				checked.push( $(this).val() );
			});

			if( checked < 1){
				alert( 'Pilih Order yang akan di ubah setatusnya' );
			}else {
                console.log(checked);

                let konfirm = confirm('anda Yakin ingin menghapus data yang di pilih?');

                if( konfirm == true ){

                    $(this).html('Process').attr('disabled','disabled');

                    jQuery.ajax({
        				type: "POST",
        				url: phpjs.ajax_url,
        				dataType: "json",
        				data: {
        					action: 'delete_ordercsi_single',
        					nonce: phpjs.nonce,
        					data: JSON.stringify(checked),
        				},
        				success: function(data){
                            $('#orderCsiDelete').html('Delete Selected Data').removeAttr('disabled');
                            orderCsiTable.ajax.reload( function(json){
                                $('.OrderCsiTotalIncPpn').html(json.totalSalesIncPpn);
                            });
        				}
                    });
                }
            }
		});



		// $('#reject-list').on('click', '.trash', function(){
		// 	let id = $(this).attr('ids');
		// 	var ids = [];
		// 	ids.push(id);
		// 	$(this).html('Proses..');
		// 	$.ajax({
		// 		type: "POST",
		// 		url: phpjs.ajax_url,
		// 		dataType: "json",
		// 		data: {
		// 			action: 'trash_order_status',
		// 			nonce: phpjs.nonce,
		// 			data: JSON.stringify(ids),
		// 		},
		// 		success: function(data){
		// 			ordertable.ajax.reload();
		// 			logtable.ajax.reload();
		// 			rejecttable.ajax.reload();
		// 		}
		// 	});
		//})

	});

    var hitung_total = function(){

        var values = 0;
        $('.prices').each(function(index, invoice){
            values = values + parseInt($(invoice).attr('data-price'));
        });

        $('#totale').val(format_number(values));
    }

})( jQuery );

var insight_load_more = function(page, store_id){

    jQuery('.insight-loadmore').text('Loading...');

    var user_id = jQuery('#user_id').val();

    jQuery.ajax({
        url: phpjs.ajax_url,
        dataType: "json",
        data: {
            action: 'get_insight',
            page: page,
            user_id: user_id,
            store_id: store_id,
            nonce: phpjs.nonce,
        },
        success: function(response){
            var li = '';
            jQuery.each(response.data, function(key,val){
                li += '<li class="list-group-item">'+val.date+' : '+val.learned+'</li>';
            });

            if( response.more ){
                li += '<li style="cursor:pointer" class="list-group-item insight-loadmore" onclick="insight_load_more(\''+response.more+'\',\''+store_id+'\');">Load More</li>';
            }

            jQuery('.insight-loadmore').remove();
            jQuery('.insight').append(li);
        },
    });
}

var ordercsi_load_more = function(page, store_id){

    jQuery('.ordecsi-loadmore').text('Loading...');

    var user_id = jQuery('#user_id').val();

    jQuery.ajax({
        url: phpjs.ajax_url,
        dataType: "json",
        data: {
            action: 'get_ordercsi',
            page: page,
            user_id: user_id,
            store_id: store_id,
            nonce: phpjs.nonce,
        },
        success: function(response){
            var li = '';
            jQuery.each(response.data, function(key,val){
                li += '<li class="list-group-item">'+val.date+' '+val.data+'</li>';
            });

            if( response.more ){
                li += '<li style="cursor:pointer" class="list-group-item ordercsi-loadmore" onclick="ordercsi_load_more(\''+response.more+'\',\''+store_id+'\');">Load More</li>';
            }

            jQuery('.ordercsi-loadmore').remove();
            jQuery('.ordercsi').append(li);
        },
    });
}

var telegram_load_more = function(page, store_id){

    jQuery('.telegram-loadmore').text('Loading...');

    var user_id = jQuery('#user_id').val();

    jQuery.ajax({
        url: phpjs.ajax_url,
        dataType: "json",
        data: {
            action: 'get_telegram',
            page: page,
            user_id: user_id,
            store_id: store_id,
            nonce: phpjs.nonce,
        },
        success: function(response){
            var li = '';
            jQuery.each(response.data, function(key,val){
                li += '<li class="list-group-item">'+val.date+val.data+'</li>';
            });

            if( response.more ){
                li += '<li style="cursor:pointer" class="list-group-item telegram-loadmore" onclick="telegram_load_more(\''+response.more+'\',\''+store_id+'\');">Load More</li>';
            }

            jQuery('.telegram-loadmore').remove();
            jQuery('.telegram').append(li);
        },
    });
}

var visitUserModal = function(datas){

    jQuery('.visitUserModalData').empty();

    let data = JSON.parse(atob(datas));
    console.log(data);
    let html = '';

    html += '<p class="form-group"><label>Customer :</label><br/><span style="font-size:20px;"><a href="'+data.customer.link+'">'+data.customer.name+'</a></span></p></span></p>';

    html += '<p class="form-group"><label>Visit Date :</label><br/><span style="font-size:20px;">'+data.date_visit+'</span></p>';

    html += '<p class="form-group"><label>Revisit Date :</label><br/><span style="font-size:20px;">'+data.date_revisit+'</span></p>';

    html += '<p class="form-group"><label>Learned :</label><br/><span style="font-size:18px;">'+data.learned+'</span></p>';

    html += '<p class="form-group"><label>Photo :</label><br/><img src="'+data.photo+'" style="width:100%;height:auto" /></p>';

    html += '<p class="form-group"><label>Location :</label><br/><span style="font-size:20px;">'+data.location.address+'</span><div id="googleMap" style="width:100%; height: 300px;"></div></p>';

    jQuery.each(data.detail, function(key,val){
        html += '<p class="form-group"><label>'+val.label+' :</label><br/><span style="font-size:18px;">'+val.value+'</span></p>';
    })

    jQuery('.visitUserModalData').html(html);

    var uluru = {lat: data.location.lat, lng: data.location.lng};
    var map = new google.maps.Map(
        document.getElementById('googleMap'), {zoom: 15, center: uluru}
    );
    var marker = new google.maps.Marker({position: uluru, map: map});

    jQuery('#visitUserModal').modal('show');

}

var confirmDeleteContact = function(e){

    if(confirm('Anda Yakin akan menhapus Contact ini?')){

    }else{
        console.log('spadadada');
        e.preventDefault();
        return false;
    }
}

var format_number = function(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}



function dropdown_search_toko(){
	var $ = jQuery;

	$('body').find('.search-toko-select2').select2({
		ajax: {
			url: phpjs.ajax_url,
			dataType: "json",
			data: function(params){

                var user_id = $('#user_id').val();
                if( user_id == 'undefined' ){
                    var uid = false;
                }else{
                    var uid = user_id;
                }
				var query = {
					key: params.term,
					action: 'search_toko',
                    page: params.page || 1,
                    user_id: uid,
				}

				return query;
			},
		}
		});
}

function check_all(){
    jQuery('body').on('change','.select-all',function(){
        if(jQuery(this).is(':checked')){
            jQuery(this).closest('table').find('input[type="checkbox"]').prop('checked',true);
        }else{
            jQuery(this).closest('table').find('input[type="checkbox"]').prop('checked',false);
        }

        if(typeof set_enable_disable_button === 'function'){
        	set_enable_disable_button();
        }
    });
}