(function( $ ) {
	'use strict';

	var ordertable;
	var logtable;
	var commenttable;
	$(document).ready(function(){
		// var user_via = [];
		// $('.user_pengiriman').each(function(){
		// 	user_via.push( $(this).val() );
		// });
		//
		// console.log(user_via);
		//
		// var page = $("#store-page").val();
		// $("#date-order").val("");
		// if( 'yes' == page ){
		//
		// }else {
		// 	$("#store-order").val("");
		// }
		// $("#status-order").val("");
		// $("#item-order").val("");
		// $("#user-order").val("");
		// $("#tempo-order").val("");
		// // if(user_via > 0 ){
		// // 	console.log('bbbb');
		// // } else {
		// // 	console.log('ccc');
		// // 	$("#via-order").val("");
		// // }
		//
		// $('#date-order').datetimepicker({
		// 	timepicker:false,
		// 	format:'d-M-Y'
		// });
		//
		// ordertable = $('#order-list').DataTable({
		// 	"processing": true,
		// 	"serverSide": true,
		// 	"searching": false,
		// 	"lengthChange": false,
		// 	"pageLength": 20,
		// 	"ajax": {
		// 		"url" : phpjs.ajax_url,
		// 		"type" : "GET",
		// 		"data" : function(d){
		// 			d.action   = "get_order_list";
		// 			d.nonce    = phpjs.nonce;
		// 			d.date     = $("#date-order").val();
		// 			d.store    = $("#store-order").val();
		// 			d.status   = $("#status-order").val();
		// 			d.item     = $("#item-order").val();
		// 			d.tempo    = $("#tempo-order").val();
		// 			d.via      = $("#via-order").val();
		// 			d.user     = $("#user-order").val();
		// 			//d.user_via = user_via;
		// 		}
		// 	},
		// 	"columnDefs": [
		// 	{
		// 		targets: 0,
		// 		width : '20px',
		// 		render: function( data, type, row ){
		// 			return '<input type="checkbox" class="allcheck" name="order[]" value="'+data+'"/>';
		// 		}
		// 	},
		// 	{
		// 		targets: 1,
		// 		width : '100px',
		// 		render: function ( data, type, row ) {
		// 			return data.first_name +' '+data.last_name+'<br>@'+data.username;
		// 		}
		// 	},
		// 	{
		// 		targets: 2,
		// 		width : '82px',
		// 	},
		// 	{
		// 		targets: 3,
		// 		width : '82px',
		// 		render: function ( data, type, row ) {
		// 			if( data.url ){
		// 				return '<a target="_blank" href="'+data.url+'">'+data.name+'</a>';
		// 			} else {
		// 				return data.name;
		// 			}
		// 		}
		// 	},
		// 	{
		// 		targets: 4,
		// 		width : '82px',
		// 		render: function ( data, type, row ) {
		// 			return data;
		// 		}
		// 	},
		// 	{
		// 		targets: 5,
		// 		width : '80px',
		// 		render: function ( data, type, row ) {
		// 			return data;
		// 		}
		// 	},
		// 	{
		// 		targets: 6,
		// 		width : '40px',
		// 		render: function ( data, type, row ) {
		// 			return data;
		// 		}
		// 	},
		// 	{
		// 		targets: 7,
		// 		width : '40px',
		// 		render: function ( data, type, row ) {
		// 			return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-default">Detail</a></div>';
		// 		}
		// 	}]
		// });
		//
		// logtable = $('#log-list').DataTable({
		// 	"processing": true,
		// 	"serverSide": true,
		// 	"searching": false,
		// 	"lengthChange": false,
		// 	"pageLength": 100,
		// 	"ajax": {
		// 		"url" : phpjs.ajax_url,
		// 		"type" : "GET",
		// 		"data" : function(d){
		// 			d.action = "get_log_list";
		// 			d.nonce  = phpjs.nonce;
		// 		}
		// 	},
		// 	"columnDefs": [
		// 	{
		// 		targets: 0,
		// 		width : '20px',
		// 		render: function( data, type, row ){
		// 			return '<input type="checkbox" class="allcheck" name="order[]" value="'+data+'"/>';
		// 		}
		// 	},
		// 	{
		// 		targets: 1,
		// 		width : '100px',
		// 		render: function( data, type, row ){
		// 			return data.first_name+' '+data.last_name+' On : '+data.date;
		// 		}
		// 	},
		// 	{
		// 		targets: 2,
		// 		width : '200px',
		// 		render: function( data, type, row ){
		// 			if( data.date ){
		// 				return data.status+' Updated By : '+data.first_name+' '+data.last_name+' On : '+data.date;
		// 			}else{
		// 				return data.status;
		// 			}
		// 		}
		// 	},
		// 	{
		// 		targets: 3,
		// 		width : '40px',
		// 		render: function ( data, type, row ) {
		// 			return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-default">Detail</a></div>';
		// 		}
		// 	}]
		// });
		//
		// commenttable = $('#comment-list').DataTable({
		// 	"processing": true,
		// 	"serverSide": true,
		// 	"searching": false,
		// 	"lengthChange": false,
		// 	"pageLength": 20,
		// 	"ajax": {
		// 		"url" : phpjs.ajax_url,
		// 		"type" : "GET",
		// 		"data" : function(d){
		// 			d.action = "get_comment_list";
		// 			d.nonce  = phpjs.nonce;
		// 		}
		// 	},
		// 	"columnDefs": [
		// 	{
		// 		targets: 0,
		// 		width : '100px',
		// 		render: function( data, type, row ){
		// 			return data;
		// 		}
		// 	},
		// 	{
		// 		targets: 1,
		// 		width : '40px',
		// 		render: function( data, type, row ){
		// 			return '#'+data;
		// 		}
		// 	},
		// 	{
		// 		targets: 2,
		// 		width : '100px',
		// 		render: function( data, type, row ){
		// 			return data;
		// 		}
		// 	},
		// 	{
		// 		targets: 3,
		// 		width : '400px',
		// 		render: function ( data, type, row ) {
		// 			return data;
		// 		}
		// 	},
		// 	{
		// 		targets: 4,
		// 		width : '40px',
		// 		render: function ( data, type, row ) {
		// 			return '<div style="text-align:right"><a href="'+data+'" target="_blank" class="btn btn-info">Reply</a></div>';
		// 		}
		// 	}]
		// });
		//
		// $("#order-filter").on('click', function(){
		// 	ordertable.ajax.reload();
		// });
		//
		// $('.select-all').on('change', function(){
		// 	if( $(this).is(':checked') ){
		// 		$('.allcheck').attr('checked', 'checked');
		// 	} else {
		// 		$('.allcheck').removeAttr('checked');
		// 	}
		// });
		//
		// $('#order-pending').on('click', function(){
		// 	var checked = [];
		// 	$('.allcheck:checked').each(function(){
		// 		checked.push( $(this).val() );
		// 	});
		// 	console.log(checked);
		// 	if( checked < 1){
		// 		alert( 'Pilih Order yang akan di ubah setatusnya' );
		// 	}else {
		// 		$(this).html('Proses..');
		// 		$.ajax({
		// 			type: "POST",
		// 			url: phpjs.ajax_url,
		// 			dataType: "json",
		// 			data: {
		// 				action: 'change_order_status',
		// 				nonce: phpjs.nonce,
		// 				data: JSON.stringify(checked),
		// 				status: 'pending',
		// 			},
		// 			success: function(data){
		// 				$('#order-pending').html('Pending');
	    //                 ordertable.ajax.reload();
		// 			}
		// 		});
		// 	}
		// });
		//
		// $('#order-done').on('click', function(){
		// 	var checked = [];
		// 	$('.allcheck:checked').each(function(){
		// 		checked.push( $(this).val() );
		// 	});
		// 	console.log(checked);
		// 	if( checked < 1){
		// 		alert( 'Pilih Order yang akan di ubah setatusnya' );
		// 	}else {
		// 		$(this).html('Proses..');
		// 		$.ajax({
		// 			type: "POST",
		// 			url: phpjs.ajax_url,
		// 			dataType: "json",
		// 			data: {
		// 				action: 'change_order_status',
		// 				nonce: phpjs.nonce,
		// 				data: JSON.stringify(checked),
		// 				status: 'done',
		// 			},
		// 			success: function(data){
		// 				$('#order-done').html('Done');
	    //                 ordertable.ajax.reload();
		// 			}
		// 		});
		// 	}
		// });
		//
		//
		// $('#order-reject').on('click', function(){
		// 	var checked = [];
		// 	$('.allcheck:checked').each(function(){
		// 		checked.push( $(this).val() );
		// 	});
		// 	console.log(checked);
		// 	if( checked < 1){
		// 		alert( 'Pilih Order yang akan di ubah setatusnya' );
		// 	}else {
		// 		$(this).html('Proses..');
		// 		$.ajax({
		// 			type: "POST",
		// 			url: phpjs.ajax_url,
		// 			dataType: "json",
		// 			data: {
		// 				action: 'change_order_status',
		// 				nonce: phpjs.nonce,
		// 				data: JSON.stringify(checked),
		// 				status: 'reject',
		// 			},
		// 			success: function(data){
		// 				$('#order-reject').html('Reject');
	    //                 ordertable.ajax.reload();
		// 			}
		// 		});
		// 	}
		// });
		//
		// $('#order-export').on('click', function(){
		// 	$(this).html('Proses..');
		//
		// 	$.ajax({
		// 		type: "POST",
		// 		url: phpjs.ajax_url,
		// 		dataType: "json",
		// 		data: {
		// 			action: 'export_orders',
		// 			nonce: phpjs.nonce,
		// 			date: $("#date-order").val(),
		// 			store: $("#store-order").val(),
		// 			status: $("#status-order").val(),
		// 			item: $("#item-order").val(),
		// 			tempo: $("#tempo-order").val(),
		// 			via: $("#via-order").val(),
		// 			user: $("#user-order").val(),
		// 		},
		// 		success: function(data){
		// 			$('#order-export').html('Export To excel');
		// 			console.log(data.url);
		// 			window.open(data.url, '_blank');
		// 		}
		// 	});
		// });
		//
		// $('#store-order').select2({
		// 	ajax: {
		// 		url: phpjs.ajax_url,
		// 		dataType: "json",
		// 		data: function(params){
		// 			var query = {
		// 				key: params.term,
		// 				action: 'search_toko'
		// 			}
		//
		// 			return query;
		// 		},
		// 	}
		// });


		$('#order-list tbody').on('click', 'tr', function(){
            $('.popupLoader').show();
            $('.popupContent').hide();
            $('#orderModal').hide();
			let id = $(this).attr('data-id');
            $('#order-list tbody tr').removeAttr('style')
            $('#orderModal').show();
			// $.ajax({
			// 	type: "POST",
			// 	url: phpjs.ajax_url,
			// 	dataType: "json",
			// 	data: {
			// 		action: 'get_single_order',
			// 		nonce: phpjs.nonce,
			// 		data: id,
			// 	},
			// 	success: function(data){
            //         $('#order-list tbody tr[data-id="'+id+'"]').css({'background-color':'#337ab7','color':'#ffffff'});
            //         $('#popupOrderId').html(data.order_id);
            //         $('#popupTempo').html(data.tempo);
            //         $('#popupPengiriman').html(data.pengiriman);
            //         $('.popupPermalink').attr('href', data.permalink);
            //         if(data.mapping == true){
            //             $('.no-mapping').hide();
            //             $('.mapping').show();
            //             $('#popupToko').html(data.toko);
            //             $('#popupAlamat').html(data.alamat);
            //         }else {
            //             $('.no-mapping').show();
            //             $('.mapping').hide();
            //             $('#popupNoMapping').html(data.nomapping);
            //         }
            //         if(data.items){
            //             var tr = '';
            //             $.each(data.items, function(key,val){
            //                 tr += '<tr><td>'+val.item+'</td><td>'+val.qty+'</td><td>'+val.price+'</td><td>'+val.sub+'</td></tr>';
            //             });
			//
            //             $('.table-item tbody').empty().append(tr);
            //         }
			//
            //         var logs = '<li class="list-group-item d-flex justify-content-between align-items-center">\
            //            Created : <span style="font-weight:bold">'+data.author+'</span> On : <span style="font-weight:bold">'+data.created_date+'</span></li>';
			//
            //         if(data.update.update == true){
            //             logs += '<li class="list-group-item d-flex justify-content-between align-items-center">\
            //                 Change Status : <span style="font-weight:bold">'+data.update.status+'</span> By <span style="font-weight:bold">'+data.update.updated_by+'</span> On : <span style="font-weight:bold">'+data.update.updated_date+'</span></li>'
            //         }
			//
            //         if(data.logs){
            //             $.each(data.logs, function(key,val){
            //                 logs += '<li class="list-group-item d-flex justify-content-between align-items-center">\
            //                     Change Status : <span style="font-weight:bold">'+val.status+'</span> By <span style="font-weight:bold">'+val.updated_by+'</span> On : <span style="font-weight:bold">'+val.updated_date+'</span> '+val.reason+'</li>';
            //             });
            //         }
			//
            //         if(data.total){
            //             $('.totales').html(data.total);
            //         }
			//
            //         $('#popupLogs').empty().append(logs);
			//
            //         if(data.comments){
            //             var comment = '';
            //             $.each(data.comments, function(key,val){
            //                 comment += '<li id="comment-'+val.comment_id+'" class="comment" style="margin-bottom:30px;">\
            //                     <div class="media-left">\
            //                         <a href="" class="media-object">\
            //                             <img alt="" src="'+val.comment_avatar+'" class="avatar avatar-50 photo" width="50" height="50"></a>\
            //                         </div>\
            //                         <div class="media-body">\
            //                             <h4 class="media-heading">'+val.comment_author+'</h4><time datetime="'+val.comment_date+'">'+val.comment_date+'</time>\
            //                         <div class="comment-content">\
            //                             <p>'+val.comment_content+'</p>\
            //                             <div class="comment-tagged">Tagged: '+val.comment_tagged+'</div>\
            //                         </div><!-- .comment-content -->\
            //                         <ul class="list-inline comment-reply">\
            //          					<li class="reply-link"><a rel="nofollow" class="comment-reply-link" href="'+val.comment_reply_link+'" target="_blank" aria-label="Reply to '+val.comment_author+'">Reply</a></li>\
            //          				</ul>\
            //                     </div>\
            //                 </li>';
            //             });
			//
            //             $('#popupComment').empty().append(comment);
            //         }
			//
            //         $('.popupLoader').hide();
            //         $('.popupContent').show();
			// 	}
			});
		});

	});

})( jQuery );
