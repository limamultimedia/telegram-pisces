<form action="" method="POST" id="form-store">

	<div class="row">
	
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Tempat</label>
				<input type="text" name="nama" class="form-control" required>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label>Kategori</label>
				<select name="kategori" class="form-control" required>
					<option value="">Pilih</option>
					<option>Toko</option>
					<option>Resto</option>
					<option>Bengkel</option>
				</select>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Pemilik</label>
				<input type="text" name="nama_pemilik" class="form-control" required>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label>No HP</label>
				<input type="text" name="phone" class="form-control" required>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				<label>Alamat</label>
				<textarea name="alamat" class="form-control" required></textarea>
			</div>

			<div class="form-group text-center ">
				<button type="submit" class="btn btn-primary btn-block" data-loading="Proses..">SIMPAN</button>
			</div>
		</div>

	</div>

	<input type="hidden" name="action" value="action_add_store">
</form>