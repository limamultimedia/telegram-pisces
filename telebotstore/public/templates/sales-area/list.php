<table class="table table-striped datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Sales</th>
            <th>Toko</th>
            <th>New Request</th>
            <th>Pilihan</th>
        </tr>
    </thead>
    <tbody>
    <?php 
        $no = 1;
       
        foreach($query as $row){ 

            $users = get_user_by('ID',$row->request_by);
            $sales = $users->display_name;

            $andWhere = " AND request_by='".$row->request_by."' ";
            $data_sales = $Telebotstore_Sales_Area->get($andWhere);

           
            $request = $approve = [];
            foreach($data_sales as $row_sales){
                $post = get_post($row_sales->toko_id);
                $toko = $post->post_title; 

                $data_toko = [
                                'id' => $row_sales->toko_id,
                                'name' => $toko,
                                'datecreated' => $row_sales->datecreated
                            ];

                if($row_sales->status == 0){
                    $request[] = $data_toko; 
                }else if ($row_sales->status == 1){
                    $approve[] = $data_toko;
                }
                
                
            }

            if($approve){
                 $toko_html = '<ol>';
                 foreach($approve as $arr){
                    $toko_html .= '<li>'.$arr['name'].'</li>';
                 }
                 $toko_html .= '</ol>';
            }else{
                $toko_html = '-';
            }

            if($request){
                 $request_html = '<ol>';
                 foreach($request as $arr){

                    $request_date = date('d-m-Y H:i',strtotime($arr['datecreated']));

                    $request_html .= '<li>'.$arr['name'].'<br><small>'.$request_date.'</small></li>';
                 }
                 $request_html .= '</ol>';
            }else{
                $request_html = '-';
            }

          
        
    ?>
        <tr>
            <td><?= $no; ?></td>
            <td><?= $sales; ?></td>
            <td><?= $toko_html; ?></td>
            <td><?= $request_html; ?></td>
            <td><button class="btn btn-primary action-sales-area" data-sales="<?= $row->request_by; ?>">Action</button></td>
        </tr>
    <?php 
        $no++;
        } 
    ?>
    </tbody>
</table>