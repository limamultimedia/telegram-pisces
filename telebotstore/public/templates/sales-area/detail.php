<?php 

$user = get_user_by('ID',$query[0]->request_by);
$sales_name = $user->display_name;

?>

<h3 class="text-center"><?= $sales_name; ?></h3>

<div class="panel panel-warning">
	<div class="panel-heading">
		<h4 class="text-waring">Sales Area - Pending</h4>
	</div>
	<div class="panel-body">

		<form action="" method="POST" class="form-sales-area" >
			<div class="btn-group pull-right">
				<button class="btn btn-success action" data-action="approve">Approve</button>
				<button class="btn btn-warning action" data-action="reject" >Reject</button>
				<button class="btn btn-danger action" data-action="delete" >Delete</button>
			</div>

			<div class="clearfix"></div>

			<table class="table ">
				<thead>
					<tr>
						<th><input type="checkbox" name="select_all" class="select-all"> No</th>
						<th>Tgl Request</th>
						<th>Toko</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach($query as $row){
						if($row->status != 1){
							$toko = get_post($row->toko_id)->post_title;

							if($row->status == 99){//is rejected
								$css_class = 'rejected';
							}else{
								$css_class = '';
							}
							?>
							<tr class="<?= $css_class; ?>" >
								<td><input type="checkbox" class="checkbox-row" name="id[]" value="<?= $row->id; ?>"> <?= $no; ?></td>
								<td><?= date('d-m-Y H:i',strtotime($row->datecreated)); ?></td>
								<td><?= $toko; ?></td>
							</tr>
							<?php
							$no++;
						}
					}
					?>

				</tbody>
			</table>
		</form>
		</div>
		</div>



<div class="panel panel-success">
	<div class="panel-heading">
		<h4 class="text-success">Sales Area - Approve</h4>
	</div>
	<div class="panel-body">

		<form action="" method="POST" class="form-sales-area">
			<div class="btn-group pull-right">
				<button class="btn btn-warning action" data-action="reject" >Reject</button>
				<button class="btn btn-danger action" data-action="delete" >Delete</button>
			</div>
			<div class="clearfix"></div>

			<table class="table ">
				<thead>
					<tr>
						<th>No <input type="checkbox" name="select_all" class="select-all"></th>
						<th>Tgl Request</th>
						<th>Toko</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach($query as $row){
						if($row->status == 1){
							$toko = get_post($row->toko_id)->post_title;
							?>
							<tr>
								<td><?= $no; ?> <input type="checkbox" class="checkbox-row" name="id[]" value="<?= $row->id; ?>"></td>
								<td><?= date('d-m-Y H:i',strtotime($row->datecreated)); ?></td>
								<td><?= $toko; ?></td>
							</tr>
							<?php
							$no++;
						}
					}
					?>

				</tbody>
			</table>
		</form>
</div>
</div>

<input type="hidden" id="sales_id" value="<?= $sales_id; ?>">

<style type="text/css">
	tr.rejected td{
	  text-decoration: line-through;
	}
</style>