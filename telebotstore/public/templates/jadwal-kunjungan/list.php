<div class="btn-group pull-right form-group">
    <button class="btn btn-success action" data-action="approve">Approve</button>
    <button class="btn btn-warning action" data-action="reject" >Reject</button>
    <button class="btn btn-danger action" data-action="delete" >Delete</button>
</div>

<div class="clearfix"></div>

<table class="table table-striped datatable" style="width:100%;">
    <thead>
        <tr>
            <th><input type="checkbox" name="select_all" class="select-all"> No</th>
            <th>Sales</th>
            <th>Tanggal</th>
            <th>Toko</th>
            <th>Last Visit</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            
            $no = 1;
            foreach($query as $row){

                $sales = get_display_name($row->user_id);

                $toko = get_post($row->toko_id)->post_title;

                $last_visit = get_toko_last_visit($row->toko_id,$row->tanggal);

                if($last_visit){
                    $visit_date = date('d M Y',strtotime($last_visit->visit_datetime));
                    $sales_visit = get_display_name($last_visit->user_id);
                    $data_last_visit = $visit_date .' @ '.$sales_visit;
                }else{
                    $data_last_visit = '-';
                }

                if($row->status == 0){
                    $status = '<span class="text-warning"><i class="fa fa-circle-exclamation"></i> Pending</span>';
                }else if($row->status == 1){
                     $status = '<span class="text-success"><i class="fa fa-circle-check"></i> Approve</span>';
                }else if($row->status == 99){
                     $status = '<span class="text-danger"><i class="fa fa-ban"></i> Reject</span>';
                }
        ?>
            <tr>
                <td><input type="checkbox" name="id[]" value="<?= $row->id; ?>" class="checkbox-row"> <?= $no; ?> </td>
                <td><?= $sales; ?></td>
                <td><?= date_to_hari($row->tanggal).', '.date('d M y',strtotime($row->tanggal)); ?></td>
                <td><?= $toko; ?></td>
                <td><?= $data_last_visit; ?></td>
                <td><?= $status; ?></td>
            </tr>
        <?php $no++; } ?>    
    </tbody>
</table>