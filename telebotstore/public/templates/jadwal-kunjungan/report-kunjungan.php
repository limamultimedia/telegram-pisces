
<table class="table table-striped datatable" style="width:100%;">
    <thead>
        <tr>
            <th>No</th>
            <th>Sales</th>
            <th>Tanggal</th>
            <th>Jadwal Ke</th>
            <th>Waktu Visit</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $now = date('Y-m-d');
            $no = 1;
            foreach($query as $row){

                $sales = get_display_name($row->user_id);

                $toko = get_post($row->toko_id)->post_title;

                $is_visit = check_toko_is_visit($row->toko_id,$row->tanggal,$row->user_id);

                if($is_visit){
                    $visit_date = date('d M Y H:i',strtotime($is_visit->visit_datetime));
                    $sales_visit = get_display_name($is_visit->user_id);
                    $data_visit = $visit_date .' <br> '.$sales_visit;

                    $status = '<span class="text-success"><i class="fa fa-circle-check"></i> Dikunjungi</span>';

                }else{
                    $data_visit = '-';

                    if( strtotime($now)<= strtotime($row->tanggal) ) {
                        $status = '<span class="text-warning"><i class="fa fa-circle-exclamation"></i> Belum Dikunjungi</span>';
                    }else{
                        $status = '<span class="text-danger"><i class="fa fa-ban"></i> Tidak Dikunjungi</span>';
                    }

                    
                }

        ?>
            <tr>
                <td><?= $no; ?> </td>
                <td><?= $sales; ?></td>
                <td><?= date_to_hari($row->tanggal).', '.date('d M y',strtotime($row->tanggal)); ?></td>
                <td><?= $toko; ?></td>
                <td><?= $data_visit; ?></td>
                <td><?= $status; ?></td>
            </tr>
        <?php $no++; } ?>    
    </tbody>
</table>