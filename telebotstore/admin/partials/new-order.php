<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      0.1.0
 *
 * @package    wooimporter
 * @subpackage wooimporter/admin/partials
 */

if ( isset($_POST["new_order"]) ) :
    $post = $_POST;

    $new_post = array(
        'post_author' => $post['sales'],
        'post_content' => '...',
        'post_status' => "publish",
        'post_title' => $post['toko'],
        'post_type' => "telebotstore_order",
    );

    $post_id = wp_insert_post( $new_post );
    add_post_meta( $post_id, '_order_status', 'pending');
    add_post_meta( $post_id, '_order_store', $post['toko']);
    add_post_meta( $post_id, '_order_toko', $post['store']);
    add_post_meta( $post_id, '_order_tempo', $post['tempo']);
    add_post_meta( $post_id, '_order_pengiriman', $post['pengiriman']);

    foreach( (array) $post['item'] as $key=>$val ):
        $add_item = $val.'|'.$post['qty'][$key];
        add_post_meta( $post_id, '_order_items', $add_item);
    endforeach;

    $tokos[] = $post_id;

    ?>
    <div id="message" class="updated notice notice-success is-dismissible">
        <p>
            SUKSES menambahkan order dengan ID <a href="<?php echo get_edit_post_link( $post_id ); ?>"><?php echo $post_id; ?></a>.
        </p>
        <p>
            <a target="_blank" href="<?php echo get_permalink( $post_id ); ?>">Lihat Order</a>.
        </p>
        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
    </div>
    <?php

endif;
 ?>
<div class="wrap">
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="postbox-container-2" class="postbox-container">
                <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                    <div id="itsec_get_started" class="postbox ">
                        <h3 class="hndle">
                            <span><?php echo __( 'Tambah Order baru', 'telebotstore' ); ?></span>
                        </h3>
                        <div class="inside">
                            <form action="" method="post" enctype="multipart/form-data" id="importer-data">
                                <p>
                                    <label>Sales</label><br />
                                    <select id="sales" name="sales" class="regular-text" required>
                                        <option value="">Pilih Sales</option>
                                    </select>
                                </p>
                                <p>
                                    <label>Toko</label><br />
                                    <textarea name="toko" class="regular-text" required></textarea><br />
                                    <select id="order-toko" class="regular-text" name="store" required>
                                        <option value="" selected="selected">Pilih Toko</option>
                                    </select>
                                </p>
                                <p>
                                    <label>Tempo Pembayaran</label><br />
                                    <select name="tempo" class="regular-text" required>
                                        <?php
                                        $payments = explode(PHP_EOL, carbon_get_theme_option('order_item_pembayaran'));

                                        $obj = array();
                                        if( $payments ):
                                            foreach($payments as $key=>$val ):
                                                ?>
                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </p>
                                <p>
                                    <label>Mode Pengiriman</label><br />
                                    <select name="pengiriman" class="regular-text" required>
                                        <?php
                                        $pengiriman = explode(PHP_EOL, carbon_get_theme_option('order_item_pengiriman'));

                                        $obj = array();
                                        if( $pengiriman ):
                                            foreach($pengiriman as $key=>$val ):
                                                ?>
                                                <option value="<?php echo rtrim($val); ?>"><?php echo $val; ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </p>
                                <p id="items">
                                    <label>Item</label><br />
                                </p>
                                <div style="position:relative;width: 27em">
                                    <div id="czContainer">
                                        <div id="first">
                                            <div class="recordset" style="position:relative;">
                                                <select class="item" name="item[]" style="width:60%;display:inline-block;vertical-align:top" required>
                                                    <option value="">Pilih Item</option>
                                                </select>
                                                <input type="number" name="qty[]" style="width:20%;display: inline-block">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p style="margin-top: 50px;">
                                    <input type="submit" class="button button-primary" name="new_order" id="new-order" value="Tambah Order"/>
                                </p>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

<!-- <script>
jQuery(document).ready(function($) {
    jQuery('#importer').click(function(){
        jQuery(this).text('Process Import......');
        var formdata = new FormData( jQuery("#importer-data")[0] );
        console.log(formdata);
        //formdata.append('action', 'import_toko');
        // jQuery.ajax({
        //     type: 'POST',
        //     url: ajaxurl,
        //     dataType: "json",
        //     data: formdata,
        //     success:function(data) {
        //         //console.log(data);
        //         //location.reload();
        //     }
        // });
        return false;
    });
});
</script> -->
