<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/admin/partials
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<input type="hidden" name="_order_status_id_update" value="<?php echo get_current_user_id(); ?>"/>
<table class="widefat fixed striped" cellpadding="15" style="border: none">
    <tr>
        <td style="width:150px;font-weight:bold">User</td>
        <td><?php echo get_the_author_meta( 'first_name', $post->post_author ); ?> <?php echo get_the_author_meta( 'last_name', $post->post_author ); ?></td>
    </tr>
    <tr>
        <td style="width:150px;font-weight:bold">Toko</td>
        <td>
            <?php $toko_id = get_post_meta($post->ID, '_order_toko', true); ?>
            <?php
            echo get_post_meta($post->ID, '_order_store', true);
            echo '<div style="margin-bottom:10px;"></div>';
            if( empty($toko_id) ) :
                $toko_name = 'Pilih Toko';
            else :
                $provider = get_post_meta($toko_id, '_provider', true);

                if($provider == 'api'):
                    $toko_name = carbon_get_post_meta($toko_id, 'name');
                    echo sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), $toko_name );
                else :
                    $toko_name = get_the_title($toko_id);
                    echo sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), $toko_name );
                endif;
                echo '<div style="margin-bottom:10px;"></div>';
            endif;
            ?>
            <br />
            <select id="order-toko" class="regular-text" name="_order_toko">
                <option value="<?php echo $toko_id; ?>" selected="selected"><?php echo $toko_name; ?></option>
            </select>
            <?php
            if( empty($toko_id) ) :
                echo '<br /><span style="color:red">Order ini belum dihubungkan dengan toko yang ada pada database</span>';
            endif;
            ?>
        </td>
    </tr>
    <tr>
        <td style="width:150px;font-weight:bold">Status</td>
        <td>
            <?php $status = get_post_meta( $post->ID, '_order_status', true); ?>
            <select name="_order_status" style="width: 100%" id="order-statuses">
                <option value="pending" <?php if( 'pending' == $status ){ echo 'selected="selected"'; } ?>>Pending</option>
                <option value="done"  <?php if( 'done' == $status ){ echo 'selected="selected"'; } ?>>Done</option>
                <option value="reject"  <?php if( 'reject' == $status ){ echo 'selected="selected"'; } ?>>Reject</option>
            </select>
        </td>
    </tr>
    <tr id="reject-reason" <?php if($status !== 'reject'){ ?>style="display:none"<?php } ?>>
        <td style="width:150px;font-weight:bold">Reject Reason</td>
        <td>
            <textarea style="width:100%" name="_order_reject_reason"><?php echo get_post_meta( $post->ID, '_order_reject_reason', true); ?></textarea>
        </td>
    </tr>
    <tr>
        <td style="width:150px;font-weight:bold">Tempo Pembayaran</td>
        <td>
            <select name="_order_tempo" style="width: 100%">
	        <?php
	            $tempo = get_post_meta($post->ID, '_order_tempo', true);
	            foreach ( $this->get_all_tempo() as $key => $val) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $key,
	                        $key == $tempo ? ' selected="selected"':'',
	                        $val
	                    );
	                }
	        ?>
	        </select>
        </td>
    </tr>
    <tr>
        <td style="width:150px;font-weight:bold">Pengiriman Via</td>
        <td>
            <select name="_order_pengiriman" style="width: 100%">
	        <?php
	            $tempo = get_post_meta($post->ID, '_order_pengiriman', true);
	            foreach ( $this->get_all_kurir() as $key => $val) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $key,
	                        $key == $tempo ? ' selected="selected"':'',
	                        $val
	                    );
	                }
	        ?>
	        </select>
        </td>
    </tr>
    <tr>
        <td style="width:150px;font-weight:bold">Items</td>
        <td>
            <table class="widefat fixed striped" cellpadding="15" style="border: none">
                <tr>
                    <td>Item</td>
                    <td style="text-align:right">Quantity</td>
                    <td style="text-align:right">Status</td>
                </tr>
                <?php
                $items = get_post_meta($post->ID, '_order_items', false);
                foreach( $items as $key=>$val ):
                    $value = explode('|', $val);
                ?>
                    <tr>
                        <td>
                            <?php echo $value[0]; ?>
                        </td>
                        <td style="text-align:right">
                            <?php echo $value[1]; ?>
                        </td>
                        <td style="text-align:right">
                            <?php echo isset($value[2]) ? $value[2] : $status; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
</table>
