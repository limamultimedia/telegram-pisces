<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;

if ( isset($_POST["toko_import"]) ) :
    $csv = $_FILES['toko_excel'];
    $data = array();

    // $file = fopen( $csv['tmp_name'],"r");
    // while (($row = fgetcsv($file, 0, ",")) !== FALSE) :
    //     $data[] = $row;
    // endwhile;

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    //$reader->setLoadSheetsOnly(["Data toko"]);
    //$reader->getReadDataOnly(true);
    $spreadsheet = $reader->load($csv['tmp_name']);

    $data = $spreadsheet->getActiveSheet()->toArray();

    $fields = array();
    foreach( (array)$data[0] as $key=>$val ):
        $val = preg_replace('/\s+/', '_', $val);
        $fields[] = strtolower($val);

    endforeach;

    //telebotstore_debug($fields);

    unset($data[0]);

    $tokos = array();

    //telebotstore_debug($data);

    foreach( $data as $toko ):

        $post = array(
            'post_author' => get_current_user_id(),
            //'post_content' => $toko[1],
            'post_status' => "publish",
            'post_title' => $toko[2],
            'post_type' => "telebotstore_toko",
        );

        if( $toko[0] && get_post(intval($toko[0])) ):
            $post['ID'] = intval($toko[0]);
        endif;

        $post_id = wp_insert_post( $post );

        foreach( (array) $fields as $key=>$val ):
            if( 0 === $key || 2 === $key ) continue;
            update_post_meta( $post_id, '_telebotstore_toko_'.$val, $toko[$key]);
        endforeach;

        $tokos[] = $post_id;

    endforeach;
    ?>
    <div id="message" class="updated notice notice-success is-dismissible">
        <p>
            Sukses <?php echo count($tokos); ?> data toko telah terimport dan di simpan.
        </p>
        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
    </div>
    <?php

endif;

if ( isset($_POST["contact_import"]) ) :
    $file = $_FILES['contact_excel'];
    $data = array();

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    //$reader->setLoadSheetsOnly(["Data toko"]);
    //$reader->getReadDataOnly(true);
    $spreadsheet = $reader->load($file['tmp_name']);

    $data = $spreadsheet->getActiveSheet()->toArray();

    $fields = array();
    foreach( (array)$data[0] as $key=>$val ):
        $val = preg_replace('/\s+/', '_', $val);
        $fields[] = strtolower($val);

    endforeach;

    //telebotstore_debug($data);

    unset($data[0]);

    $tokos = array();

    //telebotstore_debug($data);

    foreach( $data as $toko ):

        if( isset($toko[0]) && empty($toko[0]) )continue;

        if( empty( get_post($toko[0]) ) )continue;

        $serialize = array();

        foreach($fields as $key=>$val):
            $serialize[$val] = $toko[$key];
        endforeach;

        $args = array(
            'toko_id'    => intval($toko[0]),
            'first_name' => $toko[1],
            'last_name'  => $toko[2],
            'email'      => $toko[3],
            'phone'      => $toko[5],
            'address'    => $toko[4],
            'birth_day'  => date('Y-m-d', strtotime($toko[6])),
            'hobby'      => $toko[7],
            'detail'     => serialize($serialize),
        );

        $q = new Telebotstore_Query_Toko_Contact(['ID'=>intval($toko[1])]);

        // if( $toko[1] && $q->query() ):
        //     Telebotstore_Toko_Contact::update($args, ['ID'=>intval($toko[1])]);
        //
        //     $contact_id = intval($toko[1]);
        // else :
            $contact_id = Telebotstore_Toko_Contact::insert($args);
        //endif;

        $tokos[] = $contact_id;

    endforeach;
    ?>
    <div id="message" class="updated notice notice-success is-dismissible">
        <p>
            Sukses <?php echo count($tokos); ?> data kontak telah terimport dan di simpan.
        </p>
        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
    </div>
    <?php

endif;

if ( isset($_POST["order_csi_import"]) ) :
    $file = $_FILES['order_csi_excel'];
    $data = array();

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    //$reader->setLoadSheetsOnly(["Data toko"]);
    //$reader->getReadDataOnly(true);
    $spreadsheet = $reader->load($file['tmp_name']);

    $data = $spreadsheet->getActiveSheet()->toArray('', TRUE, TRUE, TRUE);

    $fields = array();
    foreach( (array)$data[1] as $key=>$val ):
        $val = preg_replace('/\s+/', '_', $val);
        $fields[] = strtolower($val);

    endforeach;

    //telebotstore_debug($fields);

    unset($data[1]);

    $tokos = array();

    //telebotstore_debug($data);

    foreach( $data as $toko ):

        if( isset($toko['A']) && empty($toko['A']) )continue;

        if( empty( get_post($toko['A']) ) )continue;

        $args = array(
            'entry_no' => isset($toko['B']) ? sanitize_text_field($toko['B']) : '',
        );

        $query = new Telebotstore_Query_Toko_Order_Csi($args);

        if( $query->query() )continue;

        $args = array(
            'toko_id'          => intval($toko['A']),
            'entry_no'         => isset($toko['B']) ? sanitize_text_field($toko['B']): '',
            'posting_date'     => isset($toko['C']) ? date('Y-m-d', strtotime($toko['C'])) : '',
            'document_no'      => isset($toko['D']) ? sanitize_text_field($toko['D']) : '',
            'document_type'    => isset($toko['E']) ? sanitize_text_field($toko['E']) : '',
            'customer_name'    => isset($toko['F']) ? sanitize_text_field($toko['F']) : '',
            'salesperson_code' => isset($toko['G']) ? sanitize_text_field($toko['G']) : '',
            'item_no'          => isset($toko['H']) ? sanitize_text_field($toko['H']) : '',
            'quantity'         => isset($toko['I']) ? sanitize_text_field($toko['I']) : '',
            'price'            => isset($toko['J']) ? sanitize_text_field($toko['J']) : '',
            'location_code'    => isset($toko['K']) ? sanitize_text_field($toko['K']) : '',
            'sales_inc_ppn'    => isset($toko['L']) ? sanitize_text_field($toko['L']) : '',
            'item_category'    => isset($toko['M']) ? sanitize_text_field($toko['M']) : '',
            'manufacture_code' => isset($toko['N']) ? sanitize_text_field($toko['N']) : '',
            'city'             => isset($toko['O']) ? sanitize_text_field($toko['O']) : '',
            'dlm_lr_kota'      => isset($toko['P']) ? sanitize_text_field($toko['P']) : '',
            'iq'               => isset($toko['Q']) ? sanitize_text_field($toko['Q']) : '',
            'sales_exc_ppn'    => isset($toko['R']) ? sanitize_text_field($toko['R']) : '',
        );

        $order_id = Telebotstore_Toko_Order_Csi::insert($args);

        $tokos[] = $order_id;

    endforeach;
    ?>
    <div id="message" class="updated notice notice-success is-dismissible">
        <p>
            Sukses <?php echo count($tokos); ?> data Order CSI telah terimport dan di simpan.
        </p>
        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
    </div>
    <?php

endif;

 ?>
<div class="wrap">
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
	<!-- Comment by Lima Multimedia 03052021 -->
    <div id="poststuff" style="display: none;">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="postbox-container-2" class="postbox-container">
                <div id="itsec_get_started" class="postbox ">
                    <h3 class="hndle">
                        <span><?php echo __( 'Import Data Toko/Customer/Perusahaan', 'telebotstore' ); ?></span>
                    </h3>
                    <div class="inside" style="text-align:center">
                        <form action="" method="post" enctype="multipart/form-data">
                            <p>
                                <label>Upload File Excel Toko</label><br />
                                <input type="file" value="" name="toko_excel"/>
                            </p>
                            <p>
                                <input type="submit" class="button button-primary" name="toko_import" value="Import Sekarang"/>
                            </p>
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
			</div>
		</div>
	</div>

    
    <div id="poststuff" style="display: none;">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="postbox-container-2" class="postbox-container">
                <div id="itsec_get_started" class="postbox ">
                    <h3 class="hndle">
                        <span><?php echo __( 'Import Data Contact', 'telebotstore' ); ?></span>
                    </h3>
                    <div class="inside" style="text-align:center">
                        <form action="" method="post" enctype="multipart/form-data">
                            <p>
                                <label>Upload File Excel Contact Toko</label><br />
                                <input type="file" value="" name="contact_excel"/>
                            </p>
                            <p>
                                <input type="submit" class="button button-primary" name="contact_import" value="Import Sekarang"/>
                            </p>
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
			</div>
		</div>
	</div>

    <div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="postbox-container-2" class="postbox-container">
                <div id="itsec_get_started" class="postbox ">
                    <h3 class="hndle">
                        <span><?php echo __( 'Import Data Order CSI', 'telebotstore' ); ?></span>
                    </h3>
                    <div class="inside" style="text-align:center">
                        <form action="" method="post" enctype="multipart/form-data">
                            <p>
                                <label>Upload File Excel Order CSI</label><br />
                                <input type="file" value="" name="order_csi_excel"/>
                            </p>
                            <p>
                                <input type="submit" class="button button-primary" name="order_csi_import" value="Import Sekarang"/>
                            </p>
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

<!-- <script>
jQuery(document).ready(function($) {
    jQuery('#importer').click(function(){
        jQuery(this).text('Process Import......');
        var formdata = new FormData( jQuery("#importer-data")[0] );
        console.log(formdata);
        //formdata.append('action', 'import_toko');
        // jQuery.ajax({
        //     type: 'POST',
        //     url: ajaxurl,
        //     dataType: "json",
        //     data: formdata,
        //     success:function(data) {
        //         //console.log(data);
        //         //location.reload();
        //     }
        // });
        return false;
    });
});
</script> -->
