<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.ridwanarifandi.com
 * @since      1.0.0
 *
 * @package    Telebotstore
 * @subpackage Telebotstore/admin
 */
class Telebotstore_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( 'datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.18/jquery.datetimepicker.min.css', array(), NULL, 'all' );

		wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array(), NULL, 'all' );

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/telebotstore-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( 'datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.18/jquery.datetimepicker.full.min.js', array( 'jquery' ), NULL, false );

		wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', array( 'jquery' ), NULL, false );

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/telebotstore-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * telebotstore plugin init
	 * hooked via wp init
	 * @return [type] [description]
	 */
	public function telebotstore_init()
	{
		$post_type = new Telebotstore_PostType();

		$post_type->order();
		$post_type->item();
		$post_type->toko();
		$post_type->ticket();

	}

	/**
	 * add custom column to telebotstore_order post type
	 * manage_edit-affiliate_columns hook
	 * @param [type] $columns [description]
	 */
	public function order_columns( $columns )
	{
		$columns = array(
			'cb'               => '<input type="checkbox" />',
			'order_user'       => __( 'User', 'telebotstore' ),
			'order_date'       => __( 'Tanggal', 'telebotstore' ),
			'order_store'      => __('Nama Toko', 'telebotstore'),
			'order_tempo'      => __('Tempo Pembayaran', 'telebotstore'),
			'order_pengiriman' => __('Di kirim Via', 'telebotstore'),
			'order_status'     => __('Status', 'telebotstore'),
			'order_action'     => __('&nbsp', 'telebotstore'),
		);

		return $columns;
	}

	/**
	 * manage telebotstore_order post type custom column
	 * @param  [type] $column  [description]
	 * @param  [type] $post_id [description]
	 * @return [type]          [description]
	 */
	public function manage_order_columns( $column, $post_id )
	{
		global $post;

		switch( $column ) :
			case 'order_user' :
			    echo '<span style="font-weight:bold">'.get_the_author().'</span><br />';
				echo '<span>@'.get_the_author_meta( 'user_login', $post->post_author ).'</span><br />';
				break;
			case 'order_date' :
			    echo date('d-M-Y', strtotime(get_the_date()));
				break;
			case 'order_store' :
			    $toko_id = get_post_meta($post->ID, '_order_toko', true);
				if( $toko_id ):
					$provider = get_post_meta($toko_id, '_provider', true);
	                if($provider == 'api'):
	                    $toko_name = carbon_get_post_meta($toko_id, 'name');
	                    echo sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), $toko_name );
	                else :
	                    $toko_name = get_the_title($toko_id);
	                    echo sprintf( '<a href="%s">%s</a>', get_edit_post_link($toko_id), $toko_name );
	                endif;
				else :
				    echo get_post_meta( $post->ID, '_order_store', true);
				endif;
				break;
			case 'order_tempo' :
			    echo get_post_meta( $post->ID, '_order_tempo', true);
				break;
			case 'order_pengiriman' :
			    echo get_post_meta( $post->ID, '_order_pengiriman', true);
				break;
			case 'order_status' :
			    echo ucfirst(get_post_meta( $post->ID, '_order_status', true));
				break;
			case 'order_action' :
			    echo '<div style="text-align:right">';
				echo '<a href="'.get_edit_post_link( $post->ID ).'" class="button">Edit</a>&nbsp';
				echo '<a target="_blank" href="'.get_the_permalink( $post->ID ).'" class="button">View</a>';
				echo '</div>';
                break;
			default:
			    break;
  		endswitch;

	}

	/**
	 * add custom column to telebotstore_toko post type
	 * manage_edit-affiliate_columns hook
	 * @param [type] $columns [description]
	 */
	public function store_columns( $columns )
	{
		$columns = array(
			'cb'           => '<input type="checkbox" />',
			'toko_name'    => __( 'Nama Toko', 'telebotstore' ),
			'toko_address' => __( 'Alamat', 'telebotstore' ),
			'toko_action'  => __('&nbsp', 'telebotstore'),
		);

		return $columns;
	}

	/**
	 * manage telebotstore_toko post type custom column
	 * @param  [type] $column  [description]
	 * @param  [type] $post_id [description]
	 * @return [type]          [description]
	 */
	public function manage_store_columns( $column, $post_id )
	{
		global $post;

		switch( $column ) :
			case 'toko_name' :
			    echo '<span style="font-weight:bold">'.$post->post_title.'</span><br />';
				break;
			case 'toko_address' :
			    $address_meta = get_post_meta($post_id, '_telebotstore_toko_address', true);
			    if( $address_meta ):
					$address = $address_meta;
				else :
					$address = $post->post_content;
				endif;

			    echo '<span style="font-weight:bold">'.$address.'</span><br />';
				break;
			case 'toko_action' :
			    echo '<div style="text-align:right">';
				if( isset($_GET['post_status']) && $_GET['post_status'] == 'trash' ):
					$restore_link = wp_nonce_url('post.php?action=untrash&amp;post='.$post_id,'untrash-post_'.$post_id);
					echo '<a href="'.$restore_link.'" class="button">Restore</a>&nbsp';
					echo '<a href="'.get_delete_post_link( $post->ID, '', true ).'" class="button">Delete Permanently</a>&nbsp';
				else:
					echo '<a href="'.get_edit_post_link( $post->ID ).'" class="button">Edit</a>&nbsp';
					if( !isset($_GET['post_status']) || isset($_GET['post_status']) && $_GET['post_status'] !== 'draft' ):
						echo '<a target="_blank" href="'.get_the_permalink( $post->ID ).'" class="button">View</a>';
					endif;
					echo '<a href="'.get_delete_post_link( $post->ID ).'" class="button">Delete</a>&nbsp';
				endif;
				echo '</div>';
                break;
			default:
			    break;
  		endswitch;

	}

	/**
	 * add custom column to telebotstore_item post type
	 * manage_edit-affiliate_columns hook
	 * @param [type] $columns [description]
	 */
	public function item_columns( $columns )
	{
		$columns = array(
			'cb'          => '<input type="checkbox" />',
			'item_name'   => __( 'Nama', 'telebotstore' ),
			'item_price'   => __( 'Harga (@) Rp', 'telebotstore' ),
			'item_action' => __('&nbsp', 'telebotstore'),
		);

		return $columns;
	}

	/**
	 * manage telebotstore_item post type custom column
	 * @param  [type] $column  [description]
	 * @param  [type] $post_id [description]
	 * @return [type]          [description]
	 */
	public function manage_item_columns( $column, $post_id )
	{
		global $post;

		switch( $column ) :
			case 'item_name' :
			    echo '<span style="font-weight:bold">'.get_the_title($post->ID).'</span><br />';
				break;
			case 'item_price' :
			    echo '<span style="font-weight:bold">Rp '.number_format(carbon_get_post_meta($post->ID, 'item_price'),0,',','.').'</span><br />';
				break;
			case 'item_action' :
			    echo '<div style="text-align:right">';
				echo '<a href="'.get_edit_post_link( $post->ID ).'" class="button">Edit</a>&nbsp';
				//echo '<a target="_blank" href="'.get_the_permalink( $post->ID ).'" class="button">View</a>';
				echo '<a href="'.get_delete_post_link( $post->ID ).'" class="button">Delete</a>&nbsp';
				echo '</div>';
                break;
			default:
			    break;
  		endswitch;

	}

	/**
	 * initialisation carbon fields
	 * @return [type] [description]
	 */
	public function carbon_init()
	{
		\Carbon_Fields\Carbon_Fields::boot();
	}

	/**
	 * add post meta
	 * hooked via carbon_fields_register_fields
	 * @return [type] [description]
	 */
	public function add_post_meta()
	{
		add_meta_box(
			'telebotstore_order',
			'Order Detail',
			[$this, 'order_detail_page'],
			'telebotstore_order',
			'normal',
			'high'
		);

		// add_meta_box(
		// 	'telebotstore_order_page',
		// 	'Set Halaman sebagai Order List',
		// 	[$this, 'order_list_page'],
		// 	'page',
		// 	'side',
		// 	'high'
		// );
	}

	public function order_detail_page()
	{
		global $post;

		include( plugin_dir_path( __FILE__ ).'partials/metabox-order.php' );
	}

	public function order_list_page()
	{
		global $post;
		$value = get_post_meta( $post->ID, '_telebotstore_order_list_page', true)
		?>
		<input type="hidden" name="_telebotstore_order_list_page" value=""/>
		<input type="checkbox" name="_telebotstore_order_list_page" <?php if($value){echo 'checked="checked"'; }; ?>/> YA
		<?php
	}

	public function post_meta_save( $post_id, $post )
	{
		// return if autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
			return;
		}

		// Return if the user doesn't have edit permissions.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		if ( isset( $_REQUEST['_order_status'] ) ) {
			update_post_meta( $post_id, '_order_status', sanitize_text_field( $_POST['_order_status'] ) );
			// update_post_meta( $post_id, '_order_status_id_update', sanitize_text_field( $_POST['_order_status_id_update']) );
			// update_post_meta( $post_id, '_order_status_date_update', date('Y-m-d H:i:s'));

			if( $_REQUEST['_order_status'] == 'reject'):
				update_post_meta( $post_id, '_order_reject_reason', sanitize_text_field( $_POST['_order_reject_reason'] ) );
			else:
				update_post_meta( $post_id, '_order_reject_reason', '' );
			endif;

			$args = array(
				'user_id' => get_current_user_id(),
				'order_id' => $post_id,
				'status' => $_POST['_order_status'],
			);
			$log = new Telebotstore_Order_Status_Log($args);
			$log->insert();

			$store =  get_post_meta( $post_id, '_order_store', true);

			$user = get_userdata( $_POST['_order_status_id_update'] );

			$title      = 'Update status order oleh '.$user->user_login;
		    $content    = 'Order dari toko '.$store.' dari pending ke '.$_REQUEST['_order_status'].'>> KLIK DISINI untuk melihat.';
			$url        = get_the_permalink($post_id);

			Telebotstore_OneSignal::send($title,$content,$url);
		}

		if ( isset( $_REQUEST['_order_toko'] ) ) {
			update_post_meta( $post_id, '_order_toko', sanitize_text_field( $_POST['_order_toko'] ) );
		}

		if ( isset( $_REQUEST['_order_tempo'] ) ) {
			update_post_meta( $post_id, '_order_tempo', sanitize_text_field( $_POST['_order_tempo'] ) );
		}

		if ( isset( $_REQUEST['_order_pengiriman'] ) ) {
			update_post_meta( $post_id, '_order_pengiriman', sanitize_text_field( $_POST['_order_pengiriman'] ) );
		}

		if ( isset( $_REQUEST['_telebotstore_order_list_page'] ) ) {
			update_post_meta( $post_id, '_telebotstore_order_list_page', sanitize_text_field( $_POST['_telebotstore_order_list_page'] ) );
		}
	}

	public function list_page()
	{
		$args = array(
			'post_type' => 'page',
			'posts_per_page' => 30,
		);

		$query = new WP_Query();

		$pages = $query->query($args);

		$objs = array();

		foreach( $pages as $page ):
			$objs[$page->ID] = $page->post_title;
		endforeach;

		return $objs;
	}

	public function add_options()
	{
		/**
		 * post meta for order post type
		 * @var [type]
		 */
		Container::make( 'theme_options', __( 'Telegram Bot Options', 'telebotstore' ) )
				->set_page_menu_position( 20 )
				->add_tab( __('Options'), array(
					Field::make( 'textarea', 'order_item_qty', 'Quantity List')->set_width( 30 ),
					Field::make( 'textarea', 'order_item_pembayaran', 'Pembayaran List')->set_width( 40 ),
					Field::make( 'textarea', 'order_item_pengiriman', 'Pengiriman List')->set_width( 30 ),
					Field::make( 'text', 'onesignal_app_id', 'Onesignal APP ID')->set_width( 50 ),
					Field::make( 'text', 'onesignal_app_key', 'Onesignal APP Key')->set_width( 50 ),
					Field::make( 'text', 'telegram_api_key', 'Telegram Bot Api Key')->set_width( 40 ),
					Field::make( 'text', 'telegram_bot_username', 'Telegram Bot Username')->set_width( 30 ),
					Field::make( 'text', 'telegram_bot_admin', 'Telegram Bot Admin Username')->set_width( 30 ),
					Field::make( 'text', 'telebotstore_s3_key', 'Amazon S3 Api Key')->set_width( 30 ),
					Field::make( 'text', 'telebotstore_s3_secret', 'Amazon S3 Secret')->set_width( 30 ),
					Field::make( 'text', 'telebotstore_s3_bucket_name', 'Amazon S3 Bucket Name')->set_width( 40 ),
					Field::make( 'select', 'list_order', 'List Order Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'list_order_log', 'List Order Log Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'list_comment', 'List Comment Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'list_order_pending', 'List Order Pending Page' )->set_width(50)
						->add_options($this->list_page()),
					Field::make( 'select', 'list_order_reject', 'List Order Reject Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'list_notification', 'List Notification Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'order_form', 'Order Form Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'checkin', 'Check In Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'visit_count', 'Visit Count Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'order_sumary_store', 'Order Sumary Store' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'ticket', 'Ticket Page' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'toko_contact', 'Customer Contact' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'list_order_csi', 'List Order CSI' )->set_width(50)
					    ->add_options($this->list_page()),
					//Lima Multimedia 180622
					Field::make( 'select', 'checkin_log', 'Check In Log' )->set_width(50)
					    ->add_options($this->list_page()), 
					Field::make( 'select', 'sales_area', 'Sales Area' )->set_width(50)
					    ->add_options($this->list_page()),   
					Field::make( 'select', 'jadwal_kunjungan', 'Jadwal Kunjungan' )->set_width(50)
					    ->add_options($this->list_page()), 
					Field::make( 'select', 'list_jadwal_kunjungan', 'List Jadwal Kunjungan' )->set_width(50)
					    ->add_options($this->list_page()),              
					Field::make( 'select', 'report_kunjungan', 'Report Kunjungan' )->set_width(50)
					    ->add_options($this->list_page()),
					Field::make( 'select', 'statistik_toko', 'Statistik Toko' )->set_width(100)
					    ->add_options($this->list_page()),        

					/** Notifikasi (Reminder) */
					Field::make( 'text', 'reminder_order_pending_spv', 'Notif Order Pending SPV > x Hari' )->set_width(50),    
					Field::make( 'text', 'reminder_order_pending_manager', 'Notif Order Pending Manager > x Hari' )->set_width(50),
					Field::make( 'text', 'email_reminder_spv', 'Email Reminder Order Pending - SPV' )->set_width(50), 
					Field::make( 'text', 'email_reminder_manager', 'Email Reminder Order Pending - Manager' )->set_width(50), 
					/** End Notifikasi */

					Field::make( 'file', 'google_sheet_api', 'Google Sheet Api Key (json file)')->set_value_type('url')->set_width(30),
					Field::make( 'text', 'google_sheet_spreadsheet_id', 'Spreadsheet ID' )->set_width(35),
					Field::make( 'text', 'google_sheet_spreadsheet_sheet', 'Sheet Name' )->set_width(35),
					Field::make( 'text', 'google_maps_api', 'Google Maps Api Key')->set_width(100),
					Field::make( 'complex', 'api_data_key', 'Key Data Api' )
					    ->add_fields( array(
							Field::make( 'text', 'key_name' )->set_width(100),
						))->set_layout('tabbed-vertical')
						->set_header_template( '<% if (key_name) { %> <%- key_name %> <% } %> ' ),
					Field::make( 'association', 'telebotstore_checkin_exclude_user', 'Exclude User on Checkin Statistict')
					    ->set_types(array(
							array(
								'type' => 'user',
							),
						)),
				))
				->add_tab(__('Check In Custom Fields'), array(
					Field::make( 'complex', 'checkin_detail_fields')->add_fields( array(
						Field::make('text', 'label', 'Field Label')->set_width(40),
						Field::make('text', 'name', 'Field Name')->set_help_text('Please Use lower text')->set_width(30),
						Field::make('select', 'type', 'Field Type')->add_options(['text'=>'Text','textarea'=>'Textarea','select'=>'Select','radio'=>'Radio','checkbox'=>'Checkbox','file'=>'File Upload'])->set_width(30),
						Field::make('textarea', 'option', 'Field Options')->set_width(100)->set_help_text('Define Options, sparate by comma')->set_rows( 2 )->set_conditional_logic(array(
							'relation' => 'OR',
							array(
								'field' => 'type',
								'value' => 'select',
								'compare' => '='
							),
							array(
								'field' => 'type',
								'value' => 'radio',
								'compare' => '='
							),
						)),
						Field::make('text', 'checkbox_value', 'Checkbox Value')->set_width(100)->set_conditional_logic(array(
							array(
								'field' => 'type',
								'value' => 'checkbox',
								'compare' => '='
							),
						))
					))->set_header_template( '<% if (name) { %> <%- name %> <% } %> ' ),
				))
				->add_tab(__('Contact Toko Fields'), array(
					Field::make( 'complex', 'toko_contact_detail_fields')->add_fields( array(
						Field::make('text', 'label', 'Field Label')->set_width(40),
						Field::make('text', 'name', 'Field Name')->set_help_text('Please Use lower text')->set_width(30),
						Field::make('select', 'type', 'Field Type')->add_options(['text'=>'Text','textarea'=>'Textarea','select'=>'Select','radio'=>'Radio','checkbox'=>'Checkbox','file'=>'File Upload'])->set_width(30),
						Field::make('textarea', 'option', 'Field Options')->set_width(100)->set_help_text('Define Options, sparate by comma')->set_rows( 2 )->set_conditional_logic(array(
							'relation' => 'OR',
							array(
								'field' => 'type',
								'value' => 'select',
								'compare' => '='
							),
							array(
								'field' => 'type',
								'value' => 'radio',
								'compare' => '='
							),
						)),
						Field::make('text', 'checkbox_value', 'Checkbox Value')->set_width(100)->set_conditional_logic(array(
							array(
								'field' => 'type',
								'value' => 'checkbox',
								'compare' => '='
							),
						))
					))->set_header_template( '<% if (name) { %> <%- name %> <% } %> ' ),
				));

		// /**
		//  * post meta for page post type
		//  * @var [type]
		//  */
		// Container::make( 'post_meta', __( 'Set Halaman sebagai Log List', 'telebotstore' ) )
		// 		->where( 'post_type', '=', 'page' )
		// 		->set_context( 'side' )
		// 		->add_fields( array(
		// 			Field::make( 'checkbox', 'telebotstore_log_list_page', 'Ya')
		// 			    ->set_option_value( 'yes' ),
		// 		));
		//
		// /**
		//  * post meta for page post type
		//  * @var [type]
		//  */
		// Container::make( 'post_meta', __( 'Set Halaman sebagai Comment List', 'telebotstore' ) )
		// 		->where( 'post_type', '=', 'page' )
		// 		->set_context( 'side' )
		// 		->add_fields( array(
		// 			Field::make( 'checkbox', 'telebotstore_comment_list_page', 'Ya')
		// 			    ->set_option_value( 'yes' ),
		// 		));

		/**
		 * post meta for page post type
		 * @var [type]
		 */

		$store_datas = $this->api_data_key();
		$fields = array();

		foreach( (array) $store_datas as $key=>$val){
			$val = preg_replace('/\s+/', '_', $val);
			$fields[] = Field::make( 'text', 'telebotstore_toko_'.strtolower($val), $val);
		}

		Container::make( 'post_meta', __( 'Sales', 'telebotstore' ) )
				->where( 'post_type', '=', 'telebotstore_toko' )
				->add_fields( array(
					Field::make( 'association', 'telebotstore_toko_sales', '')
					    ->set_types(array(
							array(
								'type' => 'user',
							),
						))
						->set_max(1),
				));

		Container::make( 'post_meta', __( 'Detail', 'telebotstore' ) )
				->where( 'post_type', '=', 'telebotstore_toko' )
				->add_fields($fields);

		Container::make( 'post_meta', __( 'Item Detail', 'telebotstore' ) )
				->where( 'post_type', '=', 'telebotstore_item' )
				->add_fields( array(
					Field::make( 'text', 'item_price', 'Harga (Rp)')->set_help_text('Bilangan Bulat misal : 20000 (untuk dua puluh ribu)'),
				));



		/**
		 * post meta for page post type
		 * @var [type]
		 */
		$couriers = explode(PHP_EOL, carbon_get_theme_option('order_item_pengiriman'));

		foreach( $couriers as $key=>$val ) :
			$kurir[rtrim($val)] = $val;
		endforeach;

		Container::make( 'user_meta', __( 'Order', 'telebotstore' ) )
				->add_fields( array(
					Field::make( 'set', 'telebotstore_user_pengiriman', 'Izin Lihat Order By Pengiriman ? ')
					    ->add_options($kurir)
				));
	}

	/**
     * register admin menu
     * @return [type] [description]
     */
    public function telebotstore_menu()
    {
        add_submenu_page(
			'edit.php?post_type=telebotstore_toko',
            __('Importer', 'telebotstore'),
            __('Importer', 'telebotstore'),
            'publish_telebotstores',
            'admin.php?page=importer',
            [$this, 'importer_page_menu']
        );

		add_submenu_page(
			'edit.php?post_type=telebotstore_order',
            __('Order Baru', 'telebotstore'),
            __('Order Baru', 'telebotstore'),
            'publish_telebotstores',
            'admin.php?page=new-order',
            [$this, 'order_page_menu']
        );
    }

    /**
     * admin page
     * @return [type] [description]
     */
    public function importer_page_menu()
    {
        include( plugin_dir_path( __FILE__ ) . 'partials/importer.php' );
    }

	/**
     * admin page
     * @return [type] [description]
     */
    public function order_page_menu()
    {
        include( plugin_dir_path( __FILE__ ) . 'partials/new-order.php' );
    }


	/**
	 * order bulk actions
	 * hooked via bulk_action-edit-telebotstore_order
	 * @return [type] [description]
	 */
	public function order_bulk_action( $bulk_actions )
	{
		$actions['order_done'] = __( 'Done', 'domain');
		$actions['order_reject'] = __( 'Reject', 'domain');
		$actions['trash'] = __( 'Trash', 'domain');
		return $actions;
	}

	public function delete_admin_bulk_action( $bulk_actions )
	{
		$actions['trash'] = __( 'Trash', 'domain');
		return $actions;
	}

	/**
	 * remove default date filter on order
	 * @param  [type] $months    [description]
	 * @param  [type] $post_type [description]
	 * @return [type]            [description]
	 */
	public function remove_month_filters( $months, $post_type )
	{
		return in_array( $post_type, array( 'telebotstore_order' ) ) ? array() : $months;
	}

	/**
	 * order bulk actions handler
	 * hooked via handle_bulk_action-edit-telebotstore_order
	 * @return [type] [description]
	 */
	public function order_bulk_action_handler( $redirect_to, $action_name, $post_ids )
	{
		if( 'order_done' == $action_name ):
			foreach( $post_ids as $post_id ):
				$status = get_post_meta( $post_id, '_order_status', true );
				if( 'pending' == $status ):
					update_post_meta($post_id, '_order_status', 'done');
					update_post_meta($post_id, '_order_status_id_update', get_current_user_id() );
					update_post_meta($post_id, '_order_status_date_update', date('Y-m-d H:i:s'));
				endif;
			endforeach;
		endif;

		if( 'order_reject' == $action_name ):
			foreach( $post_ids as $post_id ):
				$status = get_post_meta( $post_id, '_order_status', true );
				if( 'pending' == $status ):
					update_post_meta($post_id, '_order_status', 'reject');
					update_post_meta($post_id, '_order_status_id_update', get_current_user_id() );
					update_post_meta($post_id, '_order_status_date_update', date('Y-m-d H:i:s'));
				endif;
			endforeach;
		endif;

		return $redirect_to;
	}

	/**
	 * custom filter on order
	 * @return [type] [description]
	 */
	public function custom_order_filter()
	{
		global $wpdb;

		$type = 'post';
	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }

		$table = $wpdb->prefix.'postmeta';

		$sql = 'SELECT meta_value FROM '.$table.' WHERE meta_key = "_order_store"';

		$stores = $wpdb->get_results( $sql, OBJECT );
		$toko = array();

		foreach( $stores as $store ):
			$toko[$store->meta_value] = $store->meta_value;
		endforeach;

	    //only add filter to post type you want
	    if ('telebotstore_order' == $type){
	        $current_v = isset($_GET['order-by-date'])? $_GET['order-by-date']: '';
	        ?>
	        <input type="text" name="order-by-date" id="date-order" value="<?php echo $current_v; ?>" placeholder="Filter By Date">

	        <?php
	            $current_toko = isset($_GET['order-by-toko'])? $_GET['order-by-toko']:'';
				if( $current_toko ):
					$t = get_post($current_toko);
					$current_toko_name = $t->post_title;
				else :
					$current_toko_name = 'Filter By Toko';
				endif;
	        ?>
			<select id="order-toko" class="regular-text" name="order-by-toko">
				<option value="<?php echo $current_toko; ?>" selected="selected"><?php echo $current_toko_name; ?></option>
			</select>
	        <?php

			$values = array(
	            'pending' => 'Pending',
	            'done' => 'Done',
	            'reject' => 'Reject',
	        );
	        ?>
	        <select name="order-by-status" id="status-order">
	        <option value=""><?php _e('Filter By Status', 'telebotstore'); ?></option>
	        <?php
	            $current_status = isset($_GET['order-by-status'])? $_GET['order-by-status']:'';
	            foreach ($values as $key => $val) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $key,
	                        $key == $current_status? ' selected="selected"':'',
	                        $val
	                    );
	                }
	        ?>
	        </select>


	        <select name="order-by-item" id="item-order">
	        <option value=""><?php _e('Filter By Item', 'telebotstore'); ?></option>
	        <?php
	            $current_item = isset($_GET['order-by-item'])? $_GET['order-by-item']:'';
	            foreach ( $this->get_all_items() as $key => $val) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $key,
	                        $key == $current_item? ' selected="selected"':'',
	                        $val
	                    );
	                }
	        ?>
	        </select>

			<select name="order-by-tempo" id="tempo-order">
	        <option value=""><?php _e('Filter By Tempo', 'telebotstore'); ?></option>
	        <?php
	            $current_item = isset($_GET['order-by-tempo'])? $_GET['order-by-tempo']:'';
	            foreach ( $this->get_all_tempo() as $key => $val) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $key,
	                        $key == $current_item? ' selected="selected"':'',
	                        $val
	                    );
	                }
	        ?>
	        </select>

			<select name="order-by-kurir" id="via-order">
	        <option value=""><?php _e('Filter By Pengiriman', 'telebotstore'); ?></option>
	        <?php
	            $current_kurir = isset($_GET['order-by-kurir']) ? $_GET['order-by-kurir']:'';
	            foreach ( $this->get_all_kurir() as $key => $val) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $key,
	                        $key == $current_kurir ? ' selected="selected"':'',
	                        $val
	                    );
	                }
	        ?>
	        </select>

			<select name="order-by-user" id="user-order">
	        <option value=""><?php _e('Filter By User', 'telebotstore'); ?></option>
	        <?php
	            $current_kurir = isset($_GET['order-by-user']) ? $_GET['order-by-user']:'';
	            foreach ( $this->get_all_user() as $key => $val) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $key,
	                        $key == $current_kurir ? ' selected="selected"':'',
	                        $val
	                    );
	                }
	        ?>
	        </select>
	        <?php

	    }
	}

	/**
	 * get all list item
	 * @return [type] [description]
	 */
	public function get_all_user()
	{
		$args = array();

		$users = get_users( $args );

		foreach( $users as $user ):
			$u[$user->ID] = $user->data->display_name;
		endforeach;

		return $u;
	}

	/**
	 * get all list toko
	 * @return [type] [description]
	 */
	public function get_all_toko()
	{
		$args = array(
			'post_type'=> 'telebotstore_toko',
			'posts_per_page' => -1,
		);

		$posts = get_posts( $args );

		foreach( $posts as $post ):
			$p[$post->ID] = $post->post_title;
		endforeach;

		return $p;
	}


	/**
	 * get all list item
	 * @return [type] [description]
	 */
	public function get_all_kurir()
	{
		$couriers = explode(PHP_EOL, carbon_get_theme_option('order_item_pengiriman'));

		$user_couriers = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );

		$vias = array();
		foreach( (array) $couriers as $key=>$val ):
			if( $user_couriers ):
				if( in_array( rtrim($val), (array) $user_couriers) ):
					$vias[rtrim($val)] = $val;
				endif;
			else :
				$vias[rtrim($val)] = $val;
			endif;
		endforeach;

		return $vias;
	}

	/**
	 * get all list item
	 * @return [type] [description]
	 */
	public function get_all_tempo()
	{
		$payments = explode(PHP_EOL, carbon_get_theme_option('order_item_pembayaran'));;

		$items = array();
		foreach( (array) $payments as $key=>$val ):
			$items[$val] = $val;
		endforeach;

		return $items;
	}

	/**
	 * get all list item
	 * @return [type] [description]
	 */
	public function get_all_items()
	{
		$args = array(
			'post_type' => 'telebotstore_item',
			'posts_per_page' => -1,
			'post_status' => 'publish',
		);

		$posts = get_posts( $args );

		$items = array();
		foreach( (array) $posts as $item ):
			$items[$item->post_name] = $item->post_name;
		endforeach;

		return $items;
	}

	/**
	 * result custom filter
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function result_order_filter( $query )
	{
		global $pagenow;

	    $type = 'post';
		$meta_query = array();

		$user_pengiriman = carbon_get_user_meta(get_current_user_id(), 'telebotstore_user_pengiriman' );

	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }

		if ( 'telebotstore_order' == $type && is_admin() && $pagenow == 'edit.php' ) :
			if( isset($_GET['order-by-date']) && $_GET['order-by-date'] != '' ) :
				$before = date('d-M-Y',date(strtotime("+1 day", strtotime($_GET['order-by-date']))));
				$date = array(
					'after'  => $_GET['order-by-date'],
					'before' => $before,
				);
		        $query->set('date_query', array($date));
		    endif;

			if( isset($_GET['order-by-user']) && $_GET['order-by-user'] != '' ) :
		        $query->set('author', $_GET['order-by-user']);
		    endif;

			if ( isset($_GET['order-by-toko']) && $_GET['order-by-toko'] != '') :
				$meta_query[] = array(
			            'key' => '_order_toko',
			            'value' => $_GET['order-by-toko'],
			            'compare' => '='
			        );
		    endif;

			if ( isset($_GET['order-by-status']) && $_GET['order-by-status'] != '') :
				$meta_query[] = array(
			            'key' => '_order_status',
			            'value' => $_GET['order-by-status'],
			            'compare' => 'LIKE'
			        );
		    endif;

			if ( isset($_GET['order-by-item']) && $_GET['order-by-item'] != '') :
				$meta_query[] = array(
			            'key' => '_order_items',
			            'value' => $_GET['order-by-item'],
			            'compare' => 'LIKE'
			        );
		    endif;

			if ( isset($_GET['order-by-tempo']) && $_GET['order-by-tempo'] != '') :
				$meta_query[] = array(
			            'key' => '_order_tempo',
			            'value' => $_GET['order-by-tempo'],
			            'compare' => 'LIKE'
			        );
		    endif;

			if ( isset($_GET['order-by-kurir']) && $_GET['order-by-kurir'] != '') :
				if( $user_pengiriman ):
					if( in_array( $_GET['order-by-kurir'], $user_pengiriman) ):
						$kurir = $_GET['order-by-kurir'];
					else :
						$kurir = $user_pengiriman[0];
					endif;
				else :
					$kurir = $_GET['order-by-kurir'];
				endif;

				$meta_query[] = array(
			            'key' => '_order_pengiriman',
			            'value' => $kurir,
			            'compare' => 'LIKE'
			        );
		    endif;

			if ( count($meta_query) > 1 ) :
				$meta_query['relation'] = 'AND';
			endif;

			if ( $meta_query ):
				$query->query_vars['meta_query'] = $meta_query;
			endif;

		endif;

	}

	/**
	 * add custom capabilities to admnisitrator,editor,andadmin_toko role
	 */
	function add_role_caps()
	{
		// Add the roles you'd like to administer the custom post types
	    $roles = array('admin_toko','editor','administrator');

	    // Loop through each role and assign capabilities
	    foreach($roles as $the_role) :

			$role = get_role($the_role);
			$role->add_cap( 'read' );
			$role->add_cap( 'read_telebotstore');
			$role->add_cap( 'read_private_telebotstores' );
			$role->add_cap( 'edit_telebotstore' );
			$role->add_cap( 'edit_telebotstores' );
			$role->add_cap( 'edit_others_telebotstores' );
			$role->add_cap( 'edit_published_telebotstores' );
			$role->add_cap( 'publish_telebotstores' );
			$role->add_cap( 'delete_others_telebotstores' );
			$role->add_cap( 'delete_private_telebotstores' );
			$role->add_cap( 'delete_published_telebotstores' );

		endforeach;
	}

	public function add_export_button()
	{
	    global $current_screen;

	    // Not our post type, exit earlier
	    // You can remove this if condition if you don't have any specific post type to restrict to.
	    if ('telebotstore_order' != $current_screen->post_type) {
	        return;
	    }
	    ?>
	        <script type="text/javascript">
	            jQuery(document).ready( function($)
	            {
	                jQuery(jQuery(".wrap h1")[0]).append("<a  id='export-excel' class='add-new-h2'>Export To Excel</a>");

					jQuery('#export-excel').on('click', function(){
						$(this).html('Proses..');

						$.ajax({
							type: "POST",
							url: '<?php echo admin_url('admin-ajax.php'); ?>',
							dataType: "json",
							data: {
								action: 'export_orders',
								nonce: '<?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
								date: $("#date-order").val(),
								store: $("#store-order").val(),
								status: $("#status-order").val(),
								item: $("#item-order").val(),
								tempo: $("#tempo-order").val(),
								via: $("#via-order").val(),
							},
							success: function(data){
								$('#export-excel').html('Export To excel');
								console.log(data.url);
								window.open(data.url, '_blank');
							}
						});
					})
	            });
	        </script>
	    <?php
	}

	public function add_toko_export_button()
	{
	    global $current_screen;

	    // Not our post type, exit earlier
	    // You can remove this if condition if you don't have any specific post type to restrict to.
	    if ('telebotstore_toko' != $current_screen->post_type) return;

	    ?>
	        <script type="text/javascript">
	            jQuery(document).ready( function($)
	            {
	                jQuery(jQuery(".wrap h1")[0]).append("<a  id='export-excel' class='add-new-h2'>Export To Excel</a>");

					jQuery('#export-excel').on('click', function(){
						$(this).html('Proses..');

						$.ajax({
							type: "POST",
							url: '<?php echo admin_url('admin-ajax.php'); ?>',
							dataType: "json",
							data: {
								action: 'export_toko',
								nonce: '<?php echo wp_create_nonce( 'telebotstore_nonce' ); ?>',
							},
							success: function(data){
								$('#export-excel').html('Export To excel');
								console.log(data.url);
								window.open(data.url, '_blank');
							}
						});
					})
	            });
	        </script>
	    <?php
	}

	/**
	 * filter post list in admin
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function extend_admin_search( $query )
	{

		// if( in_array ( $query->get('post_type'), array('telebotstore_toko') ) ):
		//
		// 	$query->set( 'meta_key', '_provider' );
		// 	$query->set( 'meta_value', 'api' );
		//
		// 	return;
		//
		// endif;
	}

	/**
	 * ajax search toko
	 * @return [type] [description]
	 */
	public function ajax_seacrh_toko()
	{
		global $post;

		$key     = isset($_GET['key']) ? sanitize_text_field( $_GET['key'] ) : '';
		$page    = isset($_GET['page']) ? intval( $_GET['page'] ) : '';
		$user_id = isset($_GET['user_id']) ? intval( $_GET['user_id'] ) : '';

		$args = array(
			'post_type'      => 'telebotstore_toko',
			'posts_per_page' => 20,
			'orderby'        => 'title',
			'order'          => 'ASC',
			'paged'          => $page
		);

		$meta_query = array();
		// $meta_query[] = array(
		//     'key' => '_name',
		//     'value' => $key,
		//     'compare' => 'LIKE',
		// );
		//
		// $meta_query[] = array(
		//     'key' => '_address',
		//     'value' => $key,
		//     'compare' => 'LIKE',
		// );

		// if($user_id):
		// 	$meta_query[] = array(
		// 		'key' => '_telebotstore_toko_sales|||0|id',
		// 		'value' => $user_id,
		// 		'compare' => '=',
		// 	);
		// endif;

		//if there is more than one meta query 'or' them
		if(count($meta_query) > 1) {
		    $meta_query['relation'] = 'OR';
		}

		if( $key ):
			$args['s'] = $key;
		endif;

// 		$args['_meta_or_title'] = $key; //not using 's' anymore
 		$args['meta_query'] = $meta_query;


		$query 	= new WP_Query($args);
		$tokos = array();
		if($query->have_posts()) :
			while($query->have_posts()) :
				$query->the_post();

				$address_meta = get_post_meta($post->ID, '_telebotstore_toko_address', true);

				if( $address_meta ):
					$address = $address_meta;
				else:
					$address = $post->post_content;
				endif;

				$tokos[] = array(
					'id' => $post->ID,
					'text' => $post->post_title . ' ===> ' . $address,
				);
			endwhile;
		endif;

		if( $query->found_posts == false || $query->found_posts <= ($page * 20) ):
			$more = false;
		else:
			$more = true;
		endif;

		$response['results'] = $tokos;
		$response['pagination'] = array(
			'more' => $more,
			'max' => $query->found_posts,
		);

		echo json_encode( $response );
		exit;
	}

	public function ajax_seacrh_item()
	{
		global $post;

		$key   = sanitize_text_field( $_GET['key'] );

		$args = array(
			'post_type'=> 'telebotstore_item',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC',
			's' => $key,
		);

		$query 	= new WP_Query($args);
		$tokos = array();
		if($query->have_posts()) :
			while($query->have_posts()) :
				$query->the_post();

				$price = carbon_get_post_meta($post->ID, 'item_price');

				$tokos[] = array(
					'id' => $post->post_name,
					'text' => $post->post_name.'  @Rp '.number_format($price, 0,',','.'),
					'price' => $price,
				);
			endwhile;
		endif;

		$response['results'] = $tokos;
		$response['pagination'] = array(
			'more' => false
		);

		echo json_encode( $response );
		exit;
	}

	public function ajax_seacrh_user()
	{
		global $post;

		$key   = sanitize_text_field( $_GET['key'] );

		$args = array(
			'search' => '*'.$key.'*',
			'search_columns' => array( 'user_login', 'user_email' )
		);

		$query 	= new WP_User_Query($args);
		$get_users = $query->get_results();
		$users = array();
		if($get_users) :
			foreach( $get_users as $user ) :
				$users[] = array(
					'id' => $user->ID,
					'text' => $user->user_login,
				);
			endforeach;
		endif;

		$response['results'] = $users;
		$response['pagination'] = array(
			'more' => false
		);

		echo json_encode( $response );
		exit;
	}

	public function search_by_title_only( $search,$wp_query )
	{
	    global $wpdb;
	    if(empty($search)) {
	        return $search; // skip processing - no search term in query
	    }
	    $q = $wp_query->query_vars;
	    $n = !empty($q['exact']) ? '' : '%';
	    $search = $searchand = '';
	    foreach ((array)$q['search_terms'] as $term) {
	        $term = esc_sql($wpdb->esc_like($term));
	        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
	        $searchand = ' AND ';
	    }

	    return ' AND '.$search;
	}

	/**
	 * cronjob for checking database
	 * @return [type] [description]
	 */
	public function cron_webapi_checking(){

		set_time_limit(0);

		$this->get_data_from_api();

		//$this->delete_toko_web();

		$this->update_toko_web();
	}

	/**
	 * cronjob for checking database
	 * @return [type] [description]
	 */
	public function cron_sheetapi_checking(){

		set_time_limit(0);

		$this->update_toko_sheet();

		exit;
	}

	/**
	 * cronjob for checking database
	 * @return [type] [description]
	 */
	public function manual_checking(){

		set_time_limit(0);

		if(isset($_GET['type']) && $_GET['type'] == 'webapi' ):
			$error = $this->get_data_from_api();

			set_transient('webapierror', $error, 20 * MINUTE_IN_SECONDS);

			//$this->delete_toko();

			$this->update_toko_web();
		elseif(isset($_GET['type']) && $_GET['type'] == 'sheetapi' ):
			$this->update_toko_sheet();
		endif;

		echo 1;

		exit;
	}

	public function update_toko_sheet(){

		$args = array(
		    'post_type'      => 'telebotstore_toko',
		    'post_status'    => 'publish',
		    'fields'         => 'ids',
		    'posts_per_page' => -1,
		    'orderby'        => 'ID',
			'order'          => 'ASC',
		    'meta_key'       => '_provider',
		    'meta_value'     => 'api',
		);

		$post_stores = get_posts($args);
		$stores = array();

		$store_datas = $this->api_data_key();

		foreach($post_stores as $id ):
			$fields = array();
			foreach( $store_datas as $key=>$val){
				$fields[$val] = carbon_get_post_meta( $id, strtolower($val));
			}

			$stores[] = $fields;
		endforeach;

		if( !($stores) ):

			set_transient('sheet_datatoko_last_sync', date('Y-m-d H:i:s', current_time('timestamp')), 2 * DAY_IN_SECONDS);
			set_transient('sheet_datatoko_last_sync_status', 'failed', 2 * DAY_IN_SECONDS);

			return;
		endif;

		$tokos = array();

		foreach( (array) $stores as $store ):

			if( !isset($store['No']) ) continue;

			$tokos[] = $store;

		endforeach;

		$header = $data = array();

		foreach( (array) $tokos[0] as $key=>$val):
			$header[] = $key;
		endforeach;

		$data[] = $header;

		foreach( (array) $tokos as $store ):

			$toko = array();
			foreach( (array) $store as $key=>$val ):
				$toko[] = $val;
			endforeach;

			$data[] = $toko;
		endforeach;

		$key_data = count($store_datas) - 1;
		$abjad = range('A', 'Z');
		$line = $abjad[$key_data].count($data);

		$spreadsheetId = carbon_get_theme_option('google_sheet_spreadsheet_id');
		$sheetName = carbon_get_theme_option('google_sheet_spreadsheet_sheet');

		$client = new \Google_Client();
		$client->setApplicationName('Pisces Telebot Store');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->addScope('https://www.googleapis.com/auth/drive');
		$client->addScope('https://www.googleapis.com/auth/drive.appdata');

		$jsonAuth = file_get_contents(carbon_get_theme_option('google_sheet_api'));
		$client->setAuthConfig(json_decode($jsonAuth, true));

		$sheets = new \Google_Service_Sheets($client);
		// $drive = new Google_Service_Drive($client);
		//
		// $properties = new \Google_Service_Sheets_SpreadsheetProperties();
		// $properties->setTitle($post['title'].' '.date('Y-m-d H:i:s'));
		//
		// $newSpreadsheet = new Google_Service_Sheets_Spreadsheet();
		// $newSpreadsheet->setProperties($properties);
		//
		// $response = $sheets->spreadsheets->create($newSpreadsheet);
		//
		// foreach( (array) $emails as $email ):
		// 	if( !is_email($email) ):
		// 		continue;
		// 	endif;
		//
		// 	$userPermission = new Google_Service_Drive_Permission(
		// 		array(
		// 			'type' => 'user',
		// 			'role' => 'writer',
		// 			'emailAddress' => $email,
		// 		)
		// 	);
		//
		// 	$drive->permissions->create($response->spreadsheetId, $userPermission, array('fields' => 'id'));
		// endforeach;

		// $body = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(
		// 	array('requests' => array(
		// 		'addSheet' => array(
		// 			'properties' => array(
		// 				'title' => $sheet_name
		// 			)
		// 		)
		// 	)
		// ));
		//
		// $sheets->spreadsheets->batchUpdate($spreadsheetId, $body);

		$range = $sheetName.'!A1:'.$line;

		$updateBody = new \Google_Service_Sheets_ValueRange([
			'range' => $range,
			'majorDimension' => 'ROWS',
			'values' => $data,
		]);
		$sheets->spreadsheets_values->update(
			$spreadsheetId,
			$range,
			$updateBody,
			['valueInputOption' => 'USER_ENTERED']
		);

		set_transient('sheet_datatoko_last_sync', date('Y-m-d H:i:s', current_time('timestamp')), 2 * DAY_IN_SECONDS);
		set_transient('sheet_datatoko_last_sync_status', 'success', 2 * DAY_IN_SECONDS);

	}



	/**
	 * cronjob for checking database
	 * @return [type] [description]
	 */
	private function update_toko_web(){

		set_time_limit(0);

		$stores = get_transient('datatoko');

		if( !($stores) ):

			set_transient('web_datatoko_last_sync', date('Y-m-d H:i:s', current_time('timestamp')), 2 * DAY_IN_SECONDS);
			set_transient('web_datatoko_last_sync_status', 'failed', 2 * DAY_IN_SECONDS);

			return;
		endif;

		$keys = $this->api_data_key();

		foreach( (array) $stores as $store ):

			if( !isset($store['No']) ) continue;

			$id = post_exists($store['No']);

			$post = array(
	            'post_author'  => 1,
	            'post_content' => '',
	            'post_status'  => "publish",
	            'post_title'   => $store['No'],
	            'post_type'    => "telebotstore_toko",
	        );

			if($id):
				$post['ID'] = $id;
			endif;

	        $post_id = wp_insert_post( $post );
			update_post_meta( $post_id, '_provider', 'api');
			foreach( $keys as $key=>$val ):
				update_post_meta( $post_id, '_'.strtolower($val), $store[$val]);
			endforeach;
		endforeach;

		set_transient('web_datatoko_last_sync', date('Y-m-d H:i:s', current_time('timestamp')), 2 * DAY_IN_SECONDS);
		set_transient('web_datatoko_last_sync_status', 'success', 2 * DAY_IN_SECONDS);

		return;
	}

	private function delete_toko_web(){

		$args = array(
		    'post_type'      => 'telebotstore_toko',
		    'post_status'    => 'publish',
		    'fields'         => 'ids',
		    'posts_per_page' => 1000,
		    'orderby'        => 'ID',
			'order'          => 'ASC',
		    'meta_key'       => '_provider',
		    'meta_value'     => 'api',
		);

		$tokos = get_posts($args);

		foreach( $tokos as $key=>$val ):
			wp_delete_post($val, true);
		endforeach;
	}

	/**
	 * get data from ivendor api
	 * @return [type] [description]
	 */
	private function get_data_from_api(){

		set_time_limit(0);

		$stores = get_transient('datatoko');

		if($stores) return 'OK';

		$res = array();
		$i = 0;
		$repeat = true;
		while( $repeat == true ){

			$i++;

			$response = wp_remote_get(
				'http://api.ivendor.co.id/v1/customer',
				array(
					'timeout'     => 120,
					'httpversion' => '1.1',
				)
			);

			//$response->errors['http_request_failed'][0];

			if( isset($response->errors) ):

				$error = $response->errors['http_request_failed'][0];

				$repeat = false;

			else :

				$error = 'OK';

				if($response['body']):
					$respon = json_decode($response['body'], true);

					foreach( $respon as $val ):
						$res[] = $val;
					endforeach;

					if( $i == 1 ){
						$repeat = false;
					};
				else :
					$repeat = false;
				endif;
			endif;

		}

		set_transient('datatoko', $res, 20 * MINUTE_IN_SECONDS);

		return $error;
	}

	public function manual_update_toko($views){

		$webapierror = get_transient('webapierror');
		$webtime = get_transient('web_datatoko_last_sync');
		$web_get_status = get_transient('web_datatoko_last_sync_status') ? get_transient('web_datatoko_last_sync_status') : 'failed';

		$web_status = $web_get_status == 'success' ? '<span style="color:green">'.strtoupper($web_get_status).'</span>' : '<span style="color:red">'.strtoupper($web_get_status).'</span> : '.$webapierror;

		$sheettime = get_transient('sheet_datatoko_last_sync');
		$sheet_get_status = get_transient('sheet_datatoko_last_sync_status') ? get_transient('sheet_datatoko_last_sync_status') : 'failed';

		$sheet_status = $sheet_get_status == 'success' ? '<span style="color:green">'.strtoupper($sheet_get_status).'</span>' : '<span style="color:red">'.strtoupper($sheet_get_status);

		ob_start();
		?>
		<div id="welcome-panel" class="welcome-panel">
			<div class="welcome-panel-content" style="width:46%;float:left">
				<p><h2>Sync Website To Api</h2></p>
				<p><strong>Last Synchronize Time: <?php echo $webtime; ?></strong></p>
				<p><strong>Last Synchronize Status: <?php echo $web_status; ?></strong></p>
				<button class="button button-primary button-hero manual-sync-toko-web">Synchronize Now</button>
				<div class="welcome-panel-column-container">
					<p>
				</div>
		   </div>
		   <div class="welcome-panel-content" style="width:46%;float:right">
			   <p><h2>Sync Spreadsheet To Website</h2></p>
			   <p><strong>Last Synchronize Time: <?php echo $sheettime; ?></strong></p>
			   <p><strong>Last Synchronize Status: <?php echo $sheet_status; ?></strong></p>
			   <button class="button button-primary button-hero manual-sync-toko-sheet">Synchronize Now</button>
			   <div class="welcome-panel-column-container">
				   <p>
			   </div>
		  </div>
		</div>
		<?php

		$content = ob_get_contents();
		ob_end_clean();

		echo $content;

		return $views;
	}

	public function api_data_key(){
		$datas = carbon_get_theme_option('api_data_key');

		$keys = array();

		foreach( (array) $datas as $data ):
			$keys[] = $data['key_name'];
		endforeach;

		return $keys;
	}

	public function custom_myme_types($mime_types){
		$mime_types['json'] = 'application/json';
		return $mime_types;
	}

	public function ajax_toko_export_excel()
	{
		set_time_limit(0);

		$nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';

		if( ! wp_verify_nonce( $nonce, 'telebotstore_nonce' )) exit('Invalid Nonce');

		$next = true;
		$stores = array();
		$i = 0;

		$args = array(
			'post_type' => 'telebotstore_toko',
			//'fields' => 'ids',
			'posts_per_page' => 100,
			'post_status' => 'any',
			'orderby' => 'ID',
			'order' => 'ASC',
		);

		$q = new WP_Query();

		while ( $next == true ) :

			$args['offset'] = $args['posts_per_page'] * $i;

			$q->query($args);

			if( $q->posts ):
				$i++;
				$stores = array_merge($stores, $q->posts);
			else :
				$next = false;
				break;
			endif;

		endwhile;

		$upload_dir   = wp_upload_dir();
		$filename = date('y-m-d-H-i-s').'-stores.xlsx';

		$header = array(
			'Toko ID'     => 'string',
			'Toko Name'   => 'string',
			'Toko Sales' => 'string',
		);

		$store_datas = $this->api_data_key();
		$fields = array();

		foreach( (array) $store_datas as $key=>$val){
			$header['Toko '.$val] = 'string';
		}

		$styles = array( 'font'=>'Arial','font-size'=>11,'font-style'=>'bold', 'fill'=>'#eee', 'border'=>'left,right,top,bottom');

		$objs = array();

		foreach( $stores as $store ):

			$users = carbon_get_post_meta($store->ID, 'telebotstore_toko_sales');
			$sales = array();
			foreach((array)$users as $user ){
			    $u = get_userdata($user['id']);
			    $sales[] = $u->user_login;
			}

			$objs_1 = $objs_2 = array();

			$objs_1 = array(
				$store->ID,
				$store->post_title,
				implode(',', $sales),
			);
			foreach( (array) $store_datas as $key=>$val){
				$objs_2[] = get_post_meta($store->ID, '_telebotstore_toko_'.strtolower($val), true);
			}

			$objs[] = array_merge($objs_1, $objs_2);
		endforeach;

		$writer = new XLSXWriter();
		$writer->writeSheetHeader('Sheet1', $header, $styles);
		foreach($objs as $row):
		  $writer->writeSheetRow('Sheet1', $row );
		endforeach;

		$writer->writeToFile( $upload_dir['basedir'].'/'.$filename);

		$response['url'] = site_url() . '/wp-content/uploads/' . $filename;
		echo json_encode($response);
		exit;
	}

}
