/*
Title: Cozeit More plugin by Yasir Atabani
Documentation: na
Author: Yasir O. Atabani
Website: http://www.cozeit.com
Twitter: @yatabani

MIT License, https://github.com/cozeit/czMore/blob/master/LICENSE.md
*/
(function ($, undefined) {
    $.fn.czMore = function (options) {

        //Set defauls for the control
        var defaults = {
            max: 5,
            min: 0,
            onLoad: null,
            onAdd: null,
            onDelete: null
        };
        //Update unset options with defaults if needed
        var options = $.extend(defaults, options);
        $(this).bind("onAdd", function (event, data) {
            options.onAdd.call(event, data);
        });
        $(this).bind("onLoad", function (event, data) {
            options.onLoad.call(event, data);
        });
        $(this).bind("onDelete", function (event, data) {
            options.onDelete.call(event, data);
        });
        //Executing functionality on all selected elements
        return this.each(function () {
            var obj = $(this);
            var i = obj.children(".recordset").size();
            var divPlus = '<div id="btnPlus" />';
            var count = '<input id="' + this.id + '_czMore_txtCount" name="' + this.id + '_czMore_txtCount" type="hidden" value="0" size="5" />';

            obj.before(count);
            var recordset = obj.children("#first");
            obj.after(divPlus);
            var set = recordset.children(".recordset").children().first();
            var btnPlus = obj.siblings("#btnPlus");

            btnPlus.css({
                'float': 'left',
                'border': '0px',
                'background-position': 'center center',
                'background-repeat': 'no-repeat',
                'height': '25px',
                'width': '100px',
                'cursor': 'pointer',
            });

            if (recordset.length) {
                obj.siblings("#btnPlus").click(function () {
                    var i = obj.children(".recordset").size();
                    var item = recordset.clone().html();
                    i++;
                    item = item.replace(/\[([0-9]\d{0})\]/g, "[" + i + "]");
                    item = item.replace(/\_([0-9]\d{0})\_/g, "_" + i + "_");
                    //$(element).html(item);
                    //item = $(item).children().first();
                    //item = $(item).parent();

                    obj.append(item);
                    loadMinus(obj.children().last());
                    minusClick(obj.children().last());
                    if (options.onAdd != null) {
                        obj.trigger("onAdd", i);
                    }

                    obj.siblings("input[name$='czMore_txtCount']").val(i);
                    return false;
                });
                recordset.remove();
                for (var j = 0; j <= i; j++) {
                    loadMinus(obj.children()[j]);
                    minusClick(obj.children()[j]);
                    if (options.onAdd != null) {
                        obj.trigger("onAdd", j);
                    }
                }

                if (options.onLoad != null) {
                    obj.trigger("onLoad", i);
                }
                //obj.bind("onAdd", function (event, data) {
                //If you had passed anything in your trigger function, you can grab it using the second parameter in the callback function.
                //});
            }

            function resetNumbering() {
                $(obj).children(".recordset").each(function (index, element) {
                   $(element).find('input:text, input:password, input:file, select, textarea').each(function(){
                        old_name = this.name;
                        new_name = old_name.replace(/\_([0-9]\d{0})\_/g, "_" + (index + 1) + "_");
                        this.id = this.name = new_name;
                        //alert(this.name);
                    });
                    index++
                    minusClick(element);
                });
            }

            function loadMinus(recordset) {
                var divMinus = '<div id="btnMinus" />';
                $(recordset).children().first().before(divMinus);
                var btnMinus = $(recordset).children("#btnMinus");
                btnMinus.css({
                    'float': 'right',
                    'border': '0px',
                    'background-position': 'center center',
                    'background-repeat': 'no-repeat',
                    'height': '25px',
                    'width': '50px',
                    'cursor': 'pointer',
                });
            }

            function minusClick(recordset) {
                $(recordset).children("#btnMinus").click(function () {
                    var i = obj.children(".recordset").size();
                    var id = $(recordset).attr("data-id")
                    $(recordset).remove();
                    resetNumbering();
                    obj.siblings("input[name$='czMore_txtCount']").val(obj.children(".recordset").size());
                    i--;
                    if (options.onDelete != null) {
                        if (id != null)
                            obj.trigger("onDelete", id);
                    }
                });
            }
        });
    };
})(jQuery);

(function( $ ) {
	'use strict';

	$(document).ready(function(){

        jQuery('body').find('select.select2').select2();

		$('#date-order').datetimepicker({
			timepicker:false,
			format:'d-M-Y'
		});

		$('#order-toko').select2({
			ajax: {
				url: ajaxurl,
				dataType: "json",
				data: function(params){
					var query = {
						key: params.term,
						action: 'search_toko'
					}

					return query;
				},
			}
		});

		$('#sales').select2({
			ajax: {
				url: ajaxurl,
				dataType: "json",
				data: function(params){
					var query = {
						key: params.term,
						action: 'search_user'
					}

					return query;
				},
			}
		});

		$("#czContainer").czMore({
			// maximum number of form fields allowed
		    max: 95,

		    // minimum number of form fields allowed
		    min: 1,

		    // callback functions
		    onLoad: null,
		    onAdd: function(){
				$('.item').select2({
					ajax: {
						url: ajaxurl,
						dataType: "json",
						data: function(params){
							var query = {
								key: params.term,
								action: 'search_item'
							}

							return query;
						},
					}
				});
			},
		    onDelete: null

		});

        $('#order-statuses').on('change', function(){
            var status =  $(this).find(":selected").val();

            if(status == 'reject'){
                $('#reject-reason').show();
            }else{
                $('#reject-reason').hide();
            }
        });

        $('.manual-sync-toko-web').on('click', function(){
			$(this).html('Proses..');
			$.ajax({
				type: "GET",
				url: ajaxurl,
				dataType: "json",
				data: {
					action: 'manual_sync_toko',
                    type: 'webapi',
				},
				success: function(data){
                    $('.manual-sync-toko').html('Synchronize Now');
                    location.reload();
				}
			});
		});

        $('.manual-sync-toko-sheet').on('click', function(){
			$(this).html('Proses..');
			$.ajax({
				type: "GET",
				url: ajaxurl,
				dataType: "json",
				data: {
					action: 'manual_sync_toko',
                    type: 'sheetapi',
				},
				success: function(data){
                    $('.manual-sync-toko').html('Synchronize Now');
                    location.reload();
				}
			});
		});
	});

})( jQuery );
